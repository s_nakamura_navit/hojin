/*!
 * jQuery Loading Library v1.0.0
 */
(function($) {
	$.fn.loading = function(){
		/* 初期化(レイヤー定義) */
		var $obj = $("<div/>").attr("id","load-wrapper");
		var $contents = $("<div/>").attr("id","load-img");
		$contents.html('<img src="img/loading.gif">');
		//$contents.append($("<img/>").attr("src","img/loading.gif"));
		$contents.append($("<br/>")).append("<br/>");
		$contents.append("ただ今処理中です。<br />少々お待ちください。");
		$("body").append($obj.append($contents));
		
		/* イベント定義 */
		$("#search_corp_button").click(function(e) {
			e.preventDefault();
			$('#f_get').submit();
			$("#load-wrapper").css("background-color","#000");
			$("#load-wrapper").fadeIn(500);//.fadeOut(1000);
			document.s_corp.submit();
		});
	}
})(jQuery);

/* 実行 */
$( document ).ready(function() {
  $.fn.loading();
});
