<?php
/**
 *  Batch/UpdateStatus.php
 *  送信ステータス更新バッチ
 *
 *  mst_fax_transmissionの送信ステータスを更新する
 *
 *  @author     Genya Takahashi
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  batch_updatestatus Form implementation.
 *
 *  @author     Genya Takahashi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Cli_Form_BatchUpdateStatus extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array();
}

/**
 *  batch_updatestatus action implementation.
 *
 *  @author     Genya Takahashi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Cli_Action_BatchUpdateStatus extends Opensite_ActionClass
{
    /**
     *  authenticate
     *
     *  @access    public
     *  @return    string  Forward name (null if no errors.)
     */
    function authenticate()
    {
        return;
    }

    /**
     *  preprocess of batch_updatestatus Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        return null;
    }

    /**
     *  batch_updatestatus アクション実装
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        $ctl    =& Ethna_Controller::getInstance();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $list = Opensite_Dao_Fax::get_fax_list(3);
        
        // Fax送信マネージャ
        $fm = $this->backend->getManager("fax");
        
        
        
        var_dump($list);
        
//        $sql = 'SELECT id, sessid FROM user_session
//                                        WHERE updated < current_timestamp + interval -30 minute and flg_delete =  0';
//        $rows = $db->getAll($sql);
//        if(!$rows){
//                echo "No Seesions for Kill Now\n";
//                $this->logger->log(LOG_INFO, "killsessions: no list");
//                return null;
//        }
//
//        foreach($rows as $row){
//                $msg = "Kill User session : {$row['sessid']} User ID : {$row['id']}";
//                $sql = 'UPDATE user_session SET flg_delete = 1, updated = now() WHERE sessid = ?';
//                $db->query($sql, array($row['sessid']));
//                echo "{$msg} \n";
//                $this->logger->log(LOG_INFO, $msg);
//        }
        return null;
    }
}

?>
