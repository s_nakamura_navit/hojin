<?php
/**
 *  Opensite_Dao.php
 *  汎用 Data Access Object クラス
 *
 *  @copyright  (C)2010 Navit co.,ltd.
 *  @author     Tetsuo Nakanishi
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  Opensite Dao Class.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Dao
{
    /**
     *  select_memberlist_count
     *  会員リストの抽出取得（件数）
     *
     *  @access protected
     *  @param  array   in      パラメータ連想配列
     *  @return string  counts  件数
     */
    function select_memberlist_count($in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $rows   = array();
        $sql    =  '
SELECT
    count(*) COUNTS
FROM
    TRN_MEMBER       m,
    TRN_MEMBERCONFIG c,
    TRN_USER         u
WHERE
    m.MEMBER_ID = u.MEMBER_ID
AND m.MEMBER_ID = c.MEMBER_ID
';
        list($sql_mod, $cond) = Opensite_Dao::makecond_select_memberlist($in);
        $sql = $sql.$sql_mod;

        $result =& $db->query($sql, $cond);
        $data   = $result->fetchRow();
        $counts = $data{"COUNTS"};
        $logger->log(LOG_DEBUG, "total rows:".$counts);

        return $counts;
    }

    /**
     *  select_memberlist
     *  会員リストの抽出取得
     *
     *  @access protected
     *  @param  array   in        パラメータ連想配列
     *          string  sort      ソートカラム値
     *          string  order     ソートオーダ値 (DESC/ASC)
     *          int     start_row 開始行
     *          int     fetch_row 取得行数
     *  @return array   rows      結果オブジェクト
     */
    function select_memberlist($in, $sort, $order, $start_row, $fetch_row)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $rows   = array();
        $sql    =  '
SELECT
    m.*,
    c.*,
    u.*
FROM
    TRN_MEMBER       m,
    TRN_MEMBERCONFIG c,
    TRN_USER         u
WHERE
    m.MEMBER_ID = u.MEMBER_ID
AND m.MEMBER_ID = c.MEMBER_ID
';
        list($sql_mod, $cond) = Opensite_Dao::makecond_select_memberlist($in, $sort, $order);
        $sql = $sql.$sql_mod;

        $result  =& $db->db->SelectLimit($sql, $fetch_row, $start_row, $cond);

        return $result;
    }

    /**
     *  select_member
     *  会員詳細取得
     *
     *  @access protected
     *  @param  array   member_id 会員ID
     *  @return array   row       結果配列
     */
    function select_member($member_id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $row    = array();
        $sql    =  '
SELECT
    m.*,
    c.*,
    u.*
FROM
    TRN_MEMBER       m,
    TRN_MEMBERCONFIG c,
    TRN_USER         u
WHERE
    m.MEMBER_ID = ?
AND m.MEMBER_ID = u.MEMBER_ID
AND m.MEMBER_ID = c.MEMBER_ID
';
        $row  =& $db->getRow($sql, array($member_id));

        return $row;
    }

    /**
     *  makecond_select_memberlist
     *  条件生成：会員リストの抽出取得
     *
     *  @access protected
     *  @param  array   in      パラメータ連想配列
     *  @return string  sql     SQL文
     *          array   cond    抽出値連想配列
     */
    function makecond_select_memberlist($in, $sort = "", $order = "")
    {
        $sql  = "";
        $cond = array();

        if($in{"member_id"} != ""){
            $sql.=  'AND (m.MEMBER_ID = ?) '.DELIMITTER_CRLF;
            array_push($cond, $in{"member_id"});
        }
        if($in{"name"} != ""){
            $sql.=  'AND (u.NAME LIKE ? OR u.YOMI LIKE ?) '.DELIMITTER_CRLF;
            array_push($cond, '%'.$in{"name"}.'%');
            array_push($cond, '%'.$in{"name"}.'%');
        }
        if($in{"uid"} != ""){
            $sql.=  'AND m.UID LIKE ? '.DELIMITTER_CRLF;
            array_push($cond, '%'.$in{"uid"}.'%');
        }
        if($in{"device"} != ""){
            $sql.=  'AND m.DEVICE_CODE = ? '.DELIMITTER_CRLF;
            array_push($cond, $in{"device"});
        }
/*
        if($in{"changed"} != ""){
            $sql.=  'AND c.FLG_APP_CHANGED = ? '.DELIMITTER_CRLF;
            array_push($cond, $in{"changed"});
        }
*/
        if($in{"pref"} != ""){
            $sql.=  'AND u.PREF_CODE = ? '.DELIMITTER_CRLF;
            array_push($cond, $in{"pref"});
        }
        if($in{"status"} != ""){
            $sql.=  'AND m.USER_STATUS = ? '.DELIMITTER_CRLF;
            array_push($cond, $in{"status"});
        }
/*
        if($in{"appli"} != ""){
            $sql.=  'AND (m.APP1_ID = ? OR m.APP2_ID = ? OR m.APP3_ID = ?) '.DELIMITTER_CRLF;
            array_push($cond, $in{"appli"});
            array_push($cond, $in{"appli"});
            array_push($cond, $in{"appli"});
        }
*/
        if($in{"last_login_s"} == "" && $in{"last_login_e"} != ""){
            $sql.=  'AND m.CREATED < DATE_ADD(?, INTERVAL 1 DAY) '.DELIMITTER_CRLF;
            array_push($cond, $in{"last_login_e"});
        }
        if($in{"last_login_s"} != ""){
            $sql.=  'AND m.LAST_LOGIN > DATE_SUB(?, INTERVAL 1 DAY) '.DELIMITTER_CRLF;
            array_push($cond, $in{"last_login_s"});
            if($in{"last_login_e"} != ""){
                $sql.=  'AND m.LAST_LOGIN < DATE_ADD(?, INTERVAL 1 DAY) '.DELIMITTER_CRLF;
                array_push($cond, $in{"last_login_e"});
            }
        }
        if($in{"created_s"} == "" && $in{"created_e"} != ""){
            $sql.=  'AND m.CREATED < DATE_ADD(?, INTERVAL 1 DAY) '.DELIMITTER_CRLF;
            array_push($cond, $in{"created_e"});
        }
        if($in{"created_s"} != ""){
            $sql.=  'AND m.CREATED > DATE_SUB(?, INTERVAL 1 DAY) '.DELIMITTER_CRLF;
            array_push($cond, $in{"created_s"});
            if($in{"created_e"} != ""){
                $sql.=  'AND m.CREATED < DATE_ADD(?, INTERVAL 1 DAY) '.DELIMITTER_CRLF;
                array_push($cond, $in{"created_e"});
            }
        }
        if($sort != "" && $order != ""){
            $sql.=  'ORDER BY '.$sort.' '.$order.DELIMITTER_CRLF;
        }

        return (array($sql, $cond));
    }

    /**
     *  select_summarylist_count
     *  会員統計件数取得 (SUMMARY)
     *
     *  @access protected
     *  @return array   count     件数
     */
    function select_summarylist_count()
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb('sum');

        $sql    =  '
SELECT
    count(*) counts
FROM
    trn_member_count
';

        $result =& $db->query($sql);
        $data   = $result->fetchRow();
        $counts = $data{"counts"};
        $logger->log(LOG_DEBUG, "total rows:".$counts);

        return $counts;
    }

    /**
     *  select_summarylist
     *  会員統計取得 (SUMMARY)
     *
     *  @access protected
     *  @return array   data      結果配列
     */
    function select_summarylist()
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb('sum');

        $sql    =  '
SELECT
    *
FROM
    trn_member_count
';

        $row  =& $db->getAll($sql);

        return $row;
    }

    /**
     *  select_server
     *  サーバ一覧取得 (OBS)
     *
     *  @access protected
     *  @return array   data      結果配列
     */
    function select_server()
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb('obs');

        $sql    =  '
SELECT
    *
FROM
    mst_server
';

        $data =& $db->getAll($sql);

        return $data;
    }

    /**
     *  select_observate
     *  サーバの現在の監視情報取得 (OBS)
     *
     *  @access protected
     *  @param  string  host_id   ホストID
     *  @return array   data      結果配列
     */
    function select_observate($host_id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb('obs');

        $sql    =  '
SELECT
    *
FROM
    trn_observate
WHERE
    obs_id = ?
';

        $data =& $db->getRow($sql, array($host_id));

        return $data;
    }
}
?>
