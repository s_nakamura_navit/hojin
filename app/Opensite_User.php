<?php
/**
 *  Opensite_User.php
 *
 *  @author     {$author}
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  Opensite_UserManager
 *
 *  @author     {$author}
 *  @access     public
 *  @package    Opensite
 */
class Opensite_UserManager extends Ethna_AppManager
{
}

/**
 *  Opensite_User
 *
 *  @author     {$author}
 *  @access     public
 *  @package    Opensite
 */
class Opensite_User extends Ethna_AppObject
{
    /**
     *  property display name getter.
     *
     *  @access public
     */
    function getName($key)
    {
        return $this->get($key);
    }
}

?>
