<?php
/**
 *  Opensite_Menu.php
 *  メニュー生成クラス
 *
 *  @copyright  (C)2010 Navit co.,ltd.
 *  @author     Tetsuo Nakanishi
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  Opensite Menu Class.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Menu
{
    /**
     *  menu
     *  メニュー欄生成
     *
     *  @access protected
     *  @param  string  now_action 現在のアクション
     *  @return string  html       HTML
     */
    function menu($now_action)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $levels = split("_", $now_action);
        $lv1 = $levels[0];
        if(isset($levels[1])) { $lv2 = $levels[1]; } else { $lv2 = ""; }
        if(isset($levels[2])) { $lv3 = $levels[2]; } else { $lv3 = ""; }

        $logger->log(LOG_DEBUG, "menu:now_action:".$now_action);
        $logger->log(LOG_DEBUG, "menu:action_levels/ Lv1:".$lv1." Lv2:".$lv2." Lv3:".$lv3);
        $menus = array(
            "inquiry"      => "問合せ管理",
// 20121215 deleted by sano
/***
            "request"      => "データ管理",
***/
// 20121215 deleted by sano
            "config_admin" => "設定",
            "history"      => "作業履歴",
        );

// 20121217 chenged by sano
/***
        $role = array(
            "inquiry"      => 1,
            "config_admin" => 9,
            "history"      => 5,
        );
***/
        $role = array(
            "nomail"       => 1,
            "inquiry"      => 3,
            "config_admin" => 5,
            "history"      => 9,
        );
// 20121217 chenged by sano

        // ユーザ権限
        $user_role = $this->session->get("role");

        // 通常階層メニュー(Lv1)
        $html = '<table border="0" class="menu"><tr>'.DELIMITTER_CRLF;
        foreach($menus as $k => $v){
            if(strcmp($k, "login")==0){
                // トップ
                $html .= '<td style="cursor:pointer; background-color:#ffffff; padding:0px; width:100px;" onClick="location.href=\'index.php?action_login=true\';" rowspan="2" align="center"><img src="img/navitmobile.png"></td>'.DELIMITTER_CRLF;

            }elseif(strcmp($k, "br")==0){
                // 列変更
                $html .='</tr><tr>'.DELIMITTER_CRLF;

            }elseif(strcmp($v, "")==0){
                // 空
                $html .='<td class="menu"></td>'.DELIMITTER_CRLF;

            }elseif(strcmp($lv1, $k)==0){
                // 現在表示しているアクション
                if(isset($drilldown{$lv1}) && $lv2 != ""){
                    foreach($drilldown{$lv1} as $k2 => $v2){
                        if(strcmp($lv2, $k2)==0){
                            $html .= '<td class="menunow"><a href="index.php?action_'.$k.'=true">'.$v.'</a> > '.$v2;
                        }
                    }
                }else{
                    $html .= '<td class="menunow" style="cursor:pointer;" onClick="location.href=\'index.php?action_'.$k.'=true\';">'.$v;
                }
                $html .='</td>'.DELIMITTER_CRLF;

            }else{
                if($user_role >= $role{$k}) {
                    // 権限あり
                    $html .= '<td class="menu" style="cursor:pointer;" onClick="location.href=\'index.php?action_'.$k.'=true\';">'.$v.'</td>'.DELIMITTER_CRLF;
// 20121218 deleted by sano
//                } else {
//                    // 権限無し
//                    $html .= '<td class="menu" style="color:#999999;">'.$v.'</td>'.DELIMITTER_CRLF;
// 20121218 deleted by sano
                }
            }
        }
        $html .= '</tr></table>'.DELIMITTER_CRLF;

/*
        $html .= 'ログイン：'.$this->session->get("name")." 様<br />".DELIMITTER_CRLF;
*/

        return $html;
    }
}
?>
