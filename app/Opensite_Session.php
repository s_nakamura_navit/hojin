<?php
/**
 *  Opensite_Session.php
 *
 *  @author     Genya Takahashi
 *  @package    Opensite
 *  @version    $Id: d4af361a99e2aaa95cedee2132d1ca3f10920c6b $
 */

/**
 *  Opensite Session Class.
 *
 *  @author     Genya Takahashi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Session extends Ethna_Session
{
    function Opensite_Session($appid, $save_dir, $logger) {
        parent::Ethna_Session($appid, $save_dir, $logger);
        session_cache_limiter('nocache');
    }
}