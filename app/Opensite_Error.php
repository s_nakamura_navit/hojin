<?php
/**
 *  Opensite_Error.php
 *
 *  @package   Opensite
 *
 *  $Id$
 */

/*--- Application Error Definition ---*/
/*
 *  TODO: Write application error definition here.
 *        Error codes 255 and below are reserved
 *        by Ethna, so use over 256 value for error code.
 *
 *  Example:
 *  define('E_LOGIN_INVALID', 256);
 */

/** エラーコード: 共通エラー */
define('E_Opensite_COMMON', -100);

/** エラーコード: DBエラー */
define('E_Opensite_DB',     -101);

/** エラーコード: ユーザ認証エラー */
define('E_Opensite_AUTH',   -128);

//todo:// Geo関連のエラーコード定義

/** エラーコード: データベース関係 */
//DB登録更新
//ポイント消費処理に失敗
define('E_FURU_DB_POINT_USE_FAILD',              11000 );
//ポイントログの書き込みに失敗
define('E_FURU_DB_POINT_LOG_FAILD',              11001 );
//新規登録時のポイント管理レコード登録失敗
define('E_FAX_DB_POINT_INIT_FAILD',             11003 );
//付与依頼登録失敗
define('E_FURU_DB_INSERT_REQUEST_FAILD',         11004 );
//付与依頼情報更新失敗
define('E_FURU_DB_UPDATE_REQUEST_FAILD',         11005 );
//ユーザー情報更新失敗
define('E_FURU_DB_UPDATE_MST_USER_FAILD',        11006 );
//ポイント購入依頼情報の論理削除に失敗
define('E_FURU_DB_DELETE_REQUEST_POINT_FAILD',   11007 );
//ポイント購入依頼(クレジットカード事前履歴)情報の作成に失敗
define('E_FURU_DB_INSERT_REQUEST_CC_POINT_FAILD',      11008 );
//ポイント購入依頼(クレジットカード事前履歴)情報の論理削除に失敗
define('E_FURU_DB_DELETE_REQUEST_CC_POINT_FAILD',      11009 );

//情報取得
//既読フラグの読み込み失敗
define('E_FURU_DB_USER_FLG_VALID',               12000 );
//ユーザーポイント情報の取得失敗
define('E_FURU_DB_USER_POINT_FAILD',             12001 );
//ポイント情報が存在しない
define('E_FURU_DB_USER_POINT_NOT_EXIST',         12002 );
//ユーザー情報が存在しない
define('E_FURU_DB_USER_NOT_FOUND',               12003 );
//依頼情報が存在しない
define('E_FURU_DB_REQUEST_NOT_FOUND',            12004 );
//既存のレコードが存在する
define('E_FURU_DB_USER_POINT_ALREADY_EXIST',     12005 );

/** エラーコード: ファイル関係 */
//ファイルアップロードに失敗
define('E_FURU_UPLOAD_LIST_NOT_EXIST',           21000 );
//ファイルダウンロードに失敗
define('E_FURU_DL_FILE_NOT_FOUND',               22000 );
define('E_FURU_DL_FILE_DL_ERROR',                22001 );
?>
