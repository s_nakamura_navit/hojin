<?php
/**
 *  SecureRegistration.php
 */

/**
 *  SecureRegistration Form implementation.
 */
class Opensite_Form_SecureRegistration extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_company_name'              => array('type' => VAR_TYPE_STRING, 'name' => '会社名',         'required' => false,),
        'in_branch_name'               => array('type' => VAR_TYPE_STRING, 'name' => '支社名',         'required' => false,),
        'in_zip1'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 3, 'max' => 3,),
        'in_zip2'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 4, 'max' => 4,),
        'in_address'                   => array('type' => VAR_TYPE_STRING, 'name' => '住所',           'required' => false,),
        'in_phone_no1'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no2'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no3'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号3',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_department_name'           => array('type' => VAR_TYPE_STRING, 'name' => '部署名',         'required' => false,),
        'in_post_name'                 => array('type' => VAR_TYPE_STRING, 'name' => '役職名',         'required' => false,),
        'in_contractor_lname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（姓）', 'required' => false,),
        'in_contractor_fname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（名）', 'required' => false,),
        'in_email'                     => array('type' => VAR_TYPE_STRING, 'name' => 'メールアドレス', 'required' => false,),
        'in_from'                      => array('type' => VAR_TYPE_STRING, 'name' => 'サイトを知ったきっかけ', 'required' => false,),
        'in_privacy'                   => array('type' => VAR_TYPE_STRING, 'name' => '利用規約の同意',  'required' => false, 'option' => array('1'=>'上記「入札なう利用規約」に同意します。 ')),
        'in_act'                       => array('type' => VAR_TYPE_STRING, 'name' => 'お試し機能からの遷移証明', 'required' => false,),
    );
    
}

/**
 *  SecureRegistration action implementation.
 */
class Opensite_Action_SecureRegistration extends Opensite_ActionClass
{
    function authenticate()
    {
		//
		//	アライアンス企業からの引数付きのURLの時の対応で下記追加	2017-02-22
		//
		if ( isset($_GET["allcd"]) ){
			$allcd = $_GET["allcd"];
		}else if (isset($_COOKIE["opensite_alliance"])){
			$allcd = $_COOKIE["opensite_alliance"];
		}else{
			$allcd = "";
		}
		$alliance = "";
		if ($allcd){
			$all_rec = Opensite_Dao_Alliance::valid_alliance($allcd);
			if ( count($all_rec) ){
				$cookie_limit = time() + $all_rec[0]["cookie_days"] * 24 * 60 * 60;
				setcookie("opensite_alliance", $allcd, $cookie_limit, "/", ".navit-j.com");
				Opensite_Dao_Alliance::add_log($all_rec[0]["id"]);
				$alliance = $all_rec[0]["come_from"];
			}
		}
		$this->af->setApp("alliance", $alliance);
		//
		//	以上まで
		//
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
    }

    /**
     *  preprocess of SecureRegistration Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_SecureRegistration prepare()");
        return null;
    }

    /**
     *  SecureRegistration action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        //フォーム入力値
        
        //バリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_SecureRegistration perform()");
        return 'secure_registration';
    }
}

?>
