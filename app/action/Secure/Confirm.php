<?php
/**
 *  SecureConfirm.php
 */

/**
 *  SecureConfirm Form implementation.
 */
class Opensite_Form_SecureConfirm extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_company_name'              => array('type' => VAR_TYPE_STRING, 'name' => '会社名',         'required' => false,),
        'in_branch_name'               => array('type' => VAR_TYPE_STRING, 'name' => '支社名',         'required' => false,),
        'in_zip1'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 3, 'max' => 3,),
        'in_zip2'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 4, 'max' => 4,),
        'in_address'                   => array('type' => VAR_TYPE_STRING, 'name' => '住所',           'required' => false,),
        'in_phone_no1'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no2'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no3'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号3',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_department_name'           => array('type' => VAR_TYPE_STRING, 'name' => '部署名',         'required' => false,),
        'in_post_name'                 => array('type' => VAR_TYPE_STRING, 'name' => '役職名',         'required' => false,),
        'in_contractor_lname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（姓）', 'required' => false,),
        'in_contractor_fname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（名）', 'required' => false,),
        'in_email'                     => array('type' => VAR_TYPE_STRING, 'name' => 'メールアドレス', 'required' => false,'regexp' => '/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/'),
        'in_from'                      => array('type' => VAR_TYPE_STRING, 'name' => 'サイトを知ったきっかけ', 'required' => false,),
        'in_privacy'                   => array('type' => VAR_TYPE_STRING, 'name' => '利用規約の同意',  'required' => false, 'option' => array('1'=>'上記「入札なう利用規約」に同意します。 ')),
	'in_act'                       => array('type' => VAR_TYPE_STRING, 'name' => 'お試し機能からの遷移証明', 'required' => false,),
    );
    
}

/**
 *  SecureConfirm action implementation.
 */
class Opensite_Action_SecureConfirm extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
    }

    /**
     *  preprocess of SecureConfirm Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_SecureConfirm prepare()");
        return null;
    }

    /**
     *  SecureConfirm action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
		//
		//	アライアンス企業からの引数付きのURLの時の対応で下記追加	2017-02-22
		//	「サイトを知ったきっかけ」をアライアンス経由の時は表示しないため
		//
		if (isset($_COOKIE["opensite_alliance"])){
			$allcd = $_COOKIE["opensite_alliance"];
		}else{
			$allcd = "";
		}
		if ($allcd){
			$all_rec = Opensite_Dao_Alliance::valid_alliance($allcd);
			if ( count($all_rec) ){
				$this->af->setApp("alliance", $all_rec[0]["come_from"]);
			}
		}
		//
		//	以上まで
		//
        // セッション情報取得
        $id = $this->session->get('id');
        
        //入力値取得
        $data{'company_name'}           = $this->af->get('in_company_name');
        $data{'zip1'}                   = $this->af->get('in_zip1');      
        $data{'zip2'}                   = $this->af->get('in_zip2');
        $data{'address'}                = $this->af->get('in_address');
        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
        $data{'phone_no2'}              = $this->af->get('in_phone_no2');            
        $data{'phone_no3'}              = $this->af->get('in_phone_no3');            
        $data{'department_name'}        = $this->af->get('in_department_name'); 
        $data{'post_name'}              = $this->af->get('in_post_name');            
        $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
        $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
        $data{'email'}                  = $this->af->get('in_email');
        $data{'from'}                   = $this->af->get('in_from');
        $data{'privacy'}                = $this->af->get('in_privacy');

        if($_GET["back"] != "1"){
          
          
            // バリデート
            mb_regex_encoding("UTF-8");
            if($data{'company_name'}==""){
              $this->ae->add("in_company_name", "会社名を入力してください", E_FORM_INVALIDVALUE);
            }else{
	      //空白のみ入力された場合のバリデート
	      $company_name = str_replace(" ","",$data{'company_name'});
	      $company_name = str_replace("　","",$company_name);
	      if($company_name == ""){
		$this->ae->add("in_company_name", "会社名を入力してください", E_FORM_INVALIDVALUE);
	      }
	    }
            if($data{'zip1'}==""){
              $this->ae->add("in_zip1", "郵便番号1を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'zip2'}==""){
              $this->ae->add("in_zip2", "郵便番号2を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'address'}==""){
              $this->ae->add("in_address", "住所を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'phone_no1'}==""){
              $this->ae->add("in_phone_no1", "電話番号1を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'phone_no2'}==""){
              $this->ae->add("in_phone_no2", "電話番号2を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'phone_no3'}==""){
              $this->ae->add("in_phone_no3", "電話番号3を入力してください", E_FORM_INVALIDVALUE);
            }
            //if($data{'department_name'}==""){
            //  $this->ae->add("in_department_name", "部署名を入力してください", E_FORM_INVALIDVALUE);
            //}
            //if($data{'post_name'}==""){
            //  $this->ae->add("in_post_name", "役職名を入力してください", E_FORM_INVALIDVALUE);
            //}            
            if($data{'contractor_lname'}==""){
              $this->ae->add("in_contractor_lname", "担当者名（姓）を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'contractor_fname'}==""){
              $this->ae->add("in_contractor_fname", "担当者名（名）を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'email'}==""){
              $this->ae->add("in_email", "メールアドレスを入力してください", E_FORM_INVALIDVALUE);
            }else{
              $ret = Opensite_Mail::mail_format_check($data{'email'});
              if(!$ret){
                $this->ae->add("in_email", "正しいメールアドレスを入力してください", E_FORM_INVALIDVALUE);
              }else{
                $result_data = null;
                $result_count = null;
                //登録済みメールアドレスかどうかをチェック
                list($result_count, $result_data) = Opensite_Dao_Search::select_mail_exist($data{'email'});
                if($result_count > 0){
                  //登録済みメールアドレスの場合
                  $this->ae->add("in_email", "既に登録済みのメールアドレスです", E_FORM_INVALIDVALUE);
                }
              }
            }
            if($data{'from'}=="" && ! count($all_rec) ){
              $this->ae->add("in_from", "サイトを知ったきっかけを選択してください", E_FORM_INVALIDVALUE);
            }
			//
			//	何度でも会員登録できるように、下記if文を無効に、 2016-12-05　中村
			//
			/*
			if($_COOKIE['opensite_new_user'] != ""){
				$result = Opensite_Dao_Login::check_cookie_newuser($_COOKIE['opensite_new_user']);
				if($result){
					$this->af->setApp("title", "新規登録エラー");
					$this->af->setApp("message", "すでに登録済みのユーザー様の可能性があります");
					return "message";
				}
			}
			*/
			//	以上まで、

            

            // バリデート
            if ($this->af->validate() != 0) {
                //入力値に不備ありのため戻す          
                return 'secure_registration';
            }else{
                // 入力値OK
                

            }
        }
        
        
        
        //バリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_SecureConfirm perform()");
        return 'secure_confirm';
    }
}

?>
