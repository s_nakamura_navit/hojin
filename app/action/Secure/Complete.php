<?php
/**
 *  SecureComplete.php
 */

/**
 *  SecureComplete Form implementation.
 */
class Opensite_Form_SecureComplete extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_company_name'              => array('type' => VAR_TYPE_STRING, 'name' => '会社名',         'required' => false,),
        'in_zip1'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 3, 'max' => 3,),
        'in_zip2'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 4, 'max' => 4,),
        'in_address'                   => array('type' => VAR_TYPE_STRING, 'name' => '住所',           'required' => false,),
        'in_phone_no1'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no2'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no3'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号3',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_department_name'           => array('type' => VAR_TYPE_STRING, 'name' => '部署名',         'required' => false,),
        'in_post_name'                 => array('type' => VAR_TYPE_STRING, 'name' => '役職名',         'required' => false,),
        'in_contractor_lname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（姓）', 'required' => false,),
        'in_contractor_fname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（名）', 'required' => false,),
        'in_email'                     => array('type' => VAR_TYPE_STRING, 'name' => 'メールアドレス', 'required' => false,),
        'in_from'                      => array('type' => VAR_TYPE_STRING, 'name' => 'サイトを知ったきっかけ', 'required' => false,),
        'in_privacy'                   => array('type' => VAR_TYPE_STRING, 'name' => '利用規約の同意',  'required' => false, 'option' => array('1'=>'上記「入札なう利用規約」に同意します。 ')),
	'in_act'                       => array('type' => VAR_TYPE_STRING, 'name' => 'お試し機能からの遷移証明', 'required' => false,),
    );
    
}

/**
 *  SecureComplete action implementation.
 */
class Opensite_Action_SecureComplete extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
    }

    /**
     *  preprocess of SecureComplete Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_SecureComplete prepare()");
        return null;
    }

    /**
     *  SecureComplete action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
		//
		//	アライアンス経由フラグのcookieを削除	2017-02-28	中村
		//
		setcookie("opensite_alliance", '', time() - 1800, "/", ".navit-j.com");
		//
		//	以上まで
		//
        // セッション情報取得
        $id = $this->session->get('id');
        
        //入力値取得
        $data{'company_name'}           = $this->af->get('in_company_name');
        $data{'zip1'}                   = $this->af->get('in_zip1');      
        $data{'zip2'}                   = $this->af->get('in_zip2');
        $data{'address'}                = $this->af->get('in_address');
        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
        $data{'phone_no2'}              = $this->af->get('in_phone_no2');            
        $data{'phone_no3'}              = $this->af->get('in_phone_no3');            
        $data{'department_name'}        = $this->af->get('in_department_name'); 
        $data{'post_name'}              = $this->af->get('in_post_name');            
        $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
        $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
        $data{'email'}                  = $this->af->get('in_email');
        $data{'from'}                   = $this->af->get('in_from');
        $data{'act'}                    = $this->af->get('in_act');
        $data{'privacy'}                = $this->af->get('in_privacy');
		//	アライアンス情報取得
		$top_id = "";
		if($data['act'] == "furufuru_trial"){
			$contents = $data['act'];
		}else{
			$contents = "hojin";
		}
		if ( isset($_COOKIE["opensite_alliance"]) ){
			$all_rec = Opensite_Dao_Alliance::valid_alliance($_COOKIE["opensite_alliance"]);
			if ( isset($all_rec[0]["top_id"]) ){
				$top_id = $all_rec[0]["top_id"] . "_";
				$data{'from'} = $all_rec[0]["come_from"];
				$contents = $all_rec[0]["contents"];
			}
		}
        
        if($_GET["back"] != "1"){
            $result_data = null;
            $result_count = null;
            //登録済みメールアドレスかどうかをチェック
            list($result_count, $result_data) = Opensite_Dao_Search::select_mail_exist($data{'email'});
            if($result_count > 0){
                //エラー(再描画用)
                $this->af->set('db_regist_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
	        return "secure_complete"; 
	      
            }
            
            $affected = "";
            $inserted_serial_no = "";
            // バリデート
            if ($this->af->validate() != 0) {
                //入力値に不備ありのため戻す          
                return 'secure_confirm';
            }else{
                // 入力値OK
                
                //暫定
                $data{'target_field1'} = "";
                $data{'target_field2'} = "";
              
                //登録日時セット
                $date_now = date("Y-m-d H:i:s");
                $data{'updated'} = $date_now;
                $data{'created'} = $date_now;
              
                //認証キーの作成
                $KEY = "joseikin-now";
                $authkey = md5($data{'email'}.$KEY.$date_now);
                //認証有効期限を算出(24時間後)
                $auth_limit = date('Y-m-d H:i:s', strtotime('+24 hours'));
                //会員ランク(無料会員)
                $rank = "0";

                //ログインIDとパスワードを発行
                //ユニークな値
                $u_str = ceil(microtime(true));
                //$login_id = self::CreateUniqStr(5).$u_str;//ログインID
                $login_id = self::CreateLoginId($top_id);//ログインID
                $passwd = self::CreateUniqStr(5).self::CreateUniqStr(5);
                $md5_passwd = md5($passwd);
                //会員ランク(0：無料会員、1：有料会員A、2：有料会員B、3：有料会員C)
                $rank = 0;


		$err_count = 0;
                //DBへ登録
                list($affected, $inserted_serial_no) = Opensite_Dao_Search::insert_new_user($data, $login_id, $md5_passwd, $authkey, $auth_limit, $rank, $contents);
                if($affected == null || $affected == "" || $affected == 0 || $inserted_serial_no == ""){
		  $err_count++;
		}else{
                  // DBのポイントテーブルに初期レコード追加
                  $init_point = $GLOBALS['hojin_request_privilege'];      //新規会員登録特典ポイント付与20150330
                  $res = Opensite_Dao_Mypage::init_user_point($inserted_serial_no,$init_point);
		  if(Ethna::isError($res)){
		    $err_count++;
		  }
		}		

		if($err_count > 0){
                  //エラー
                  $this->af->set('db_regist_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
                }else{
                  $this->af->set('db_regist_result','1');
                  //DB登録OK
		  //cookieをセット
		  setcookie("opensite_new_user",md5($inserted_serial_no . "opensite_new_user"),time() + 3650 * 24 * 60 * 60,"/",".navit-j.com");	

                  //DBへ登録(認証OKフラグもONにする)
                  //Opensite_Dao_Search::update_user_regcomplete($data{'auth'}, $login_id ,$md5_passwd, $rank);


                  //メール送信
                  //宛先
                  $to = $data{'email'};
                  //$to = "m.hirai@navit-j.com";
                  //件名
                  $subject="【法人番号検索】会員登録完了のお知らせ";
                  //差出人
                  $fromname="法人番号検索 事務局";
                  $fromaddress="info_opensite@navit-j.net";

                  //本文をファイルから読み込み
                  //ファイル名
                  $reg_mail_file = BASE."/mail/" . $top_id . "registration.txt";
                  //ファイル読み込み
                  mb_language('Japanese');
                  $reg_mail = file_get_contents($reg_mail_file);
                  $reg_mail = mb_convert_encoding($reg_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
                  //読み込んだメール内の変数に値をセット
                  $reg_mail = str_replace('[LOGINID]', $login_id, $reg_mail);
                  $reg_mail = str_replace('[PASSWORD]', $passwd, $reg_mail);
                  $reg_mail = str_replace('[COMPANY_NAME]', $data{'company_name'}, $reg_mail);
                  $reg_mail = str_replace('[BRANCH_NAME]', $data{'branch_name'}, $reg_mail);
                  $reg_mail = str_replace('[DEPARTMENT_NAME]', $data{'department_name'}, $reg_mail);
                  $reg_mail = str_replace('[POST_NAME]', $data{'post_name'}, $reg_mail);
                  $reg_mail = str_replace('[CONTRACTOR_NAME]', $data{'contractor_lname'}."　".$data{'contractor_fname'}, $reg_mail);
                  $reg_mail = str_replace('[ZIP]', $data{'zip1'}."-".$data{'zip2'}, $reg_mail);
                  $reg_mail = str_replace('[ADDRESS]', $data{'address'}, $reg_mail);
                  $reg_mail = str_replace('[PHONE_NO]', $data{'phone_no1'}.$data{'phone_no2'}.$data{'phone_no3'}, $reg_mail);
                  $reg_mail = str_replace('[FAX_NO]', $data{'fax_no1'}.$data{'fax_no2'}.$data{'fax_no3'}, $reg_mail);
                  $reg_mail = str_replace('[EMAIL]', $data{'email'}, $reg_mail);

                  $reg_mail = str_replace('[FROM]', $data{'from'}, $reg_mail);
                  $reg_mail = str_replace('[MAILMAGA]', $data{'mailmaga'}, $reg_mail);

                  $reg_mail = str_replace('[FIELD1]', $data{'target_field1_str'}, $reg_mail);
                  $reg_mail = str_replace('[FIELD2]', $data{'target_field2_str'}, $reg_mail);
                  $reg_mail = str_replace('[BUSINESSFORM]', $data{'target_businessform_str'}, $reg_mail);

                  //本文
                  $body = $reg_mail;

                  $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);
                  if($return_flag) {
                    //メール送信OK
                    $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND OK] TO:<".$to.">");
                    $this->af->set('mail_send_result','1');

                    //webmasterへのメール通知
                    //$to="uemura@navit-j.com";
                    //$to="m.hirai@navit-j.com";
                    $to = array(array('btobmarketing@navit-j.com','送信1'),array('webmaster@navit-j.com','送信2'));
                    $subject="【通知】「法人番号検索」会員登録通知";
                    $reg_mail_file = BASE."/mail/" . $top_id . "registration_for_master.txt";
                    mb_language('Japanese');
                    $reg_mail = file_get_contents($reg_mail_file);
                    $reg_mail = mb_convert_encoding($reg_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
                    $reg_mail = str_replace('[COMPANY_NAME]', $data{'company_name'}, $reg_mail);
                    $reg_mail = str_replace('[BRANCH_NAME]', $data{'branch_name'}, $reg_mail);
                    $reg_mail = str_replace('[DEPARTMENT_NAME]', $data{'department_name'}, $reg_mail);
                    $reg_mail = str_replace('[POST_NAME]', $data{'post_name'}, $reg_mail);
                    $reg_mail = str_replace('[CONTRACTOR_NAME]', $data{'contractor_lname'}."　".$data{'contractor_fname'}, $reg_mail);
                    $reg_mail = str_replace('[ZIP]', $data{'zip1'}."-".$data{'zip2'}, $reg_mail);
                    $reg_mail = str_replace('[ADDRESS]', $data{'address'}, $reg_mail);
                    $reg_mail = str_replace('[PHONE_NO]', $data{'phone_no1'}.$data{'phone_no2'}.$data{'phone_no3'}, $reg_mail);
                    $reg_mail = str_replace('[FAX_NO]', $data{'fax_no1'}.$data{'fax_no2'}.$data{'fax_no3'}, $reg_mail);
                    $reg_mail = str_replace('[EMAIL]', $data{'email'}, $reg_mail);

                    $reg_mail = str_replace('[FROM]', $data{'from'}, $reg_mail);
                    $reg_mail = str_replace('[MAILMAGA]', $data{'mailmaga'}, $reg_mail);

                    $reg_mail = str_replace('[FIELD1]', $data{'target_field1_str'}, $reg_mail);
                    $reg_mail = str_replace('[FIELD2]', $data{'target_field2_str'}, $reg_mail);
                    $reg_mail = str_replace('[BUSINESSFORM]', $data{'target_businessform_str'}, $reg_mail);
                    $body = $reg_mail;
                    $fromname="法人番号検索　システムよりお知らせ";
                    $fromaddress="info_opensite@navit-j.net";
                    $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);

                  }else{
                    //メール送信NG
                    $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND NG] TO:<".$to.">");
                    $this->af->set('mail_send_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
                  }
              }
          }
	  if($data['act'] == "furufuru_trial"){
	      //自動ログイン
	      $la     =& new Opensite_Authentication();
	      $result =  $la->auth_autologin($inserted_serial_no);
	      if(Ethna::isError($result)){
		  $this->af->set('auto_login_result','-1');
	      }
	  }

          $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Completeregistration perform()");
          return 'secure_complete';

	}

    }

    function CreateUniqStr( $len = 8 ){
        // l（小文字のエル）、o（小文字のオウ）、I（大文字のアイ）、O（大文字のオウ）、0（数字のゼロ）、1（数字のイチ）は除外
        // ↑当初は大文字を含んでいたが、光通信対応でアルファベットは小文字のみとした
        $pwdStrings = array( "sletter" => array( 'a','b','c','d','e','f','g','h','i','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z' ),
                             //"cletter" => array( 'A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z' ),
                             "number"  => array( '2','3','4','5','6','7','8','9' ),
                            );
        $pwd = array();
        while( count( $pwd ) < $len ){
            // 英(大小)数字と記号を必ず入れる
            if( count( $pwd ) < count($pwdStrings) ){
                $key = key( $pwdStrings );
                next( $pwdStrings );
            // 残りはランダムに取得
            } else {
                $key = array_rand( $pwdStrings );
            }
            $pwd[] = $pwdStrings[$key][array_rand( $pwdStrings[$key]  )];
        }

        // 生成した文字列の順番をランダムに並び替え
        shuffle( $pwd );
        $pwdTxt = implode( $pwd );

        return( $pwdTxt );
    }

    function CreateLoginId($top_id){
        //ユニークチェックを繰り返してユニークなるまで繰り返し
        while(1){
            $u_str = ceil(microtime(true));
			if ($top_id){
				$login_id = str_replace("_", "", $top_id) . $u_str;//ログインID
			}else{
				$login_id = self::CreateUniqStr(5).$u_str;//ログインID
			}
            list($result_count, $data) = Opensite_Dao_Search::select_loginid_exist($login_id);
            if($result_count == 0){
                break;
            }
        }
        return $login_id;

    }

}
?>
