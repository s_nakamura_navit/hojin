<?php
/**
 *  Sendresult.php
 */

/**
 *  Send Form implementation.
 */
class Opensite_Form_SendResult extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'fax_id' => array(
            'type' => VAR_TYPE_STRING,
        ),
    );
    
}

/**
 *  Sendresult action implementation.
 */
class Opensite_Action_SendResult extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
    }

    /**
     *  preprocess of Send Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Sendresult prepare()");
        return null;
    }

    /**
     *  Sendresult action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // セッション値取得
        $id   = $this->session->get("id");
        $name = $this->session->get("name");
        
        // GET値取得
        $fax_id = $this->af->get("fax_id");
        //$fax_id = "9436"; // デバッグ用
       
        // Fax送信マネージャ
        $fm = $this->backend->getManager("fax");
        
        if(null != ($status = $fm->getTransmissionStatus($fax_id)))
        {
            $fileName = "result.csv";
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . $fileName);
            echo $status;
            exit();
//            $status = str_getcsv($status,"\r\n");
//
//            $header = NULL;
//            $list = array();
//            foreach($status as $line)
//            {
//                $buf = str_getcsv($line);
//
//                if(!$header)
//                    $header = $buf;
//                else
//                    $list[] = array_combine($header, $buf);
//            }
//            $this->af->setApp("message",print_r($list,true));
        }
        else
        {
            $this->af->setApp("message","送信結果が取得できませんでした。未送信、または送信予約中の可能性があります。");
        }
        // 送信
//        $fm->sendFax($fax_id,$datetime);
//        Opensite_Dao_Fax::receipt_fax_transmission($fax_id);
        // 確認表示用		

        
        // selectionバリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Send perform()");
        return 'message';
    }
}

?>
