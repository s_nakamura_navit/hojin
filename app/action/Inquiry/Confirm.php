<?php
/**
 *  InquiryConfirm.php
 */

/**
 *  InquiryConfirm Form implementation.
 */
class Opensite_Form_InquiryConfirm extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_company_name'              => array('type' => VAR_TYPE_STRING, 'name' => '会社名',         'required' => false,),
        'in_zip1'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 3, 'max' => 3,),
        'in_zip2'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 4, 'max' => 4,),
        'in_address'                   => array('type' => VAR_TYPE_STRING, 'name' => '住所',           'required' => false,),
        'in_phone_no1'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no2'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no3'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号3',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_department_name'           => array('type' => VAR_TYPE_STRING, 'name' => '部署名',         'required' => false,),
        'in_post_name'                 => array('type' => VAR_TYPE_STRING, 'name' => '役職名',         'required' => false,),
        'in_contractor_lname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（姓）', 'required' => false,),
        'in_contractor_fname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（名）', 'required' => false,),
        'in_email'                     => array('type' => VAR_TYPE_STRING, 'name' => 'メールアドレス', 'required' => false,),
	'in_inquiry'                   => array('type' => VAR_TYPE_STRING, 'name' => 'お問い合わせ内容', 'required' => false,),
        'in_matching'                  => array('type' => VAR_TYPE_INT,    'name' => 'マッチング総行数', 'required' => false,),
        'in_unmatch'                   => array('type' => VAR_TYPE_INT,    'name' => '非マッチング総行数', 'required' => false,),
        'in_unmatch_mobile'            => array('type' => VAR_TYPE_INT,    'name' => '非マッチング携帯電話数', 'required' => false,),
        'in_unmatch_num_err'           => array('type' => VAR_TYPE_INT,    'name' => '非マッチング桁数不適合数', 'required' => false,),
        'in_unmatch_blank'             => array('type' => VAR_TYPE_INT,    'name' => '非マッチング値なし', 'required' => false,),
        'in_tel_value_line'            => array('type' => VAR_TYPE_INT,    'name' => '電話番号行数', 'required' => false,),
        'in_header_check'              => array('type' => VAR_TYPE_STRING, 'name' => 'ヘッダーの有無情報', 'required' => false,),
        'in_header_line'               => array('type' => VAR_TYPE_INT,    'name' => 'ヘッダー行数', 'required' => false,),
        'in_latlon_type'               => array('type' => VAR_TYPE_STRING, 'name' => '測地系名', 'required' => false,),
        'in_list_name'                 => array('type' => VAR_TYPE_STRING, 'name' => '元リスト名', 'required' => false,),
        'in_use_point'                 => array('type' => VAR_TYPE_INT,    'name' => '必要ポイント', 'required' => false,),
        'in_list_count'                => array('type' => VAR_TYPE_INT,    'name' => '元リスト件数', 'required' => false,),
        'in_count_get'                 => array('type' => VAR_TYPE_INT,    'name' => 'マッチ件数', 'required' => false,),
        'in_lv_7_count'                => array('type' => VAR_TYPE_INT,    'name' => 'LV7マッチ件数', 'required' => false,),
        'in_lv_5_count'                => array('type' => VAR_TYPE_INT,    'name' => 'LV5マッチ件数', 'required' => false,),
        'in_lv_3_count'                => array('type' => VAR_TYPE_INT,    'name' => 'LV3マッチ件数', 'required' => false,),
        'in_get_csv'                   => array('type' => VAR_TYPE_STRING, 'name' => '処理ステータス', 'required' => false,),
        'in_lv_check'                  => array('type' => array(VAR_TYPE_STRING), 'name' => '選択マッチングレベル', 'required' => false,),
        'in_up_file_path'              => array('type' => VAR_TYPE_STRING, 'name' => '元リストファイルパス', 'required' => false,),
        'in_agree_privacy'             => array('type' => VAR_TYPE_STRING, 'name' => '個人情報の同意',  'required' => false, 'option' => array('1'=>'上記「個人情報取扱い書」に同意します。 ')),
    );
    
}

/**
 *  InquiryConfirm action implementation.
 */
class Opensite_Action_InquiryConfirm extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
    }

    /**
     *  preprocess of InquiryConfirm Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_InquiryConfirm prepare()");
        return null;
    }

    /**
     *  InquiryConfirm action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {

        //ログインチェック
        //Opensite_Dao_Login::check_user_session();

        // セッション情報取得
        $id = $this->session->get('id');
        
        //入力値取得
        $data{'company_name'}           = $this->af->get('in_company_name');
        $data{'zip1'}                   = $this->af->get('in_zip1');      
        $data{'zip2'}                   = $this->af->get('in_zip2');
        $data{'address'}                = $this->af->get('in_address');
        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
        $data{'phone_no2'}              = $this->af->get('in_phone_no2');            
        $data{'phone_no3'}              = $this->af->get('in_phone_no3');            
        $data{'department_name'}        = $this->af->get('in_department_name'); 
        $data{'post_name'}              = $this->af->get('in_post_name');            
        $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
        $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
        $data{'email'}                  = $this->af->get('in_email');
        $data{'inquiry'}                = $this->af->get('in_inquiry');
        $data{'agree_privacy'}          = $this->af->get('in_agree_privacy');

        //マッチング結果の引き継ぎ内容
        $this->af->setApp('matching',           $this->af->get('in_matching'));
        $this->af->setApp('unmatch',            $this->af->get('in_unmatch'));
        $this->af->setApp('unmatch_mobile',     $this->af->get('in_unmatch_mobile'));
        $this->af->setApp('unmatch_num_err',    $this->af->get('in_unmatch_num_err'));
        $this->af->setApp('unmatch_blank',      $this->af->get('in_unmatch_blank'));
        $this->af->setApp('tel_value_line',     $this->af->get('in_tel_value_line'));
        $this->af->setApp('header_check',       $this->af->get('in_header_check'));
        $this->af->setApp('header_line',        $this->af->get('in_header_line'));
        $this->af->setApp('latlon_type',        $this->af->get('in_latlon_type'));
        $this->af->setApp('list_name',          $this->af->get('in_list_name'));
        $this->af->setApp('use_point',          $this->af->get('in_use_point'));
        $this->af->setApp('list_count',         $this->af->get('in_list_count'));
        $this->af->setApp('count_get',          $this->af->get('in_count_get'));
        $this->af->setApp('lv_7_count',         $this->af->get('in_lv_7_count'));
        $this->af->setApp('lv_5_count',         $this->af->get('in_lv_5_count'));
        $this->af->setApp('lv_3_count',         $this->af->get('in_lv_3_count'));
        $this->af->setApp('get_csv',            $this->af->get('in_get_csv'));
        $this->af->setApp('lv_check',           $this->af->get('in_lv_check'));
        $this->af->setApp('up_file_path',       $this->af->get('in_up_file_path'));

        if($_GET["back"] != "1"){
          
          
            // バリデート
            mb_regex_encoding("UTF-8");
            if($data{'company_name'}==""){
              $this->ae->add("in_company_name", "会社名を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'zip1'}==""){
              $this->ae->add("in_zip1", "郵便番号1を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'zip2'}==""){
              $this->ae->add("in_zip2", "郵便番号2を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'address'}==""){
              $this->ae->add("in_address", "住所を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'phone_no1'}==""){
              $this->ae->add("in_phone_no1", "電話番号1を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'phone_no2'}==""){
              $this->ae->add("in_phone_no2", "電話番号2を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'phone_no3'}==""){
              $this->ae->add("in_phone_no3", "電話番号3を入力してください", E_FORM_INVALIDVALUE);
            }
            //if($data{'department_name'}==""){
            //  $this->ae->add("in_department_name", "部署名を入力してください", E_FORM_INVALIDVALUE);
            //}
            //if($data{'post_name'}==""){
            //  $this->ae->add("in_post_name", "役職名を入力してください", E_FORM_INVALIDVALUE);
            //}            
            if($data{'contractor_lname'}==""){
              $this->ae->add("in_contractor_lname", "担当者名（姓）を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'contractor_fname'}==""){
              $this->ae->add("in_contractor_fname", "担当者名（名）を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'email'}==""){
              $this->ae->add("in_email", "メールアドレスを入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'inquiry'}==""){
              $this->ae->add("in_inquiry", "お問い合せ内容を入力して下さい", E_FORM_INVALIDVALUE);
            }

            // バリデート
            if ($this->af->validate() != 0) {
                //入力値に不備ありのため戻す          
                return 'inquiry_registration';
            }else{
                // 入力値OK
                

            }
        }

        //バリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_InquiryConfirm perform()");
        return 'inquiry_confirm';
    }
}

?>
