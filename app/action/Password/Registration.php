<?php
/**
 *  PasswordRegistration.php
 */

/**
 *  PasswordRegistration Form implementation.
 */
class Opensite_Form_PasswordRegistration extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_company_name'              => array('type' => VAR_TYPE_STRING, 'name' => '会社名',         'required' => false,),
        'in_phone_no1'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no2'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no3'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号3',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
//      'in_k_phone_no1'               => array('type' => VAR_TYPE_STRING, 'name' => '携帯電話番号1',  'required' => false, 'regexp' => '/^[0-9]+$/', 'max' => 5,),
//      'in_k_phone_no2'               => array('type' => VAR_TYPE_STRING, 'name' => '携帯電話番号2',  'required' => false, 'regexp' => '/^[0-9]+$/', 'max' => 5,),
//      'in_k_phone_no3'               => array('type' => VAR_TYPE_STRING, 'name' => '携帯電話番号3',  'required' => false, 'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_email'                     => array('type' => VAR_TYPE_STRING, 'name' => 'メールアドレス', 'required' => false,),
//      'back'                         => array('type' => VAR_TYPE_INT, 'name' => 'flag_back', 'required' => false,)
    );

}

/**
 *  PasswordRegistration action implementation.
 */
class Opensite_Action_PasswordRegistration extends Opensite_ActionClass
{
    function authenticate()
    {
//      return null;
        // セッション有効性確認
//      return parent::authenticate();

    	//ログイン状態であるならトップページへ遷移する
    	if($this->session->get('id') != null && $this->session->get('serial') != null){
    		return 'index';
    	}
    }

    /**
     *  preprocess of PasswordRegistration Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_PasswordRegistration prepare()");
        return null;
    }

    /**
     *  PasswordRegistration action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        //フォーム入力値

        //バリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_PasswordRegistration perform()");
        return 'password_registration';
    }
}

?>
