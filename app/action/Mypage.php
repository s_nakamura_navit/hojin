<?php
/**
 *  Mypage.php
 */

/**
 *  mypage Form implementation.
 */
class Opensite_Form_Mypage extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_request_id' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_step' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'offset' => array(
            'type' => VAR_TYPE_INT,
            'min' => 1
        ),
		'pref_cd' => array(
				'type' => VAR_TYPE_STRING,
				'required' => false
		),
		'city_cd' => array(
				'type' => VAR_TYPE_STRING,
				'required' => false
		)
    );

}

/**
 *  mypage action implementation.
 */
class Opensite_Action_Mypage extends Opensite_ActionClass
{
    function authenticate()
    {
        // 未ログインの場合、トップページのリンクは無くなるが、
        // お気に入り等でアクセス可能なためアクセス制御
        return parent::authenticate();
    }

    /**
     *  preprocess of mypage Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        //$this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_MYPAGE prepare()");
        return null;
    }

    /**
     *  mypage action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {

        //ログインチェック
        Opensite_Dao_Login::check_user_session();

        // セッション値取得
        $account   = $this->session->get("id");
        $user_name = $this->session->get("name");
        $user_id = $this->session->get("serial");

        $fm = $this->backend->getManager("furu");

	$request_id = $this->af->get("in_request_id");
	$step	    = $this->af->get("in_step");

        // 現在のポイント取得
        $current_point = Opensite_Dao_Mypage::get_current_point($user_id);
        if(!$current_point)
        {
            $current_point = 0;
        }

        // ユーザー情報取得
        $data = Opensite_Dao_Mypage::get_current_data($user_id);

        //付与項目
        $property_list = Opensite_MasterManager::get_master_tsv("property", 1);

	//ページネーション情報
        $one_page_rows = 20;
        $offset = 1;
        if($this->af->get('offset') != "")    { $offset     = $this->af->get('offset'); }

        // 購入履歴情報取得
        list($counts, $result) = Opensite_Dao_Mypage::get_current_purchase_history($user_id,($offset - 1) * $one_page_rows,$one_page_rows);

        //付与項目
        $this->af->setApp("property_list",      $GLOBALS['hojin_request_property_list']);

        if(!$result){
                // 表示データ無し
                $history = array();
        } else {
                // 表示用マスタ変換
                $history = array();
                $i = 0;
                while ($history[$i] = $result->fetchRow()) {

					$exp_sel = explode(",",$history[$i]{'select_property'});
					$exp_get = explode(",",$history[$i]{'get_property_row'});
					$exp_use = explode(",",$history[$i]{'use_property'});

					if($history[$i]['flg_processed'] < 2){
						$history[$i]['property_name'] = $exp_sel;
					}else{
						$history[$i]['property_name'] = $exp_use;
					}

						//ファイルダウンロード期限を判別
                        $limit_time = $history[$i]{"client_order"};
                        $limit_time = strtotime($limit_time);
                        $limit_time = date('Y-m-d H:i:s', strtotime("+7 days",$limit_time));
                        $date_time = date('Y-m-d H:i:s');
                        if($limit_time > $date_time){
                                $history[$i]{"dl_flag"} = true;
                        }else{
                                $history[$i]{"dl_flag"} = false;
                        }

                        //作成日時を日本語化
                        $history[$i]{'created'} = $fm->change_to_j_date($history[$i]{'created'});

                        //購入日時を日本語化
                        $history[$i]{'client_order'} = $fm->change_to_j_date($history[$i]{'client_order'});

                        //付与件数の抽出（キャンセル以外[ != 9 ]）
						if($history[$i]['flg_processed'] != 9){
							$select_get_row = 0;
							$c = 0;
							foreach($exp_sel as $sel){
								$property_row[$sel] = $exp_get[$c];
				                $select_get_row += $property_row[$sel];
								$c++;
				            }
							if($history[$i]['flg_processed'] == 1){
				                $history[$i]{'get_row'} = $select_get_row;
				            }else if($history[$i]['flg_processed'] == 2){
								foreach($exp_use as $use){
									$history[$i]{'get_row'} += $property_row[$use];
								}
							}
						}

						//手動か自動化の判断　自動の場合、マッチング方法名を取得
						if($history[$i]['created_estimate_by'] == "auto"){
							$history[$i]['use_matching_phase'] = "未使用"; //todo autoの表示名が微妙？
						}else if($history[$i]['created_estimate_by'] == "trial"){
							$history[$i]['use_matching_phase'] = "trial";
						}else if($history[$i]['created_estimate_by'] == "trialone"){
							$history[$i]['use_matching_phase'] = "trialone";
							$select_property = explode(',',$history[$i]{'select_property'});

							$history[$i]['corp_id'] = $select_property[0];
							$history[$i]['pref_cd'] = $select_property[1];

							//見積もり期限の判別
							$limit_time = $history[$i]{"created_estimate"};
							$limit_time = strtotime($limit_time);
							$limit_time = date('Y-m-d H:i:s', strtotime("+7 days",$limit_time));
							$date_time = date('Y-m-d H:i:s');
							if($limit_time > $date_time){
								$history[$i]{"est_flag"} = true;
							}else{
								$history[$i]{"est_flag"} = false;
							}

							$history[$i]{'created_estimate'} = $fm->change_to_j_date($history[$i]{'created_estimate'});
							$history[$i]['req_id'] = $history[$i]{'id'};

						}else{
							foreach($GLOBALS['matching_phase_list'] as $key => $val){
								if($history[$i]['created_estimate_by'] == $key){
									$history[$i]['use_matching_phase'] = $val;
									break;
								}
							}
						}

                        if($history[$i]['flg_processed'] > 1){
							if($history[$i]['use_matching_phase'] == ""){
                                //戻しポイントの抽出
                                $history[$i]['point_back'] = Opensite_Dao_Mypage::get_request_use_point($history[$i]['id'],2);
							}else{
								//マッチング方法が登録されている場合は、自動付与なので使用ポイントを抽出
								$history[$i]['use_point'] = Opensite_Dao_Mypage::get_request_use_point($history[$i]['id'],0);
							}
                        }

						//ページレイアウトの決定
						if(($i % 2) == 0){
							$history[$i]{'style'} = "s_Cel_01";
						}else{
							$history[$i]{'style'} = "s_Cel_01_g";
						}

                        $i++;
                }
		}

        // ページャ生成
        $pager = Opensite_Pager::pager($offset, $counts, $one_page_rows);

        // テンプレートに設定
	$this->af->setApp('name',  $name);
        $this->af->setApp('point', $current_point);
        $this->af->setApp('data',  $data);
        $this->af->setApp('history',  $history);
	$this->af->setApp('counts', $counts);

	$pref_code = $this->af->get("pref_cd");
	$this->af->setApp("pref_cd",	$pref_code);
	$city_code = $this->af->get("city_cd");
	$this->af->setApp("city_cd",	$city_code);

	//ページネーション
	$this->af->setAppNe('pager', $pager);

        if($step == "csv_dl"){
                $sql = "select inspect_filepath from hojin_request where id = ".$request_id;
                $result = mysql_query($sql);
                $arr = mysql_fetch_array($result);

		$error = $fm->csv_dl($arr['inspect_filepath']);

                if ($error['message'] != "") {
                        $this->af->setApp('error',$error);
                        return 'system_error';
                }

                Opensite_AdminLogger::logger("mypage_download", $id, $request_id, $arr['inspect_filepath']);
        }

        return 'mypage';
    }
}

?>
