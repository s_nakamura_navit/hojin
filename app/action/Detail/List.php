<?php
/**
 *  detail_list.php
 */

/**
 *  Registration Form implementation.
 */
class Opensite_Form_DetailList extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_request_id' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_list_type' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        )
    );
    
}

/**
 *  mypage action implementation.
 */
class Opensite_Action_DetailList extends Opensite_ActionClass
{
    function authenticate()
    {
        // 未ログインの場合、トップページのリンクは無くなるが、
        // お気に入り等でアクセス可能なためアクセス制御
        return parent::authenticate();
    }

    /**
     *  preprocess of mypage Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        //$this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_MYPAGE prepare()");
        return null;
    }

    /**
     *  mypage action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // セッション値取得
        $account   = $this->session->get("id");
        $user_name = $this->session->get("name");
        $user_id = $this->session->get("serial");

        $fm = $this->backend->getManager("furu");
        
	$request_id = $this->af->get("in_request_id");
	$list_type = $this->af->get("in_list_type");

        //付与項目
        $this->af->setApp("property_list",      $GLOBALS['hojin_request_property_list']);
        $this->af->setApp("matching_phase_list",      $GLOBALS['matching_phase_list']);

        // 現在のポイント取得
        $point['current'] = Opensite_Dao_Mypage::get_current_point($user_id);
        if(!$point['current'])
        {
            $point['current'] = 0;
        }

        // 依頼情報取得
	$unit_point = array();
        $data = Opensite_Dao_Furu::get_hojin_request_data($request_id);

	if($list_type == "manual"){

	    //付与項目単価・件数・小計の取得
	    //依頼した選択項目を配列化
	    $exp_sel = explode(",",$data['select_property']);
	    $this->af->setApp('select_property',  $exp_sel);
	    //依頼項目に付与できた件数の配列化
	    $exp_get = explode(",",$data['get_property_row']);
	    $i = 0;
	    foreach($exp_sel as $key){
        	//単価の抽出
        	$unit_point[$key] = $GLOBALS['hojin_request_property_unit_point_list'][$key]; 
		//件数の抽出
		$count[$key] = $exp_get[$i];
		//小計の算出
		$point[$key] = $exp_get[$i] * $unit_point[$key];
		$i++;
	    }

	    $arr = Opensite_Dao_Furu::get_usepoint_for_request($request_id,0);
	    $point['pay'] = $arr['use_point'];
	    $arr = Opensite_Dao_Furu::get_usepoint_for_request($request_id,2);
	    $point['back'] = $arr['use_point'];
	    $point['total'] = $point['pay'] - $point['back'];

	}else if($list_type == "auto"){

            //マッチング件数取得
            $attribute_data = array();
            $attribute_data = Opensite_Dao_Furu::get_searches_childs_info($request_id,$data['created_estimate_by']);
            foreach($attribute_data as $name => $item){
                   $count[$name] = $item['row'];
                   $point[$name] = $item['point'];
                   $unit_point[$name] = $item['unit_point'];
            }

	    $arr = Opensite_Dao_Furu::get_usepoint_for_request($request_id,0);
	    $point['total'] = $arr['use_point'];

	}

	//ファイルダウンロード期限を判別
	$limit_time = $data{"client_order"};
	$limit_time = strtotime($limit_time);
	$limit_time = date('Y-m-d H:i:s', strtotime("+7 days",$limit_time));
	$date_time = date('Y-m-d H:i:s');
	if($limit_time > $date_time){
		$data{"dl_flag"} = true;
	}else{
		$data{"dl_flag"} = false;
	}

	$use_property = explode(",",$data['use_property']);;

        //基本料金の抽出
        $point['base'] = $GLOBALS['hojin_request_base_point']; 
        //税率の抽出
        $point['tax_rate'] = $GLOBALS['hojin_request_tax'];

        // テンプレートに設定
	$this->af->setApp('name',  $name);
	$this->af->setApp('count',  $count);
        $this->af->setApp('point', $point);
        $this->af->setApp('unit_point', $unit_point);
        $this->af->setApp('data',  $data);
        $this->af->setApp('request_id',  $request_id);
        $this->af->setApp('list_type',  $list_type);
        $this->af->setApp('use_property',  $use_property);

        if($step == "csv_dl"){
                $sql = "select inspect_filepath from hojin_request where id = ".$request_id;
                $result = mysql_query($sql);
                $arr = mysql_fetch_array($result);

                $error = $fm->csv_dl($arr['inspect_filepath']);

                if ($error['message'] != "") {
                        $this->af->setApp('error',$error);
                        return 'system_error';
                }

                Opensite_AdminLogger::logger("mypage_download", $id, $request_id, $arr['inspect_filepath']);
        }

        return 'detail_list';
    }
}

?>
