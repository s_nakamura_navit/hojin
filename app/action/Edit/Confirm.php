<?php
/**
 *  EditConfirm.php
 */

/**
 *  EditConfirm Form implementation.
 */
class Opensite_Form_EditConfirm extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_user_id'		       => array('type' => VAR_TYPE_STRING, 'name' => '会員ID',         'required' => false,),
        'in_password'		       => array('type' => VAR_TYPE_STRING, 'name' => 'パスワード',     'required' => false,),
        'in_company_name'              => array('type' => VAR_TYPE_STRING, 'name' => '会社名',         'required' => false,),
        'in_zip1'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 3, 'max' => 3,),
        'in_zip2'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 4, 'max' => 4,),
        'in_address'                   => array('type' => VAR_TYPE_STRING, 'name' => '住所',           'required' => false,),
        'in_phone_no1'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no2'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no3'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号3',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_department_name'           => array('type' => VAR_TYPE_STRING, 'name' => '部署名',         'required' => false,),
        'in_post_name'                 => array('type' => VAR_TYPE_STRING, 'name' => '役職名',         'required' => false,),
        'in_contractor_lname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（姓）', 'required' => false,),
        'in_contractor_fname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（名）', 'required' => false,),
        'in_email'                     => array('type' => VAR_TYPE_STRING, 'name' => 'メールアドレス', 'required' => false,)
    );
    
}

/**
 *  EditConfirm action implementation.
 */
class Opensite_Action_EditConfirm extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
    }

    /**
     *  preprocess of EditConfirm Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_EditConfirm prepare()");
        return null;
    }

    /**
     *  EditConfirm action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {

        //ログインチェック
        Opensite_Dao_Login::check_user_session();

        // セッション情報取得
        $id = $this->session->get('id');
	$serial = $this->session->get('serial');
        
        //入力値取得
        $data{'user_id'}           	= $this->af->get('in_user_id');
        $data{'password'}		= $this->af->get('in_password');
        $data{'company_name'}           = $this->af->get('in_company_name');
        $data{'zip1'}                   = $this->af->get('in_zip1');      
        $data{'zip2'}                   = $this->af->get('in_zip2');
        $data{'address'}                = $this->af->get('in_address');
        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
        $data{'phone_no2'}              = $this->af->get('in_phone_no2');            
        $data{'phone_no3'}              = $this->af->get('in_phone_no3');            
        $data{'department_name'}        = $this->af->get('in_department_name'); 
        $data{'post_name'}              = $this->af->get('in_post_name');            
        $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
        $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
        $data{'email'}                  = $this->af->get('in_email');

        if($_GET["back"] != "1"){
          
          
            // バリデート
            mb_regex_encoding("UTF-8");
            if($data{'user_id'}==""){
              $this->ae->add("in_user_id", "会員IDを入力してください", E_FORM_INVALIDVALUE);
            }else{
		$id_len = mb_strlen($data{'user_id'});
		//if($id_len < 4 || $id_len > 16){
		if(!preg_match("|^[0-9a-zA-Z_./?-]{4,16}$|",$data{'user_id'})){
                    $this->ae->add("in_user_id", "会員IDは、４文字以上１６文字以内の半角英数字でお願いします。", E_FORM_INVALIDVALUE);
                }else{
		    list($affect,$result_serial) = Opensite_Dao_Mypage::check_mst_user_id($data{'user_id'}); 
		    if($affect > 0){
		        if($result_serial != $serial){
			    $this->ae->add("in_user_id", "この会員IDはすでに登録されています", E_FORM_INVALIDVALUE);
		        }
		    }
		}
	    }
	    if($data{'password'}==""){
	      $this->af->setApp('pw_edit',  'none');
            }else{
	      $pw_len = mb_strlen($data{'password'});
              //if($pw_len < 4 || $pw_len > 16){
              if(!preg_match("|^[0-9a-zA-Z_./?-]{4,16}$|",$data{'password'})){
		$this->ae->add("in_password", "パスワードは、４文字以上１６文字以内の半角英数字でお願いします。", E_FORM_INVALIDVALUE);
	      }
	    }
            if($data{'company_name'}==""){
              $this->ae->add("in_company_name", "会社名を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'zip1'}==""){
              $this->ae->add("in_zip1", "郵便番号1を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'zip2'}==""){
              $this->ae->add("in_zip2", "郵便番号2を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'address'}==""){
              $this->ae->add("in_address", "住所を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'phone_no1'}==""){
              $this->ae->add("in_phone_no1", "電話番号1を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'phone_no2'}==""){
              $this->ae->add("in_phone_no2", "電話番号2を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'phone_no3'}==""){
              $this->ae->add("in_phone_no3", "電話番号3を入力してください", E_FORM_INVALIDVALUE);
            }
            //if($data{'department_name'}==""){
            //  $this->ae->add("in_department_name", "部署名を入力してください", E_FORM_INVALIDVALUE);
            //}
            //if($data{'post_name'}==""){
            //  $this->ae->add("in_post_name", "役職名を入力してください", E_FORM_INVALIDVALUE);
            //}            
            if($data{'contractor_lname'}==""){
              $this->ae->add("in_contractor_lname", "担当者名（姓）を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'contractor_fname'}==""){
              $this->ae->add("in_contractor_fname", "担当者名（名）を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'email'}==""){
              $this->ae->add("in_email", "メールアドレスを入力してください", E_FORM_INVALIDVALUE);
	    }else{
              $ret = Opensite_Mail::mail_format_check($data{'email'});
              if(!$ret){
                $this->ae->add("in_email", "正しいメールアドレスを入力してください", E_FORM_INVALIDVALUE);
              }else{
                $result_data = null;
                $result_count = null;
                //登録済みメールアドレスかどうかをチェック
                list($result_count, $result_data) = Opensite_Dao_Search::select_mail_exist($data{'email'});
                if($result_count > 0){
		  if($result_data[0]['serial'] != $serial){
                    //登録済みメールアドレスの場合
                    $this->ae->add("in_email", "既に登録済みのメールアドレスです", E_FORM_INVALIDVALUE);
		  }
                }
              }
            }

            // バリデート
            if ($this->af->validate() != 0) {
                //入力値に不備ありのため戻す      
                return 'edit_registration';
            }else{
                // 入力値OK
                for($i=1;$i<=$pw_len;$i++){
			 $pw_str .= "･";
		}
		$this->af->setApp('pw_str',  $pw_str);
            }
        }
        
        
        
        //バリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_EditConfirm perform()");
        return 'edit_confirm';
    }
}

?>
