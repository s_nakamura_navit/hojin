<?php
/**
 *  EditComplete.php
 */

/**
 *  EditComplete Form implementation.
 */
class Opensite_Form_EditComplete extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_user_id'                   => array('type' => VAR_TYPE_STRING, 'name' => '会員ID',         'required' => false,),
        'in_password'                  => array('type' => VAR_TYPE_STRING, 'name' => 'パスワード',     'required' => false,),
        'in_pw_edit'                   => array('type' => VAR_TYPE_STRING, 'name' => 'パスワード変更フラグ',     'required' => false,),
        'in_company_name'              => array('type' => VAR_TYPE_STRING, 'name' => '会社名',         'required' => false,),
        'in_zip1'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 3, 'max' => 3,),
        'in_zip2'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 4, 'max' => 4,),
        'in_address'                   => array('type' => VAR_TYPE_STRING, 'name' => '住所',           'required' => false,),
        'in_phone_no1'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no2'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no3'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号3',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_department_name'           => array('type' => VAR_TYPE_STRING, 'name' => '部署名',         'required' => false,),
        'in_post_name'                 => array('type' => VAR_TYPE_STRING, 'name' => '役職名',         'required' => false,),
        'in_contractor_lname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（姓）', 'required' => false,),
        'in_contractor_fname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（名）', 'required' => false,),
        'in_email'                     => array('type' => VAR_TYPE_STRING, 'name' => 'メールアドレス', 'required' => false,),
    );
    
}

/**
 *  EditComplete action implementation.
 */
class Opensite_Action_EditComplete extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
    }

    /**
     *  preprocess of EditComplete Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_EditComplete prepare()");
        return null;
    }

    /**
     *  EditComplete action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {

        //ログインチェック
        Opensite_Dao_Login::check_user_session();

        // セッション情報取得
        $id = $this->session->get('id');
        $serial = $this->session->get('serial');

        $db = $this->backend->getDb();
        $session_id = session_id();

if($this->session->get('id') != null) {
                    // ログアウト処理
        $sql = '
update
    user_session
set
    flg_delete = 1
where
    sessid ="'.$session_id.'"
limit 1';

                    $ret  = $db->query($sql);

                    $id   = $this->session->get("id");
                    $name = $this->session->get("name");

                    // 管理者ログ出力
                    Opensite_AdminLogger::logger("logout_do", "", "", "");

                    // セッション削除
                    $this->session->destroy();
}
                    $db = $this->backend->getDb();

        
        //入力値取得
        $data{'user_id'}           	= $this->af->get('in_user_id');
        $data{'password'}           	= $this->af->get('in_password');
        $data{'pw_edit'}           	= $this->af->get('in_pw_edit');
        $data{'company_name'}           = $this->af->get('in_company_name');
        $data{'zip1'}                   = $this->af->get('in_zip1');      
        $data{'zip2'}                   = $this->af->get('in_zip2');
        $data{'address'}                = $this->af->get('in_address');
        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
        $data{'phone_no2'}              = $this->af->get('in_phone_no2');            
        $data{'phone_no3'}              = $this->af->get('in_phone_no3');            
        $data{'department_name'}        = $this->af->get('in_department_name'); 
        $data{'post_name'}              = $this->af->get('in_post_name');            
        $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
        $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
        $data{'email'}                  = $this->af->get('in_email');
            


            $affected = "";
            $inserted_serial_no = "";
            // バリデート
            if ($this->af->validate() != 0) {
                //入力値に不備ありのため戻す          
                return 'edit_registration';
            }else{
                // 入力値OK
              
                //DB更新
                $affected = Opensite_Dao_Mypage::edit_mst_user($serial,$data);
                if($affected == null || $affected == "" || $affected == 0){
                  //エラー
                  $this->af->set('db_regist_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
                }else{
		 $affected = Opensite_Dao_Mypage::edit_mst_user_point($serial,$data['user_id']);
                 if($affected == null || $affected == "" || $affected == 0){
                  //エラー
                  $this->af->set('db_regist_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
                 }else{
                  $this->af->set('db_regist_result','1');
                  //DB登録OK

		  //宛先
                  $to = $data{'email'};
                  //$to = "uemura@navit-j.com";
                  //件名
                  $subject="【法人番号検索】会員情報編集完了メール";
                  //差出人
                  $fromname="法人番号検索 事務局";
                  $fromaddress="info_opensite@navit-j.net";

                  //本文をファイルから読み込み
                  //ファイル名
                  $user_edit_mail_file = BASE."/mail/user_edit.txt";
                  //ファイル読み込み
                  mb_language('Japanese');
                  $user_edit_mail = file_get_contents($user_edit_mail_file);
                  $user_edit_mail = mb_convert_encoding($user_edit_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
                  //読み込んだメール内の変数に値をセット
                  $user_edit_mail = str_replace('[ID]', $data['user_id'], $user_edit_mail);
		  if($data['pw_edit'] != "none"){
                  	$user_edit_mail = str_replace('[PW]', $data['password'], $user_edit_mail);
		  }else{
                  	$user_edit_mail = str_replace('[PW]', "パスワードの変更はありません", $user_edit_mail);
		  }
                  $user_edit_mail = str_replace('[COMPANY_NAME]', $data['company_name'], $user_edit_mail);
                  $user_edit_mail = str_replace('[DEPARTMENT_NAME]', $data['department_name'], $user_edit_mail);
                  $user_edit_mail = str_replace('[POST_NAME]', $data['post_name'], $user_edit_mail);
                  $user_edit_mail = str_replace('[CONTRACTOR_NAME]', $data['contractor_lname']."　".$data['contractor_fname'], $user_edit_mail);
                  $user_edit_mail = str_replace('[ZIP]', $data['zip1']."-".$data['zip2'], $user_edit_mail);
                  $user_edit_mail = str_replace('[ADDRESS]', $data['address'], $user_edit_mail);
                  $user_edit_mail = str_replace('[EMAIL]', $data['email'], $user_edit_mail);

                  //本文
                  $body = $user_edit_mail;

                  $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);
                  if($return_flag) {
                    //メール送信OK
                    $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND OK] TO:<".$to.">");
                    $this->af->set('mail_send_result','1');
                  }else{
                    //メール送信NG
                    $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND NG] TO:<".$to.">");
                    $this->af->set('mail_send_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
                    //仮登録したデータの削除
                    if($inserted_serial_no != "" && $inserted_serial_no != null){
                      Opensite_Dao_Search::delete_user_data($inserted_serial_no);
                    }
                  }
                }
	      }
            }
                //バリデート(入力項目無し)
                $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_EditComplete perform()");
                return 'edit_complete';
    }
  
  
}
?>
