<?php
/**
 *  Sendoffer.php
 */

/**
 *  Sendoffer Form implementation.
 */
class Opensite_Form_Sendoffer extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_callkind'       => array('type' => VAR_TYPE_STRING, 'name' => '検索種別',     'required' => false,),
        'in_contents_id'    => array('type' => VAR_TYPE_STRING, 'name' => 'コンテンツID', 'required' => false,),
        'in_contents_mod_html'    => array('type' => VAR_TYPE_STRING, 'name' => 'コンテンツ', 'required' => false,),
    );
    
}

/**
 *  Sendoffer action implementation.
 */
class Opensite_Action_Sendoffer extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
        return parent::authenticate();
    }

    /**
     *  preprocess of Sendoffer Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Sendoffer prepare()");
        return null;
    }

    /**
     *  SecureComplete action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // セッション情報取得
        $id = $this->session->get('id');
        
        //入力値取得
        $data{"callkind"}  = $this->af->get("in_callkind");
        $data{"contents_id"}  = $this->af->get("in_contents_id");
        $data{"contents_mod_html"}  = $this->af->get("in_contents_mod_html");
     
        //登録日時セット
        $date_now = date("Y-m-d H:i:s");
        $data{'updated'} = $date_now;
        $data{'created'} = $date_now;
        
        if($_GET["back"] != "1"){
            
            
            $affected = "";
            $inserted_serial_no = "";
            // バリデート
            if ($this->af->validate() != 0) {
                //入力値に不備ありのため戻す          
                return 'detail';
            }else{
                // 入力値OK
                
                //会員情報を取得
                list($result_counts, $result_data) = Opensite_Dao_Search::select_loginid_exist($id);
                //案件情報を取得
                list($contents_counts, $contents_data) = Opensite_Dao_Search::select_content($data{"contents_id"}, $data{"callkind"});
                //ログDB登録
                list($affected, $inserted_serial_no) = Opensite_Dao_Search::insert_interest_offer($result_data[0], $id, $data);
                //ログファイル出力
                $member_info = "";
                foreach($result_data[0] as $key => $val){
                  $member_info .= $val.", ";
                }
                $this->logger->log(LOG_INFO, "興味通知オファー送信[LOGIN_ID:".$this->session->get("id")."]"."[案件種別:".$data{"callkind"}."]"."[コンテンツID:".$data{"contents_id"}."]"."------会員情報:".$member_info);
                //管理者あてにメール送信
                //宛先
                //$to = "webmaster@navit-j.com";
                $to = "m.hirai@navit-j.com";
                //$to = array(array('m.hirai@navit-j.com','送信1'),array('webmaster@navit-j.com','送信2'));
                //件名
                $subject="【助成金なう】会員様より興味通知オファーが届きました";
                //差出人
                $fromname="助成金なう 事務局";
                $fromaddress="info@joseikin-now.com";
                //本文をファイルから読み込み
                //ファイル名
                $offer_mail_file = BASE."/mail/interest_offer_for_master.txt";
                //ファイル読み込み
                mb_language('Japanese');
                $offer_mail = file_get_contents($offer_mail_file);
                $offer_mail = mb_convert_encoding($offer_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
                //読み込んだメール内の変数に値をセット
                $offer_mail = str_replace('[COMPANY_NAME]', $result_data[0]{'company_name'}, $offer_mail);
                $offer_mail = str_replace('[BRANCH_NAME]', $result_data[0]{'branch_name'}, $offer_mail);
                $offer_mail = str_replace('[DEPARTMENT_NAME]', $result_data[0]{'department_name'}, $offer_mail);
                $offer_mail = str_replace('[POST_NAME]', $result_data[0]{'post_name'}, $offer_mail);
                $offer_mail = str_replace('[CONTRACTOR_NAME]', $result_data[0]{'contractor_lname'}."　".$result_data[0]{'contractor_fname'}, $offer_mail);
                $offer_mail = str_replace('[ZIP]', $result_data[0]{'zip1'}."-".$result_data[0]{'zip2'}, $offer_mail);
                $offer_mail = str_replace('[ADDRESS]', $result_data[0]{'address'}, $offer_mail);
                $offer_mail = str_replace('[PHONE_NO]', $result_data[0]{'phone_no1'}.$result_data[0]{'phone_no2'}.$result_data[0]{'phone_no3'}, $offer_mail);
                $offer_mail = str_replace('[FAX_NO]', $result_data[0]{'fax_no1'}.$result_data[0]{'fax_no2'}.$result_data[0]{'fax_no3'}, $offer_mail);
                $offer_mail = str_replace('[EMAIL]', $result_data[0]{'email'}, $offer_mail);
                
                $offer_mail = str_replace('[FROM]', $result_data[0]{'come_from'}, $offer_mail);
                $offer_mail = str_replace('[MAILMAGA]', $result_data[0]{'mailmaga'}, $offer_mail);
                
                $offer_mail = str_replace('[FIELD1]', $result_data[0]{'target_field1_str'}, $offer_mail);
                $offer_mail = str_replace('[FIELD2]', $result_data[0]{'target_field2_str'}, $offer_mail);
                $offer_mail = str_replace('[BUSINESSFORM]', $result_data[0]{'target_businessform_str'}, $offer_mail);
              
                $callkind_str = "";
                if($data{"callkind"}=="autonomy"){
                  $callkind_str = "自治体案件";
                }else if($data{"callkind"}=="foundation"){
                  $callkind_str = "財団法人案件";
                }
                $offer_mail = str_replace('[KIND]', $callkind_str, $offer_mail);
                $offer_mail = str_replace('[CONTENTS_ORG_ID]', $data{"contents_id"}, $offer_mail);
                $offer_mail = str_replace('[MAIN_TITLE]', $contents_data[0]{"main_title"}, $offer_mail);
                $offer_mail = str_replace('[COMMENTS]', $contents_data[0]{"comments"}, $offer_mail);
                
              
                //本文
                $body = $offer_mail;
        
                //コンテンツファイルを作成(添付ファイルとして送付)
                //CSSファイル読み込み
                $css_default = file_get_contents(BASE."/css/default.css");
                $css_default = mb_convert_encoding($css_default, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
                $css_ditail = file_get_contents(BASE."/css/ditail.css");
                $css_ditail = mb_convert_encoding($css_ditail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
                $css_first = file_get_contents(BASE."/css/first.css");
                $css_first = mb_convert_encoding($css_first, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
                //ファイル内容を作成;
                $attach_file = '<html xmlns="http://www.w3.org/1999/xhtml">';
                $attach_file .= '<head>';
                $attach_file .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">';
                $attach_file .= '<meta http-equiv="Content-Style-Type" content="text/css">';
                $attach_file .= '<style type="text/css">';
                $attach_file .= $css_default;
                $attach_file .= $css_ditail;
                $attach_file .= $css_first;
                $attach_file .= '</style>';
                $attach_file .= '</head>';
                $attach_file .= '<body>';
                $attach_file .= $data{"contents_mod_html"};
                $attach_file .= '</body>';
                $attach_file .= '</html>';
                //ファイル名(interest_offer_[id][種別][コンテンツID]_[生成日時].html)
                $attach_file_name = "interest_offer_".$id.$data{"callkind"}.$data{"contents_id"}."_".$data{'created'}.".html";
                $attach_file_name = BASE."/tmp/offer_attach_file/".$attach_file_name;
                //ファイル保存
                file_put_contents($attach_file_name, $attach_file);
                
                //メール送信
                //$return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);
                $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress,false,$attach_file_name); 
                if($return_flag) {
                  //メール送信OK
                  $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND OK] TO:<".$to.">");
                  $this->af->set('mail_send_result','1');
                }else{
                  //メール送信NG
                  $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND NG] TO:<".$to.">");
                  $this->af->set('mail_send_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
                }
                
            }
            $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Sendoffer perform()");
            return 'sendoffer';
        }
    }
  
  
}
?>
