<?php
/**
 *  Thanks.php
 */

/**
 *  Thanks Form implementation.
 */
class Opensite_Form_Thanks extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_header_check' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
        'in_header_line' => array(
            'type' => VAR_TYPE_INT,
            'required' => true
        ),
        'in_list_name' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
        'in_up_file_path' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_use_point' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
        'in_list_count' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
	'in_property_key' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true,
        ),
	'in_property_name' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true,
        ),
        'in_request_id' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
	'in_act' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true,
        ),
	'in_trial_serial' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true,
        ),
    );
    
}

/**
 *  Thanks action implementation.
 */
class Opensite_Action_Thanks extends Opensite_ActionClass
{
//    function authenticate()
//    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
//    }

    /**
     *  preprocess of Thanks Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        // フォームバリデーションを実行し、EthnaErrorオブジェクトを登録する
        $this->af->validate();

        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Thanks prepare()");
        return null;
    }

    /**
     *  Thanks action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {

        //ログインチェック
        Opensite_Dao_Login::check_user_session();

        // セッション情報取得
        $id = $this->session->get('id');
        $serial = $this->session->get('serial');

	$fm = $this->backend->getManager("furu");

	//再描画時のチェック項目、値が入っている場合DB処理、メール送信はしない
	$re_send_check	   = $this->session->get("re_send_check");

        //管理画面側からリストをインクルード
        $this->af->setApp("property_list",      $GLOBALS['hojin_request_property_list']);

	$list_count = $this->af->get("in_list_count");
	$list_name = $this->af->get("in_list_name");
	$up_file_path = $this->af->get("in_up_file_path");
	$use_point = $this->af->get("in_use_point");
	$property_key = $this->af->get("in_property_key");
	$property_name = $this->af->get("in_property_name");
	$act = $this->af->get("in_act");

	//ユーザー情報
	$data = Opensite_Dao_Mypage::get_user_info_by_id($serial);

	$header_line = $this->af->get("in_header_line");
        //登録日時セット
/*
        $date_now = date("Y-m-d H:i:s");
        $data{'updated'} = $date_now;
        $data{'created'} = $date_now;
*/

        //依頼番号
	$request_id = $this->af->get("in_request_id");
              
        //DB登録
        if(($act == "furufuru_trial" || $act == "request_estimate") && $re_send_check == ""){

	    if($act == "furufuru_trial"){

		// 依頼番号を生成
		$request_id = ceil(microtime(true) * 1000);

		// session値を利用してファイルを生成
		$up_file_path = "upload/furu_trial_". $request_id . "_org.csv";
		$trial_data = $this->session->get('trial_data');
		$list_row = $fm->create_trial_csv($up_file_path,$trial_data);
		
		// property_nameは全項目
		$property_key = "";
		$property_name = "";
		$i = 0;
		foreach($GLOBALS['hojin_request_property_list'] as $key => $val){
			$i++;
			if($i != 1){
				$property_key .= ",";
				$property_name .= ",";
			}
			$property_key .= $key;
			$property_name .= $val;
		}

		// 依頼の登録
		$list_name = "お試し".$request_id;
        	$res = Opensite_Dao_Furu::insert_hojin_request($request_id,$serial,$data['account'],$list_name,$list_row,1,$property_key,$up_file_path,0,"trial");
        	if(Ethna::isError($res))
        	{
		    $this->af->setApp("title","システムエラー");
                    $this->af->setApp("message","ファイル取得エラー");
                    return 'message';
        	}

		//依頼内容表示のための文字列作成
		$request_detail = "";
		foreach($trial_data as $key => $item){
			if($item['corp'] != ""){
				if($request_detail != ""){
					$request_detail .= "\n\n";
				}
				$request_detail .= "企業情報\n企業名：".$item['corp']."\n電話：".$item['tel']."\n住所：".$item['address'];
				//$request_detail .= "企業情報".$key."\n企業名：".$item['corp']."\n住所：".$item['address']."\n電話：".$item['tel']; //20151021改修によりコメントアウト
			}
		}	

	    }else if($act == "request_estimate"){
		$res = Opensite_Dao_Mypage::use_point($id,$serial,$use_point,$request_id,0);
        	if(Ethna::isError($res))
        	{
        	    $this->af->setApp("title","システムエラー");
            	    $this->af->setApp("message","ファイル取得エラー");
            	    return 'message';
        	}

		$process = 0;	//依頼の本登録
		$res = Opensite_Dao_Furu::update_hojin_request($request_id,"","",$process);
        	if(Ethna::isError($res))
        	{
            	    $this->af->setApp("title","システムエラー");
            	    $this->af->setApp("message","ファイル取得エラー");
            	    return 'message';
        	}
	    }

            //宛先
            $to = $data{'email'};
            //件名
	    if($act == "furufuru_trial"){
        	$subject="【法人番号検索】依頼メール";
        	//本文ファイル名
        	$tmp_inq_mail_file = BASE."/mail/trial_commission.txt";
	    }else if($act == "request_estimate"){
		$subject="【法人番号検索】依頼メール";
        	//本文ファイル名
        	$tmp_inq_mail_file = BASE."/mail/commission.txt";
	    }
            //差出人
            $fromname="法人番号検索 事務局";
            $fromaddress="info_opensite@navit-j.net";

            //本文をファイルから読み込み
            //ファイル読み込み
            mb_language('Japanese');
            $tmp_inq_mail = file_get_contents($tmp_inq_mail_file);
            $tmp_inq_mail = mb_convert_encoding($tmp_inq_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');          
	    //読み込んだメール内の変数に値をセット
            $tmp_inq_mail = str_replace('[COMPANY_NAME]', $data['company_name'], $tmp_inq_mail);
	    if($data['branch_name'] == ""){
	    	$tmp_inq_mail = str_replace('[BRANCH_NAME]', 'なし', $tmp_inq_mail);
	    }else{
            	$tmp_inq_mail = str_replace('[BRANCH_NAME]', $data['branch_name'], $tmp_inq_mail);
	    }
	    if($data['post_name'] == ""){
            	$tmp_inq_mail = str_replace('[POST_NAME]', 'なし', $tmp_inq_mail);
	    }else{
            	$tmp_inq_mail = str_replace('[POST_NAME]', $data['post_name'], $tmp_inq_mail);
	    }
            $tmp_inq_mail = str_replace('[DEPARTMENT_NAME]', $data['department_name'], $tmp_inq_mail);
            $tmp_inq_mail = str_replace('[CONTRACTOR_NAME]', $data['contractor_lname']."　".$data['contractor_fname'], $tmp_inq_mail);
            $tmp_inq_mail = str_replace('[ZIP]', $data['zip1']."-".$data['zip2'], $tmp_inq_mail);
            $tmp_inq_mail = str_replace('[ADDRESS]', $data['address'], $tmp_inq_mail);
            $tmp_inq_mail = str_replace('[PHONE_NO]', $data['phone_no1']."-".$data['phone_no2']."-".$data['phone_no3'], $tmp_inq_mail);
	    if($data['fax_no1'] == "" || $data['fax_no2'] == "" || $data['fax_no3'] == ""){
            	$tmp_inq_mail = str_replace('[FAX_NO]', 'なし', $tmp_inq_mail);
	    }else{
                $tmp_inq_mail = str_replace('[FAX_NO]', $data['fax_no1']."-".$data['fax_no2']."-".$data['fax_no3'], $tmp_inq_mail);
	    }
            $tmp_inq_mail = str_replace('[EMAIL]', $data['email'], $tmp_inq_mail);
	    if($act == "furufuru_trial"){
        	$tmp_inq_mail = str_replace('[REQUEST_DETAIL]', $request_detail, $tmp_inq_mail);
	    }else if($act == "request_estimate"){
        	$tmp_inq_mail = str_replace('[LIST_NAME]', $list_name, $tmp_inq_mail);
        	$tmp_inq_mail = str_replace('[PROPERTY]', $property_name, $tmp_inq_mail);
	    }
              
            //本文
            $body = $tmp_inq_mail;
            $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);                
            if($return_flag) {
            	//メール送信OK
            	$this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND OK] TO:<".$to.">");
            	$this->af->set('mail_send_result','1');

            	//webmasterへのメール通知
            	//$to="uemura@navit-j.com";
		$to = array(array('btobmarketing@navit-j.com','送信1'),array('webmaster@navit-j.com','送信2'));
            	//$to = array('k.kobayashi@navit-j.com','送信1');
            	//件名
		if($act == "furufuru_trial"){
        	    $subject="【通知】 法人番号検索　お試しサービス 依頼メール";
		    //ファイル名
            	    $tmp_inq_mail_file = BASE."/mail/trial_commission_for_master.txt";
		}else if($act == "request_estimate"){
		    $subject="【通知】 法人番号検索 依頼メール";
		    //ファイル名
            	    $tmp_inq_mail_file = BASE."/mail/commission_for_master.txt";
		}

            	//ファイル読み込み
            	mb_language('Japanese');
            	$tmp_inq_mail = file_get_contents($tmp_inq_mail_file);
            	$tmp_inq_mail = mb_convert_encoding($tmp_inq_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
            	//読み込んだメール内の変数に値をセット
	    	$tmp_inq_mail = str_replace('[USER_ID]', $id, $tmp_inq_mail);
            	$tmp_inq_mail = str_replace('[COMPANY_NAME]', $data['company_name'], $tmp_inq_mail);
	    	if($data['branch_name'] == ""){
		    $tmp_inq_mail = str_replace('[BRANCH_NAME]', 'なし', $tmp_inq_mail);
	    	}else{
              	    $tmp_inq_mail = str_replace('[BRANCH_NAME]', $data['branch_name'], $tmp_inq_mail);
	    	}
	    	if($data['post_name'] == ""){
              	    $tmp_inq_mail = str_replace('[POST_NAME]', 'なし', $tmp_inq_mail);
	    	}else{
               	    $tmp_inq_mail = str_replace('[POST_NAME]', $data['post_name'], $tmp_inq_mail);
	    	}
                $tmp_inq_mail = str_replace('[DEPARTMENT_NAME]', $data['department_name'], $tmp_inq_mail);
                $tmp_inq_mail = str_replace('[CONTRACTOR_NAME]', $data['contractor_lname']."　".$data['contractor_fname'], $tmp_inq_mail);
                $tmp_inq_mail = str_replace('[ZIP]', $data['zip1']."-".$data['zip2'], $tmp_inq_mail);
                $tmp_inq_mail = str_replace('[ADDRESS]', $data['address'], $tmp_inq_mail);
                $tmp_inq_mail = str_replace('[PHONE_NO]', $data['phone_no1']."-".$data['phone_no2']."-".$data['phone_no3'], $tmp_inq_mail);
	        if($data['fax_no1'] == "" || $data['fax_no2'] == "" || $data['fax_no3'] == ""){
               	    $tmp_inq_mail = str_replace('[FAX_NO]', 'なし', $tmp_inq_mail);
		}else{
                    $tmp_inq_mail = str_replace('[FAX_NO]', $data['fax_no1']."-".$data['fax_no2']."-".$data['fax_no3'], $tmp_inq_mail);
		}
                $tmp_inq_mail = str_replace('[EMAIL]', $data['email'], $tmp_inq_mail);
                $tmp_inq_mail = str_replace('[FILE_PATH]', $up_file_path, $tmp_inq_mail);
                $tmp_inq_mail = str_replace('[PROPERTY]', $property_name, $tmp_inq_mail);

                $body = $tmp_inq_mail;
                $fromname="法人番号検索　管理システムよりお知らせ";
                $fromaddress="info_opensite@navit-j.net";
                $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);
	    }else{
            	//メール送信NG
            	$this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND NG] TO:<".$to.">");
            	$this->af->set('mail_send_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
            }
            //再描画時に処理を重複させないためチェックする値を格納
            $this->session->set("re_send_check",        "send_after");
	}

        $this->af->setApp("request_id", $request_id);
        $this->af->setApp("act",	$act);

        //バリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_InquiryComplete perform()");
        return 'thanks';
    }
}

?>
