<?php
/**
 *  Index.php
 */

/**
 *  index Form implementation.
 */
include('Opensite_Trialone.php');

class Opensite_Form_Index extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
	var $form = array(
        'in_up_list' => array(
            'type' => VAR_TYPE_FILE,
            'form_type'   => FORM_TYPE_FILE,
            'required' => true,
        ),
        'in_header_check' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false,
        ),
        'in_header_line' => array(
            'type' => VAR_TYPE_INT,
            'required' => true,
        ),
        'in_property_check' => array(
            'type' => array(VAR_TYPE_STRING),
            'required' => false
        ),
        'in_auto_check' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false,
        ),
        'in_corp_1' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_address_1' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_tel_1' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_corp_2' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_address_2' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_tel_2' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_corp_3' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_address_3' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_tel_3' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),

		'sc_name' => array(
				'type' => VAR_TYPE_STRING,
				'required' => false
		),
		'sc_address' => array(
				'type' => VAR_TYPE_STRING,
				'required' => false
		),
		'corp_id' => array(
				'type' => VAR_TYPE_INT,
				'required' => false
		),
		'in_act' => array(
				'type' => VAR_TYPE_STRING,
				'required' => false
		)
    );

}

/**
 *  index action implementation.
 */
class Opensite_Action_Index extends Opensite_ActionClass
{
    function authenticate()
    {
        // Indexは認証の必要がないので
        // 親クラスのauthenticate()を無処理でオーバーライド
    }

    /**
     *  preprocess of index Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
		/////
		/////	アライアンスcookie対応	2017-02-28	中村
		/////
		if ( isset($_GET["allcd"]) ){
			$allcd = $_GET["allcd"];
		}else if (isset($_COOKIE["opensite_alliance"])){
			$allcd = $_COOKIE["opensite_alliance"];
		}else{
			$allcd = "";
		}
		$alliance = "";
		if ($allcd){
			$all_rec = Opensite_Dao_Alliance::valid_alliance($allcd);
			if ( count($all_rec) ){
				$cookie_limit = time() + $all_rec[0]["cookie_days"] * 24 * 60 * 60;
				setcookie("opensite_alliance", $allcd, $cookie_limit, "/", ".navit-j.com");
				Opensite_Dao_Alliance::add_log($all_rec[0]["id"]);
				$alliance = $all_rec[0]["come_from"];
			}
		}else{
			setcookie("opensite_alliance", "", $cookie_limit, "/", ".navit-j.com");
		}
		$this->af->setApp("alliance", $alliance);
		/////
		/////	以上まで
		/////
        return null;
    }

    /**
     *  index action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {

        //ログインチェック
        //Opensite_Dao_Login::check_user_session();
		//新規登録時発行のcookieを持っている場合は継続更新する
		if($_COOKIE['opensite_new_user'] != ""){
			setcookie("opensite_new_user",$_COOKIE['opensite_new_user'],time() + 3650 * 24 * 60 * 60,"/",".navit-j.com");
		}

		$property_check = $this->af->get("in_property_check");
		if(isset($property_check)){
	            foreach($property_check as $key){
	        	$property_checked[$key] = "checked";
	            }
		}
        $this->af->setApp("property_checked",    $property_checked);

        //依頼時の二重処理防止フラグ　必ずトップで初期化
        $this->session->set("re_send_check", 	"");

		//お試し情報の初期化
		$this->session->set("trial_data",	"");

		// session情報の取得（光通信bizcompass用）
		$id = $this->session->get("id");
		if(preg_match("/^HT[0-9]{10}$/", $id)) {
			$this->af->setApp("is_HTflg", true);
		} else {
			$this->af->setApp("is_HTflg", false);
		}

		$this->af->setApp("property_list",    $GLOBALS['hojin_request_property_list']);

        $this->af->setApp("header_check",   $this->af->get("in_header_check"));
        $this->af->setApp("header_line",    $this->af->get("in_header_line"));
        $this->af->setApp("auto_check",    $this->af->get("in_auto_check"));

		$this->af->setApp("data_corp_1",    $this->af->get("in_corp_1"));
		$this->af->setApp("data_address_1",    $this->af->get("in_address_1"));
		$this->af->setApp("data_tel_1",    $this->af->get("in_tel_1"));
		$this->af->setApp("data_corp_2",    $this->af->get("in_corp_2"));
		$this->af->setApp("data_address_2",    $this->af->get("in_address_2"));
		$this->af->setApp("data_tel_2",    $this->af->get("in_tel_2"));
		$this->af->setApp("data_corp_3",    $this->af->get("in_corp_3"));
		$this->af->setApp("data_address_3",    $this->af->get("in_address_3"));
		$this->af->setApp("data_tel_3",    $this->af->get("in_tel_3"));


		/*
		 * 新規追加（企業検索）
		 */
		$act = $this->af->get("in_act");
		if($act=='search_corp') {
			$this->af->setApp("sc_name",    $this->af->get("sc_name"));
			$this->af->setApp("sc_address",    $this->af->get("sc_address"));
			$this->af->setApp("start_search",	1);

			//フォーム内容の変数格納
			$sc['name'] = $this->af->get("sc_name");
			$sc['address'] = $this->af->get("sc_address");

//			if (!empty($sc['name']) && !empty($sc['address'])) {
			if (!empty($sc['name']) ) {
				$string = str_replace(array(" ", "　", "株式会社"), "", $sc['name']);
				if(!empty($string)) {
					//企業検索名準備
					$sc_name = Opensite_Trialone::searchWordCorp($sc['name']);
					$sc_name_half = mb_convert_kana($sc_name, 'a');
					//【SMTY】検索企業名をsmへパス
					$this->af->setApp("sc_name",	$sc['name']);

					
					if (!empty($sc['address'])){
						//都道府県切り出し（簡易的な判定用）
						$sc_pref = Opensite_Trialone::cutPref($sc['address']);
					}

//					if($sc_pref != false) {
						$db =& $this->backend->getDB();
						$select_table = Opensite_Trialone::getTrnInspect();

						$adress = $this->af->get("sc_address");

						//県判別
						$pref_sch = Opensite_Trialone::searchPref($adress);
						if($pref_sch) {
							$pref_num = $pref_sch['code'];		//都道府県コード
							$pref_name = $pref_sch['name'];		//都道府県名
							$this->af->setApp("pref_code",	$pref_num);
						} else {
//							$search_check_err_address = "都道府県名が正しくありません。";
//							$this->af->setApp("search_check_err_address",	$search_check_err_address);
//							return 'index';
						}

						//市区町村判別
						$city_sch = Opensite_Trialone::searchCity($adress);
						if($city_sch) {
							$city_num = $city_sch["citycode"];
							$this->af->setApp("city_code",	$city_num);

							if($city_sch != false) {
								$city_check = str_replace($city_sch['longname'], "", $adress);
								$city_check_cnt = mb_strlen($city_check);
								//市区町村以下が入力されていた場合
								if($city_check_cnt > 0) {
									$search_check_err_address = "市区町村名以下は入力できません。";
									$this->af->setApp("search_check_err_address",	$search_check_err_address);
									return 'index';
								}
							}
						} else {
							$city_check = str_replace($pref_name, "", $adress);
							if($city_check != '') {
								$search_check_err_address = "所在地が正しくありません。";
								$this->af->setApp("search_check_err_address",	$search_check_err_address);
								return 'index';
							}
						}

						//【SMTY】検索都道府県をsmへパス
						$this->af->setApp("sc_pref",	$pref_name);

//						$table_name = $select_table . "_" . $pref_num;
						//検索ヒット件数
						$result_corp_cnt = Opensite_Trialone::searchCorpCnt($select_table,$sc_name,$city_num, $pref_num, $sc_name_half);
						$this->af->setApp("result_corp_cnt",	$result_corp_cnt);

						if ($result_corp_cnt['sc_cnt'] > 0) {
							//企業検索
							$result = Opensite_Trialone::searchCorp($select_table,$sc_name,$city_num, $pref_num, $sc_name_half);
							$i=0;
							while ($row =& $result->fetchRow()) {
								if ($this->af->get("sc_name") == $row['corp_num']){
									$hit_type = "corp_num";
								}else{
									$hit_type = "corp_name";
								}
								$result_corp[$i]['corp_num'] = $row['corp_num'];
								$result_corp[$i]['corp_id'] = $row['corp_id'];
								$result_corp[$i]['corp_name'] = $row['corp_name'];
								$result_corp[$i]['pref'] = $row['pref'];
								$result_corp[$i]['corp_city'] = $row['corp_city'];
								$i = $i + 1;
							}
							//【SMTY】検索結果をsmへパス
							$this->af->setApp("result_corp", $result_corp);
							$this->af->setApp("hit_type", $hit_type);
						} else {
							//検索結果が０件の場合
							$search_none = "見つかりませんでした。";
							$this->af->setApp("search_none",	$search_none);
						}
//					} else {
//						$search_check_err_address = "都道府県名が正しくありません。";
//						$this->af->setApp("search_check_err_address",	$search_check_err_address);
//					}
				} else {
					$search_check_err = "法人番号・企業名が入力されていません。";
					$this->af->setApp("search_check_err",	$search_check_err);
/*
					if(empty($sc['address'])) {
						$search_check_err_address = "都道府県名が入力されていません。";
						$this->af->setApp("search_check_err_address",	$search_check_err_address);
					}
*/					
				}
			} else {
				if(empty($sc['name'])) {
					$search_check_err = "法人番号・企業名が入力されていません。";
					$this->af->setApp("search_check_err",	$search_check_err);
				}
/*
				if(empty($sc['address'])) {
					$search_check_err_address = "都道府県名が入力されていません。";
					$this->af->setApp("search_check_err_address",	$search_check_err_address);
				}
*/				
			}
		}

        return 'index';
    }


}

?>
