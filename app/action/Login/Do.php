<?php
/**
 *  Login/Do.php
 *  ログイン実施アクションフォーム
 *
 *  @copyright  (C)2010 Navit co.,ltd.
 *  @author     Tetsuo Nakanishi
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  login_do Form implementation.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Form_LoginDo extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_id' => array(
            'name' => 'ログインID',
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
        'in_pw' => array(
            'name' => 'パスワード',
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
        'in_act' => array(
            'name' => '処理段階',
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
    	'in_captcha_code' => array(
    			'name' => '画像認証',
    			'type' => VAR_TYPE_STRING,
    			'required' => false
    	),
    );


    /**
     *  Form input value convert filter : sample
     *
     *  @access protected
     *  @param  mixed   $value  Form Input Value
     *  @return mixed           Converted result.
     */
    /*
    function _filter_sample($value)
    {
        //  convert to upper case.
        return strtoupper($value);
    }
    */
}

/**
 *  login_do action implementation.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Action_LoginDo extends Opensite_ActionClass
{
    /**
     *  authenticate
     *
     *  @access    public
     *  @return    string  Forward name (null if no errors.)
     */
    function authenticate()
    {
        // 認証時呼び出されるアクションのため
        // ここでは authenticate を呼び出すとログイン出来なくなるため
        // そのまま return を返す
        return;
    }

    /**
     *  preprocess of login_do Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] view login_do");

        // バリデートに失敗したらやりなおし
        if ($this->af->validate() > 0) {
            // フォーム入力値
            $data{"id"}  = $this->af->get('in_id');
            // エスケープ有
            $this->af->setApp('data',  $data);

            return 'login';
        }
        return null;
    }

    /**
     *  login_do action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
	if($this->af->get('in_act') == "overlap"){
	    Opensite_Dao_Login::delete_user_session($this->af->get('in_id'));
	}

		$code_disp = $this->session->get("securimage_code_disp");

        // ログイン認証
        $la     =& new Opensite_Authentication();
        //$result =  $la->auth($this->af->get('in_id'), $this->af->get('in_pw'));
        $result =  $la->auth($this->af->get('in_id'), $this->af->get('in_pw'), $this->af->get('in_captcha_code'), $code_disp['default'] );
        if (Ethna::isError($result)) {
            // ログインNG
            $this->ae->addObject(null, $result);
            // フォーム入力値
            $data{"id"}  = $this->af->get('in_id');
            // エスケープ有
            $this->af->setApp('data',  $data);

            return 'login';
        }else if($result == "overlap"){
	    $this->af->setApp('act', 'overlap');
            // フォーム入力値
            $data{"id"}  = $this->af->get('in_id');
            $data{"pw"}  = $this->af->get('in_pw');
            // エスケープ有
            $this->af->setApp('data',  $data);

            return 'login';
	}

        // セッション値取得
        $serial = $this->session->get("serial");
        $id     = $this->session->get("id");
        $name   = $this->session->get("name");
        $rank   = $this->session->get("rank");
        $company_name   = $this->session->get("company_name");

        // エスケープ有
        $this->af->setApp('serial', $serial);
        $this->af->setApp('id',     $id);
        $this->af->setApp('name',   $name);
        $this->af->setApp('rank',   $rank);
        $this->af->setApp('company_name',   $company_name);

        // ログインOK
        $src = "index";
        header('Location: index.php');
        return null;
        //return $src;
    }
}

?>
