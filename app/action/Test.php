<?php
/**
 *  test.php
 */

/**
 *  test Form implementation.
 */
class Opensite_Form_Test extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        
      
    );
    
}

/**
 *  test action implementation.
 */
class Opensite_Action_Test extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
    }

    /**
     *  preprocess of test Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Test prepare()");
        return null;
    }

    /**
     *  autonomy action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // selectionフォーム入力値(入力項目無し)
        
        // selectionバリデート(入力項目無し)

        //$this->is_layout = false;
        
        //echo ".";
        //header("Cache-Control: no-cache"); // HTTP/1.1
        return 'index';
    }
}

?>
