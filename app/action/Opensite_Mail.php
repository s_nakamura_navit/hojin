<?php
/**
 *  Opensite_Mail.php
 *  メール送信クラス
 */

/**
 *  Opensite_Mail Class.
 */

//Qdmailをロード
require_once('qdmail.php');
//Qdsmtpをロード
require_once('qdsmtp.php');

class Opensite_Mail
{
    /**
     *  send_by_code
     *  テンプレートを使用してメール送信
     *
     *  PEAR::Mailを使用しています
     *
     *  @access protected
     *  @param  string  mail_code  メールテンプレートファイルの番号
     *  @param  string  to         To送信先
     *  @param  string  cc         Cc送信先
     *  @param  string  bcc        Bcc送信先
     *  @param  string  mail_data  置換する本文用データ
     *  @return boolean            送信結果
     */

  //メールアドレス形式チェック
  function mail_format_check($mailaddr){
    
    if (filter_var($mailaddr, FILTER_VALIDATE_EMAIL) === false) {
      return false;
    }else{
      return true;
    }
    
  }
  //メール送信関数
  // $to：送信先メールアドレス
  // $subject：件名（日本語OK）
  // $body：本文（日本語OK）
  // $fromname：送信元名（日本語OK）
  // $fromaddress：送信元メールアドレス
  // $is_html：htmlメールか？
  // $attach_file：添付ファイル
  function mailsender($to,$subject,$body,$fromname,$fromaddress,$is_html=false,$attach_file=""){
    //SMTP送信
    $mail = new Qdmail();
    $mail -> errorDisplay( false );
    $mail -> smtp(true);
    $param = array(
        'host'=>'navit.xsrv.jp',
        'port'=> 587 ,
//        'from'=>'info@joseikin-now.com',
        'from'=>'info_opensite@navit-j.net',
        'protocol'=>'SMTP_AUTH',
//        'user'=>'info@joseikin-now.com',
        'user'=>'info_opensite@navit-j.net',
      'pass' => 'navit5215',
    );
    $mail ->smtpServer($param);
    $mail ->to($to);
    $mail ->subject($subject);
    $mail ->from($fromaddress,$fromname);
    if($is_html){
      $mail ->html($body);
    }else{
      $mail ->text($body);
    }
    if($attach_file){
      $attach[] = array($attach_file,$attach_file);
      $mail ->attach($attach);
    }
    $return_flag = $mail ->send();
    return $return_flag;
  }

    
    
    
    
    
    
    
    
    function send_by_code($mail_code, $to, $cc, $bcc, $mail_data)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();

        $dir       = $ctl->getDirectory('mail');
        $file_path = $dir."/".$mail_code.".txt";
        $subject    = "";
        $attachfile = "";
        $body       = "";
        $flg_body   = 0;

        $lines = explode("\n", file_get_contents($file_path));
        foreach($lines as $line){

            if(preg_match("/^subject:/", $line)){
                $subject = str_replace("subject:", "", $line);
                $subject = trim($subject);
            }
            if(preg_match("/^attachfile:/", $line)){
                $attachfile = str_replace("attachfile:", "", $line);
                $attachfile = trim($attachfile);
            }
            if(preg_match("/^body:/", $line)){
                $flg_body = 1;
                $line     = str_replace("body:", "", $line);
            }
            if($flg_body == 1){
                foreach($mail_data as $rep_key => $rep_val){
                    $line = preg_replace("/\[$rep_key\]/", $rep_val, $line);
                }
                $body .= trim($line)."\r\n";
            }
        }

        $logger->log(LOG_INFO, "Mail::send_by_code[".$mail_code."] to[".$to."] subject[".$subject."] を生成しました");

        if($subject == "" || $body == ""){
            $logger->log(LOG_INFO, "Mail::send_by_code[".$mail_code."] to[".$to."] subject[".$subject."] subjectまたはbodyが空のため送信処理を中止しました");
            return false;
        }
        $logger->log(LOG_INFO, "Mail::send_by_code[".$mail_code."] to[".$to."] subject[".$subject."] sendモジュールへの送信依頼開始");
        $ret = Opensite_Mail::send($to, $cc, $bcc, $subject, $body, $attachfile);
        $logger->log(LOG_INFO, "Mail::send_by_code[".$mail_code."] to[".$to."] subject[".$subject."] sendモジュールへの送信依頼終了");

        return $ret;
    }

    /**
     *  send
     *  メール送信
     *
     *  PEAR::Mailを使用しています
     *
     *  @access protected
     *  @param  string  to         To送信先
     *  @param  string  cc         Cc送信先
     *  @param  string  bcc        Bcc送信先
     *  @param  string  subject    タイトル
     *  @param  string  body       本文
     *  @param  string  attachfile 添付ファイル
     *  @return boolean            送信結果
     */
    function send($to, $cc, $bcc, $subject, $body, $attachfile)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();

/*
        $mail = Mail::factory('smtp', array(
            'host'     => 'tls://smtp.gmail.com',
            'port'     => 465,
            'auth'     => true,
            'debug'    => false,
            'username' => 'yoshiakikato3',
            'password' => 'geed98ef'
        ));
        $mime = new Mail_Mime("\n");
*/
$logger->log(LOG_INFO, "param生成");

$params = array(
  "host" => "navit.xsrv.jp",
  "port" => 587,
  "auth"     => false,
  'username' => 'info@joseikin-now.com',
  'password' => 'navit5215',
);
$logger->log(LOG_INFO, "factoryセット");
$mailObject = Mail::factory("smtp", $params);
if (PEAR::isError($mailObject)) {
print $mailObject->getMessage();
print $mailObject->getDebugInfo();
$errmsg = $mailObject->getMessage();
$logger->log(LOG_INFO, "errmsg=".$errmsg);
}


//        $mail = Mail::factory('smtp', array(
//            'host'     => 'navit.xsrv.jp',
 //           'port'     => 587,
 //           'auth'     => true,
 //           'debug'    => false,
 //           'username' => 'info@joseikin-now.com',
 //           'password' => 'navit5215'
 //       ));

       // if(PEAR::isError($mail)) {
       //     $errmsg = $mail->getMessage();
       //     $logger->log(LOG_INFO, "errmsg=".$errmsg);
       // }

        
        
        
        
        $mime = new Mail_Mime("\n");

        // 本文
$logger->log(LOG_INFO, "bodyセット");
        $body = mb_convert_encoding($body, "JIS");
        $mime->setTxtBody($body);

        // 添付ファイル追加
        if($attachfile) {
            $mime->addAttachment($attachfile, "application/octet-stream");
        }

        // 本文エンコード
        $body_encode = array(
            "head_charset" => "ISO-2022-JP",
            "text_charset" => "ISO-2022-JP"
        );
        $body = $mime->get($body_encode);
$logger->log(LOG_INFO, "bodyセット");
        $org = mb_internal_encoding(); // 元のエンコーディングを保存
        mb_internal_encoding("ISO-2022-JP"); // 変換したい文字列のエンコーディングをセット
        // 送信元設定
//        $from = mb_encode_mimeheader(mb_convert_encoding(MAIL_FROM_NAME, "JIS"))."<".MAIL_FROM.">";
        $from = mb_encode_mimeheader(mb_convert_encoding(MAIL_FROM_NAME, "ISO-2022-JP", "UTF-8"),"ISO-2022-JP","B","\r\n")."<".MAIL_FROM.">";

        // ヘッダ
        $subject = mb_encode_mimeheader(mb_convert_encoding($subject, "ISO-2022-JP", "UTF-8"),"ISO-2022-JP","B","\r\n");
        $headers = array(
            "To"      => $to,
            "Cc"      => $cc,
            "Bcc"     => $bcc,
            "From"    => $from,
            "Subject" => $subject
        );
        $header = $mime->headers($headers);
        mb_internal_encoding($org); // エンコーディングを戻す

        // 送信
$logger->log(LOG_INFO, "送信処理開始");
        $return = $mail->send($to, $header, $body);
        if (PEAR::isError($return)){
            $this->logger->log(LOG_WARNING, "メール送信に失敗しました To:".$to." Subject:".$subject);
            return false;
        }

        $this->logger->log(LOG_INFO, "メールを送信しました To:".$to." Subject:".$subject);

        return true;
    }
}
?>
