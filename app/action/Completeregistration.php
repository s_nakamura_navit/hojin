<?php
/**
 *  Completeregistration.php
 */

/**
 *  Completeregistration Form implementation.
 */
class Opensite_Form_Completeregistration extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
      
      'auth'  => array('type' => VAR_TYPE_STRING, 'name' => '認証キー',  'required' => false),
        
    );
    
}

/**
 *  Completeregistration action implementation.
 */
class Opensite_Action_Completeregistration extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
    }

    /**
     *  preprocess of Completeregistration Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Completeregistration prepare()");
        return null;
    }

    /**
     *  Completeregistration action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
          // セッション情報取得
          $id = $this->session->get('id');

          //入力値取得
          $data{'auth'}           = $this->af->get('auth');
          
          // バリデート
          mb_regex_encoding("UTF-8");
          if($data{'auth'}==null || $data{'auth'}==""){
              $this->ae->add("auth", "認証キーが正しくありません", E_FORM_INVALIDVALUE);
          }
          if ($this->af->validate() != 0) {
              //エラー
              $this->af->set('auth_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
          }else{
              // 入力値OK
              //認証キーから該当の仮登録データを取得(有効期限も条件に入れて検索する)
              list($result_count, $result_data) = Opensite_Dao_Search::select_auth_exist($data{'auth'});
              if($result_count != 1){
                  //該当データが正しく取得できなかった場合(有効期限エラーも含む)
                  //エラー
                  $this->af->set('auth_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
              }else{
                  //該当データが正しく取得できた
                  //ログインIDとパスワードを発行
                  //ユニークな値
                  $u_str = ceil(microtime(true));
                  //$login_id = self::CreateUniqStr(5).$u_str;//ログインID
                  $login_id = self::CreateLoginId();//ログインID
                  $passwd = self::CreateUniqStr(5).self::CreateUniqStr(5);
                  $md5_passwd = md5($passwd);
                  //会員ランク(0：無料会員、1：有料会員A、2：有料会員B、3：有料会員C)
                  $rank = 0;

                  //DBへ登録(認証OKフラグもONにする)
                  Opensite_Dao_Search::update_user_regcomplete($data{'auth'}, $login_id ,$md5_passwd, $rank);

                  // DBのポイントテーブルに初期レコード追加
		  $init_point = $GLOBALS['hojin_request_privilege'];      //新規会員登録特典ポイント付与20150330
                  Opensite_Dao_Mypage::init_user_point($result_data[0]['serial'],$init_point);

                  //メール送信
                  //宛先
                  $to = $result_data[0]{'email'}; 
                  //$to = "m.hirai@navit-j.com";
                  //件名
                  $subject="【法人番号検索】会員登録完了のお知らせ";
                  //差出人
                  $fromname="法人番号検索 事務局";
                  $fromaddress="info_opensite@navit-j.net";

                  //本文をファイルから読み込み
                  //ファイル名
                  $reg_mail_file = BASE."/mail/registration.txt";
                  //ファイル読み込み
                  mb_language('Japanese');
                  $reg_mail = file_get_contents($reg_mail_file);
                  $reg_mail = mb_convert_encoding($reg_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');          
                  //読み込んだメール内の変数に値をセット
                  $reg_mail = str_replace('[LOGINID]', $login_id, $reg_mail);
                  $reg_mail = str_replace('[PASSWORD]', $passwd, $reg_mail);
                  $reg_mail = str_replace('[COMPANY_NAME]', $result_data[0]{'company_name'}, $reg_mail);
                  $reg_mail = str_replace('[BRANCH_NAME]', $result_data[0]{'branch_name'}, $reg_mail);
                  $reg_mail = str_replace('[DEPARTMENT_NAME]', $result_data[0]{'department_name'}, $reg_mail);
                  $reg_mail = str_replace('[POST_NAME]', $result_data[0]{'post_name'}, $reg_mail);
                  $reg_mail = str_replace('[CONTRACTOR_NAME]', $result_data[0]{'contractor_lname'}."　".$result_data[0]{'contractor_fname'}, $reg_mail);
                  $reg_mail = str_replace('[ZIP]', $result_data[0]{'zip1'}."-".$result_data[0]{'zip2'}, $reg_mail);
                  $reg_mail = str_replace('[ADDRESS]', $result_data[0]{'address'}, $reg_mail);
                  $reg_mail = str_replace('[PHONE_NO]', $result_data[0]{'phone_no1'}.$result_data[0]{'phone_no2'}.$result_data[0]{'phone_no3'}, $reg_mail);
                  $reg_mail = str_replace('[FAX_NO]', $result_data[0]{'fax_no1'}.$result_data[0]{'fax_no2'}.$result_data[0]{'fax_no3'}, $reg_mail);
                  $reg_mail = str_replace('[EMAIL]', $result_data[0]{'email'}, $reg_mail);
                  
                  $reg_mail = str_replace('[FROM]', $result_data[0]{'come_from'}, $reg_mail);
                  $reg_mail = str_replace('[MAILMAGA]', $result_data[0]{'mailmaga'}, $reg_mail);
                  
                  $reg_mail = str_replace('[FIELD1]', $result_data[0]{'target_field1_str'}, $reg_mail);
                  $reg_mail = str_replace('[FIELD2]', $result_data[0]{'target_field2_str'}, $reg_mail);
                  $reg_mail = str_replace('[BUSINESSFORM]', $result_data[0]{'target_businessform_str'}, $reg_mail);
                  
                  //本文
                  $body = $reg_mail;
                  $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);                
                  if($return_flag) {
                    //メール送信OK
                    $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND OK] TO:<".$to.">");
                    $this->af->set('mail_send_result','1');
                    
                    //webmasterへのメール通知
                    //$to="uemura@navit-j.com";
                    //$to="m.hirai@navit-j.com";
                    $to = array(array('btobmarketing@navit-j.com','送信1'),array('webmaster@navit-j.com','送信2'));
                    $subject="【通知】「法人番号検索」会員登録通知";
                    $reg_mail_file = BASE."/mail/registration_for_master.txt";
                    mb_language('Japanese');
                    $reg_mail = file_get_contents($reg_mail_file);
                    $reg_mail = mb_convert_encoding($reg_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
                    $reg_mail = str_replace('[COMPANY_NAME]', $result_data[0]{'company_name'}, $reg_mail);
                    $reg_mail = str_replace('[BRANCH_NAME]', $result_data[0]{'branch_name'}, $reg_mail);
                    $reg_mail = str_replace('[DEPARTMENT_NAME]', $result_data[0]{'department_name'}, $reg_mail);
                    $reg_mail = str_replace('[POST_NAME]', $result_data[0]{'post_name'}, $reg_mail);
                    $reg_mail = str_replace('[CONTRACTOR_NAME]', $result_data[0]{'contractor_lname'}."　".$result_data[0]{'contractor_fname'}, $reg_mail);
                    $reg_mail = str_replace('[ZIP]', $result_data[0]{'zip1'}."-".$result_data[0]{'zip2'}, $reg_mail);
                    $reg_mail = str_replace('[ADDRESS]', $result_data[0]{'address'}, $reg_mail);
                    $reg_mail = str_replace('[PHONE_NO]', $result_data[0]{'phone_no1'}.$result_data[0]{'phone_no2'}.$result_data[0]{'phone_no3'}, $reg_mail);
                    $reg_mail = str_replace('[FAX_NO]', $result_data[0]{'fax_no1'}.$result_data[0]{'fax_no2'}.$result_data[0]{'fax_no3'}, $reg_mail);
                    $reg_mail = str_replace('[EMAIL]', $result_data[0]{'email'}, $reg_mail);
                    
                    $reg_mail = str_replace('[FROM]', $result_data[0]{'come_from'}, $reg_mail);
                    $reg_mail = str_replace('[MAILMAGA]', $result_data[0]{'mailmaga'}, $reg_mail);
                    
                    $reg_mail = str_replace('[FIELD1]', $result_data[0]{'target_field1_str'}, $reg_mail);
                    $reg_mail = str_replace('[FIELD2]', $result_data[0]{'target_field2_str'}, $reg_mail);
                    $reg_mail = str_replace('[BUSINESSFORM]', $result_data[0]{'target_businessform_str'}, $reg_mail);
                    $body = $reg_mail;
                    $fromname="法人番号検索　システムよりお知らせ";
                    $fromaddress="info_opensite@navit-j.net";
                    $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);
                    
                  }else{
                    //メール送信NG
                    $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND NG] TO:<".$to.">");
                    $this->af->set('mail_send_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
                  }
              }
          }
          $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Completeregistration perform()");
          return 'completeregistration';
        
        
    }
    
    function CreateUniqStr( $len = 8 ){
        // l（小文字のエル）、o（小文字のオウ）、I（大文字のアイ）、O（大文字のオウ）、0（数字のゼロ）、1（数字のイチ）は除外
        $pwdStrings = array( "sletter" => array( 'a','b','c','d','e','f','g','h','i','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z' ),
                             "cletter" => array( 'A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z' ),
                             "number"  => array( '2','3','4','5','6','7','8','9' ),
                            );
        $pwd = array();
        while( count( $pwd ) < $len ){
            // 英(大小)数字と記号を必ず入れる
            if( count( $pwd ) < 4 ){
                $key = key( $pwdStrings );
                next( $pwdStrings );
            // 残りはランダムに取得
            } else {
                $key = array_rand( $pwdStrings );
            }
            $ary_key = array();
            $ary_key[] = $pwdStrings[$key] ;
            $pwd[] = $pwdStrings[$key][array_rand( $pwdStrings[$key]  )];
        }

        // 生成した文字列の順番をランダムに並び替え
        shuffle( $pwd );
        $pwdTxt = implode( $pwd );

        return( $pwdTxt );
    }
    
    function CreateLoginId(){
        //ユニークチェックを繰り返してユニークなるまで繰り返し
        while(1){
            $u_str = ceil(microtime(true));
            $login_id = self::CreateUniqStr(5).$u_str;//ログインID
            list($result_count, $result_data) = Opensite_Dao_Search::select_loginid_exist($login_id);
            if($result_count == 0){
                break;
            }
        }
        return $login_id;
      
    }
  
  
}
?>
