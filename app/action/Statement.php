<?php
/**
 *  Statement.php
 */

/**
 *  Statement Form implementation.
 */
class Opensite_Form_Statement extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_list_name' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
        'in_list_count' => array(
            'type' => VAR_TYPE_INT,
            'required' => true
        ),
        'in_header_check' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_header_line' => array(
            'type' => VAR_TYPE_INT,
            'required' => true
        ),
        'in_up_file_path' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
    );
    
}

/**
 *  Statement action implementation.
 */
class Opensite_Action_Statement extends Opensite_ActionClass
{
//    function authenticate()
//    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
//    }

    /**
     *  preprocess of Statement Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        // フォームバリデーションを実行し、EthnaErrorオブジェクトを登録する
        $this->af->validate();

        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Statement prepare()");
        return null;
    }

    /**
     *  Statement action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // セッション値取得
        $id   = $this->session->get("id");
        $name = $this->session->get("name");
        $user_id = $this->session->get("serial");
        
        // Fax送信マネージャ
        $gm = $this->backend->getManager("geo");

        // 初期化
        $list_name	   = $this->af->get("in_list_name");
        $lv_check	   = $this->af->get("in_lv_check");
        $count['list']	   = $this->af->get("in_list_count");
        $count['matching'] = $this->af->get("in_matching");
        $count['unmatch']  = $this->af->get("in_unmatch");
        $count['mobile']   = $this->af->get("in_unmatch_mobile");
        $count['num_err']  = $this->af->get("in_unmatch_num_err");
        $count['blank']  = $this->af->get("in_unmatch_blank");
        $count['lv_7'] 	   = $this->af->get("in_lv_7_count");
        $count['lv_5'] 	   = $this->af->get("in_lv_5_count");
        $count['lv_3'] 	   = $this->af->get("in_lv_3_count");
        $latlon_type 	   = $this->af->get("in_latlon_type");
        //todo:// getHeaderColumns() でも実装して項目名リストを持ってきて比較する
        
        // 現在のポイント
        $point = Opensite_Dao_Mypage::get_current_point($user_id);

        //測地系名称配列
        $latlon_type_name = array(
                'tky_d' => '日本測地系・度単位',
                'tky_ms' => '日本測地系・ミリ秒単位',
                'tky_dms' => '日本測地系・度分秒形式',
                'wgs_d' => '世界測地系・度単位',
                'wgs_ms' => '世界測地系・ミリ秒単位',
                'wgs_dms' => '世界測地系・度分秒形式'
        );
        $this->af->setApp("latlon_type_name", $latlon_type_name);
            
	//測地系別単価の抽出
	if(strpos($latlon_type,"tky") !== false){
		$common_point = TKY_POINT;	//日本測地系の場合
	}else if(strpos($latlon_type,"wgs") !== false){
		$common_point = WGS_POINT;	//世界測地系の場合
	}
        $this->af->setApp("common_point",		$common_point);

//        if($data['insert'] != "{{テスト}}"){
//            $this->ae->add("insert", "宛先リストにない項目名です", E_Opensite_COMMON);
//        }

        // エラーチェック
/*
var_dump($this->af->get("in_list_name"));
var_dump($this->af->get("in_lv_check"));
var_dump($this->af->get("in_list_count"));
var_dump($this->af->get("in_matching"));
var_dump($this->af->get("in_lv_7_count"));
var_dump($this->af->get("in_lv_5_count"));
var_dump($this->af->get("in_lv_3_count"));
var_dump($this->af->get("in_tel_value_line"));
var_dump($this->af->get("in_header_check"));
var_dump($this->af->get("in_header_line"));
var_dump($this->af->get("in_up_file_path"));
echo $this->ae->count();
exit;
*/
        if ($this->ae->count() > 0) {

            foreach($this->ae->getMessageList() as $item)
            {
                error_log((print_r($item,true)));
            }
            
            //echo print_r($this->af->validate(),true);
            //echo print_r($this->ae->getMessageList(),true);
	    

            $this->af->setApp("list_name", $this->af->get("in_list_name"));
            $this->af->setApp("tel_value_line", $this->af->get("in_tel_value_line"));
            $this->af->setApp("header_check", $this->af->get("in_header_check"));
            $this->af->setApp("header_line", $this->af->get("in_header_line"));
            $this->af->setApp("count", $count);
            $this->af->setApp("point", $point);
            $this->af->setApp("up_file_path", $this->af->get("in_up_file_path"));
            $this->af->setApp("latlon_type", $latlon_type);
            
            //入力値に不備ありのため戻す          
            return 'confirmation';
        }

	// ポイント計算
	//購入件数と必要ポイントの算出

	foreach($lv_check as $val){
		$count['get'] += $count[$val];
		if($val == "lv_7"){
		    $unit_point[$val] = $common_point;
		    $lv_name[$val] = LV7_NAME;
		}else if($val == "lv_5"){
		    $unit_point[$val] = $common_point;
		    $lv_name[$val] = LV5_NAME;
		}else if($val == "lv_3"){
		    $unit_point[$val] = LV3_POINT;
		    $lv_name[$val] = LV3_NAME;
		}
        	// 消費ポイント
		$lv_point[$val] = $count[$val] * $unit_point[$val];
		$ask_point += $lv_point[$val];
	}
	//合計消費ポイント
	$ask_point = floor($ask_point);
	//消費税課税
	$tax_point = floor($ask_point * 0.08);//!!!消費税20150328!!!
	//基本料金
	$bace_point = 10000;//!!!消費税20150328!!!
	//合計ポイント
	$spend = $ask_point + $tax_point + $bace_point;

        // Todo:// ポイント不足はバリデーションに変更？
        // Todo:// ポイントが足りない場合のページ遷移追加

        // ポイントが不足している場合
        if( $point - $spend < 0 )
        {
            $this->af->setApp('lack',	($spend - $point));
            $this->af->setApp('act',	'no_point');
            // Todo:// ここにポイント購入導線追加
            //return 'not_enough';
        }


/*
        // FAX情報登録
        $res = $gm->regist($user_id,$id);

        // 登録エラー
        if (Ethna::isError($res)) {
            $this->af->setApp("title","システムエラー");
            $this->af->setApp("message",$res->getMessage()."(".$res->getCode().")");
            return 'message';
        }
*/
        
        //die();
  
	//値引き継ぎ     
        $this->af->setApp("tel_value_line", 	$this->af->get("in_tel_value_line"));
        $this->af->setApp("header_check",	$this->af->get("in_header_check"));
        $this->af->setApp("header_line",	$this->af->get("in_header_line"));
        $this->af->setApp("up_file_path",	$this->af->get("in_up_file_path"));
        $this->af->setApp("latlon_type", $latlon_type);
 
        // 確認表示用
        $this->af->setApp("list_name",		$list_name);
        $this->af->setApp("lv_check",		$lv_check);
        $this->af->setApp("point",		$point);
        $this->af->setApp("ask_point",		$ask_point);
        $this->af->setApp("count",		$count);
        $this->af->setApp("lv_name",		$lv_name);
        $this->af->setApp("unit_point",		$unit_point);
        $this->af->setApp("lv_point",		$lv_point);
        $this->af->setApp("ask_point",		$ask_point);
        $this->af->setApp("tax_point",		$tax_point);
        $this->af->setApp("spend",		$spend);

        // セッションに値を保存
        $this->session->set("fax_id",		$gm->fax_id);
        $this->session->set("fax_count",	$gm->count);
        $this->session->set("datetime",		$this->af->get("datetimepicker"));
        //die("");
        //$gm->send();
        //$gm->sendFax($gm->fax_id,$gm->datetime);
        //die("");
		
        // selectionバリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Statement perform()");
        return 'statement';
    }
}

?>
