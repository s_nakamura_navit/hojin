<?php
/** action
 *  Trialoneconf.php
*/

/**
 *  Trialoneconf Form implementation.
*/
include('Opensite_Trialone.php');

class Opensite_Form_Trialoneconf extends Opensite_ActionForm
{
	/**
	 *  @access private
	 *  @var    array   form definition.
	 */
	var $form = array(
			'sc_name' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'sc_address' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'corp_id' => array(
					'type' => VAR_TYPE_INT,
					'required' => false
			),
			'in_act' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'req_id' => array(
					'type' => VAR_TYPE_INT,
					'required' => false
			),
			'pref_cd' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'city_cd' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'items' => array(
					'type' => array(VAR_TYPE_INT),
					'required' => false
			),
			'hit_type' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			)
			

	);

}

/**
 *  Trialoneconf action implementation.
 */
class Opensite_Action_Trialoneconf extends Opensite_ActionClass
{
	function authenticate()
	{
		// Indexは認証の必要がないので
		// 親クラスのauthenticate()を無処理でオーバーライド
	}

	/**
	 *  preprocess of index Action.
	 *
	 *  @access public
	 *  @return string    forward name(null: success.
	 *                                false: in case you want to exit.)
	 */
	function prepare()
	{
		return null;
	}

	/**
	 *  index action implementation.
	 *
	 *  @access public
	 *  @return string  forward name.
	 */
	function perform()
	{
//print_r($this->af->get("items"));
//echo "<br>===" . $this->af->get("items");

		//ログインチェック
		//Opensite_Dao_Login::check_user_session();

		// セッション値取得
		$id   = $this->session->get("id");
		$name = $this->session->get("name");
		$user_id = $this->session->get("serial");


		//直接アクセスされた時用のホーム遷移
		 $act = $this->af->get("in_act");
		 if($act != "trialoneconf" && $act != "trialoneconf_m" && $act != "trialoneconf_b"){
			return "index";
		}
		$this->af->setApp("hit_type", $this->af->get("hit_type"));
		$this->af->setApp("items", $this->af->get("items"));

		// 現在のポイント
		$point = array();
		$point['user'] = Opensite_Dao_Mypage::get_current_point($user_id);
		$this->af->setApp("point",		$point);

		$this->af->setApp("corp_id",    $this->af->get("corp_id"));
		//フォーム内容の変数格納
		$corp_id = $this->af->get("corp_id");
		if(isset($corp_id)) {
			$db =& $this->backend->getDB();
			$select_table = Opensite_Trialone::getTrnInspect();

			$pref_code = $this->af->get("pref_cd");
			$this->af->setApp("pref_cd",	$pref_code);
			$city_code = $this->af->get("city_cd");
			$this->af->setApp("city_cd",	$city_code);

//			$table_name = $select_table . "_" . $pref_code;
			$table_name = $select_table;
			//企業詳細取得
			$corp_detail = Opensite_Trialone::corpDetail($table_name,$corp_id, $pref_code);

			$grant_item = array();

			$point_list = $GLOBALS['hojin_request_auto_property_unit_point_list'];
			$ttl_list = $GLOBALS['hojin_request_property_list'];

			$subtotal = 0;
			$use_property = array();
			$get_property_row = array();
			$i=0;
/*
			if ($this->af->get("hit_type") == "corp_name"){
				$grant_item[$i]['ttl'] = "法人番号";
				$grant_item[$i]['point'] = 50;
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "corp_num";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
*/
			if(!empty($this->af->get("items")["corp_num"])) {
				$grant_item[$i]['ttl'] = $ttl_list['corp_num'];
				$grant_item[$i]['point'] = $point_list['corp_num'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "corp_num";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
			
			if(!empty($this->af->get("items")["business"])) {
				$grant_item[$i]['ttl'] = str_replace("（大分類）", "", $ttl_list['business']);
				$grant_item[$i]['point'] = $point_list['business'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "business";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
			if(!empty($this->af->get("items")["fax"])) {
				$grant_item[$i]['ttl'] = $ttl_list['fax'];
				$grant_item[$i]['point'] = $point_list['fax'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "fax";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
			if(!empty($this->af->get("items")["listed_type"])) {
				$grant_item[$i]['ttl'] = $ttl_list['listed_type'];
				$grant_item[$i]['point'] = $point_list['listed_type'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "listed_type";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
			if(!empty($this->af->get("items")["employee_div"])) {
				$grant_item[$i]['ttl'] = $ttl_list['employee_div'];
				$grant_item[$i]['point'] = $point_list['employee_div'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "employee_div";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
			if(!empty($this->af->get("items")["sale_div"])) {
				$grant_item[$i]['ttl'] = $ttl_list['sale_div'];
				$grant_item[$i]['point'] = $point_list['sale_div'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "sale_div";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
			if(!empty($this->af->get("items")["capital_div"])) {
				$grant_item[$i]['ttl'] = $ttl_list['capital_div'];
				$grant_item[$i]['point'] = $point_list['capital_div'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "capital_div";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
			if(!empty($this->af->get("items")["founding_dt"])) {
				$grant_item[$i]['ttl'] = $ttl_list['founding_dt'];
				$grant_item[$i]['point'] = $point_list['founding_dt'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "founding_dt";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
			if(!empty($this->af->get("items")["url"])) {
				$grant_item[$i]['ttl'] = $ttl_list['url'];
				$grant_item[$i]['point'] = $point_list['url'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "url";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
			if(!empty($this->af->get("items")["mailaddr"]) ) {
				$grant_item[$i]['ttl'] = $ttl_list['mailaddr'];
				$grant_item[$i]['point'] = $point_list['mailaddr'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "mailaddr";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
			if(!empty($this->af->get("items")["tel"]) ) {
				$grant_item[$i]['ttl'] = $ttl_list['tel'];
				$grant_item[$i]['point'] = $point_list['tel'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$use_property[] = "tel";
				$get_property_row[] = 1;
				$i = $i + 1;
			}
			//小計
			$this->af->setApp("subtotal",	$subtotal);
			//消費税
			$tax = floor($subtotal * $GLOBALS['hojin_request_tax']);
			$this->af->setApp("tax",	$tax);
			//合計
			$total = $subtotal + $tax;
			$this->af->setApp("total",	$total);

			//履歴用
			$use_property = implode(",", $use_property);
			$get_property_row = implode(",", $get_property_row);

			// ポイントが不足している場合
			if( $point['user'] - $total < 0 )
			{
				$point_lack = $total - $point['user'];
				$this->af->setApp('act',	'no_point');
				$this->af->setApp('point_lack',	$point_lack);
			}

			$this->af->setApp("grant_item",	$grant_item);

			$this->af->setApp("corp_detail",	$corp_detail);

			$this->af->setApp("sc_name",    $this->af->get("sc_name"));
			$this->af->setApp("sc_address",    $this->af->get("sc_address"));


			$select_property = $corp_detail['ID'] . ',' . $pref_code;

			if($act == "trialoneconf") {

				if(!Ethna_Util::isDuplicatePost()) {
					//リクエスト履歴への書き込み
					$prams['user_serial'] = $user_id;
					$prams['user_id'] = $id;
					$prams['list_name'] = $corp_detail['I_CORP'];
					$prams['list_row'] = 1;
					$prams['select_property'] = $select_property;
					$prams['get_property_row'] = $get_property_row;
					$prams['created_estimate_by'] = 'trialone';
					$prams['use_property'] = $use_property;
					$prams['flg_processed'] = 3;
					$prams['created_estimate'] = date("Y-m-d H:i:s");
					$prams['created'] = date("Y-m-d H:i:s");
					$prams['sc_name'] = $this->af->get("sc_name");
					$prams['sc_address'] = $this->af->get("sc_address");

					$res = Opensite_Trialone::insertFurufuruRequest($prams);
					if(Ethna::isError($res))
					{
						$this->af->setApp("title","システムエラー");
						$this->af->setApp("message","依頼情報登録エラー");
						return 'message';
					}
					$req_id = $db->getInsertId();		//直近のID取得
					$this->af->setApp("req_id",   $req_id);

				} else {
					//二重ポストの場合の最新見積ID取得
					$req_id = Opensite_Trialone::getFurufuruRequestEstimateId($user_id,$id);
					$this->af->setApp("req_id",   $req_id);
				}

				$this->af->setApp("page_f",   1);

			} else if($act == "trialoneconf_m") {
				$this->af->setApp("req_id",   $this->af->get("req_id"));

				$this->af->setApp("page_f",   2);
			} else if($act == "trialoneconf_b") {
				$this->af->setApp("req_id",   $this->af->get("req_id"));
				$this->af->setApp("page_f",   1);
			}

		}


		return 'trialoneconf';
	}

}






























?>
