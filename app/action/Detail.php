<?php
/**
 *  Detail.php
 */

/**
 *  Detail Form implementation.
 */
class Opensite_Form_Detail extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        //'in_kind'      => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]種別',                'required' => false,),
        'in_kind_1000'   => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]種別',                'required' => false,),
        'in_kind_2000'   => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]種別',                'required' => false,),
        'in_kind_3000'   => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]種別',                'required' => false,),
      
        'in_area1'       => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]エリア(都道府県)',      'required' => false,),
        'in_area2'       => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]エリア(市区町村)',      'required' => false,),
        'in_field_90'    => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
        'in_field_91'    => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
        'in_field_92'    => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
        'in_field_93'    => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
        'in_field_94'    => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
        'in_field_95'    => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
        'in_field_96'    => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
        'in_field_97'    => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
        'in_field_98'    => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
        'in_field_99'    => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
        'in_field_100'   => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
        'in_field_101'   => array('type' => VAR_TYPE_STRING, 'name' => '[自治体案件検索]分野',                'required' => false,),
      
        'in_field2_1'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]分野',                'required' => false,),
        'in_field2_2'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]分野',                'required' => false,),
        'in_field2_3'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]分野',                'required' => false,),
        'in_field2_4'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]分野',                'required' => false,),
        'in_field2_5'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]分野',                'required' => false,),
        'in_field2_6'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]分野',                'required' => false,),
        'in_field2_7'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]分野',                'required' => false,),
        'in_field2_8'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]分野',                'required' => false,),
        'in_field2_9'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]分野',                'required' => false,),
        'in_field2_10'   => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]分野',                'required' => false,),
        'in_field2_11'   => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]分野',                'required' => false,),

        'in_field3_1'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_2'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_3'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_4'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_5'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_6'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_7'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_8'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_9'    => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_10'   => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_11'   => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_12'   => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_13'   => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_14'   => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_field3_15'   => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]形態',                'required' => false,),
        'in_keyword'     => array('type' => VAR_TYPE_STRING, 'name' => '[財団案件検索]キーワード',     'required' => false,),
      
        'in_callkind'          => array('type' => VAR_TYPE_STRING, 'name' => '検索種別',                         'required' => false,),
        'in_contents_id'    => array('type' => VAR_TYPE_STRING, 'name' => 'コンテンツID',                  'required' => false,),
    );
}

/**
 *  Detail action implementation.
 */
class Opensite_Action_Detail extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
        return parent::authenticate();
    }

    /**
     *  preprocess of Searchresult Action.
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] view Detail");
        return null;
    }

    /**
     *  Searchresult action implementation.
     */
    function perform()
    {
        // セッション情報取得
        $id = $this->session->get('id');

        //フォーム入力値
        //$data{"kind"}    = $this->af->get("in_kind");
        $data{"kind_1000"}  = $this->af->get("in_kind_1000");
        $data{"kind_2000"}  = $this->af->get("in_kind_2000");
        $data{"kind_3000"}  = $this->af->get("in_kind_3000");
        $data{"area1"}     = $this->af->get("in_area1");
        $data{"area2"}     = $this->af->get("in_area2");
        $data{"field_90"}  = $this->af->get("in_field_90");
        $data{"field_91"}  = $this->af->get("in_field_91");
        $data{"field_92"}  = $this->af->get("in_field_92");
        $data{"field_93"}  = $this->af->get("in_field_93");
        $data{"field_94"}  = $this->af->get("in_field_94");
        $data{"field_95"}  = $this->af->get("in_field_95");
        $data{"field_96"}  = $this->af->get("in_field_96");
        $data{"field_97"}  = $this->af->get("in_field_97");
        $data{"field_98"}  = $this->af->get("in_field_98");
        $data{"field_99"}  = $this->af->get("in_field_99");
        $data{"field_100"} = $this->af->get("in_field_100");
        $data{"field_101"} = $this->af->get("in_field_101");
        $data{"field2_1"}  = $this->af->get("in_field2_1");
        $data{"field2_2"}  = $this->af->get("in_field2_2");
        $data{"field2_3"}  = $this->af->get("in_field2_3");
        $data{"field2_4"}  = $this->af->get("in_field2_4");
        $data{"field2_5"}  = $this->af->get("in_field2_5");
        $data{"field2_6"}  = $this->af->get("in_field2_6");
        $data{"field2_7"}  = $this->af->get("in_field2_7");
        $data{"field2_8"}  = $this->af->get("in_field2_8");
        $data{"field2_9"}  = $this->af->get("in_field2_9");
        $data{"field2_10"} = $this->af->get("in_field2_10");
        $data{"field2_11"} = $this->af->get("in_field2_11");
        $data{"field3_1"}  = $this->af->get("in_field3_1");
        $data{"field3_2"}  = $this->af->get("in_field3_2");
        $data{"field3_3"}  = $this->af->get("in_field3_3");
        $data{"field3_4"}  = $this->af->get("in_field3_4");
        $data{"field3_5"}  = $this->af->get("in_field3_5");
        $data{"field3_6"}  = $this->af->get("in_field3_6");
        $data{"field3_7"}  = $this->af->get("in_field3_7");
        $data{"field3_8"}  = $this->af->get("in_field3_8");
        $data{"field3_9"}  = $this->af->get("in_field3_9");
        $data{"field3_10"} = $this->af->get("in_field3_10");
        $data{"field3_11"} = $this->af->get("in_field3_11");
        $data{"field3_12"} = $this->af->get("in_field3_12");
        $data{"field3_13"} = $this->af->get("in_field3_13");
        $data{"field3_14"} = $this->af->get("in_field3_14");
        $data{"field3_15"} = $this->af->get("in_field3_15");
        $data{"keyword"}   = $this->af->get("in_keyword");
        $data{"callkind"}  = $this->af->get("in_callkind");
        $data{"contents_id"}  = $this->af->get("in_contents_id");

        // 前後スペース除去
        $data{"keyword"} = trim($data{"keyword"});
        // 禁則文字変換
        $data{"keyword"} = $this->replace_prohibition($data{"keyword"});
        // 禁則文字除去
        $data{"keyword"} = $this->delete_prohibition($data{"keyword"});
       
        // チェックボックス系をcsvに(データ持ち回りのため)
        $kind_ar = array();
        $data{"kind"} = "";
        if($data{"kind_1000"} != "") {array_push($kind_ar, $data{"kind_1000"});}
        if($data{"kind_2000"} != "") {array_push($kind_ar, $data{"kind_2000"});}
        if($data{"kind_3000"} != "") {array_push($kind_ar, $data{"kind_3000"});}
        //カンマ区切りで連結
        if(count($kind_ar)>0) {$data{"kind"} = implode(',', $kind_ar);}
        
        $field_ar = array();
        $data{"field"} = "";
        if($data{"field_90"} != "") {array_push($field_ar, $data{"field_90"});}
        if($data{"field_91"} != "") {array_push($field_ar, $data{"field_91"});}
        if($data{"field_92"} != "") {array_push($field_ar, $data{"field_92"});}
        if($data{"field_93"} != "") {array_push($field_ar, $data{"field_93"});}
        if($data{"field_94"} != "") {array_push($field_ar, $data{"field_94"});}
        if($data{"field_95"} != "") {array_push($field_ar, $data{"field_95"});}
        if($data{"field_96"} != "") {array_push($field_ar, $data{"field_96"});}
        if($data{"field_97"} != "") {array_push($field_ar, $data{"field_97"});}
        if($data{"field_98"} != "") {array_push($field_ar, $data{"field_98"});}
        if($data{"field_99"} != "") {array_push($field_ar, $data{"field_99"});}
        if($data{"field_100"} != "") {array_push($field_ar, $data{"field_100"});}
        if($data{"field_101"} != "") {array_push($field_ar, $data{"field_101"});}
        //カンマ区切りで連結
        if(count($field_ar)>0) {$data{"field"} = implode(',', $field_ar);}

        $field2_ar = array();
        $data{"field2"} = "";
        if($data{"field2_1"} != "") {array_push($field2_ar, $data{"field2_1"});}
        if($data{"field2_2"} != "") {array_push($field2_ar, $data{"field2_2"});}
        if($data{"field2_3"} != "") {array_push($field2_ar, $data{"field2_3"});}
        if($data{"field2_4"} != "") {array_push($field2_ar, $data{"field2_4"});}
        if($data{"field2_5"} != "") {array_push($field2_ar, $data{"field2_5"});}
        if($data{"field2_6"} != "") {array_push($field2_ar, $data{"field2_6"});}
        if($data{"field2_7"} != "") {array_push($field2_ar, $data{"field2_7"});}
        if($data{"field2_8"} != "") {array_push($field2_ar, $data{"field2_8"});}
        if($data{"field2_9"} != "") {array_push($field2_ar, $data{"field2_9"});}
        if($data{"field2_10"} != "") {array_push($field2_ar, $data{"field2_10"});}
        if($data{"field2_11"} != "") {array_push($field2_ar, $data{"field2_11"});}
        //カンマ区切りで連結
        if(count($field2_ar)>0) {$data{"field2"} = implode(',', $field2_ar);}
        
        $field3_ar = array();
        $data{"field3"} = "";
        if($data{"field3_1"} != "") {array_push($field3_ar, $data{"field3_1"});}
        if($data{"field3_2"} != "") {array_push($field3_ar, $data{"field3_2"});}
        if($data{"field3_3"} != "") {array_push($field3_ar, $data{"field3_3"});}
        if($data{"field3_4"} != "") {array_push($field3_ar, $data{"field3_4"});}
        if($data{"field3_5"} != "") {array_push($field3_ar, $data{"field3_5"});}
        if($data{"field3_6"} != "") {array_push($field3_ar, $data{"field3_6"});}
        if($data{"field3_7"} != "") {array_push($field3_ar, $data{"field3_7"});}
        if($data{"field3_8"} != "") {array_push($field3_ar, $data{"field3_8"});}
        if($data{"field3_9"} != "") {array_push($field3_ar, $data{"field3_9"});}
        if($data{"field3_10"} != "") {array_push($field3_ar, $data{"field3_10"});}
        if($data{"field3_11"} != "") {array_push($field3_ar, $data{"field3_11"});}
        if($data{"field3_12"} != "") {array_push($field3_ar, $data{"field3_12"});}
        if($data{"field3_13"} != "") {array_push($field3_ar, $data{"field3_13"});}
        if($data{"field3_14"} != "") {array_push($field3_ar, $data{"field3_14"});}
        if($data{"field3_15"} != "") {array_push($field3_ar, $data{"field3_15"});}
        //カンマ区切りで連結
        if(count($field3_ar)>0) {$data{"field3"} = implode(',', $field3_ar);}
        
        if($_GET["back"] != "1"){
            // バリデート
            if ($this->af->validate() != 0) {
                return 'index';
            }
        }

        return 'detail';
    }

    function replace_prohibition($str)
    {
        $str = Opensite_Sanitization::replace_prohibition($str, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1);
        return $str;
    }

    function delete_prohibition($str)
    {
        $str = Opensite_Sanitization::replace_prohibition($str, "", "", "", "", "", 0, "", "", "", "");
        return $str;
    }
}
