<?php
/** action
 *  Trialonecomp.php
*/

/**
 *  Trialonecomp Form implementation.
*/
include('Opensite_Trialone.php');

class Opensite_Form_Trialonecomp extends Opensite_ActionForm
{
	/**
	 *  @access private
	 *  @var    array   form definition.
	 */
	var $form = array(
			'in_act' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'sc_name' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'sc_address' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'corp_id' => array(
					'type' => VAR_TYPE_INT,
					'required' => false
			),
			'req_id' => array(
					'type' => VAR_TYPE_INT,
					'required' => false
			),
			'pref_cd' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'city_cd' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'items' => array(
					'type' => array(VAR_TYPE_INT),
					'required' => false
			),
			'hit_type' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			)
	);

}

/**
 *  Trialoneconf action implementation.
 */
class Opensite_Action_Trialonecomp extends Opensite_ActionClass
{
	function authenticate()
	{
		// Indexは認証の必要がないので
		// 親クラスのauthenticate()を無処理でオーバーライド
	}

	/**
	 *  preprocess of index Action.
	 *
	 *  @access public
	 *  @return string    forward name(null: success.
	 *                                false: in case you want to exit.)
	 */
	function prepare()
	{
		return null;
	}

	/**
	 *  index action implementation.
	 *
	 *  @access public
	 *  @return string  forward name.
	 */
	function perform()
	{

		//ログインチェック
		Opensite_Dao_Login::check_user_session();

		// セッション値取得
		$id   = $this->session->get("id");
		$name = $this->session->get("name");
		$user_id = $this->session->get("serial");

		//直接アクセスされた時用のホーム遷移
		 $act = $this->af->get("in_act");
		 if($act != "trialonecomp"){
			return "index";
		}

		$this->af->setApp("corp_id",    $this->af->get("corp_id"));
		//フォーム内容の変数格納
		$corp_id = $this->af->get("corp_id");
		if(!empty($corp_id)) {
			$db =& $this->backend->getDB();
			$select_table = Opensite_Trialone::getTrnInspect();

			$pref_code = $this->af->get("pref_cd");
			$this->af->setApp("pref_cd",	$pref_code);
			$city_code = $this->af->get("city_cd");
			$this->af->setApp("city_cd",	$city_code);
			
			$items = $this->af->get("items");

//			$table_name = $select_table . "_" . $pref_code;
			$table_name = $select_table;

			//企業情報取得
			$corp_detail = Opensite_Trialone::getCorpDetailAoc($table_name,$corp_id);

			$grant_item = array();

			$point_list = $GLOBALS['hojin_request_auto_property_unit_point_list'];
			$ttl_list = $GLOBALS['hojin_request_property_list'];

			$subtotal = 0;
			$i=0;
			//ここから共通情報
			if(!empty($corp_detail['I_CORP'])) {
				$grant_item[$i]['ttl'] = '掲載名';
				$grant_item[$i]['name'] = $corp_detail['I_CORP'];
				$i = $i + 1;
			}
			if(!empty($corp_detail['I_ZIP_1']) && !empty($corp_detail['I_ZIP_2'])) {
				$grant_item[$i]['ttl'] = '郵便番号';
				$grant_item[$i]['name'] = $corp_detail['I_ZIP_1'] . '-' . $corp_detail['I_ZIP_2'];
				$i = $i + 1;
			}
			if(!empty($corp_detail['I_ADDR_ALL'])) {
				$grant_item[$i]['ttl'] = '住所';
				$grant_item[$i]['name'] = $corp_detail['I_ADDR_ALL'];
				$i = $i + 1;
			}
			//ここから付与情報
			if(!empty($items["business"]) ) {
				$grant_item[$i]['ttl'] = $ttl_list['business'];
				$grant_item[$i]['name'] = $corp_detail['BUSINESS_CD_L_NAME'];
				$grant_item[$i]['point'] = $point_list['business'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;

				$grant_item[$i]['ttl'] = $ttl_list['business_m'];
				$grant_item[$i]['name'] = $corp_detail['BUSINESS_CD_M_NAME'];
				$i = $i + 1;

				$grant_item[$i]['ttl'] = $ttl_list['business_s'];
				$s = "";
				for($x=1; $x<=10; $x++){
					if ($s && $corp_detail['BUSINESS_CD_S_NAME_' . $x]){
						$s .= "、" . $corp_detail['BUSINESS_CD_S_NAME_' . $x];
					}else if ($corp_detail['BUSINESS_CD_S_NAME_' . $x]){
						$s = $corp_detail['BUSINESS_CD_S_NAME_' . $x];
					}
				}
				$grant_item[$i]['name'] = $s;
				$i = $i + 1;
			}
			if(!empty($items["fax"])) {
				$grant_item[$i]['ttl'] = $ttl_list['fax'];
				$grant_item[$i]['name'] = $corp_detail['I_FAX'];
				$grant_item[$i]['point'] = $point_list['fax'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
			if(!empty($items["listed_type"])) {
				$grant_item[$i]['ttl'] = $ttl_list['listed_type'];
				$grant_item[$i]['name'] = $corp_detail['LISTED_TYPE_NAME'];
				$grant_item[$i]['point'] = $point_list['listed_type'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
			if(!empty($items["employee_div"])) {
				$grant_item[$i]['ttl'] = $ttl_list['employee_div'];
				$grant_item[$i]['name'] = $corp_detail['EMPLOYEE_DIV_NAME'];
				$grant_item[$i]['point'] = $point_list['employee_div'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
			if(!empty($items["sale_div"]) ) {
				$grant_item[$i]['ttl'] = $ttl_list['sale_div'];
				$grant_item[$i]['name'] = $corp_detail['SALE_DIV_NAME'];
				$grant_item[$i]['point'] = $point_list['sale_div'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
			if(!empty($items["capital_div"]) ) {
				$grant_item[$i]['ttl'] = $ttl_list['capital_div'];
				$grant_item[$i]['name'] = $corp_detail['CAPITAL_DIV_NAME'];
				$grant_item[$i]['point'] = $point_list['capital_div'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
			if(!empty($items["founding_dt"])) {
				$grant_item[$i]['ttl'] = $ttl_list['founding_dt'];
				$yyyymm = str_split($corp_detail['I_FOUNDING_DT'], 4);
				$grant_item[$i]['name'] = $yyyymm[0] . '年' . ltrim($yyyymm[1],'0') . '月';
				$grant_item[$i]['point'] = $point_list['founding_dt'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
/*
			if(!empty($corp_detail['I_DEPT_POSITION'])) {
				$grant_item[$i]['ttl'] = $ttl_list['dept_position'];
				$grant_item[$i]['name'] = $corp_detail['I_DEPT_POSITION'];
				$grant_item[$i]['point'] = $point_list['dept_position'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
			if(!empty($corp_detail['I_DEPT_NAME'])) {
				$grant_item[$i]['ttl'] = $ttl_list['dept_name'];
				$grant_item[$i]['name'] = $corp_detail['I_DEPT_NAME'];
				$grant_item[$i]['point'] = $point_list['dept_name'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
*/			
			if(!empty($items["url"])) {
				$grant_item[$i]['ttl'] = $ttl_list['url'];
				$grant_item[$i]['name'] = $corp_detail['I_URL'];
				$grant_item[$i]['point'] = $point_list['url'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
			if(!empty($items["corp_num"]) ) {
				$grant_item[$i]['ttl'] = $ttl_list['corp_num'];
				$grant_item[$i]['name'] = $corp_detail['I_CORP_NUM'];
				$grant_item[$i]['point'] = $point_list['corp_num'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
			if(!empty($items["mailaddr"]) ) {
				$grant_item[$i]['ttl'] = $ttl_list['mailaddr'];
				$grant_item[$i]['name'] = $corp_detail['I_MAILADDR'];
				$grant_item[$i]['point'] = $point_list['mailaddr'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
			if(!empty($items["tel"]) ) {
				$grant_item[$i]['ttl'] = $ttl_list['tel'];
				$grant_item[$i]['name'] = $corp_detail['I_TEL'];
				$grant_item[$i]['point'] = $point_list['tel'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}

			$tax = floor($subtotal * $GLOBALS['hojin_request_tax']);		//消費税（小数点以下切捨て）
			//合計
			$total = $subtotal + $tax;
			$this->af->setApp("total",	$total);

			// 現在のポイント
			$user_point_br = Opensite_Dao_Mypage::get_current_point($user_id);

			if($user_point_br > $total) {

				//二重POST防止
				if(!Ethna_Util::isDuplicatePost()) {
					//ポイント減算
					$user_point_aft = $user_point_br - $total;
					$params['user_point_aft'] = $user_point_aft;
					$params['user_serial'] = $user_id;

					$params['user_point_br'] = $user_point_br;
					$params['total'] = $total;

					$res = Opensite_Trialone::updateUserPointSubtraction($params);
					if(Ethna::isError($res))
					{
						$this->af->setApp("title","システムエラー");
						$this->af->setApp("message","ポイント処理エラー");
						return 'message';
					}


					$this->af->setApp("user_point_br",		$user_point_br);
					$this->af->setApp("total",		$total);
					$this->af->setApp("user_point_aft",		$user_point_aft);
					$this->af->setApp("user_point_nowdb",		Opensite_Dao_Mypage::get_current_point($user_id));

					//ユーザ側履歴を購入に変更
					$req_id = $this->af->get("req_id");
					$res = Opensite_Trialone::updateFurufuruRequestAoc($req_id);
					if(Ethna::isError($res))
					{
						$this->af->setApp("title","システムエラー");
						$this->af->setApp("message","依頼更新処理エラー");
						return 'message';
					}

					//管理側のポイント利用履歴に書き込み
					$params['user_serial'] = $user_id;
					$params['user_id'] = $id;
					$params['process_id'] = $req_id;
					$params['use_point'] = $total;
					$params['created_by'] = $user_id;
					$res = Opensite_Trialone::insertMstUsePointLogs($params);
					if(Ethna::isError($res))
					{
						$this->af->setApp("title","システムエラー");
						$this->af->setApp("message","利用履歴更新処理エラー");
						return 'message';
					}
//
//		FAX番号、購入・レンタル時にwebmasterあてにメール配信されるよう以下追加　2017-01-16 中村
//
//echo "++++++++" . $user_id;;
		$user_info = Opensite_Dao_Mypage::get_user_info_by_id($user_id);
		$to = array(array('btobdownload@navit-j.com','送信1'),array('webmaster@navit-j.com','送信2'));
//		$to = array(array('s.nakamura@navit-j.com','送信1'));
		//件名
		$subject="【通知】 法人番号検索 依頼メール";
		//ファイル名
		$tmp_faxlist_mail_file = BASE."/mail/request_one_for_master.txt";
		//ファイル読み込み
		mb_language('Japanese');
		$tmp_faxlist_mail = file_get_contents($tmp_faxlist_mail_file);
		$tmp_faxlist_mail = mb_convert_encoding($tmp_faxlist_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
		//読み込んだメール内の変数に値をセット
		$tmp_faxlist_mail = str_replace('[USER_ID]', $id, $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[COMPANY_NAME]', $user_info['company_name'], $tmp_faxlist_mail);
		if($user_info['post_name'] == ""){
		    $tmp_faxlist_mail = str_replace('[POST_NAME]', 'なし', $tmp_faxlist_mail);
		}else{
		    $tmp_faxlist_mail = str_replace('[POST_NAME]', $user_info['post_name'], $tmp_faxlist_mail);
		}
		$tmp_faxlist_mail = str_replace('[DEPARTMENT_NAME]', $user_info['department_name'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[CONTRACTOR_NAME]', $user_info['contractor_lname']."　".$user_info['contractor_fname'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[ZIP]', $user_info['zip1']."-".$user_info['zip2'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[ADDRESS]', $user_info['address'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[PHONE_NO]', $user_info['phone_no1']."-".$user_info['phone_no2']."-".$user_info['phone_no3'], $tmp_faxlist_mail);
		if($user_info['fax_no1'] == "" || $user_info['fax_no2'] == "" || $user_info['fax_no3'] == ""){
		    $tmp_faxlist_mail = str_replace('[FAX_NO]', 'なし', $tmp_faxlist_mail);
		}else{
		    $tmp_faxlist_mail = str_replace('[FAX_NO]', $user_info['fax_no1']."-".$user_info['fax_no2']."-".$user_info['fax_no3'], $tmp_faxlist_mail);
		}
		$tmp_faxlist_mail = str_replace('[EMAIL]', $user_info['email'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[ID]', $req_id, $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[LIST]', $corp_detail["I_CORP"], $tmp_faxlist_mail);
		$body = $tmp_faxlist_mail;
		$fromname="法人番号検索　管理システムよりお知らせ";
		$fromaddress="info_opensite@navit-j.net";
		$return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);
		//
		//	以下ユーザー宛
		//
		//件名
		$subject="【法人番号検索】依頼メール";
		//ファイル名
		$tmp_faxlist_mail_file = BASE."/mail/request_one.txt";
		//ファイル読み込み
		mb_language('Japanese');
		$tmp_faxlist_mail = file_get_contents($tmp_faxlist_mail_file);
		$tmp_faxlist_mail = mb_convert_encoding($tmp_faxlist_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
		//読み込んだメール内の変数に値をセット
		$tmp_faxlist_mail = str_replace('[COMPANY_NAME]', $user_info['company_name'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[CONTRACTOR_NAME]', $user_info['contractor_lname']."　".$user_info['contractor_fname'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[ID]', $req_id, $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[LIST]', $corp_detail["I_CORP"], $tmp_faxlist_mail);
		
		$body = $tmp_faxlist_mail;
		$fromname="法人番号検索事務局";
		$fromaddress="info_opensite@navit-j.net";
		$return_flag = Opensite_Mail::mailsender($user_info['email'],$subject,$body,$fromname,$fromaddress);
//
//	以上まで
//
				}

				//付与情報をセット
				$this->af->setApp("grant_item",	$grant_item);

			}

			$this->af->setApp("sc_name",    $this->af->get("sc_name"));
			$this->af->setApp("sc_address",    $this->af->get("sc_address"));

		}

		return 'trialonecomp';
	}

}






























?>
