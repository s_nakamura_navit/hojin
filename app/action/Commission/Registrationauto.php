<?php
/**
 *  CommissionRegistrationauto.php
 */

/**
 *  CommissionRegistrationauto Form implementation.
 */
class Opensite_Form_CommissionRegistrationauto extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_up_list' => array(
            'type' => VAR_TYPE_FILE,
            'form_type'   => FORM_TYPE_FILE,
            'required' => false
        ),
        'in_header_check' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_header_line' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_list_name' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_up_file_path' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_list_count' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_act' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_property_check' => array(
            'type' => array(VAR_TYPE_STRING),
            'required' => false,
        ),
        'in_name_line_num' => array(
            'type' => VAR_TYPE_INT,
            'required' => false,
        ),
        'in_zipcode_line_num' => array(
            'type' => VAR_TYPE_INT,
            'required' => false,
        ),
        'in_address_line_num' => array(
            'type' => VAR_TYPE_INT,
            'required' => false,
        ),
        'in_tel_line_num' => array(
            'type' => VAR_TYPE_INT,
            'required' => false,
        ),
        'in_I_URL_line_num' => array(
            'type' => VAR_TYPE_INT,
            'required' => false,
        ),
        'in_I_MAILADDR_line_num' => array(
            'type' => VAR_TYPE_INT,
            'required' => false,
        ),
        'in_request_id' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_total_point' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_use_matching_phase' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_column_num_str' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
    );

}

/**
 *  CommissionRegistrationauto action implementation.
 */
class Opensite_Action_CommissionRegistrationauto extends Opensite_ActionClass
{
//    function authenticate()
//    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
//    }

	/**
	 *  preprocess of CommissionRegistrationauto Action.
	 *
	 *  @access public
	 *  @return string    forward name(null: success.
	 *                                false: in case you want to exit.)
	 */
	function prepare()
	{
		// フォームバリデーションを実行し、EthnaErrorオブジェクトを登録する
		$this->af->validate();

		$this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_CommissionRegistrationauto prepare()");
		return null;
	}

	/**
	 *  CommissionRegistrationauto action implementation.
	 *
	 *  @access public
	 *  @return string  forward name.
	 */
	function perform()
	{
		//ログインチェック
		Opensite_Dao_Login::check_user_session();

		// セッション値取得
		$id   = $this->session->get("id");
		$name = $this->session->get("name");
		$user_id = $this->session->get("serial");
		$user_type = $this->session->get("flg_admin");

        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();

		//購入時の二重処理防止フラグ　必ずトップで初期化
		$this->session->set("re_send_check",        "");

		// サービスマネージャ
		$fm = $this->backend->getManager("furu");

		$err_count = $this->ae->count();

		//フェーズチェック
		$act = $this->af->get("in_act");

		// 初期化
		$result = 0;
		$up_file = $this->af->get("in_up_list");

		// 現在のポイント
		$point = array();
		$point['user'] = Opensite_Dao_Mypage::get_current_point($user_id);

		//付与項目
		$this->af->setApp("property_list", 	$GLOBALS['hojin_request_property_list']);
		$column_names = $GLOBALS['num_input_column_list'];

		//キャンセル時の処理
		if($act == "do_cancel"){
			$res = Opensite_Dao_Furu::update_hojin_request($this->af->get("in_request_id"),"","",9);
			if(Ethna::isError($res))
			{
				$this->af->setApp("title","システムエラー");
				$this->af->setApp("message","キャンセル履歴処理エラー");
				return 'message';
			}

			header("location:/service/furufuru/index.php");
		}

		//戻りでファイルチェックは行わない
		if($act != "back"){

			$property_check = $this->af->get("in_property_check");
			if(count($property_check) != 0){
				//項目名とキーをカンマ区切りで格納
				foreach($property_check as $key){
					if($property_key == ""){
						$property_key = $key;
						$property_name = $GLOBALS['hojin_request_property_list'][$key];
					}else{
						$property_key .= "," . $key;
						$property_name .= "," . $GLOBALS['hojin_request_property_list'][$key];
					}
				}
				$this->af->setApp("property_key", 	$property_key);
				$this->af->setApp("property_name", 	$property_name);
			}else{
				$err_count++;
				$this->af->setApp("property_check_err" , "付与する属性項目を選択してください。");
			}

			if($up_file['error'] != 4){
				//営業部アカウントでアクセス時のみ、指定のリスト以外受け付けない
				if($user_type == 2){
					if($up_file['name'] != "ふるふる_sample_500.csv" || $up_file['size'] != 50831){
						$err_count++;
						$this->af->setApp("up_list_err" , "デモモードでは、指定のファイルを使用してください。");
					}
					//営業部アカウントでドメインマッチは行われない(ドメインマッチの不安材料対策20151201現在)
					//新規用意されたファイルは上記要素がないため、処理をコメントアウトする(20151209)
					//unset($column_names['I_URL']);
					//unset($column_names['I_MAILADDR']);
				}
				$file = $fm->upload($this->af->get("in_up_list"));
				if($file == null){
					$result = Ethna::raiseNotice('ファイルのアップロードに失敗しました。', E_FURU_UPLOAD_LIST_NOT_EXIST);
				}else if(pathinfo($file['new_tmp_path'], PATHINFO_EXTENSION) != "csv" && pathinfo($file['new_tmp_path'], PATHINFO_EXTENSION) != "CSV"){
					$err_count++;
					$this->af->setApp("up_list_err" , "CSVファイルを選択して下さい。");
				}else{
					$up_file_path = $file['new_tmp_path'];
				}
			}else{
				$err_count++;
				$this->af->setApp("up_list_err" , "読み込みファイルを選択して下さい。");
			}

			$header_check = $this->af->get("in_header_check");

			if($header_check == "on"){
				$header_line = mb_convert_kana($this->af->get("in_header_line"),n,'UTF-8');
				if($header_line == ""){
					$err_count++;
					$this->af->setApp("header_line_err", "ヘッダー行数には数字(整数)を入力してください。");
				}else{
					if(!preg_match("/^[0-9]+$/",$header_line)){
						$err_count++;
						$header_line = "1";
						$this->af->setApp("header_line_err", "ヘッダー行数には数字(整数)を入力してください。");
					}
				}
			}else{
				$header_line = 0;
			}

			foreach($column_names as $key => $name){
				$column_num[$key] = mb_convert_kana($this->af->get("in_".$key."_line_num"),n,'UTF-8');
				if($column_num[$key] == "" || $column_num[$key] == 0){
					if($key == "name" || $key == "address" || $key == "tel"){
						$err_count++;
						$this->af->setApp($key."_line_num_err", $name."の列番号は必須入力になります。");
					}
				}else{
					if(!preg_match("/^[0-9]+$/",$column_num[$key])){
						$err_count++;
						$this->af->setApp($key."_line_num_err", $name."の列番号には数字(整数)を入力してください。");
					}
				}
			}
		}

		$logger->log(LOG_DEBUG, "通過1  ***");

		//引き継ぎ用共通
		$this->af->setApp("header_check",	$header_check);
		$this->af->setApp("header_line",	$header_line);
		$this->af->setApp("property_check",	$property_check);
		$this->af->setApp("auto_check",         "on");
		foreach($column_names as $key => $name){
			if($column_num_str == ""){
				$column_num_str = $column_num[$key];
			}else{
				$column_num_str .= ",".$column_num[$key];
			}
			$this->af->setApp($key."_line_num",     $column_num[$key]);
		}
		$this->af->setApp("column_num_str",	$column_num_str);

		
		$logger->log(LOG_DEBUG, "通過2  ***");

		// 光通信bizcompass用
		if(preg_match("/^HT[0-9]{10}$/", $id)) {
			$this->af->setApp("is_HTflg", true);
		} else {
			$this->af->setApp("is_HTflg", false);
		}

		//todo:// getHeaderColumns() でも実装して項目名リストを持ってきて比較する

//        if($data['insert'] != "{{テスト}}"){
//            $this->ae->add("insert", "宛先リストにない項目名です", E_Opensite_COMMON);
//        }

		// エラーチェック
		if ($err_count > 0) {

			foreach($this->ae->getMessageList() as $item)
			{
				error_log((print_r($item,true)));
			}

			//入力値に不備ありのため戻す

			//アップロードしたファイルを消す
			if($file['new_tmp_path'] != ""){
				$cmd = "rm ".BASE."/".$file['new_tmp_path'];
				system($cmd);
			}
			foreach($property_check as $key){
				$property_checked[$key] = "checked";
			}
			$this->af->setApp("property_checked",	$property_checked);
			$this->af->setApp("act", 			"back");
			return 'index';
		}

		// 登録エラー

		if (Ethna::isError($result)) {
			// アップロードエラー
			if($result->getCode() === E_FAX_UPLOAD_LIST_NOT_EXIST || $result->getCode() === E_FAX_UPLOAD_ARTICLE_NOT_EXIST)
			{
				//$this->ae->addObject('in_up_list', $result);
				$this->af->setApp("title","システムエラー");
				$this->af->setApp("message","ファイルのアップロードに失敗しました。");
				return 'message';
			}
		}

		//初期化
		$count = array();

		if($act != "back"){

			//処理時起案計測(デバック)
			$start = microtime(true);

			//件数出し
			$value_array = $fm->getListValue($up_file_path,$header_check,$header_line);
			$logger->log(LOG_DEBUG, "ret  value_array: ".$value_array);
			if(is_array($value_array) == false) {
			    if($value_array == -1) {
					$this->af->setApp("title","システムエラー");
					$this->af->setApp("message","入力ファイルにエラーがありました。");
					return 'message';
			    }
			}


			//リストの総件数
			$count['list'] = count($value_array);
/*
	    //特定管理者は件数制限を受けない
            if(($count['list'] > 0 && $count['list'] < 2001) || ($this->session->get("authkey") == "de0c2a9be052d0edd5f5f11e92eee411" && $user_id == 1))
            {
            }else{
*/
			if($count['list'] == 0){
				//ファイルに不備ありのため戻す
				$this->af->setApp("act", "back");
				$this->af->setApp("up_list_err" , "CSVファイル内に有効な件数がありません。");
				return 'index';
/*
		}else if($count['list'] > 2000){
			//ファイルに不備ありのため戻す
			$this->af->setApp("act", "back");
			$this->af->setApp("up_list_err" , "依頼件数は2,000件以下でお願いします。");
			$this->af->setApp("row_err" , true);
            		return 'index';

			}else if($count['list'] < 100){
				//ファイルに不備ありのため戻す
				$this->af->setApp("act", "back");
				$this->af->setApp("up_list_err" , "依頼件数は100件以上でお願いします。");
				return 'index';
*/
/*
			}else if($count['list'] > 10000){
				if($user_type == 1){
					if($count['list'] > 20000){
						//社内ユーザは2万件まで
						$this->af->setApp("act", "back");
						$this->af->setApp("up_list_err" , "依頼件数は20,000件以下でお願いします。");
						return 'index';
					}
				}else{
					//ファイルに不備ありのため戻す
					$this->af->setApp("act", "back");
					$this->af->setApp("up_list_err" , "依頼件数は10,000件以下でお願いします。");
					return 'index';
				}
*/
			}
//	    }

			//リスト名称を取得
			$list = $this->af->get("in_up_list");
			$list_name = $list['name'];

			//依頼番号
			$request_id = basename($up_file_path,".csv");
			$request_id = str_replace("furu_","",$request_id);
			$request_id = str_replace("_org","",$request_id);
			$this->af->setApp("request_id", $request_id);

			//処理時起案計測(デバック)
			$start = microtime(true);

			//マッチング処理
			$matching_count_all = $fm->get_matching_row_count($value_array,$header_line,$column_num,$property_key,"");

			//マッチング件数をチェックしてトータルで0件の場合は戻す
			$matching_data_error_flg = true;
			foreach($GLOBALS['matching_phase_list'] as $key => $val){
				if($count[$phase]['matching_row'] > 0){
					continue;
				}else{
					$matching_data_error_flg = false;
					break;
				}
			}
			if($matching_data_error_flg){
				$this->af->setApp("act", "back");
				$this->af->setApp("up_list_err" , "マッチする企業がありませんでした。");
				return 'index';
			}

			//注文情報のDB登録
			$prosess = 9;	//このまま購入まで流れてゆくため、キャンセルフラグを立てておく
			$res = Opensite_Dao_Furu::insert_hojin_request($request_id,$user_id,$id,$list_name,$count['list'],$header_line,$property_key,$up_file_path,$prosess,"auto");
			if(Ethna::isError($res))
			{
				$this->af->setApp("title","システムエラー");
				$this->af->setApp("message","依頼情報登録エラー");
				return 'message';
			}

			//処理時起案計測(デバック)
			$end = microtime(true);
			$time = $end - $start;
			$this->af->setApp("time",               $time);//処理時間デバック用

		}else{
			//戻ってきた時用　件数格納
			$data = Opensite_Dao_Furu::get_hojin_request_data($this->af->get("in_request_id"));
			$this->af->setApp("request_id", $this->af->get("in_request_id"));

			//引き継ぎ用
			if($data['list_header_line'] != ""){
				$this->af->setApp("header_check",       "on");
				$this->af->setApp("header_line",        $data['list_header_line']);
			}
			$this->af->setApp("column_num_str",     $this->af->get("in_column_num_str"));
			$this->af->setApp("use_matching_phase",     $this->af->get("in_use_matching_phase"));
			$this->af->setApp("total_point",     $this->af->get("in_total_point"));
			$this->af->setApp("use_property",     $this->af->get("in_use_property"));
			//$this->af->setApp("property_key", 	$data['select_property']);

			//マッチング件数取得
			$matching_count_all = array();
			foreach($GLOBALS['matching_phase_list'] as $key => $val){
				$attribute_data = array();
				$attribute_data = Opensite_Dao_Furu::get_searches_childs_info($this->af->get("in_request_id"),$key);
				foreach($attribute_data as $name => $item){
					$matching_count_all[$key][$name] = $item['row'];
				}
			}

			$exp = explode(",",$data['select_property']);
			$property_check = array();
			$property_name = "";
			foreach($exp as $key){
				$property_check[] = $key;
				if($property_name = ""){
					$property_name = $GLOBALS['hojin_request_property_list'][$key];
				}else{
					$property_name = "," . $GLOBALS['hojin_request_property_list'][$key];
				}
			}

			$this->af->setApp("property_check",     $property_check);
			$this->af->setApp("property_name", 	$property_name);

			$up_file_path = $data['list_filepath'];
			$count['list'] = $data['list_row'];
			$list_name = $data['list_name'];
		}

		// ポイント計算
		//購入件数と必要ポイントの算出

		//項目単価とその小計、必要ポイントの算出
		$unit_point = array();

		$conv_col_name = $fm->trn_inspect_column_name;

		foreach($fm->matching_phase as $phase){
			$point[$phase]['total'] = 0;
			$count[$phase]['matching_row'] = $matching_count_all[$phase]['matching_row'];
			foreach($property_check as $key){
				if($unit_point[$key] == ""){
					//項目単価
					$unit_point[$key] = $GLOBALS['hojin_request_auto_property_unit_point_list'][$key];
				}
				//件数再格納
				if($act != "back"){
					$count[$phase][$key] = $matching_count_all[$phase][$fm->trn_inspect_column_name[$key]];
				}else{
					$count[$phase][$key] = $matching_count_all[$phase][$key];
				}
				//項目毎の小計
				$point[$phase][$key] = $count[$phase][$key] * $unit_point[$key];
				//必要ポイントの計算
				$point[$phase]['ask'] += $point[$phase][$key];
			}
			$point[$phase]['ask'] = floor($point[$phase]['ask']);

			//基本料金
			$point['base'] = $GLOBALS['hojin_request_base_point'];

			//((件数*選択項目数)*項目単価)+基本料金=合計必要ポイント
			$point[$phase]['subtotal'] = floor($point[$phase]['ask'] + $point['base']);

			//消費税課税
			$point['tax_rate'] = $GLOBALS['hojin_request_tax'];
			$point[$phase]['tax'] = floor($point[$phase]['subtotal'] * $GLOBALS['hojin_request_tax']);//!!!消費税20150328!!!
			//合計ポイント
			$point[$phase]['total'] = $point[$phase]['subtotal'] + $point[$phase]['tax'];

			// ポイントが不足している場合
			if( $point['user'] - $point[$phase]['total'] < 0 )
			{
				$point[$phase]['lack'] = $point[$phase]['total'] - $point['user'];
			}else{
				$point[$phase]['lack'] = 0;
			}
		}

		if($act != "back"){
			//マッチング方法毎の情報のDB登録
			foreach($GLOBALS['matching_phase_list'] as $phase => $val){
				$res = Opensite_Dao_Furu::insert_hojin_searches($request_id,$phase,$count[$phase]['matching_row'],$point[$phase]['subtotal'],$user_id);
				if(Ethna::isError($res))
				{
					$this->af->setApp("title","システムエラー");
					$this->af->setApp("message","マッチング情報登録エラー");
					return 'message';
				}else{
					$sql = "select last_insert_id() from hojin_request";
					$result = mysql_query($sql);
					$arr = mysql_fetch_array($result);
					$searches_id = $arr['last_insert_id()'];

					//付与項目毎の情報のDB登録
					foreach($property_check as $key){
						$res = Opensite_Dao_Furu::insert_hojin_searches_childs($searches_id,$key,$count[$phase][$key],$point[$phase][$key],$unit_point[$key],$user_id);
						if(Ethna::isError($res))
						{
							$this->af->setApp("title","システムエラー");
							$this->af->setApp("message","付与情報登録エラー");
							return 'message';
						}
					}
				}
			}
		}

/*
var_dump($count);
var_dump($point);
var_dump($unit_point);
exit;
*/

		//引き継ぎ用
		$this->af->setApp("up_file_path",	$up_file_path);

		// 確認表示用
		$this->af->setApp("act"           , $act);
		$this->af->setApp("list_name"     , $list_name);
		$this->af->setApp("point"         , $point);
		$this->af->setApp("count"         , $count);
		$this->af->setApp("unit_point"    , $unit_point);
		$this->af->setApp("property_check", $property_check);
		$this->af->setApp("matching_phase", $GLOBALS['matching_phase_list']);

		// selectionバリデート(入力項目無し)
		$this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_CommissionRegistrationauto perform()");
		return 'commission_registrationauto';
	}

}

?>
