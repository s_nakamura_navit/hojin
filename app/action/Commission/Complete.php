<?php
/**
 *  Complete.php
 */

/**
 *  Complete Form implementation.
 */
class Opensite_Form_CommissionComplete extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_request_id' => array(
            'type' => VAR_TYPE_INT,
            'required' => true
        ),
        'in_total_count' => array(
            'type' => VAR_TYPE_INT,
            'required' => true
        ),
        'in_total_point' => array(
            'type' => VAR_TYPE_INT,
            'required' => true
        ),
        'in_back_point' => array(
            'type' => VAR_TYPE_INT,
            'required' => true
        ),
        'in_use_property' => array(
            'type' => array(VAR_TYPE_STRING),
            'required' => false
        ),
        'in_column_num_str' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_act' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_use_matching_phase' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_estimate_file_path' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
    );
    
}

/**
 *  Thanks action implementation.
 */
class Opensite_Action_CommissionComplete extends Opensite_ActionClass
{
//    function authenticate()
//    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
//    }

    /**
     *  preprocess of Thanks Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        // フォームバリデーションを実行し、EthnaErrorオブジェクトを登録する
        $this->af->validate();

        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Thanks prepare()");
        return null;
    }

    /**
     *  Thanks action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // セッション値取得
        $id   = $this->session->get("id");
        $name = $this->session->get("name");
        $user_id = $this->session->get("serial");
        
	//再描画時のチェック項目、値が入っている場合DB(ポイント増減算)処理はしない
	$re_send_check	   = $this->session->get("re_send_check");

        // ふるふるマネージャ
        $fm = $this->backend->getManager("furu");

        // 初期化
        $request_id	   = $this->af->get("in_request_id");
        $total_count	   = $this->af->get("in_total_count");
        $total_point	   = $this->af->get("in_total_point");
        $back_point	   = $this->af->get("in_back_point");
        $column_num_str	   = $this->af->get("in_column_num_str");
        $act	   	   = $this->af->get("in_act");
        $use_matching_phase = $this->af->get("in_use_matching_phase");
        $estimate_file_path = $this->af->get("in_estimate_file_path");

	$this->af->setApp("act",	$act);
	$this->af->setApp("use_matching_phase",	$use_matching_phase);

	//属性付与項目の文字列化
        $use_property = $this->af->get("in_use_property");
	foreach($use_property as $key){
		if($use_property_str == ""){
			$use_property_str = $key;
			$use_property_arr[$key] = $GLOBALS['hojin_request_property_list'][$key];
		}else{
			$use_property_str .= "," . $key;
			$use_property_arr[$key] = $GLOBALS['hojin_request_property_list'][$key];
		}
	}

        //付与項目

        // 依頼情報取得
	if($use_matching_phase == ""){
            $data = Opensite_Dao_Mypage::get_request_info_by_id($request_id);
	}else{
	    $data = Opensite_Dao_Furu::get_hojin_request_info($request_id);
	}

	//DB登録
	if($act == "get_list" && $re_send_check == ""){

            //処理時起案計測(デバック)
            $start = microtime(true);

	    if($use_matching_phase == ""){
	        $dl_file_path = $fm->getCsv($request_id,$data['list_filepath'],$data['inspect_filepath'],$data['select_property'],$use_property_arr,$data['list_header_line']);
		$use_point = $back_point;
		$status = 2;
	    }else{
		$s = 0;
		$column_num_arr = array();
		$exp = explode(",",$column_num_str);
		foreach($GLOBALS['num_input_column_list'] as $key => $val){
			$column_num_arr[$key] = $exp[$s];
			$s++;
		}
		
        // ユーザによってCSVフォーマットを変更
        if(preg_match("/^HT[0-9]{10}$/", $id)) {
            // biz compass用
            $dl_file_path = $fm->autocreateCsvBizcp($request_id,$data['list_filepath'],$data['list_header_line'],$use_property_arr,$column_num_arr,$use_matching_phase);
        } else {
            // 上記以外
            $dl_file_path = $fm->autocreateCsv($request_id,$data['list_filepath'],$data['list_header_line'],$use_property_arr,$column_num_arr,$use_matching_phase);
        }
		if($dl_file_path === 0){
                	$this->af->setApp("title","システムエラー");
	                $this->af->setApp("message","ファイル取得エラー");
	                return 'message';
		} elseif($dl_file_path === -1) {
            $this->af->setApp("title","システムエラー");
            $this->af->setApp("message","入力ファイルフォーマットエラー");
            return 'message';
        }
		
        $use_point = $total_point;
		$status = 0;
	    }

            //処理時起案計測(デバック)
            $end = microtime(true);
            $time = $end - $start;
            $this->af->setApp("time",               $time);//処理時間デバック用

	    $res = Opensite_Dao_Mypage::use_point($id,$user_id,$use_point,$request_id,$status);
            if(Ethna::isError($res))
            {
                $this->af->setApp("title","システムエラー");
                $this->af->setApp("message","ファイル取得エラー");
                return 'message';
            }
	    $res = Opensite_Dao_Furu::update_hojin_request($request_id,$use_property_str,$dl_file_path,2,$use_matching_phase);
            if(Ethna::isError($res))
            {
                $this->af->setApp("title","システムエラー");
                $this->af->setApp("message","ファイル取得エラー");
                return 'message';
            }
	    $this->af->setApp("dl_file_path",        $dl_file_path);

		
		
//
//		FAX番号、購入・レンタル時にwebmasterあてにメール配信されるよう以下追加　2017-01-16 中村
//
//print_r($data);
		$user_info = Opensite_Dao_Mypage::get_user_info_by_id($this->session->get("serial"));
		$to = array(array('btobdownload@navit-j.com','送信1'),array('webmaster@navit-j.com','送信2'));
//		$to = array(array('s.nakamura@navit-j.com','送信1'));
		//件名
		$subject="【通知】 法人番号検索　依頼メール";
		//ファイル名
		$tmp_faxlist_mail_file = BASE."/mail/request_csv_for_master.txt";
		//ファイル読み込み
		mb_language('Japanese');
		$tmp_faxlist_mail = file_get_contents($tmp_faxlist_mail_file);
		$tmp_faxlist_mail = mb_convert_encoding($tmp_faxlist_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
		//読み込んだメール内の変数に値をセット
		$tmp_faxlist_mail = str_replace('[USER_ID]', $id, $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[COMPANY_NAME]', $user_info['company_name'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[DEPARTMENT_NAME]', $user_info['department_name'], $tmp_faxlist_mail);
		if($user_info['post_name'] == ""){
		    $tmp_faxlist_mail = str_replace('[POST_NAME]', 'なし', $tmp_faxlist_mail);
		}else{
		    $tmp_faxlist_mail = str_replace('[POST_NAME]', $user_info['post_name'], $tmp_faxlist_mail);
		}
		$tmp_faxlist_mail = str_replace('[CONTRACTOR_NAME]', $user_info['contractor_lname']."　".$user_info['contractor_fname'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[ZIP]', $user_info['zip1']."-".$user_info['zip2'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[ADDRESS]', $user_info['address'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[PHONE_NO]', $user_info['phone_no1']."-".$user_info['phone_no2']."-".$user_info['phone_no3'], $tmp_faxlist_mail);
		if($user_info['fax_no1'] == "" || $user_info['fax_no2'] == "" || $user_info['fax_no3'] == ""){
		    $tmp_faxlist_mail = str_replace('[FAX_NO]', 'なし', $tmp_faxlist_mail);
		}else{
		    $tmp_faxlist_mail = str_replace('[FAX_NO]', $user_info['fax_no1']."-".$user_info['fax_no2']."-".$user_info['fax_no3'], $tmp_faxlist_mail);
		}
		$tmp_faxlist_mail = str_replace('[EMAIL]', $user_info['email'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[ID]', $request_id, $tmp_faxlist_mail);
		$huyo_list = array("business" => "業種",
							"fax" => "FAX番号",
							"listed_type" => "上場区分",
							"employee_div" => "従業員数規模",
							"sale_div" => "売上高規模",
							"capital_div" => "資本金規模",
							"founding_dt" => "設立年月",
							"url" => "URL",
							"corp_num" => "法人番号（マイナンバー）",
							"mailaddr" => "メールアドレス"
		);
		$property_str = "";
		$tmp = explode(",", $use_property_str);
		foreach($tmp as $b){
			if ($property_str) $property_str .= ",";
			$property_str .= $huyo_list[$b];
		}
		$tmp_faxlist_mail = str_replace('[PROPERTY]', $property_str, $tmp_faxlist_mail);
		$body = $tmp_faxlist_mail;
		$fromname="法人番号検索　管理システムよりお知らせ";
		$fromaddress="info_opensite@navit-j.net";
		$return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);
		//
		//	以下ユーザー宛
		//
		//件名
		$subject="【法人番号検索】依頼メール";
		//ファイル名
		$tmp_faxlist_mail_file = BASE."/mail/request_csv.txt";
		//ファイル読み込み
		mb_language('Japanese');
		$tmp_faxlist_mail = file_get_contents($tmp_faxlist_mail_file);
		$tmp_faxlist_mail = mb_convert_encoding($tmp_faxlist_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
		//読み込んだメール内の変数に値をセット
		$tmp_faxlist_mail = str_replace('[COMPANY_NAME]', $user_info['company_name'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[CONTRACTOR_NAME]', $user_info['contractor_lname']."　".$user_info['contractor_fname'], $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[ID]', $request_id, $tmp_faxlist_mail);
		$tmp_faxlist_mail = str_replace('[PROPERTY]', $property_str, $tmp_faxlist_mail);
		
		$body = $tmp_faxlist_mail;
		$fromname="法人番号検索事務局";
		$fromaddress="info_opensite@navit-j.net";
		$return_flag = Opensite_Mail::mailsender($user_info['email'],$subject,$body,$fromname,$fromaddress);
//
//	以上まで
//
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}else if($act == "dl_csv" && $re_send_check == ""){
	    //再描画時に処理を重複させないためチェックする値を格納
	    $this->session->set("re_send_check",	"send_after");
//echo "<br>==========2";exit;

	    $fm->csv_dl($estimate_file_path);
	}

        // selectionバリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Thanks perform()");
        return 'commission_complete';
    }
}

?>
