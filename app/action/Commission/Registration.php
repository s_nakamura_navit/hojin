<?php
/**
 *  Registration.php
 */

/**
 *  Registration Form implementation.
 */
class Opensite_Form_CommissionRegistration extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_request_id' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_use_property' => array(
            'type' => array(VAR_TYPE_STRING),
            'required' => false
        ),
        'in_total_count' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_total_point' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_act' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
    );
    
}

/**
 *  mypage action implementation.
 */
class Opensite_Action_CommissionRegistration extends Opensite_ActionClass
{
    function authenticate()
    {
        // 未ログインの場合、トップページのリンクは無くなるが、
        // お気に入り等でアクセス可能なためアクセス制御
        return parent::authenticate();
    }

    /**
     *  preprocess of mypage Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        //$this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_MYPAGE prepare()");
        return null;
    }

    /**
     *  mypage action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // セッション値取得
        $account   = $this->session->get("id");
        $user_name = $this->session->get("name");
        $user_id = $this->session->get("serial");

        //購入時の二重処理防止フラグ　必ずトップで初期化
        $this->session->set("re_send_check",        "");
        
        $fm = $this->backend->getManager("furu");
        
	$request_id = $this->af->get("in_request_id");
	$use_property = $this->af->get("in_use_property");
        $total_count = $this->af->get("in_total_count");
        $total_point = $this->af->get("in_total_point");
        $act = $this->af->get("in_act");

        //付与項目
        $this->af->setApp("property_list",      $GLOBALS['hojin_request_property_list']);

        // 現在のポイント取得
        $point['current'] = Opensite_Dao_Mypage::get_current_point($user_id);
        if(!$point['current'])
        {
            $point['current'] = 0;
        }

        // 依頼情報取得
        $data = Opensite_Dao_Mypage::get_request_info_by_id($request_id);

	//キャンセルポイント
	//消費税算出
	$cancel_tax = $GLOBALS['hojin_request_cancel_point'] * $GLOBALS['hojin_request_tax'];
	//キャンセルポイント
	$point['cancel'] = $GLOBALS['hojin_request_cancel_point'] + $cancel_tax;
        //キャンセル時の返却ポイント算出
        $point['cancel_back'] = $data['use_point'] - $point['cancel']; 

	//キャンセル時の処理
	if($act == "do_cancel"){

            $res = Opensite_Dao_Mypage::use_point($account,$user_id,$point['cancel_back'],$request_id,2);
            if(Ethna::isError($res))
            {
                $this->af->setApp("title","システムエラー");
                $this->af->setApp("message","ファイル取得エラー");
                return 'message';
            }

            $res = Opensite_Dao_Furu::update_hojin_request($request_id,"","",9);
            if(Ethna::isError($res))
            {
                $this->af->setApp("title","システムエラー");
                $this->af->setApp("message","ファイル取得エラー");
                return 'message';
            } 

	    header("location:./index.php?action_mypage=true");
	}

	//付与項目単価・件数・小計の取得
	//依頼した選択項目を配列化
	$exp_sel = explode(",",$data['select_property']);
	$this->af->setApp('select_property',  $exp_sel);
	//依頼項目に付与できた件数の配列化
	$exp_get = explode(",",$data['get_property_row']);
	$i = 0;
	foreach($exp_sel as $key){
        	//単価の抽出
        	$point['common'][$key] = $GLOBALS['hojin_request_property_unit_point_list'][$key]; 
		//件数の抽出
		$count[$key] = $exp_get[$i];
		//小計の算出
		$point[$key] = $exp_get[$i] * $point['common'][$key];
		$i++;
	}

        //基本料金の抽出
        $point['base'] = $GLOBALS['hojin_request_base_point']; 
        //税率の抽出
        $point['tax_rate'] = $GLOBALS['hojin_request_tax'];
        //初期の値
	if($act != "back"){
        	$point['tax'] = $point['base'] * $point['tax_rate'];
        	$point['total'] = $point['base'] + $point['tax'];
	}else{
		$sub_total = 0;
		//選択した項目ごとの小計を合計
		foreach($use_property as $key){
			$sub_total += $point[$key];
		}
		$sub_total = floor($sub_total + $point['base']);
		$point['tax'] = floor($sub_total * $point['tax_rate']);
		$point['total'] = $sub_total + $point['tax'];
	}

        // テンプレートに設定
	$this->af->setApp('name',  $name);
	$this->af->setApp('count',  $count);
        $this->af->setApp('point', $point);
        $this->af->setApp('data',  $data);
        $this->af->setApp('request_id',  $request_id);
        $this->af->setApp('use_property',  $use_property);
        $this->af->setApp('total_count',  $total_count);
        $this->af->setApp('total_point',  $total_point);

        return 'commission_registration';
    }
}

?>
