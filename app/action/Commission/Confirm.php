<?php
/**
 *  Confirm.php
 */

/**
 *  Confirm Form implementation.
 */
class Opensite_Form_CommissionConfirm extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_request_id' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
	'in_use_property' => array(
            'type' => array(VAR_TYPE_STRING),
            'required' => false
        ),
        'in_total_count' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_total_point' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_use_matching_phase' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_column_num_str' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_act' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
    );
    
}

/**
 *  mypage action implementation.
 */
class Opensite_Action_CommissionConfirm extends Opensite_ActionClass
{
    function authenticate()
    {
        // 未ログインの場合、トップページのリンクは無くなるが、
        // お気に入り等でアクセス可能なためアクセス制御
        return parent::authenticate();
    }

    /**
     *  preprocess of mypage Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        //$this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_MYPAGE prepare()");
        return null;
    }

    /**
     *  mypage action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // セッション値取得
        $account   = $this->session->get("id");
        $user_name = $this->session->get("name");
        $user_id = $this->session->get("serial");
        
        $fm = $this->backend->getManager("furu");
        
	$request_id = $this->af->get("in_request_id");
	$total_count = $this->af->get("in_total_count");
	$total_point = $this->af->get("in_total_point");
	$act	     = $this->af->get("in_act");
	$use_matching_phase = $this->af->get("in_use_matching_phase");
	if($use_matching_phase == ""){
	    $use_property = $this->af->get("in_use_property");
	}else{
	    if($act != "back"){
	    	$use_property_all = $this->af->get("in_use_property");
	    	$use_property = $use_property_all[$use_matching_phase];
	    }else{
		$use_property = $this->af->get("in_use_property");
	    }
	}
	$column_num_str = $this->af->get("in_column_num_str");

        //属性付与項目の文字列化(手動付与時、かつポイント購入画面遷移時引き継ぎ用)
        foreach($use_property as $key){
                if($use_property_str == ""){
                        $use_property_str = $key;
                }else{
                        $use_property_str .= "," . $key;
                }
        }

	//値を格納
        $this->af->setApp('request_id',  $request_id);
        $this->af->setApp('use_property',  $use_property);
        $this->af->setApp('use_property_str',  $use_property_str);
        $this->af->setApp('total_count',  $total_count);
        $this->af->setApp('total_point',  $total_point);
        $this->af->setApp('use_matching_phase',  $use_matching_phase);
        $this->af->setApp('column_num_str',  $column_num_str);

        //付与項目
        $this->af->setApp("property_list",      $GLOBALS['hojin_request_property_list']);

        // 現在のポイント取得
        $point['client'] = Opensite_Dao_Mypage::get_current_point($user_id);
        if(!$point['client'])
        {
            $point['client'] = 0;
        }

        // 依頼情報取得
	if($use_matching_phase == ""){
        	$data = Opensite_Dao_Mypage::get_request_info_by_id($request_id);
	}else{
		$data = Opensite_Dao_Furu::get_hojin_request_info($request_id);
		$property_data = Opensite_Dao_Furu::get_searches_childs_info($request_id,$use_matching_phase);
	}

	//ポイント計算

        //付与項目単価・件数・小計の取得
        //依頼した選択項目を配列化
        $exp_sel = explode(",",$data['select_property']);
        $this->af->setApp('select_property',  $exp_sel);
        $i = 0;
	if($use_matching_phase == ""){
            //依頼項目に付与できた件数の配列化
            $exp_get = explode(",",$data['get_property_row']);
            foreach($exp_sel as $key){
                //単価の抽出
                $point['common'][$key] = $GLOBALS['hojin_request_property_unit_point_list'][$key];
                //件数の抽出
                $count[$key] = $exp_get[$i];
                //小計の算出
                $point[$key] = $exp_get[$i] * $point['common'][$key];
                $i++;
            }
	}else{
            foreach($exp_sel as $key){
                //単価の抽出
                $point['common'][$key] = $property_data[$key]['unit_point'];
                //件数の抽出
                $count[$key] = $property_data[$key]['row'];
                //小計の算出
                $point[$key] = $property_data[$key]['point'];
                $i++;
            }
	}

        //基本料金の抽出
        $point['base'] = $GLOBALS['hojin_request_base_point'];
        //税率の抽出
        $point['tax_rate'] = $GLOBALS['hojin_request_tax'];

        //キャンセルポイント
        //消費税算出
        $cancel_tax = $GLOBALS['hojin_request_cancel_point'] * $point['tax_rate'];
        //キャンセルポイント
        $point['cancel'] = $GLOBALS['hojin_request_cancel_point'] + $cancel_tax;
        //キャンセル時の返却ポイント算出
        $point['cancel_back'] = $data['use_point'] - $point['cancel'];

        //選択した項目ごとの小計を合計
	$sub_total = 0;
        foreach($use_property as $key){
                $sub_total += $point[$key];
        }

	$point['sub_total'] = floor($sub_total + $point['base']);
	//消費税
	$point['tax'] = floor($point['sub_total'] * $point['tax_rate']);
	//総必要ポイント
	$point['total'] = $point['sub_total'] + $point['tax'];
	if($use_matching_phase == ""){
	    //戻しポイント
	    $point['back'] = $data['use_point'] - $point['total'];
	}else{
	    //不足ポイントチェック
	    if($point['client'] < $point['total']){
	        $point['lack'] = $point['total'] - $point['client'];
	    }
	}

        // テンプレートに設定
	$this->af->setApp('name',  $name);
        $this->af->setApp('count', $count);
        $this->af->setApp('point', $point);
        $this->af->setApp('data',  $data);

	if(count($use_property) == 0){
	    $this->af->setApp("use_property_check_err", "購入項目を選択してください。");	
	    if($use_matching_phase == ""){
	    	return "commission_registration";
	    }else{
		return "commission_registrationauto";
	    }
	}

        return 'commission_confirm';
    }
}

?>
