<?php
/**
 *  Confirmation.php
 */

/**
 *  Confirmation Form implementation.
 */
class Opensite_Form_Confirmation extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_up_list' => array(
            'type' => VAR_TYPE_FILE,
            'form_type'   => FORM_TYPE_FILE,
            'required' => false
        ),
        'in_header_check' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_header_line' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_list_name' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_up_file_path' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_list_count' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
        'in_act' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_property_check' => array(
            'type' => array(VAR_TYPE_STRING),
            'required' => false,
        ),
        'in_request_id' => array(
            'type' => VAR_TYPE_INT,
            'required' => false
        ),
    );

}

/**
 *  Confirmation action implementation.
 */
class Opensite_Action_Confirmation extends Opensite_ActionClass
{
//    function authenticate()
//    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
//    }

    /**
     *  preprocess of Confirmation Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        // フォームバリデーションを実行し、EthnaErrorオブジェクトを登録する
        $this->af->validate();

        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Confirmation prepare()");
        return null;
    }

    /**
     *  Confirmation action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {

        //ログインチェック
        Opensite_Dao_Login::check_user_session();

        // セッション値取得
        $id   = $this->session->get("id");
        $name = $this->session->get("name");
        $user_id = $this->session->get("serial");

        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();

        // サービスマネージャ
        $fm = $this->backend->getManager("furu");

	$err_count = $this->ae->count();

	//フェーズチェック
        $act = $this->af->get("in_act");

        // 初期化
	$result = 0;
	$up_file = $this->af->get("in_up_list");

       	// 現在のポイント
	$point = array();
       	$point['user'] = Opensite_Dao_Mypage::get_current_point($user_id);

	//付与項目
	$this->af->setApp("property_list", 	$GLOBALS['hojin_request_property_list']);

	//戻りでファイルチェックは行わない
	if($act != "back"){

	    $property_check = $this->af->get("in_property_check");
	    if(count($property_check) != 0){
	        //項目名とキーをカンマ区切りで格納
		foreach($property_check as $key){
		    if($property_key == ""){
			$property_key = $key;
			$property_name = $GLOBALS['hojin_request_property_list'][$key];
		    }else{
			$property_key .= "," . $key;
			$property_name .= "," . $GLOBALS['hojin_request_property_list'][$key];
		    }
	        }
		$this->af->setApp("property_key", 	$property_key);
		$this->af->setApp("property_name", 	$property_name);
	    }else{
		$err_count++;
		$this->af->setApp("property_check_err" , "付与する属性項目を選択してください。");
	    }

	    if($up_file['error'] != 4){
                $file = $fm->upload($this->af->get("in_up_list"));
	        if($file == null){
                    $result = Ethna::raiseNotice('ファイルのアップロードに失敗しました。', E_FURU_UPLOAD_LIST_NOT_EXIST);
                }else if(pathinfo($file['new_tmp_path'], PATHINFO_EXTENSION) != "csv" && pathinfo($file['new_tmp_path'], PATHINFO_EXTENSION) != "CSV"){
		    $err_count++;
		    $this->af->setApp("up_list_err" , "CSVファイルを選択して下さい。");
	        }else{
		    $up_file_path = $file['new_tmp_path'];
	        }
	    }else{
	        $err_count++;
	        $this->af->setApp("up_list_err" , "読み込みファイルを選択して下さい。");
	    }

            $header_check = $this->af->get("in_header_check");

	    if($header_check == "on"){
        	$header_line = mb_convert_kana($this->af->get("in_header_line"),n,'UTF-8');
		if($header_line == ""){
		    $err_count++;
		    $this->af->setApp("header_line_err", "ヘッダー行数には数字(整数)を入力してください。");
		}else{
		    if(!preg_match("/^[0-9]+$/",$header_line)){
			$err_count++;
			$header_line = "1";
			$this->af->setApp("header_line_err", "ヘッダー行数には数字(整数)を入力してください。");
	    	    }
		}
	    }else{
		$header_line = 0;
	    }
	}

	//引き継ぎ用共通
        $this->af->setApp("header_check",	$header_check);
        $this->af->setApp("header_line",	$header_line);
        $this->af->setApp("property_check",	$property_check);
        $this->af->setApp("auto_check",		"off");

        //todo:// getHeaderColumns() でも実装して項目名リストを持ってきて比較する

//        if($data['insert'] != "{{テスト}}"){
//            $this->ae->add("insert", "宛先リストにない項目名です", E_Opensite_COMMON);
//        }

        // エラーチェック
        if ($err_count > 0) {

            foreach($this->ae->getMessageList() as $item)
            {
                error_log((print_r($item,true)));
            }

            //入力値に不備ありのため戻す
            foreach($property_check as $key){
                $property_checked[$key] = "checked";
            }
            $this->af->setApp("property_checked",	$property_checked);
	    $this->af->setApp("act", 			"back");
            return 'index';
        }

        // 登録エラー

        if (Ethna::isError($result)) {
            // アップロードエラー
            if($result->getCode() === E_FAX_UPLOAD_LIST_NOT_EXIST || $result->getCode() === E_FAX_UPLOAD_ARTICLE_NOT_EXIST)
            {
                //$this->ae->addObject('in_up_list', $result);
                $this->af->setApp("title","システムエラー");
                $this->af->setApp("message","ファイルのアップロードに失敗しました。");
                return 'message';
            }
        }

	//初期化
	$count = array();

	if($act != "back"){

	    //処理時起案計測(デバック)
	    $start = microtime(true);

	    //件数出し
	    $value_array = $fm->getListValue($up_file_path,$header_check,$header_line);
        $logger->log(LOG_DEBUG, "ret  value_array: ".$value_array);
        if(is_array($value_array) == false) {
            if($value_array == -1) {
                $this->af->setApp("title","システムエラー");
                $this->af->setApp("message","入力ファイルにエラーがありました。");
                return 'message';
            }
        }

	    //リストの総件数
	    $count['list'] = count($value_array);
	    echo'<pre>';print_r($count);echo'</pre>';exit;
/*
	    //特定管理者は件数制限を受けない
            if(($count['list'] > 0 && $count['list'] < 2001) || ($this->session->get("authkey") == "de0c2a9be052d0edd5f5f11e92eee411" && $user_id == 1))
            {
            }else{
*/
		if($count['list'] == 0){
			//ファイルに不備ありのため戻す
			$this->af->setApp("act", "back");
			$this->af->setApp("up_list_err" , "CSVファイル内に有効な件数がありません。");
            		return 'index';
/*
		}else if($count['list'] > 2000){
			//ファイルに不備ありのため戻す
			$this->af->setApp("act", "back");
			$this->af->setApp("up_list_err" , "依頼件数は2,000件以下でお願いします。");
			$this->af->setApp("row_err" , true);
            		return 'index';

		}else if($count['list'] < 100){
			//ファイルに不備ありのため戻す
			$this->af->setApp("act", "back");
			$this->af->setApp("up_list_err" , "依頼件数は100件以上でお願いします。");
            		return 'index';
		}
*/
	    }

            //リスト名称を取得
            $list = $this->af->get("in_up_list");
            $list_name = $list['name'];

	    //依頼番号
	    $request_id = basename($up_file_path,".csv");
	    $request_id = str_replace("furu_","",$request_id);
	    $request_id = str_replace("_org","",$request_id);
	    $this->af->setApp("request_id", $request_id);

	    //注文情報のDB登録
	    $prosess = 8;	//仮登録
            $res = Opensite_Dao_Furu::insert_hojin_request($request_id,$user_id,$id,$list_name,$count['list'],$header_line,$property_key,$up_file_path,$prosess,"");
            if(Ethna::isError($res))
            {
                $this->af->setApp("title","システムエラー");
                $this->af->setApp("message","ファイル取得エラー");
                return 'message';
            }

	}else{
	    //戻ってきた時用　件数格納
	    $data = Opensite_Dao_Furu::get_hojin_request_data($this->af->get("in_request_id"));
	    $this->af->setApp("request_id", $this->af->get("in_request_id"));

	    //引き継ぎ用
	    if($data['list_header_line'] != ""){
            	$this->af->setApp("header_check",       "on");
            	$this->af->setApp("header_line",        $data['list_header_line']);
	    }

	    $this->af->setApp("property_key", 	$data['select_property']);

	    $exp = explode(",",$data['select_property']);
	    $property_check = array();
	    $property_name = "";
	    foreach($exp as $key){
		$property_check[] = $key;
		if($property_name = ""){
			$property_name = $GLOBALS['hojin_request_property_list'][$key];
		}else{
			$property_name = "," . $GLOBALS['hojin_request_property_list'][$key];
		}
	    }

            $this->af->setApp("property_check",     $property_check);
	    $this->af->setApp("property_name", 	$property_name);

	    $up_file_path = $data['list_filepath'];
	    $count['list'] = $data['list_row'];
            $list_name = $data['list_name'];
	}

	// ポイント計算
	//購入件数と必要ポイントの算出

        //件数単価
        //$item_point = ITEM_POINT;

/*
	//件数が100件に満たなかった場合の必要ポイントは、100件として計算される	2015/4/30a
	//一旦件数を格納している変数を100件にする
	if($count['list'] < 100){
		$count['calculation_list'] = 100;
	}else{
		$count['calculation_list'] = $count['list'];
	}
*/
	//100件以下は弾くようにした	2015/4/30b
	$count['calculation_list'] = $count['list'];

	//項目数
	$count['property'] = count($property_check);

        //項目単価とその小計、必要ポイントの算出
	$point['ask'] = 0;
	foreach($property_check as $key){
		//項目単価
        	$point['unit'][$key] = $GLOBALS['hojin_request_property_unit_point_list'][$key];
		//項目毎の小計
		$point['property_total'][$key] = $count['calculation_list'] * $point['unit'][$key];
		//必要ポイントの計算
		$point['ask'] += $point['property_total'][$key];
	}
	$point['ask'] = floor($point['ask']);

        //基本料金
        $point['base'] = $GLOBALS['hojin_request_base_point'];

        //((件数*選択項目数)*項目単価)+基本料金=合計必要ポイント
        $point['subtotal'] = floor($point['ask'] + $point['base']);

	//消費税課税
	$point['tax'] = floor($point['subtotal'] * $GLOBALS['hojin_request_tax']);//!!!消費税20150328!!!
	//合計ポイント
	$point['total'] = $point['subtotal'] + $point['tax'];

        // ポイントが不足している場合
        if( $point['user'] - $point['total'] < 0 )
        {
            $point['lack'] = $point['total'] - $point['user'];
            $this->af->setApp('act',	'no_point');
        }

	//引き継ぎ用
        $this->af->setApp("up_file_path",	$up_file_path);

        // 確認表示用
        $this->af->setApp("list_name",		$list_name);
        $this->af->setApp("point",		$point);
        $this->af->setApp("count",		$count);
        $this->af->setApp("property_check",     $property_check);

        // selectionバリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Confirmation perform()");
        return 'confirmation';
    }

}

/**
 *  ファイルのアップロードを試みる
 */
// アップロードファイル情報を表示する。
/*
echo "アップロードファイル名　：　" , $_FILES["in_up_list"]["name"] , "<BR>";
echo "MIMEタイプ　：　" , $_FILES["in_up_list"]["type"] , "<BR>";
echo "ファイルサイズ　：　" , $_FILES["in_up_list"]["size"] , "<BR>";
echo "テンポラリファイル名　：　" , $_FILES["in_up_list"]["tmp_name"] , "<BR>";
echo "エラーコード　：　" , $_FILES["in_up_list"]["error"] , "<BR>";

// アップロードファイルを格納するファイルパスを指定
$filename = "./download/" . $_FILES["in_up_list"]["name"];

if ( $_FILES["in_up_list"]["size"] === 0 ) {
      echo "ファイルはアップロードされてません！！ アップロードファイルを指定してください。";
}
else {
    // アップロードファイルされたテンポラリファイルをファイル格納パスにコピーする
    $result = @move_uploaded_file( $_FILES["in_up_list"]["tmp_name"], $filename);
    if ( $result === true ) {
        echo "アップロード成功！！";
    }
    else {
        echo "アップロード失敗！！";
    }
}
*/
?>
