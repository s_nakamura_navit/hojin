<?php
/**
 *  Send.php
 */

/**
 *  Send Form implementation.
 */
class Opensite_Form_Send extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array();
    
}

/**
 *  Send action implementation.
 */
class Opensite_Action_Send extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
    }

    /**
     *  preprocess of Send Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Send prepare()");
        return null;
    }

    /**
     *  send action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // セッション値取得
        $id   = $this->session->get("id");
        $name = $this->session->get("name");
        $fax_id = $this->session->get("fax_id");
        $datetime = $this->session->get("datetime");
        
        // Fax送信マネージャ
        $fm = $this->backend->getManager("fax");
        
        // 送信
        $fm->sendFax($fax_id,$datetime);
        Opensite_Dao_Fax::receipt_fax_transmission($fax_id);
		// 確認表示用		
		
        // selectionバリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Send perform()");
        return 'send';
    }
}

?>
