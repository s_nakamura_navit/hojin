<?php
/**
 *  Logout/Do.php
 *  ログアウト実施アクションフォーム
 *
 *  @copyright  (C)2010 Navit co.,ltd.
 *  @author     Tetsuo Nakanishi
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  logout_do Form implementation.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Form_LogoutDo extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    /*
    var $form = array(
        'in_id' => array(
            'name' => 'ログインID',
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
        'in_pw' => array(
            'name' => 'パスワード',
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
    );
    */

    /**
     *  Form input value convert filter : sample
     *
     *  @access protected
     *  @param  mixed   $value  Form Input Value
     *  @return mixed           Converted result.
     */
    /*
    function _filter_sample($value)
    {
        //  convert to upper case.
        return strtoupper($value);
    }
    */
}

/**
 *  logout_do action implementation.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Action_LogoutDo extends Opensite_ActionClass
{
    /**
     *  authenticate
     *
     *  @access    public
     *  @return    string  Forward name (null if no errors.)
     */
    function authenticate()
    {
        return;
    }

    /**
     *  preprocess of logout_do Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] view logout_do");
        return null;
    }

    /**
     *  logout_do action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        $db = $this->backend->getDb();
        $session_id = session_id();

        if($this->session->get('id') != null) {
        // ログアウト処理
            $sql = '
update
    user_session
set
    flg_delete = 1
where
    sessid ="'.$session_id.'"
limit 1';

            $ret  = $db->query($sql);

            $id   = $this->session->get("id");
            $name = $this->session->get("name");

            // セッション削除
            $this->session->destroy();
        }

        $db = $this->backend->getDb();
        //$this->backend->perform( "index" );
        header('Location: index.php');
        return null;
    }
}

?>
