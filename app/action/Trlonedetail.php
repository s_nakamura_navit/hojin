<?php
/*
 * Action Trlonedetail.php
 */

/**
 *  Trlonedetail Form implementation.
*/

include('Opensite_Trialone.php');

class Opensite_Form_Trlonedetail extends Opensite_ActionForm
{
	/**
	 *  @access private
	 *  @var    array   form definition.
	 */
	var $form = array(
			'id' => array(
					'type' => VAR_TYPE_INT,
					'required' => false
			),
			'in_act' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'in_request_id' => array(
					'type' => VAR_TYPE_INT,
					'required' => false
			),
			'pref_cd' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			)
	);
}

/**
 *  Trlonedetail action implementation.
 */
class Opensite_Action_Trlonedetail extends Opensite_ActionClass
{
	function authenticate()
	{
		// Indexは認証の必要がないので
		// 親クラスのauthenticate()を無処理でオーバーライド
	}

	/**
	 *  preprocess of index Action.
	 *
	 *  @access public
	 *  @return string    forward name(null: success.
	 *                                false: in case you want to exit.)
	 */
	function prepare()
	{
		return null;
	}

	/**
	 *  index action implementation.
	 *
	 *  @access public
	 *  @return string  forward name.
	 */
	function perform()
	{

		//ログインチェック
		Opensite_Dao_Login::check_user_session();

		// セッション値取得
		$id   = $this->session->get("id");
		$name = $this->session->get("name");
		$user_id = $this->session->get("serial");

		//直接アクセスされた時用のホーム遷移
		$act = $this->af->get("in_act");
		if($act != "trlonedetail"){
			return "index";
		}


		//フォーム内容の変数格納
		$corp_id = $this->af->get("in_request_id");
		$pref_cd = $this->af->get("pref_cd");
		if(!empty($corp_id)) {
			$db =& $this->backend->getDB();
			$select_table = Opensite_Trialone::getTrnInspect();
//			$table_name = $select_table . "_" . $pref_cd;
			$table_name = $select_table;

			//企業情報取得
			$corp_detail = Opensite_Trialone::getCorpDetailAoc($table_name,$corp_id);

			$grant_item = array();

			$ttl_list = $GLOBALS['hojin_request_property_list'];

			$subtotal = 0;
			$i=0;
			$hojin_request_id = $this->af->get("id");
			$hojin_req = Opensite_Dao_Furu::get_hojin_request_data($hojin_request_id);
			$property = explode(",", $hojin_req["use_property"]);
			
			//ここから共通情報
			if(!empty($corp_detail['I_CORP'])) {
				$grant_item[$i]['ttl'] = '企業名';
				$grant_item[$i]['name'] = $corp_detail['I_CORP'];
				$i = $i + 1;
			}
			if(!empty($corp_detail['I_ZIP_1']) && !empty($corp_detail['I_ZIP_2'])) {
				$grant_item[$i]['ttl'] = '郵便番号';
				$grant_item[$i]['name'] = $corp_detail['I_ZIP_1'] . '-' . $corp_detail['I_ZIP_2'];
				$i = $i + 1;
			}
			if(!empty($corp_detail['I_ADDR_ALL'])) {
				$grant_item[$i]['ttl'] = '住所';
				$grant_item[$i]['name'] = $corp_detail['I_ADDR_ALL'];
				$i = $i + 1;
			}
			if(in_array("tel", $property)) {
				$grant_item[$i]['ttl'] = '電話番号';
				$grant_item[$i]['name'] = $corp_detail['I_TEL'];
				$i = $i + 1;
			}
			//ここから付与情報
			if(in_array("business", $property)) {
				$grant_item[$i]['ttl'] = $ttl_list['business'];
				$grant_item[$i]['name'] = $corp_detail['BUSINESS_CD_L_NAME'];
				$i = $i + 1;

				$grant_item[$i]['ttl'] = $ttl_list['business_m'];
				$grant_item[$i]['name'] = $corp_detail['BUSINESS_CD_M_NAME'];
				$i = $i + 1;

				$grant_item[$i]['ttl'] = $ttl_list['business_s'];
				$s = "";
				for($x=1; $x<=10; $x++){
					if ($s && $corp_detail['BUSINESS_CD_S_NAME_' . $x]){
						$s .= "、" . $corp_detail['BUSINESS_CD_S_NAME_' . $x];
					}else if ($corp_detail['BUSINESS_CD_S_NAME_' . $x]){
						$s = $corp_detail['BUSINESS_CD_S_NAME_' . $x];
					}
				}
				$grant_item[$i]['name'] = $s;
				$i = $i + 1;

			}
			if(in_array("fax", $property)) {
				$grant_item[$i]['ttl'] = $ttl_list['fax'];
				$grant_item[$i]['name'] = $corp_detail['I_FAX'];
				$i = $i + 1;
			}
			if(in_array("listed_type", $property)) {
				$grant_item[$i]['ttl'] = $ttl_list['listed_type'];
				$grant_item[$i]['name'] = $corp_detail['LISTED_TYPE_NAME'];
				$i = $i + 1;
			}
			if(in_array("employee_div", $property)) {
				$grant_item[$i]['ttl'] = $ttl_list['employee_div'];
				$grant_item[$i]['name'] = $corp_detail['EMPLOYEE_DIV_NAME'];
				$i = $i + 1;
			}
			if(in_array("sale_div", $property)) {
				$grant_item[$i]['ttl'] = $ttl_list['sale_div'];
				$grant_item[$i]['name'] = $corp_detail['SALE_DIV_NAME'];
				$i = $i + 1;
			}
			if(in_array("capital_div", $property)) {
				$grant_item[$i]['ttl'] = $ttl_list['capital_div'];
				$grant_item[$i]['name'] = $corp_detail['CAPITAL_DIV_NAME'];
				$i = $i + 1;
			}
			if(in_array("founding_dt", $property)) {
				$grant_item[$i]['ttl'] = $ttl_list['founding_dt'];
				$yyyymm = str_split($corp_detail['I_FOUNDING_DT'], 4);
				$grant_item[$i]['name'] = $yyyymm[0] . '年' . ltrim($yyyymm[1],'0') . '月';
				$i = $i + 1;
			}
/*
			if(!empty($corp_detail['I_DEPT_POSITION'])) {
				$grant_item[$i]['ttl'] = $ttl_list['dept_position'];
				$grant_item[$i]['name'] = $corp_detail['I_DEPT_POSITION'];
				$i = $i + 1;
			}
			if(!empty($corp_detail['I_DEPT_NAME'])) {
				$grant_item[$i]['ttl'] = $ttl_list['dept_name'];
				$grant_item[$i]['name'] = $corp_detail['I_DEPT_NAME'];
				$i = $i + 1;
			}
*/			
			if(in_array("url", $property)) {
				$grant_item[$i]['ttl'] = $ttl_list['url'];
				$grant_item[$i]['name'] = $corp_detail['I_URL'];
				$i = $i + 1;
			}
			if(in_array("corp_num", $property)) {
				$grant_item[$i]['ttl'] = $ttl_list['corp_num'];
				$grant_item[$i]['name'] = $corp_detail['I_CORP_NUM'];
				$grant_item[$i]['point'] = $point_list['corp_num'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}
			if(in_array("mailaddr", $property)) {
				$grant_item[$i]['ttl'] = $ttl_list['mailaddr'];
				$grant_item[$i]['name'] = $corp_detail['I_MAILADDR'];
				$grant_item[$i]['point'] = $point_list['mailaddr'];
				$subtotal = $subtotal + $grant_item[$i]['point'];
				$i = $i + 1;
			}

			$this->af->setApp("grant_item",	$grant_item);

		}


		return 'trlonedetail';
	}

}






























?>
