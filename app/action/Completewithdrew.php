<?php
/**
 *  Completewithdrew.php
 */

/**
 *  Completewithdrew Form implementation.
 */
class Opensite_Form_Completewithdrew extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
    );
    
}

/**
 *  Completewithdrew action implementation.
 */
class Opensite_Action_Completewithdrew extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
        return parent::authenticate();
    }

    /**
     *  preprocess of Completewithdrew Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Completewithdrew prepare()");
        return null;
    }

    /**
     *  Completewithdrew action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {

        //ログインチェック
        Opensite_Dao_Login::check_user_session();

        // セッション情報取得
        $id = $this->session->get('id');
	$serial = $this->session->get('serial');

            $affected = "";
            $inserted_serial_no = "";
            // バリデート
            if ($this->af->validate() != 0) {
                //入力値に不備ありのため戻す          
                return 'request_registration';
            }else{
                // 入力値OK
                
                //更新日時セット
                $date_now = date("Y-m-d H:i:s");
                $data{'updated'} = $date_now;

		//会員情報取得
	        $data = Opensite_Dao_Mypage::get_user_info_by_id($serial);
              
                //DBへ登録(退会フラグを立てる)
                $affected = Opensite_Dao_Mypage::withdraw_mst_user($serial);
                if($affected == null || $affected == "" || $affected == 0){
                  //エラー
                  $this->af->set('db_regist_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
                }else{
                  $this->af->set('db_regist_result','1');
                  //DB登録OK

                  //宛先
                  $to = $data{'email'};
                  //$to="m.hirai@navit-j.com";
                  //件名
                  $subject="【法人番号検索】退会 確認メール";
                  //差出人
                  $fromname="法人番号検索 事務局";
                  $fromaddress="info_opensite@navit-j.net";

                  //本文をファイルから読み込み
                  //ファイル名
                  $del_user_mail_file = BASE."/mail/withdrew.txt";
                  //ファイル読み込み
                  mb_language('Japanese');
                  $del_user_mail = file_get_contents($del_user_mail_file);
                  $del_user_mail = mb_convert_encoding($del_user_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');          
                  //読み込んだメール内の変数に値をセット
                  $del_user_mail = str_replace('[COMPANY_NAME]', $data['company_name'], $del_user_mail);
                  $del_user_mail = str_replace('[CONTRACTOR_NAME]', $data['contractor_lname']."　".$data['contractor_fname'], $del_user_mail);
              
                  //本文
                  $body = $del_user_mail;
                  $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);                
                  if($return_flag) {
                    //メール送信OK
                    $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND OK] TO:<".$to.">");
                    $this->af->set('mail_send_result','1');

                    //webmasterへのメール通知
                    //$to="uemura@navit-j.com";
                    $to = array(array('btobmarketing@navit-j.com','送信1'),array('webmaster@navit-j.com','送信2'));
		    //件名
                    $subject="【通知】法人番号検索　退会のお申込み 通知";
		    //ファイル名
                    $del_user_mail_file = BASE."/mail/withdrew_for_master.txt";
		    //ファイル読み込み
                    mb_language('Japanese');
                    $del_user_mail = file_get_contents($del_user_mail_file);
                    $del_user_mail = mb_convert_encoding($del_user_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
		    //読み込んだメール内の変数に値をセット
                    $del_user_mail = str_replace('[ID]', $id, $del_user_mail);
                    $del_user_mail = str_replace('[COMPANY_NAME]', $data['company_name'], $del_user_mail);
                    $del_user_mail = str_replace('[DEPARTMENT_NAME]', $data['department_name'], $del_user_mail);
                    $del_user_mail = str_replace('[POST_NAME]', $data['post_name'], $del_user_mail);
                    $del_user_mail = str_replace('[CONTRACTOR_NAME]', $data['contractor_lname']."　".$data['contractor_fname'], $del_user_mail);
                    $del_user_mail = str_replace('[ZIP]', $data['zip1']."-".$data['zip2'], $del_user_mail);
                    $del_user_mail = str_replace('[ADDRESS]', $data['address'], $del_user_mail);
                    $del_user_mail = str_replace('[EMAIL]', $data['email'], $del_user_mail);
                    $del_user_mail = str_replace('[PHONE_NO]', $data['phone_no1']."-".$data['phone_no2']."-".$data['phone_no3'], $del_user_mail);

                    $body = $del_user_mail;
                    $fromname="法人番号検索　管理システムよりお知らせ";
                    $fromaddress="info_opensite@navit-j.net";
                    $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);

		    //ログアウト処理
	            $db = $this->backend->getDb();
        	    $session_id = session_id();

		    if($this->session->get('id') != null) {

	            // ログアウト処理
        $sql = '
update
    user_session
set
    flg_delete = 1
where
    sessid ="'.$session_id.'"
limit 1';

	            $ret  = $db->query($sql);

        	    $id   = $this->session->get("id");
	            $name = $this->session->get("name");

	            // 管理者ログ出力
	            Opensite_AdminLogger::logger("logout_do", "", "", "");

	            // セッション削除
	            $this->session->destroy();
		    }

	            $db = $this->backend->getDb();
 
                  }else{
                    //メール送信NG
                    $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND NG] TO:<".$to.">");
                    $this->af->set('mail_send_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
                    //登録した購入お申込み履歴データの削除
                    if($inserted_serial_no != "" && $inserted_serial_no != null){
                      Opensite_Dao_Request::delete_request_point($inserted_serial_no);
                    }
                  }
	      }
            //バリデート(入力項目無し)
            $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Completewithdrew perform()");
	    return 'completewithdrew';	
	}
    }
  
  
}
?>
