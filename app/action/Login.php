<?php
/**
 *  Login.php
 *  ログイン
 *
 *  @copyright  (C)2010 Navit co.,ltd.
 *  @author     Tetsuo Nakanishi
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  login Form implementation.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */

class Opensite_Form_Login extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array();
}

/**
 *  login action implementation.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Action_Login extends Opensite_ActionClass
{
    /**
     *  preprocess of login Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
  
    function authenticate()
    {
        return null;
        // セッション有効性確認
        //return parent::authenticate();
    }
    
    
    function prepare()
    {
        return null;
    }

    /**
     *  login action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
		//
		//	以下、光通信アライアンスの例外対応で、cookieをセット
		//	2017-03-15 中村
		//
		if ( strpos($_SERVER['HTTP_REFERER'], "biz-compass.net") !== false || $_SERVER['SERVER_PORT'] == '8080' ){
			if ($_SERVER['SERVER_PORT'] == '8080'){
				$allcd = "9QrN0Ko9";
			}else{
				$allcd = "1d12NGnK";
			}
		}else if (isset($_COOKIE["opensite_alliance"])){
			$allcd = $_COOKIE["opensite_alliance"];
		}else{
			$allcd = "";
		}
		$alliance = "";
		if ($allcd){
			$all_rec = Opensite_Dao_Alliance::valid_alliance($allcd);
			if ( count($all_rec) ){
				$cookie_limit = time() + $all_rec[0]["cookie_days"] * 24 * 60 * 60;
				setcookie("opensite_alliance", $allcd, $cookie_limit, "/", ".navit-j.com");
				Opensite_Dao_Alliance::add_log($all_rec[0]["id"]);
				$alliance = $all_rec[0]["come_from"];
			}
		}
		$this->af->setApp("alliance", $alliance);
		//
		//	以上まで
		//
		
      //$this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Login!!");
        return 'login';
    }
}

?>
