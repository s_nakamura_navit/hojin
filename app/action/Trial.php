<?php
/**
 *  Trial.php
 */

/**
 *  Trial Form implementation.
 */
class Opensite_Form_Trial extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_act' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_corp_1' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_address_1' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_tel_1' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_corp_2' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_address_2' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_tel_2' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_corp_3' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_address_3' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_tel_3' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
        'in_secure_status' => array(
            'type' => VAR_TYPE_STRING,
            'required' => false
        ),
    );

}

/**
 *  Trial action implementation.
 */
class Opensite_Action_Trial extends Opensite_ActionClass
{
    function authenticate()
    {
	// Trialは認証の必要がないので
	// 親クラスのauthenticate()を無処理でオーバーライド
    }

    /**
     *  preprocess of Trial Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
	/*
        // フォームバリデーションを実行し、EthnaErrorオブジェクトを登録する
        $this->af->validate();

        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Trial prepare()");
	*/
        return null;
    }

    /**
     *  Trial action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        //ログインチェック
        //Opensite_Dao_Login::check_user_session();

        // セッション値取得
        $id   = $this->session->get("id");
        $name = $this->session->get("name");
        $user_id = $this->session->get("serial");
        
	//管理画面側からリストをインクルード
	$this->af->setApp("property_list",      $GLOBALS['hojin_request_property_list']);

	//直接アクセスされた時用のホーム遷移
	$act = $this->af->get("in_act");

	//依頼件数の上限
	$request_count = 3;

	//フォーム内容の変数格納
	$act = $this->af->get("in_act");
	if($this->af->get("in_secure_status") == "back"){
		//確認画面以降から戻ってきた場合はSESSION値を開放する
		foreach($this->session->get("trial_data") as $key => $item){
			$data[$key]['corp'] = $item['corp'];
			$this->af->setApp("data_corp_".$key,  $item['corp']);
			$data[$key]['address'] = $item['address'];
			$this->af->setApp("data_address_".$key,  $item['address']);
			$data[$key]['tel'] = $item['tel'];
			$this->af->setApp("data_tel_".$key,  $item['tel']);
		}
	}else{
		for($i = 1;$i <= $request_count;$i++){
			$data[$i]['corp'] = $this->af->get("in_corp_".$i);
			$this->af->setApp("data_corp_".$i,	$this->af->get("in_corp_".$i));
			$data[$i]['address'] = $this->af->get("in_address_".$i);
			$this->af->setApp("data_address_".$i,	$this->af->get("in_address_".$i));
			$data[$i]['tel'] = mb_convert_kana($this->af->get("in_tel_".$i),"a","UTF-8");
			$this->af->setApp("data_tel_".$i,	$data[$i]['tel']);
		}
	}

        // サービスマネージャ
        $fm = $this->backend->getManager("furu");

	$err_count = $this->ae->count();

	// バリデート
	$uninput_flg = true;
	for($i = 1;$i <= $request_count;$i++){
		if($data[$i]['corp'] != "" || $data[$i]['address'] != "" || $data[$i]['tel'] != ""){
			$uninput_flg = false;
			if($data[$i]['corp'] == ""){
				$err_count++;
				if($trial_check_err['uncolumn'] != ""){
					$trial_check_err['uncolumn'] .= "\n";
				}
				$trial_check_err['uncolumn'] .= "企業情報の「企業名」が入力されていません。";
				//$trial_check_err['uncolumn'] .= "企業情報".$i." の「企業名」が入力されていません。"; //20151021改修によりコメントアウト
			}
			if($data[$i]['tel'] == ""){
				if($trial_check_err['uncolumn'] != ""){
					$trial_check_err['uncolumn'] .= "\n";
				}
				$err_count++;
				$trial_check_err['uncolumn'] .= "企業情報の「電話番号」が入力されていません。";
				//$trial_check_err['uncolumn'] .= "企業情報".$i." の「電話番号」が入力されていません。"; //20151021改修によりコメントアウト
			}
/*
			if($data[$i]['address'] == ""){
				if($trial_check_err['uncolumn'] != ""){
					$trial_check_err['uncolumn'] .= "\n";
				}
				$err_count++;
				$trial_check_err['uncolumn'] .= "企業情報".$i." の「住所」が入力されていません。";
			}
*/
		}
	}
	if($uninput_flg){
		$err_count++;
		$trial_check_err['uninput'] = "企業情報が入力されていません。";
	}

        // エラーチェック
        if ($err_count > 0) {

            foreach($this->ae->getMessageList() as $item)
            {
                error_log((print_r($item,true)));
            }
            
            //入力値に不備ありのため戻す
	    $this->af->setApp("trial_check_err",	$trial_check_err);
	    $this->af->setApp("act",	"back");
            return 'index';
        }

	// 値のSESSION格納
	if($user_id == ""){
		//ログインしていない場合は、セッションスタートをする（対引継ぎのため）
		$ctl    =& Ethna_Controller::getInstance();
		$back   =  $ctl->getBackend();
		$back->session->start();
	}
	$this->session->set('trial_data',	$data);
        $this->af->setApp("act",	$act);

        // selectionバリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Trial perform()");

        return 'trial';
    }
}
?>
