<?php
/** action
 *  Trialone.php
 */

/**
 *  index Form implementation.
 */

include('Opensite_Trialone.php');

class Opensite_Form_Trialone extends Opensite_ActionForm
{
	/**
	 *  @access private
	 *  @var    array   form definition.
	 */
	var $form = array(
			'sc_name' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'sc_address' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'corp_id' => array(
					'type' => VAR_TYPE_INT,
					'required' => false
			),
			'in_act' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'pref_cd' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'city_cd' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			),
			'items' => array(
					'type' => array(VAR_TYPE_INT),
					'required' => false
			),
			'hit_type' => array(
					'type' => VAR_TYPE_STRING,
					'required' => false
			)
	);

}

/**
 *  index action implementation.
 */
class Opensite_Action_Trialone extends Opensite_ActionClass
{
	function authenticate()
	{
		// Indexは認証の必要がないので
		// 親クラスのauthenticate()を無処理でオーバーライド
	}

	/**
	 *  preprocess of index Action.
	 *
	 *  @access public
	 *  @return string    forward name(null: success.
	 *                                false: in case you want to exit.)
	 */
	function prepare()
	{
		return null;
	}

	/**
	 *  index action implementation.
	 *
	 *  @access public
	 *  @return string  forward name.
	 */
	function perform()
	{
		//ログインチェック
		//Opensite_Dao_Login::check_user_session();

		// セッション値取得
		$id   = $this->session->get("id");
		$name = $this->session->get("name");
		$user_id = $this->session->get("serial");

		//直接アクセスされた時用のホーム遷移
		$act = $this->af->get("in_act");
		if($act != "trialone"){
			return "index";
		}
		$this->af->setApp("corp_id",    $this->af->get("corp_id"));
		//フォーム内容の変数格納
		$corp_id = $this->af->get("corp_id");
		if(isset($corp_id)) {
			$db =& $this->backend->getDB();
			$select_table = Opensite_Trialone::getTrnInspect();


			$pref_code = $this->af->get("pref_cd");
			$this->af->setApp("pref_cd",	$pref_code);
			$city_code = $this->af->get("city_cd");
			$this->af->setApp("city_cd",	$city_code);
//			$table_name = $select_table . "_" . $pref_code;
			$table_name = $select_table;

			//企業詳細（付与属性項目の有無）
			$corp_detail = Opensite_Trialone::corpDetailUmu($table_name, $corp_id, $pref_code);

			$point_list = $GLOBALS['hojin_request_auto_property_unit_point_list'];

			//金額計算
			$total = 0;
			if($corp_detail["I_BUSINESS_CD_L_1"]=="○") {
				$total = $total + $point_list['business'];
			}
			if($corp_detail["I_FAX"]=="○") {
				$total = $total + $point_list['fax'];
			}
			if($corp_detail["I_LISTED_TYPE"]=="○") {
				$total = $total + $point_list['listed_type'];
			}
			if($corp_detail["I_EMPLOYEE_DIV"]=="○") {
				$total = $total + $point_list['employee_div'];
			}
			if($corp_detail["I_SALE_DIV"]=="○") {
				$total = $total + $point_list['sale_div'];
			}
			if($corp_detail["I_CAPITAL_DIV"]=="○") {
				$total = $total + $point_list['capital_div'];
			}
			if($corp_detail["I_FOUNDING_DT"]=="○") {
				$total = $total + $point_list['founding_dt'];
			}
			if($corp_detail["I_TEL"]=="○") {
				$total = $total + $point_list['tel'];
			}
/*
			if($corp_detail["I_DEPT_POSITION"]=="○") {
				$total = $total + $point_list['dept_position'];
			}
			if($corp_detail["I_DEPT_NAME"]=="○") {
				$total = $total + $point_list['dept_name'];
			}
*/			
			if($corp_detail["I_URL"]=="○") {
				$total = $total + $point_list['url'];
			}
			if($corp_detail["I_CORP_NUM"]=="○") {
				$total = $total + $point_list['corp_num'];
			}
			if($corp_detail["I_MAILADDR"]=="○") {
				$total = $total + $point_list['mailaddr'];
			}

			//消費税
			$tax = 1 + $GLOBALS['hojin_request_tax'];

			//消費税分
			$total = floor($total * $tax);

			$corp_detail["total"] = $total;


			//【SMTY】検索結果をsmへパス
			$this->af->setApp("corp_detail",	$corp_detail);
			$this->af->setApp("hit_type",	$this->af->get("hit_type"));
			$this->af->setApp("point_list",	$point_list);
			$this->af->setApp("sc_name",    $this->af->get("sc_name"));
			$this->af->setApp("sc_address",    $this->af->get("sc_address"));
			
			if ( is_array($this->af->get("items")) ){
				$this->af->setApp("items",    $this->af->get("items"));
			}
		}

		return 'trialone';
	}

}

?>
