<?php
/**
 *  RequestConfirm.php
 */

/**
 *  RequestConfirm Form implementation.
 */
class Opensite_Form_RequestConfirm extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_company_name'              => array('type' => VAR_TYPE_STRING, 'name' => '会社名',         'required' => false,),
        'in_zip1'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 3, 'max' => 3,),
        'in_zip2'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 4, 'max' => 4,),
        'in_address'                   => array('type' => VAR_TYPE_STRING, 'name' => '住所',           'required' => false,),
        'in_phone_no1'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no2'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no3'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号3',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_department_name'           => array('type' => VAR_TYPE_STRING, 'name' => '部署名',         'required' => false,),
        'in_post_name'                 => array('type' => VAR_TYPE_STRING, 'name' => '役職名',         'required' => false,),
        'in_contractor_lname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（姓）', 'required' => false,),
        'in_contractor_fname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（名）', 'required' => false,),
        'in_contractor_lkana'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名かな（姓）', 'required' => false,),
        'in_contractor_fkana'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名かな（名）', 'required' => false,),
        'in_email'                     => array('type' => VAR_TYPE_STRING, 'name' => 'メールアドレス', 'required' => false,),
        'in_point'                     => array('type' => VAR_TYPE_STRING, 'name' => 'ポイント購入', 'required' => false,),
        'in_payment'                   => array('type' => VAR_TYPE_STRING, 'name' => 'お支払い方法', 'required' => false,),
        'in_privacy'                   => array('type' => VAR_TYPE_STRING, 'name' => '利用規約の同意',  'required' => false, 'option' => array('1'=>'上記「入札なう利用規約」に同意します。 ')),
        'in_request_id'	               => array('type' => VAR_TYPE_INT, 'name' => '注文番号', 'required' => false,),
        'in_use_matching_phase'        => array('type' => VAR_TYPE_STRING, 'name' => '使用マッチング方法（自動付与用）', 'required' => false,),
        'in_use_property_str'          => array('type' => VAR_TYPE_STRING, 'name' => '選択項目（自動付与用）', 'required' => false,),
        'in_column_num_str'            => array('type' => VAR_TYPE_STRING, 'name' => '列番号（自動付与用）', 'required' => false,),
        'in_act'	               => array('type' => VAR_TYPE_STRING, 'name' => '処理段階', 'required' => false,),
    );
    
}

/**
 *  RequestConfirm action implementation.
 */
class Opensite_Action_RequestConfirm extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
        return parent::authenticate();
    }

    /**
     *  preprocess of RequestConfirm Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_RequestConfirm prepare()");
        return null;
    }

    /**
     *  RequestConfirm action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {

        Opensite_Dao_Login::check_user_session();

        // セッション情報取得
        $id = $this->session->get('id');
        $user_id = $this->session->get("serial");
        $name = $this->session->get("name");

	if($this->session->get("request_point_id") == ""){
            //入力値取得
            $data{'company_name'}           = $this->af->get('in_company_name');
            $data{'zip1'}                   = $this->af->get('in_zip1');      
            $data{'zip2'}                   = $this->af->get('in_zip2');
            $data{'address'}                = $this->af->get('in_address');
            $data{'phone_no1'}              = $this->af->get('in_phone_no1');
            $data{'phone_no2'}              = $this->af->get('in_phone_no2');            
            $data{'phone_no3'}              = $this->af->get('in_phone_no3');            
            $data{'department_name'}        = $this->af->get('in_department_name'); 
            $data{'post_name'}              = $this->af->get('in_post_name');            
            $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
            $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
            $data{'contractor_lkana'}       = $this->af->get('in_contractor_lkana');  
            $data{'contractor_fkana' }      = $this->af->get('in_contractor_fkana');  
            $data{'email'}                  = $this->af->get('in_email');
            $data{'point'}                  = $this->af->get('in_point');
            $data{'payment'}                = $this->af->get('in_payment');
            $data{'privacy'}                = $this->af->get('in_privacy');
            $data{'request_id'}             = $this->af->get('in_request_id');
	    if($this->af->get('in_use_matching_phase') != ""){
            	$data{'use_matching_phase'}     = $this->af->get('in_use_matching_phase');
            	$data{'use_property_str'}       = $this->af->get('in_use_property_str');
            	$data{'column_num_str'}         = $this->af->get('in_column_num_str');
	    }
            $data{'act'}		    = $this->af->get('in_act');

	    //注文番号を発行
            $data{'request_point_id'}       = ceil(microtime(true) * 1000);
        
	}else{
	    $data = Opensite_Dao_Request::get_request_point_data($this->session->get("request_point_id"));
	    //名称が違う
	    $data{'point'} = $data{'request_lot'};

	    $data{'request_id'} = $this->session->get("request_id");
	    $data{'request_point_id'} = $this->session->get("request_point_id");
	    $data{'act'} = "request_point";
	    if($this->session->get('use_matching_phase') != ""){
            	$data{'use_matching_phase'}     = $this->session->get('use_matching_phase');
            	$data{'use_property_str'}       = $this->session->get('use_property_str');
            	$data{'column_num_str'}         = $this->session->get('column_num_str');
	    }
	}

        $flg = Opensite_Dao_User::is_tos_consent($user_id);
        if(Ethna::isError($flg))
        {
            $this->af->setApp("error",$flg);
            return 'system_error';
        }
        
        $this->af->setApp('flg_tos_consent',$flg);
        
        if($_GET["back"] != "1"){
          
          
            // バリデート
            mb_regex_encoding("UTF-8");
	    /*
            if($data{'company_name'}==""){
              $this->ae->add("in_company_name", "会社名を入力してください", E_FORM_INVALIDVALUE);
            }
	    */
            if($data{'zip1'}==""){
              $this->ae->add("in_zip1", "郵便番号1を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'zip2'}==""){
              $this->ae->add("in_zip2", "郵便番号2を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'address'}==""){
              $this->ae->add("in_address", "住所を入力してください", E_FORM_INVALIDVALUE);
            }
	    /*
            if($data{'phone_no1'}==""){
              $this->ae->add("in_phone_no1", "電話番号1を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'phone_no2'}==""){
              $this->ae->add("in_phone_no2", "電話番号2を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'phone_no3'}==""){
              $this->ae->add("in_phone_no3", "電話番号3を入力してください", E_FORM_INVALIDVALUE);
            }
	    */
            //if($data{'department_name'}==""){
            //  $this->ae->add("in_department_name", "部署名を入力してください", E_FORM_INVALIDVALUE);
            //}
            //if($data{'post_name'}==""){
            //  $this->ae->add("in_post_name", "役職名を入力してください", E_FORM_INVALIDVALUE);
            //}            
            if($data{'contractor_lname'}==""){
              $this->ae->add("in_contractor_lname", "担当者名（姓）を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'contractor_fname'}==""){
              $this->ae->add("in_contractor_fname", "担当者名（名）を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'contractor_lkana'}==""){
              $this->ae->add("in_contractor_lkana", "担当者名かな（せい）を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'contractor_fkana'}==""){
              $this->ae->add("in_contractor_fkana", "担当者名かな（めい）を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'email'}==""){
              $this->ae->add("in_email", "メールアドレスを入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'point'} != ""){
		$data{'point'} = mb_convert_kana($data{'point'},n,"UTF-8");
		if(!preg_match("/^[0-9]+$/",$data{'point'})){
			$this->ae->add("in_point", "入力内容が正しくありません", E_FORM_INVALIDVALUE);
		}
	    }else{
              $this->ae->add("in_point", "購入ポイント数を入力してください", E_FORM_INVALIDVALUE);
            }
            if($data{'payment'}==""){
              $this->ae->add("in_payment", "支払い方法を選択して下さい", E_FORM_INVALIDVALUE);
            }

            // バリデート
            if ($this->af->validate() != 0) {
                //入力値に不備ありのため戻す          
		$this->af->set('back',  1);
        	//マッチング結果の引き継ぎ内容
        	$this->af->setApp('request_id',         $this->af->get('in_request_id'));
        	$this->af->setApp('act',                $this->af->get('in_act'));
	    	if($this->af->get('in_use_matching_phase') != ""){
		    $this->af->setApp('use_matching_phase',	$this->af->get('in_use_matching_phase'));
            	    $this->af->setApp('use_property_str',	$this->af->get('in_use_property_str'));
		    $this->af->setApp('use_property',       explode(",",$this->af->get('in_use_property_str')));
            	    $this->af->setApp('column_num_str',		$this->af->get('in_column_num_str'));
	    	}
                return 'request_registration';
            }else{
                // 入力値OK
               if($data['payment'] == "credit_card"){

		    //クレジットカード決済の場合は事前にDB登録
		    //DBエラーだった場合はシステムエラーに

		    $price_org = 2000 * $data['point'];//非課税20150227

                    $credit["url"]                  = "https://www.asjpayment.jp/cgi-bin/ps_payment.cgi";
                    $credit["admin"]                = "2894";
                    $credit["amount"]               = $price_org;
                    $credit["charge"]               = $data['request_point_id'];
                    $credit["name"]                 = $id;
                    $credit["mail"]                 = $data['email'];
                    $credit["request_id"]           = $data['request_id'];	//free2
                    $this->af->setApp('credit',  $credit);

		    if($this->session->get("request_point_id") == ""){

		    	//決済画面遷移後の引き継ぎように格納
		    	$this->session->set('request_id',		$data['request_id']);
			if($data['use_matching_phase'] != ""){
		    		$this->session->set('column_num_str',		$data['column_num_str']);
		    		$this->session->set('use_property_str',		$data['use_property_str']);
		    		$this->session->set('use_matching_phase',	$data['use_matching_phase']);
			}
		    	$this->session->set('request_point_id',		$data['request_point_id']);

		    	list($affected, $inserted_serial_no) = Opensite_Dao_Request::insert_request_point($user_id,$id,$data,0);
		    	if($affected == null || $affected == "" || $affected == 0 || $inserted_serial_no == ""){
            		    $this->af->setApp("title","システムエラー");
            		    $this->af->setApp("message","ポイント購入依頼情報登録エラー");
            		    return 'message';
			}
        	    }
                }    
            }
        }
        
	$this->af->setApp('data',  $data);
	$this->af->setApp('name',  $name);
        //バリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_RequestConfirm perform()");
        return 'request_confirm';
    }
}

?>
