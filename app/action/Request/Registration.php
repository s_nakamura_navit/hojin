<?php
/**
 *  RequestRegistration.php
 */

/**
 *  RequestRegistration Form implementation.
 */
class Opensite_Form_RequestRegistration extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'back'                         => array('type' => VAR_TYPE_INT,    'name' => '戻るステータス', 'required' => false,),
        'in_company_name'              => array('type' => VAR_TYPE_STRING, 'name' => '会社名',         'required' => false,),
        'in_zip1'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 3, 'max' => 3,),
        'in_zip2'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 4, 'max' => 4,),
        'in_address'                   => array('type' => VAR_TYPE_STRING, 'name' => '住所',           'required' => false,),
        'in_phone_no1'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no2'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no3'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号3',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_department_name'           => array('type' => VAR_TYPE_STRING, 'name' => '部署名',         'required' => false,),
        'in_post_name'                 => array('type' => VAR_TYPE_STRING, 'name' => '役職名',         'required' => false,),
        'in_contractor_lname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（姓）', 'required' => false,),
        'in_contractor_fname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（名）', 'required' => false,),
        'in_contractor_lkana'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名かな（姓）', 'required' => false,),
        'in_contractor_fkana'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名かな（名）', 'required' => false,),
        'in_email'                     => array('type' => VAR_TYPE_STRING, 'name' => 'メールアドレス', 'required' => false,),
        'in_point'                     => array('type' => VAR_TYPE_STRING, 'name' => 'ポイント購入', 'required' => false,),
        'in_payment'                   => array('type' => VAR_TYPE_STRING, 'name' => 'お支払い方法', 'required' => false,),
        'in_privacy'                   => array('type' => VAR_TYPE_STRING, 'name' => '利用規約の同意',  'required' => false, 'option' => array('1'=>'上記「入札なう利用規約」に同意します。 ')),
        'in_request_id'                => array('type' => VAR_TYPE_INT,    'name' => '注文番号', 'required' => false,),
        'in_use_matching_phase'        => array('type' => VAR_TYPE_STRING, 'name' => '使用マッチング方法（自動付与用）', 'required' => false,),
        'in_use_property_str'          => array('type' => VAR_TYPE_STRING, 'name' => '選択項目（自動付与用）', 'required' => false,),
        'in_column_num_str'            => array('type' => VAR_TYPE_STRING, 'name' => '列番号（自動付与用）', 'required' => false,),
        'in_act'		       => array('type' => VAR_TYPE_STRING, 'name' => '処理段階', 'required' => false,),
    	'sc_name'		       => array('type' => VAR_TYPE_STRING, 'required' => false,),
    	'sc_address'	       => array('type' => VAR_TYPE_STRING, 'required' => false,),
    	'corp_id'		       => array('type' => VAR_TYPE_STRING, 'required' => false,),
    	'pref_cd'		       => array('type' => VAR_TYPE_STRING, 'required' => false,),
    	'city_cd'		       => array('type' => VAR_TYPE_STRING, 'required' => false,),
    );

}

/**
 *  RequestRegistration action implementation.
 */
class Opensite_Action_RequestRegistration extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
        return parent::authenticate();
    }

    /**
     *  preprocess of RequestRegistration Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_RequestRegistration prepare()");
        return null;
    }

    /**
     *  RequestRegistration action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {

	Opensite_Dao_Login::check_user_session();

        //フォーム入力値
        $user_id = $this->session->get("serial");

	//自動付与依頼作業中にポイント購入を訪れて、かつクレカ決済の場合は、値引継ぎのために、ここでセッション値を渡しておく


        //クレジットカード決済用にセッション格納した内容の初期化
        $this->session->set('request_id',           "");
        $this->session->set('use_matching_phase',   "");
        $this->session->set('in_use_property_str',  "");
        $this->session->set('in_column_num_str',    "");
        $this->session->set('request_point_id',     "");

        $flg = Opensite_Dao_User::is_tos_consent($user_id);
        if(Ethna::isError($flg))
        {
            $this->af->setApp("error",$flg);
            return 'system_error';
        }

        $this->af->setApp('flg_tos_consent',$flg);
/*
echo "tel_value_line=".$this->af->get('in_tel_value_line');
echo "header_check=".$this->af->get('in_header_check');
echo "header_line=".$this->af->get('in_header_line');
echo "list_name=".$this->af->get('in_list_name');
echo "use_point=".$this->af->get('in_use_point');
echo "list_count=".$this->af->get('in_list_count');
echo "count_get=".$this->af->get('in_count_get');
echo "get_csv=".$this->af->get('in_get_csv');
echo "lv_check=".$this->af->get('in_lv_check');
echo "up_file_path=".$this->af->get('in_up_file_path');
*/
	//マッチング結果の引き継ぎ内容
	$this->af->setApp('request_id',   	$this->af->get('in_request_id'));
	if($this->af->get('in_use_matching_phase') != ""){
		$this->af->setApp('use_matching_phase', $this->af->get('in_use_matching_phase'));
		$this->af->setApp('use_property_str',   $this->af->get('in_use_property_str'));
		$this->af->setApp('use_property',   	explode(",",$this->af->get('in_use_property_str')));
		$this->af->setApp('column_num_str',  	$this->af->get('in_column_num_str'));
	}
	$this->af->setApp('act',   		$this->af->get('in_act'));


	$in_act = $this->af->get("in_act");
	if($in_act=="request_point_trialone") {
		$this->af->setApp('sc_name', $this->af->get('sc_name'));
		$this->af->setApp('sc_address', $this->af->get('sc_address'));
		$this->af->setApp('corp_id', $this->af->get('corp_id'));
		$this->af->setApp("pref_cd",	$this->af->get("pref_cd"));
		$this->af->setApp("city_cd",	$this->af->get("city_cd"));
	}

        //バリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_RequestRegistration perform()");
        return 'request_registration';
    }
}

?>
