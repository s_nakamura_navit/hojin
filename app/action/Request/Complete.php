<?php
/**
 *  RequestComplete.php
 */

/**
 *  RequestComplete Form implementation.
 */
class Opensite_Form_RequestComplete extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_company_name'              => array('type' => VAR_TYPE_STRING, 'name' => '会社名',         'required' => false,),
        'in_zip1'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 3, 'max' => 3,),
        'in_zip2'                      => array('type' => VAR_TYPE_STRING, 'name' => '郵便番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'min' => 4, 'max' => 4,),
        'in_address'                   => array('type' => VAR_TYPE_STRING, 'name' => '住所',           'required' => false,),
        'in_phone_no1'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号1',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no2'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号2',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_phone_no3'                 => array('type' => VAR_TYPE_STRING, 'name' => '電話番号3',      'required' => false,  'regexp' => '/^[0-9]+$/', 'max' => 5,),
        'in_department_name'           => array('type' => VAR_TYPE_STRING, 'name' => '部署名',         'required' => false,),
        'in_post_name'                 => array('type' => VAR_TYPE_STRING, 'name' => '役職名',         'required' => false,),
        'in_contractor_lname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（姓）', 'required' => false,),
        'in_contractor_fname'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名（名）', 'required' => false,),
        'in_contractor_lkana'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名かな（姓）',     'required' => false,),
        'in_contractor_fkana'          => array('type' => VAR_TYPE_STRING, 'name' => '担当者名かな（名）',     'required' => false,),
        'in_email'                     => array('type' => VAR_TYPE_STRING, 'name' => 'メールアドレス', 'required' => false,),
        'in_point'                     => array('type' => VAR_TYPE_STRING, 'name' => 'ポイント購入', 'required' => false,),
        'in_payment'                   => array('type' => VAR_TYPE_STRING, 'name' => '支払い方法', 'required' => false,),
        'in_privacy'                   => array('type' => VAR_TYPE_STRING, 'name' => '利用規約の同意',  'required' => false, 'option' => array('1'=>'上記「入札なう利用規約」に同意します。 ')),
        'in_request_point_id'          => array('type' => VAR_TYPE_INT, 'name' => 'ポイント注文番号',  'required' => false,),
        'in_request_id'                => array('type' => VAR_TYPE_INT, 'name' => '注文番号',  'required' => false,),
        'in_use_matching_phase'        => array('type' => VAR_TYPE_STRING, 'name' => '使用マッチング方法（自動付与用）', 'required' => false,),
        'in_use_property_str'          => array('type' => VAR_TYPE_STRING, 'name' => '選択項目（自動付与用）', 'required' => false,),
        'in_column_num_str'            => array('type' => VAR_TYPE_STRING, 'name' => '列番号（自動付与用）', 'required' => false,),
        'in_act'		       => array('type' => VAR_TYPE_STRING, 'name' => '処理段階', 'required' => false,),
        'admin'				=> array('type' => VAR_TYPE_INT, 'name' => 'asj管理者番号', 'required' => false,),
        'amount'		       => array('type' => VAR_TYPE_INT, 'name' => 'asj決済金額', 'required' => false,),
        'charge'		       => array('type' => VAR_TYPE_INT, 'name' => 'asj伝票番号', 'required' => false,),
        'mail'		       		=> array('type' => VAR_TYPE_STRING, 'name' => 'asjメールアドレス', 'required' => false,),
        'request_id'		       => array('type' => VAR_TYPE_INT, 'name' => 'asj処理注文番号', 'required' => false,),
        'result'		       => array('type' => VAR_TYPE_INT, 'name' => 'asj決済結果', 'required' => false,),
    );

}

/**
 *  RequestComplete action implementation.
 */
class Opensite_Action_RequestComplete extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
        return parent::authenticate();
    }

    /**
     *  preprocess of RequestComplete Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_RequestComplete prepare()");
        return null;
    }

    /**
     *  RequestComplete action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {

        Opensite_Dao_Login::check_user_session();

        // セッション情報取得
        $id = $this->session->get('id');
        $user_id = $this->session->get('serial');
		$name = $this->session->get('name');

		if($this->af->get('admin') == 2894 && $this->af->get('result') == 0){
		    if($this->session->get("request_point_id") == $this->af->get('charge')){
		    	$data = Opensite_Dao_Request::get_request_point_data($this->session->get("request_point_id"));
		    	//名称が違う
	            $data{'point'} = $data{'request_lot'};

	            $data{'request_point_id'} = $this->session->get("request_point_id");
				if($this->session->get("request_id") != ""){
	            	$data{'request_id'} = $this->session->get("request_id");
			    	if($this->session->get("use_matching_phase") != ""){
			    		$data{'use_matching_phase'} = $this->session->get("in_use_matching_phase");
						$data{'use_property'} = explode(",",$this->session->get("in_use_property_str"));
	            	    $data{'column_num_str'} = $this->session->get("in_column_num_str");
			    	}
	            	$data{'act'} = "request_point";
				}

			//引き継ぎ項目格納
		    }else{
			$this->af->setApp('db_regist_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
			return 'request_complete';
		    }

		} else {
	        //入力値取得
	        $data{'company_name'}           = $this->af->get('in_company_name');
	        $data{'zip1'}                   = $this->af->get('in_zip1');
	        $data{'zip2'}                   = $this->af->get('in_zip2');
	        $data{'address'}                = $this->af->get('in_address');
	        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
	        $data{'phone_no2'}              = $this->af->get('in_phone_no2');
	        $data{'phone_no3'}              = $this->af->get('in_phone_no3');
	        $data{'department_name'}        = $this->af->get('in_department_name');
	        $data{'post_name'}              = $this->af->get('in_post_name');
	        $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');
	        $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');
	        $data{'contractor_lkana'}       = $this->af->get('in_contractor_lkana');
	        $data{'contractor_fkana'}       = $this->af->get('in_contractor_fkana');
	        $data{'email'}                  = $this->af->get('in_email');
	        $data{'point'}                  = $this->af->get('in_point');
	        $data{'payment'}                = $this->af->get('in_payment');
	        $data{'privacy'}                = $this->af->get('in_privacy');
			$data{'request_point_id'}       = $this->af->get('in_request_point_id');
			$data{'request_id'}             = $this->af->get('in_request_id');
			if($this->af->get('in_use_matching_phase') != ""){
	        	$data{'use_matching_phase'}     = $this->af->get('in_use_matching_phase');
				$data{'use_property'} = explode(",",$this->af->get("in_use_property_str"));
	        	$data{'column_num_str'}         = $this->af->get('in_column_num_str');
			}
			$data{'act'}			= $this->af->get('in_act');

		}

        $flg = Opensite_Dao_User::update_tos_consent($user_id);
        if(Ethna::isError($flg))
        {
            $this->af->setApp("error",$flg);
            return 'system_error';
        }

        $this->af->setApp('flg_tos_consent',$flg);

            $affected = "";
            $inserted_serial_no = "";
            // バリデート
            if ($this->af->validate() != 0) {
                //入力値に不備ありのため戻す
                return 'request_registration';
            }else{
                // 入力値OK

                $flg = Opensite_Dao_User::is_tos_consent($user_id);
                if(Ethna::isError($flg))
                {
                    $this->af->setApp("error",$flg);
                    return 'system_error';
                }

		$err_count = 0;

		//名前かなをmst_userに登録
                $res = Opensite_Dao_Request::update_mst_user_name_kana($user_id,$data);
                if(Ethna::isError($res))
                {
                    $this->af->setApp("title","システムエラー");
                    $this->af->setApp("message","ポイント購入依頼情報登録エラー");
                    return 'message';
                }

                //登録日時セット
                $date_now = date("Y-m-d H:i:s");
                $data{'updated'} = $date_now;
                $data{'created'} = $date_now;

                //購入ポイント情報の作成
                $lot = $data{'point'};
                $point = number_format($data{'point'} * 2000);
				$price_org = $data{'point'} * 2000;//非課税20150227
				$price = number_format($price_org);

                //DBへ登録(購入依頼履歴)
		if($data['payment'] == "bank_transfer"){
                  list($affected, $inserted_serial_no) = Opensite_Dao_Request::insert_request_point($user_id, $id, $data, 0);
                  if($affected == null || $affected == "" || $affected == 0 || $inserted_serial_no == ""){
		    $err_count++;
		  }
		}else if($data['payment'] == "credit_card"){	//条件をもっと追加する
                  //クレジットカード決済の場合ユーザーにポイント付与する
		  $use_point = $data['point'] * 2000;
                  $res = Opensite_Dao_Mypage::use_point($id,$user_id,$use_point,$data['request_point_id'],3);
                  if(Ethna::isError($res)){
		    $err_count++;
                  }else{
		    //注文履歴を処理完了に
		    $flg_process = 1;
		    $flg_delete = 0;
		    $res = Opensite_Dao_Request::update_request_point($data['request_point_id'],$flg_process,$flg_delete);
		    if(Ethna::isError($res)){
		      $err_count++;
                    }
		  }
		}else{
		  $this->af->set('db_regist_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
		  return 'request_complete';
		}

		if($err_count > 0){
                  //エラー
                  $this->af->set('db_regist_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
                }else{
                  $this->af->set('db_regist_result','1');
                  //DB登録OK

                  //宛先
                  $to = $data{'email'};
                  //件名
                  $subject="【ナビットポイント】購入 確認メール";
                  //差出人
                  $fromname="ナビットポイント 事務局";
                  $fromaddress="info_opensite@navit-j.net";

                  //本文をファイルから読み込み
                  //ファイル名
		  if($data['payment'] == "bank_transfer"){
                  	$req_p_mail_file = BASE."/mail/request_point_bank.txt";
			$payment = "銀行振込";
		  }else if($data['payment'] == "credit_card"){
			$req_p_mail_file = BASE."/mail/request_point_card.txt";
			$payment = "クレジットカード決済";
		  }
                  //ファイル読み込み
                  mb_language('Japanese');
                  $req_p_mail = file_get_contents($req_p_mail_file);
                  $req_p_mail = mb_convert_encoding($req_p_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
                  //読み込んだメール内の変数に値をセット
//echo "aaa".$data['request_id'];
                  $req_p_mail = str_replace('[REQUEST_ID]', /*$data['request_id']*/ $data['request_point_id'], $req_p_mail);
                  $req_p_mail = str_replace('[COMPANY_NAME]', $data['company_name'], $req_p_mail);
                  $req_p_mail = str_replace('[CONTRACTOR_NAME]', $data['contractor_lname']."　".$data['contractor_fname'], $req_p_mail);
                  $req_p_mail = str_replace('[LOT]', $lot, $req_p_mail);
                  $req_p_mail = str_replace('[POINT]', $point, $req_p_mail);
                  $req_p_mail = str_replace('[PAYMENT]', $payment, $req_p_mail);
                  $req_p_mail = str_replace('[PRICE]', $price, $req_p_mail);

                  //本文
                  $body = $req_p_mail;

                  $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);
                  if($return_flag) {
                    //メール送信OK
                    $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND OK] TO:<".$to.">");
                    $this->af->set('mail_send_result','1');

                    //webmasterへのメール通知
                    //$to="webmaster@navit-j.com";
                    //$to="uemura@navit-j.com";
                    $to = array(array('btobmarketing@navit-j.com','送信1'),array('webmaster@navit-j.com','送信2'));
		    //件名
                    $subject="【通知】ナビットポイント購入 通知";
		    //ファイル名
                    $req_p_mail_file = BASE."/mail/request_point_for_master.txt";
		    //ファイル読み込み
                    mb_language('Japanese');
                    $req_p_mail = file_get_contents($req_p_mail_file);
                    $req_p_mail = mb_convert_encoding($req_p_mail, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
		    //読み込んだメール内の変数に値をセット
                    $req_p_mail = str_replace('[REQUEST_ID]', /*$data['request_id']*/ $data['request_point_id'], $req_p_mail);
                    $req_p_mail = str_replace('[ID]', $id, $req_p_mail);
                    $req_p_mail = str_replace('[COMPANY_NAME]', $data['company_name'], $req_p_mail);
                    $req_p_mail = str_replace('[DEPARTMENT_NAME]', $data['department_name'], $req_p_mail);
                    $req_p_mail = str_replace('[POST_NAME]', $data['post_name'], $req_p_mail);
                    $req_p_mail = str_replace('[CONTRACTOR_NAME]', $data['contractor_lname']."　".$data['contractor_fname'], $req_p_mail);
                    $req_p_mail = str_replace('[ZIP]', $data['zip1']."-".$data['zip2'], $req_p_mail);
                    $req_p_mail = str_replace('[ADDRESS]', $data['address'], $req_p_mail);
                    $req_p_mail = str_replace('[EMAIL]', $data['email'], $req_p_mail);
                    $req_p_mail = str_replace('[LOT]', $lot, $req_p_mail);
                    $req_p_mail = str_replace('[POINT]', $point, $req_p_mail);
                    $req_p_mail = str_replace('[PAYMENT]', $payment, $req_p_mail);
                    $req_p_mail = str_replace('[PRICE]', $price, $req_p_mail);

                    $body = $req_p_mail;
                    $fromname="ナビットポイント　管理システムよりお知らせ";
                    $fromaddress="info_opensite@navit-j.net";
                    $return_flag = Opensite_Mail::mailsender($to,$subject,$body,$fromname,$fromaddress);

                  }else{
                    //メール送信NG
                    $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Mail::mailsender[MAIL SEND NG] TO:<".$to.">");
                    $this->af->set('mail_send_result','-1');//ビュー側で受け取りエラー発生した旨を判断する
                    //登録した購入依頼履歴データの削除
                    if($inserted_serial_no != "" && $inserted_serial_no != null){
                      Opensite_Dao_Request::delete_request_point($inserted_serial_no);
                    }
                  }
                }
            }

	    // エスケープ有
	    $this->af->setApp('data',  $data);
	    $this->af->setApp('name',  $name);

            //バリデート(入力項目無し)
            $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_RequestComplete perform()");
            return 'request_complete';
    }


}
?>
