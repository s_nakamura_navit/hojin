<?php
/**
 *  Landing.php
 */

/**
 *  Landing Form implementation.
 */
class Opensite_Form_Landing extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        
      
    );
    
}

/**
 *  Landing action implementation.
 */
class Opensite_Action_Landing extends Opensite_ActionClass
{
    function authenticate()
    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
    }

    /**
     *  preprocess of Landing Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Landing prepare()");
        return null;
    }

    /**
     *  Landing action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // selectionフォーム入力値(入力項目無し)
        
        // selectionバリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Landing perform()");
        return 'index';
    }
}

?>
