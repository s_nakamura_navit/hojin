<?php
/**
 *  Thanks.php
 */

/**
 *  Thanks Form implementation.
 */
class Opensite_Form_Thanks extends Opensite_ActionForm
{
    /**
     *  @access private
     *  @var    array   form definition.
     */
    var $form = array(
        'in_lv_check' => array(
            'type' => array(VAR_TYPE_STRING),
            'required' => true
        ),
        'in_tel_value_line' => array(
            'type' => VAR_TYPE_INT,
            'required' => true
        ),
        'in_header_check' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
        'in_header_line' => array(
            'type' => VAR_TYPE_INT,
            'required' => true
        ),
        'in_list_name' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
        'in_file_path' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
        'in_get_csv' => array(
            'type' => VAR_TYPE_STRING,
            'required' => true
        ),
    );
    
}

/**
 *  Thanks action implementation.
 */
class Opensite_Action_Thanks extends Opensite_ActionClass
{
//    function authenticate()
//    {
//        return null;
        // セッション有効性確認
//        return parent::authenticate();
//    }

    /**
     *  preprocess of Thanks Action.
     *
     *  @access public
     *  @return string    forward name(null: success.
     *                                false: in case you want to exit.)
     */
    function prepare()
    {
        // フォームバリデーションを実行し、EthnaErrorオブジェクトを登録する
        $this->af->validate();

        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Thanks prepare()");
        return null;
    }

    /**
     *  Thanks action implementation.
     *
     *  @access public
     *  @return string  forward name.
     */
    function perform()
    {
        // セッション値取得
        $id   = $this->session->get("id");
        $name = $this->session->get("name");
        $user_id = $this->session->get("serial");
        
        // Fax送信マネージャ
        $gm = $this->backend->getManager("geo");

        // 初期化
        $tel_value_line	   = $this->af->get("in_tel_value_line");
        $header_check	   = $this->af->get("in_header_check");
        $header_line	   = $this->af->get("in_header_line");
        $lv_check	   = $this->af->get("in_lv_check");
        $list_name	   = $this->af->get("in_list_name");
        $file_path	   = $this->af->get("in_file_path");
        $get_csv	   = $this->af->get("in_get_csv");
        //todo:// getHeaderColumns() でも実装して項目名リストを持ってきて比較する
        
//        if($data['insert'] != "{{テスト}}"){
//            $this->ae->add("insert", "宛先リストにない項目名です", E_Opensite_COMMON);
//        }
        // エラーチェック
/*
        if ($this->ae->count() > 0) {

            foreach($this->ae->getMessageList() as $item)
            {
                error_log((print_r($item,true)));
            }
            
            //echo print_r($this->af->validate(),true);
            //echo print_r($this->ae->getMessageList(),true);
	    

            $this->af->setApp("list", $this->af->get("in_list"));
            $this->af->setApp("count", $count);
            
            //入力値に不備ありのため戻す          
            return 'statement';
        }
*/
	//ファイルの作成
	//CSVダウンロード
	$records = $gm->getCsv($file_path,$header_check,$header_line,$tel_value_line,$lv_check);
        header("Content-Type: application/octet-stream");
        header("Content-Disposition: attachment; filename=".date("Ymd")."_.csv");
        print $records;

        // Todo:// ポイント不足はバリデーションに変更？
        // Todo:// ポイントが足りない場合のページ遷移追加

/*
        // FAX情報登録
        $res = $gm->regist($user_id,$id);

        // 登録エラー
        if (Ethna::isError($res)) {
            $this->af->setApp("title","システムエラー");
            $this->af->setApp("message",$res->getMessage()."(".$res->getCode().")");
            return 'message';
        }
*/
        
        //die();

        //CSV取得用
        $this->af->setApp("tel_value_line",     $this->af->get("in_tel_value_line"));
        $this->af->setApp("header_check",       $this->af->get("in_header_check"));
        $this->af->setApp("header_line",        $this->af->get("in_header_line"));
        $this->af->setApp("file_path",          $this->af->get("in_file_path"));  
        $this->af->setApp("lv_check",           $this->af->get("in_lv_check"));  

        // セッションに値を保存
        $this->session->set("fax_id",		$gm->fax_id);
        $this->session->set("fax_count",	$gm->count);
        $this->session->set("datetime",		$this->af->get("datetimepicker"));
        //die("");
        //$gm->send();
        //$gm->sendFax($gm->fax_id,$gm->datetime);
        //die("");
		
        // selectionバリデート(入力項目無し)
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_Action_Thanks perform()");
        return 'thanks';
    }
}

?>
