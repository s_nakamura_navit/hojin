<?php
// vim: foldmethod=marker
/**
 *  Opensite_ActionClass.php
 *
 *  @author     {$author}
 *  @package    Opensite
 *  @version    $Id$
 */

// {{{ Opensite_ActionClass
/**
 *  action execution class
 *
 *  @author     {$author}
 *  @package    Opensite
 *  @access     public
 */
class Opensite_ActionClass extends Ethna_ActionClass
{
    /**
    *  セッション有効性確認
    *
    *  @access public
    *  @return string  Forward name.
    *                  (null if no errors. false if we have something wrong.)
    */
    function authenticate()
    {
    	$ctl    =& Ethna_Controller::getInstance();
    	$logger =  $ctl->getLogger();
    	if ( !$this->session->isStart() ) {
    		$logger->log(LOG_DEBUG, "未ログインのためログイン前トップへ遷移");
    		return 'index';
    	}else{
    		$session_id = session_id();
    		$logger->log(LOG_DEBUG, "SESSION_ID : ".$session_id);
    		$db = $this->backend->getDB();
    		$db->db->debug = false;

    		$sql  = '
    select
        updated
    from
        user_session
    where
        sessid ="'.$session_id.'"
    and flg_delete = 0
    limit 1';
    		$ret  = $db->query($sql);
    		$row  = $ret->fetchRow();
    		$logger->log(LOG_DEBUG, "SESSUPDATED : ".$row{"updated"});

    		$sql  = '
    select
        timediff(now(), "'.$row{"updated"}.'") as diff';
    		$ret  = $db->query($sql);
    		$row  = $ret->fetchRow();
    		list($hour, $min, $sec) = split(":", $row{"diff"});
    		$elapsed = ($hour * 60 * 60) + ($min * 60) + $sec;
    		$logger->log(LOG_DEBUG, "ELAPSED     : ".$elapsed."sec");

    		$session_out_time = 60 * CONF_SESSION_TIMEOUT;
    		if($elapsed > $session_out_time){
    			// セッションタイムアウト
    			$sql = '
    update
        user_session
    set
        flg_delete = 1
    where
        sessid ="'.$session_id.'"
    limit 1';
    			$ret  = $db->query($sql);
    			$this->session->destroy();
    			$logger->log(LOG_DEBUG, "セッション削除");
    			return 'index';

    		} else {
    			// セッション延長
    			$sql = '
    update
        user_session
    set
        updated = now()
    where
        sessid ="'.$session_id.'"
    limit 1';
    			$ret  = $db->query($sql);
    			$logger->log(LOG_DEBUG, "セッション延長");
    		}
    	}
    }

    /**
     *  Preparation for executing action. (Form input check, etc.)
     *
     *  @access public
     *  @return string  Forward name.
     *                  (null if no errors. false if we have something wrong.)
     */
    function prepare()
    {
        return parent::prepare();
    }

    /**
     *  execute action.
     *
     *  @access public
     *  @return string  Forward name.
     *                  (we does not forward if returns null.)
     */
    function perform()
    {
        return parent::perform();
    }
}
// }}}

?>
