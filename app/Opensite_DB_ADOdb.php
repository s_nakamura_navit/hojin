<?php
/**
 *  Opensite_DB_ADOdb.php
 *  ADO DB 継承モデル
 *  Ethnaに実装されていない ADOdbインターフェイスを追加する
 *
 *  @copyright  (C)2010 Navit co.,ltd.
 *  @author     Tetsuo Nakanishi
 *  @package    Opensite
 *  @version    $Id$
 */
require_once 'Ethna/class/DB/Ethna_DB_ADOdb.php';

/**
 *  Opensite DB_ADOdb Class.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_DB_ADOdb extends Ethna_DB_ADOdb
{
    /**
     *  getInsertId
     *  直近で生成されたIDを取得
     *
     *  @access public
     *  @return integer ID値
     */
    function getInsertId()
    {
        return $this->db->Insert_ID();
    }

    /**
     *  rollback_AutoIncrement
     *  AutoIncrement巻き戻し
     *
     *  @access public
     *  @param  string tablename  テーブル名称
     *          intger id         現在の autoincrement値
     *  @return boolean
     */
    function rollback_AutoIncrement($tablename, $id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDB();

        $sql = 'ALTER TABLE '.$tablename.' AUTO_INCREMENT = ?';
        $id--;
        $db->query($sql, array($id));

        return true;
    }

    /**
     *  SelectLimit
     *  指定範囲の検索結果オブジェクトを取得する
     *
     *  @access public
     *  @params string  sql       SQL構文
     *  @params numeric fetch_row 取得行数
     *  @params numeric start_row 開始行数
     *  @params array   cond      抽出条件配列
     *  @return object  result    返戻オブジェクト
     */
    function SelectLimit($sql, $fetch_row, $start_row, $cond)
    {
        return $this->db->SelectLimit($sql, $fetch_row, $start_row, $cond);
    }
}
?>
