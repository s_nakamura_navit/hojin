<?php
/**
 *  Opensite_MasterManager.php
 *  マスタマネージャクラス
 *
 *  @copyright  (C)2010 Navit co.,ltd.
 *  @author     Tetsuo Nakanishi
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  Opensite MasterManager Class.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_MasterManager
{
    /**
     *  get_master
     *  DBマスタデータ取得
     *
     *  $object[<id>] = <name> の配列で返す
     *  <id> は $id の名称で
     *  <name> は $name の名称で割り当てる
     *
     *  @access protected
     *  @param  string  master_name   マスタ名
     *                  id            マスタID
     *                  name          マスタ
     *  @return array   object
     */
    function get_master($master_name, $id, $name)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDB();

        if(! $master_name){
            return Ethna::raiseNotice('システムエラー:マスタ名が設定されていません', E_Opensite_COMMON);
        }
        if(! $id){
            return Ethna::raiseNotice('システムエラー:マスタIDが設定されていません', E_Opensite_COMMON);
        }
        if(! $name){
            return Ethna::raiseNotice('システムエラー:マスタが設定されていません', E_Opensite_COMMON);
        }

        // 結果格納用ハッシュ
        $master = array();

        // マスタ取得
        $i = 0;
        $sql  = '
select
    '.$id.'   id,
    '.$name.' name
from
    '.$master_name.'
where
    flg_delete = 0';

        $result = $db->query($sql);
        while ($data[$i] = $result->fetchRow()) {
            $master{$data[$i]{"id"}} = $data[$i]{"name"};
            $i++;
        }

        return $master;
    }

// 20121215 added by sano
    /**
     *  get_master_condition
     *  DBマスタデータ取得
     *
     *  $object[<id>] = <name> の配列で返す
     *  <id> は $id の名称で
     *  <name> は $name の名称で割り当てる
     *
     *  @access protected
     *  @param  string  master_name   マスタ名
     *                  id            マスタID
     *                  name          マスタ
     *  @return array   object
     */
    function get_master_condition($master_name, $id, $name, $where, $order)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDB();

        if(! $master_name){
            return Ethna::raiseNotice('システムエラー:マスタ名が設定されていません', E_Opensite_COMMON);
        }
        if(! $id){
            return Ethna::raiseNotice('システムエラー:マスタIDが設定されていません', E_Opensite_COMMON);
        }
        if(! $name){
            return Ethna::raiseNotice('システムエラー:マスタが設定されていません',   E_Opensite_COMMON);
        }

        // 結果格納用ハッシュ
        $master = array();

        // マスタ取得
        $i = 0;
        $sql  = '
select
    '.$id.'   id,
    '.$name.' name
from
    '.$master_name.'
where
    flg_delete = 0
';

        if($where != "") {
            $sql.=  ' and ' . $where;
        }

        if($order != "") {
            $sql.=  ' order by ' . $order;
        }

        $result = $db->query($sql);
        while ($data[$i] = $result->fetchRow()) {
            $master{$data[$i]{"id"}} = $data[$i]{"name"};
            $i++;
        }

        return $master;
    }
// 20121215 added by sano

// 20121215 added by sano
    /**
     *  get_master_oderby
     *  DBマスタデータ取得
     *
     *  $object[<id>] = <name> の配列で返す
     *  <id> は $id の名称で
     *  <name> は $name の名称で割り当てる
     *
     *  @access protected
     *  @param  string  master_name   マスタ名
     *                  id            マスタID
     *                  name          マスタ
     *  @return array   object
     */
    function get_master_orderby($master_name, $id, $name, $orderby_name)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDB();

        if(! $master_name){
            return Ethna::raiseNotice('システムエラー:マスタ名が設定されていません', E_Opensite_COMMON);
        }
        if(! $id){
            return Ethna::raiseNotice('システムエラー:マスタIDが設定されていません', E_Opensite_COMMON);
        }
        if(! $name){
            return Ethna::raiseNotice('システムエラー:マスタが設定されていません',   E_Opensite_COMMON);
        }
        if(! $orderby_name){
            return Ethna::raiseNotice('システムエラー:順序項目が設定されていません', E_Opensite_COMMON);
        }

        // 結果格納用ハッシュ
        $master = array();

        // マスタ取得
        $i = 0;
        $sql  = '
select
    '.$id.'   id,
    '.$name.' name
from
    '.$master_name.'
where
    flg_delete = 0
order by
    '.$orderby_name;

        $result = $db->query($sql);
        while ($data[$i] = $result->fetchRow()) {
            $master{$data[$i]{"id"}} = $data[$i]{"name"};
            $i++;
        }

        return $master;
    }
// 20121215 added by sano

    /**
    *  get_company_master
    *  所属別DBマスタデータ取得
    *
    *  $object[<id>] = <name> の配列で返す
    *  <id> は $id の名称で
    *  <name> は $name の名称で割り当てる
    *
    *  @access protected
    *  @param  string  master_name   マスタ名
    *                  id            マスタID
    *                  name          マスタ
    *  @return array   object
    */
    function get_company_master($master_name, $id, $name, $company)
    {
    	$ctl    =& Ethna_Controller::getInstance();
    	$logger =  $ctl->getLogger();
    	$back   =  $ctl->getBackend();
    	$db     =  $back->getDB();

    	if(! $master_name){
    		return Ethna::raiseNotice('システムエラー:マスタ名が設定されていません', E_Opensite_COMMON);
    	}
    	if(! $id){
    		return Ethna::raiseNotice('システムエラー:マスタIDが設定されていません', E_Opensite_COMMON);
    	}
    	if(! $name){
    		return Ethna::raiseNotice('システムエラー:マスタが設定されていません', E_Opensite_COMMON);
    	}

    	// 結果格納用ハッシュ
    	$master = array();

    	// マスタ取得
    	$i = 0;
    	$sql  = '
    select
        '.$id.'   id,
        '.$name.' name
    from
        '.$master_name.'
    where
        company    = '.$company.'
    ';

    	$result = $db->query($sql);
    	while ($data[$i] = $result->fetchRow()) {
    		$master{$data[$i]{
    "id"}
    		} = $data[$i]{
    "name"};
    			$i++;
    	}

    	return $master;
    }

// 20121215 added by sano
    /**
    *  get_master_where_condition
    *  条件つきのDBマスタデータ取得
    *
    *  $object[<id>] = <name> の配列で返す
    *  <id> は $id の名称で
    *  <name> は $name の名称で割り当てる
    *
    *  @access protected
    *  @param  string  master_name   マスタ名
    *                  id            マスタID
    *                  name          マスタ
    *                  where_condition 選択条件
    *  @return array   object
    */
    function get_master_where_condition($master_name, $id, $name, $where_condition)
    {
    	$ctl    =& Ethna_Controller::getInstance();
    	$logger =  $ctl->getLogger();
    	$back   =  $ctl->getBackend();
    	$db     =  $back->getDB();

    	if(! $master_name){
    		return Ethna::raiseNotice('システムエラー:マスタ名が設定されていません', E_Opensite_COMMON);
    	}
    	if(! $id){
    		return Ethna::raiseNotice('システムエラー:マスタIDが設定されていません', E_Opensite_COMMON);
    	}
    	if(! $name){
    		return Ethna::raiseNotice('システムエラー:マスタが設定されていません', E_Opensite_COMMON);
    	}
    	if(! $where_condition){
    		return Ethna::raiseNotice('システムエラー:抽出条件が設定されていません', E_Opensite_COMMON);
    	}


    	// 結果格納用ハッシュ
    	$master = array();

    	// マスタ取得
    	$i = 0;
    	$sql  = '
    select
        '.$id.'   id,
        '.$name.' name
    from
        '.$master_name.'
    where
        '.$where_condition.'
    and flg_delete = 0';

    	$result = $db->query($sql);
    	while ($data[$i] = $result->fetchRow()) {
    		$master{$data[$i]{
    "id"}
    		} = $data[$i]{
    "name"};
    			$i++;
    	}


    		$master{$data[$i]{"id"}} = $data[$i]{"name"};

    	return $master;
    }
// 20121215 added by sano


// 20121220 added by sano 
    /**
    *  get_master_sendmail
    *  条件つきのDBマスタデータ取得
    *
    *  $object[<id>] = <name> の配列で返す
    *  <id> は $id の名称で
    *  <name> は $name の名称で割り当てる
    *
    *  @access protected
    *  @param  string  master_name   マスタ名
    *                  id            マスタID
    *                  name          マスタ
    *                  where_name    マスタ区分
    *                  where_condition マスタ区分
    *  @return array   object
    */
    function get_master_sendmail($condition)
    {
    	$ctl    =& Ethna_Controller::getInstance();
    	$logger =  $ctl->getLogger();
    	$back   =  $ctl->getBackend();
    	$db     =  $back->getDB();

    	if(! $condition){
    		return Ethna::raiseNotice('条件が設定されていません', E_Opensite_COMMON);
    	}

    	// 結果格納用ハッシュ
    	$master = array();

    	// マスタ取得
    	$i = 0;
    	$sql  = '
    select
        *
    from
        ENQUETE_USER
    where
        flg_delete=0
    and role > '.ROLE_NORMAL.'
    and '.$condition;
    	$result = $db->getAll($sql);

    	return $result;
    }
// 20121220 added by sano 


    /**
     *  get_tsv
     *  TSVデータ取得
     *
     *  $object[<レコード>] = <値> の一次元配列又は
     *  $object[<レコード>][<カラム>] = <値> の二次元配列で返す
     *  一次元の場合、1カラム目がレコード, 2カラム目が値に強制配置される
     *  0カラム目は無視されるので注意
     *
     *  @access protected
     *  @param  string  master_name     マスタ名
     *          int     array_dimention 配列の次元値(1or2)
     *  @return array   object
     */
    function get_master_tsv($master_name, $array_dimention = 2, $key_colunm = "")
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDB();

        if(! $master_name){
            return Ethna::raiseNotice('システムエラー:マスタ名が設定されていません', E_Opensite_COMMON);
        }
        $dir       =  $ctl->getDirectory('etc');
        $file_path = $dir."/".$master_name.".tsv";

        // TSVファイル読み込み
        $fp   = fopen($file_path, "r");
        if(!$fp){
            return $tsv;
        }

        $tsv  = array();
        while($buff = fgetcsv($fp, 100000, DELIMITTER_TAB)){
            $fields = count($buff);
            $line   = array();
            // # 行はスキップ
            if($buff[0] != "#"){
                if($array_dimention == 1){
                    // 一次元配列
                    $tsv{$buff[1]} = $buff[2];

                }else{
                    // 二次元配列
                    for($i = 0; $i < $fields; $i++){
                        // カラム
                        array_push($line, $buff[$i]);
                    }
                    // レコード
                    if($key_colunm != ""){
                        // Keyカラム指定
                        $tsv{$buff[$key_colunm]} = $line;
                    }else{
                        // Keyカラム無し
                        array_push($tsv, $line);
                    }
                }
            }
        }

        return $tsv;
    }

    /**
     *  get_csv
     *  CSVデータ取得
     *
     *  $object[<レコード>] = <値> の一次元配列又は
     *  $object[<レコード>][<カラム>] = <値> の二次元配列で返す
     *  一次元の場合、1カラム目がレコード, 2カラム目が値に強制配置される
     *  0カラム目は無視されるので注意
     *
     *  @access protected
     *  @param  string  master_name     マスタ名
     *          int     array_dimention 配列の次元値(1or2)
     *  @return array   object
     */
    function get_master_csv($master_name, $array_dimention = 2)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDB();

        if(! $master_name){
            return Ethna::raiseNotice('システムエラー:マスタ名が設定されていません', E_Opensite_COMMON);
        }
        $dir       =  $ctl->getDirectory('etc');
        $file_path = $dir."/".$master_name.".csv";

        // CSVファイル読み込み
        $fp   = fopen($file_path, "r");
        if(!$fp){
            return $csv;
        }

        $csv  = array();
        while($buff = fgetcsv($fp, 100000, DELIMITTER_COMMA)){
            $fields = count($buff);
            $line   = array();
            // # 行はスキップ
            if($buff[0] != "#"){
                if($array_dimention == 1){
                    // 一次元配列
                    $csv{$buff[1]} = $buff[2];
                }else{
                    // 二次元配列
                    for($i = 0; $i < $fields; $i++){
                        // カラム
                        array_push($line, $buff[$i]);
                    }
                    // レコード
                    array_push($csv, $line);
                }
            }
        }

        return $csv;
    }

    /**
     *  get_xml
     *  XMLデータ取得
     *
     *  $object[<レコード>] = <値> の一次元配列又は
     *  $object[<レコード>][<カラム>] = <値> の二次元配列で返す
     *  一次元の場合、1カラム目がレコード, 2カラム目が値に強制配置される
     *  0カラム目は無視されるので注意
     *
     *  @access protected
     *  @param  string  master_name     マスタ名
     *          int     array_dimention 配列の次元値(1or2)
     *  @return array   object
     */
    function get_master_xml($master_name, $array_dimention = 2, $key_colunm = "")
    {
        $ctl    =& Ethna_Controller::getInstance();
        $back   =  $ctl->getBackend();

        if(! $master_name){
            return Ethna::raiseNotice('システムエラー:マスタ名が設定されていません', E_Opensite_COMMON);
        }
        $dir       = $ctl->getDirectory('etc');
        $file_path = $dir."/".$master_name.".xml";

        // XMLファイル読み込み
        $xml = simplexml_load_file($file_path);

        if(!$xml){
            return $xml;
        }

        return $xml;
    }
}
?>
