<?php
/**
 *  Opensite_Pager.php
 *  ページャ生成クラス
 *
 *  @copyright  (C)2010 Navit co.,ltd.
 *  @author     Tetsuo Nakanishi
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  Opensite Pager Class.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Pager
{
    /**
     *  pager
     *
     *  @access protected
     *  @param  array   now (現在のページ),
     *                  rows (全レコード数),
     *                  onepage (1ページレコード数)
     *  @return string  HTML構文
     */
    function pager($now, $rows, $onepage)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDB();

        // 0件の場合
        if($rows == 0){
            return;
        }

        // 件数表示
        $hits_start = ($onepage * ($now - 1)) + 1;
        $hits_end   = $onepage * $now;
        if($hits_end > $rows) { $hits_end = $rows; }

        $pager = $rows."件ヒットしました　".$hits_start."件目 ～ ".$hits_end."件目<br />";

        // ページ数
        $pages = ceil($rows / $onepage);
        $logger->log(LOG_DEBUG, "ページャ作る now:".$now."rows:".$rows."onepage:".$onepage."pages:".$pages);

        if($now != 1){
            // 最初へボタン
            $pager .= '<input type="button" value="<<" style="width:30px;" onClick="document.getElementById(\'offset\').value=1; document.pager_m.submit();">';
            // 前へボタン
            $pager .= '<input type="button" value="<" style="width:30px;" onClick="document.getElementById(\'offset\').value='.($now - 1).'; document.pager_m.submit();">';
        }

        // ページ表示範囲（前後3ページまで）
        if($now   > 4) { $renge_s = $now - 3; } else { $renge_s = 1; }
        $renge_e = $now + 3;
        if ($renge_e > $pages) { $renge_e = $pages;}
        $logger->log(LOG_DEBUG, "範囲 :".$renge_s." - ".$renge_e);

        // ページ別のリンクを生成
        for($i = $renge_s; $i < $renge_e + 1; $i++){
            if($now == $i) {
                $pager .= '<input type="button" value="'.$i.'" style="color:#ff0000; font-weight:bold; width:30px;">';
            } else {
                $pager .= '<input type="button" value="'.$i.'" style="width:30px;" onClick="document.getElementById(\'offset\').value='.$i.'; document.pager_m.submit();">';
            }
        }

        if($pages != $now){
            // 次へボタン
            $pager .= '<input type="button" value=">" style="width:30px;" onClick="document.getElementById(\'offset\').value='.($now + 1).'; document.pager_m.submit();">';
            // 最後へボタン
            $pager .= '<input type="button" value=">>" style="width:30px;" onClick="document.getElementById(\'offset\').value='.$pages.'; document.pager_m.submit();">';
        }

        return $pager;
    }
}
?>
