<?php
/**
 *  Opensite_FuruManager.php
 *
 *  @author     Genya Takahashi
 *  @package    Opensite
 *  @version    $Id: d4af361a99e2aaa95cedee2132d1ca3f10920c6b $
 */

/**
 *  Opensite MasterManager Class.
 *
 *  @author     Genya Takahashi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_FuruManager extends Ethna_AppManager
{

    /** @var    array   マッチング対象項目 */
    var $matching_target_column = array (
	"name",
	"address",
	"tel",
	"I_URL",
	"I_MAILADDR"
    );

    /** @var    array   マッチング方法 */
    var $matching_phase = array (
	"matching_three"
	,"matching_name_addr"
	,"matching_name_tel"
	,"matching_addr_tel"
    );

    /** @var    array   付与項目名変換 */
    var $trn_inspect_column_name = array (
        "latlon" => "I_LAT"
        ,"url" => "I_URL"
        ,"fax" => "I_FAX"
        ,"listed_type" => "I_LISTED_TYPE"
        ,"account_close" => "I_ACCOUNT_CLOSE"
        ,"founding_dt" => "I_FOUNDING_DT"
        ,"capital_div" => "I_CAPITAL_DIV"
        ,"employee_div" => "I_EMPLOYEE_DIV"
        ,"sale_div" => "I_SALE_DIV"
        ,"dept_position" => "I_DEPT_POSITION"
        ,"dept_name" => "I_DEPT_NAME"
        ,"business" => "I_BUSINESS_CD_S_1"
    	,"corp_num" => "I_CORP_NUM"
    	,"mailaddr" => "I_MAILADDR"
    );

    var $business_str = array (
        "大業種" => "I_BUSINESS_CD_L_1",
        "中業種" => "I_BUSINESS_CD_M_1",
        "小業種1" => "I_BUSINESS_CD_S_1",
        "小業種2" => "I_BUSINESS_CD_S_2",
        "小業種3" => "I_BUSINESS_CD_S_3",
        "小業種4" => "I_BUSINESS_CD_S_4",
        "小業種5" => "I_BUSINESS_CD_S_5",
        "小業種6" => "I_BUSINESS_CD_S_6",
        "小業種7" => "I_BUSINESS_CD_S_7",
        "小業種8" => "I_BUSINESS_CD_S_8",
        "小業種9" => "I_BUSINESS_CD_S_9",
        "小業種10" => "I_BUSINESS_CD_S_10"
    );

    var $ht_header = array (
        "in_card_id" => "名刺ID",
        "in_name_line_num" => "会社名",
        "in_zipcode_line_num" => "郵便番号",
        "in_address_line_num" => "住所",
        "in_tel_line_num" => "電話番号",
        "in_I_URL_line_num" => "URL"
    );

    /**
     * 初期化
     *
     * @param string	$datetime	送信日時
     * @param FILE		$list		宛先ファイル情報
     * @param FILE		$article	原稿ファイル情報
     * @param string	$insert		差し込み印字
     * @return
     */
    function init($list,$tel_val_line,$header_check,$header_line)
    {
        // 宛先ファイルのアップロード、設定
        $list = $this->upload($list);
        if($list == null)
        {
            return Ethna::raiseNotice('ファイルのアップロードに失敗しました。', E_FAX_UPLOAD_LIST_NOT_EXIST);
        }
        $this->list = $list['new_tmp_path'];

	$this->header_check = $header_check;

	$this->header_line = $header_line;

        return 0;

        //Todo:// Geo不達リスト作成
        //Todo:// Post・GET関係のメソッドをリファクタリング（Contents-Type周り）

    }

    /**
     * getListCount
     * 宛先リスト行数取得
     *
     * @param   FILE $list
     * @return  宛先リストの行数
     */
    function getListValue($file_path,$header_check,$header_line)
    {
        // リストが存在しない場合
        if($file_path == "")
        {
            return 0;
        }
	$tmp = exec("nkf --guess $file_path");
	$tmp_arr = explode(" ",$tmp);
	$code = $tmp_arr[0];
	if( $code == "Shift_JIS" ){
		$code = "cp932";
	}
	if( $code == "BINARY" ){
		$code = "cp932";
	}
	$instr = file_get_contents($file_path);
	if ($instr == FALSE) {
		return 0;
	}
	if( $code != "UTF-8" ){
		$instr = mb_convert_encoding($instr, "UTF-8", $code);
	}
	$outstr = str_replace(array("\r\n", "\n", "\r"), "\n", $instr);

	$outfp = fopen($file_path, 'wb');
	fwrite($outfp, $outstr);
	fclose($outfp);

	$fp = fopen($file_path,'r');

/*
        if (($handle = fopen($file_path, "r")) !== false)
        {
*/
	    $i = 0;
            //while (($line = fgetcsv($handle, MAX_LIST_LENGTH , ",")) !== false)
            //while ($line = explode(',',trim(fgets($handle, 1000000))))
            //while ($line = fgets($handle))
	    //foreach($outstr_array as $item)
	    while( $line = fgets($fp) )
        {
        	$i++;
        	$line = str_replace("\n","",$line);
        	$data = explode(",",$line);
            if($header_check != "")
        	{
                if($i <= $header_line)
                {
//echo "header=".$header_line.",".$i."<br />";
//var_dump($line);
//echo "<br />";
                    $header[] = $data;
                }
                else
                {
//echo "records=".$header_line.",".$i."<br />";
//var_dump($line);
//echo "<br />";
                    for($j=0; $j<count($data); $j++) {
                        if((preg_match('/^(\")(.*)/', $data[$j], $m)) == true) {
                            // 先頭に「"」か「'」がついている場合はファイルフォーマットエラーにする
                            return -1;
                        }
                    }

                    $records[] = $data;
                }
            }
            else
            {
                for($j=0; $j<count($data); $j++) {
                    if((preg_match('/^(\")(.*)/', $data[$j], $m)) == true) {
                        // 先頭に「"」か「'」がついている場合はファイルフォーマットエラーにする
                        return -1;
                    }
                }
                $records[] = $data;
            }
        }
        //fclose($fp);
        //fclose($handle);
        return $records;
/*
        }
        else
        {
            // エラーの場合
            return 0;
        }
*/
    }

    /**
     * getCsv
     * 緯度経度付与CSV取得
     *
     * @param   FILE $list
     * @return  CSV取得
     */
    function getCsv($request_id,$list_filepath,$inspect_filepath,$select_property,$use_property_arr,$list_header_line)
    {
        // リストが存在しない場合
        if($inspect_filepath == "")
        {
            return 0;
        }
        // 緯度経度取得
        $header = NULL;
        $records = "";

        //依頼リストの列数を抽出
        $list_filepath = $_SERVER['DOCUMENT_ROOT']."/service/furufuru/" . $list_filepath;

        $fp = fopen($list_filepath,'r');
        $org_list_column_num = 0;
	while( $line = fgets( $fp )){
            $exp = explode(",",$line);
            if( $org_list_column_num < count($exp) ){
		$org_list_column_num = count($exp);
            }
	}
        fclose($fp);

        //追加列数
        $property_column_num = 0;
        //項目毎の値保持数カウント初期化と、追加列数の算出
        $property_key = explode(",",$select_property);
        foreach($property_key as $key){
            $contents[$key . "_row"] = 0;
            if($key == "latlon"){
                $property_column_num += 2;
            }else if($key == "business"){
                $property_column_num += 10;
            }else{
                $property_column_num++;
            }
        }

	//納品ファイルの想定列数を算出
	$assumption_column_num = $org_list_column_num + $property_column_num;

		//選択されなかった項目の抽出
		$select_property_arr = explode(",",$select_property);
		$delete_property_arr = array();
		foreach($select_property_arr as $select){
			$use_flg = 0;
			foreach($use_property_arr as $use => $val){
				if($select == $use){
					$use_flg = 1;
					break;
				}
			}
			if($use_flg == 0){
				$delete_property_arr[] = $select;
			}
		}

		$inspect_filepath = $_SERVER['DOCUMENT_ROOT']."/service/furufuru/" . $inspect_filepath;
		if (($handle = fopen($inspect_filepath, "r")) !== false)
		{

			//一旦購入項目以外の項目列を特定する
			$i = 0;
			while ($line_str = fgets($handle))
			{
				$i++;
				//改行を削除
				$line_str = rtrim($line_str);

				if($list_header_line == $i){
					//レコード文字列を配列化
					$line_arr = explode(",",$line_str);
					$estimate_list_column_num = count($line_arr);
					foreach($delete_property_arr as $key){
						for($num = $org_list_column_num; $num < $estimate_list_column_num; $num++){
							if($key == "latlon"){
								if($line_arr[$num] == "緯度"){
									$delete_column_num[] = $num;
									$delete_column_num[] = $num + 1;
									break;
								}
							}else if($key == "business"){
								if(strpos($line_arr[$num],"業種") !== false){
									$delete_column_num[] = $num;
									for($plus = 1; $plus < 10; $plus++){
										$delete_column_num[] = $num + $plus;
									}
									break;
								}
							}else if(mb_convert_kana($line_arr[$num],"a","UTF-8") == $GLOBALS['hojin_request_property_list'][$key]){
								$delete_column_num[] = $num;
								break;
							}
						}
					}
					break;
				}
			}
			fclose($handle);

			$handle = fopen($inspect_filepath, "r");
			while ($line_str = fgets($handle))
			{
				//改行を削除
				$line_str = rtrim($line_str);
				$line_arr = explode(",",$line_str);
				foreach($delete_column_num as $num){
					unset($line_arr[$num]);
				}
				$c = 0;
				foreach($line_arr as $val){
					$c++;
					if($c == 1){
						$records .= $val;
					}else{
						$records .= "," . $val;
					}
				}
				$records .= "\r\n";
			}
			fclose($handle);

	    $delivery_filepath = "download/furu_". $request_id . "_delivery.csv";
	    $records = mb_convert_encoding($records,"sjis-win","UTF8");

	    $stream = fopen($_SERVER['DOCUMENT_ROOT']."/service/furufuru/" . $delivery_filepath, 'w');
	    fwrite($stream, $records);
	    fclose($stream);

            return $delivery_filepath;


        }
        else
        {
            // エラーの場合
            return 0;
        }
    }

    /**
     * CSVファイル取得処理
     *
     * @param   STRING $filePath ダウンロードファイルパス
     * @return
     */
    function csv_dl($filePath){
	$filePath = $_SERVER['DOCUMENT_ROOT']."/service/furufuru/" . $filePath;

        /* ファイルサイズの確認 */
        if (($content_length = filesize($filePath)) == 0) {
                Ethna::raiseError('ダウンロードファイルがありません。(filePath:['.$filePath.'])',E_FURU_DL_FILE_NOT_FOUND);
                $error['message'] = 'ダウンロードファイルがありません。';
                return $error;
        }

        /* ダウンロード用のHTTPヘッダ送信 */
        header("Content-Disposition: inline; filename=\"".basename($filePath)."\"");
        header("Content-Length: ".$content_length);
        header("Content-Type: application/octet-stream");

        //$data = array($microtime,$filePath);

        /* ファイルを読んで出力 */
        if (!readfile($filePath)) {
                Ethna::raiseError('ダウンロードファイルを出力出来ませんでした。(filePath:['.$filePath.'])',E_FURU_DL_FILE_DL_ERROR);
                $error['message'] = 'ダウンロードファイルを出力出来ませんでした。';
                return $error;
        }
        //ファイルは保持しておく

        //ファイルを削除
        //exec("rm -f $filePath");
        return;
    }

    /**
     * Unicodeエスケープされた文字列をUTF-8文字列に戻す
     *
     * @param string $str
     * @return UTF-8文字列
     */
    function unicode_encode($str)
    {
            return preg_replace_callback("/\\\\u([0-9a-zA-Z]{4})/", array($this,"encode_callback"), $str);
    }

    /**
     * Unicodeエスケープ変換用コールバック
     *
     * @param array $matches
     * @return string 変換後の文字列
     */
    function encode_callback($matches) {
            return mb_convert_encoding(pack("H*", $matches[1]), "UTF-8", "UTF-16");
    }

    /**
     * ファイルアップロード
     *
     * @param FILE		$file
     * @return object	ファイル情報
     */
    function upload($file)
    {
        // アップロードされている場合のみ、一時ファイルに保存
        if ($file['error'] != UPLOAD_ERR_NO_FILE)
        {
            $file_info = pathinfo($file['name']);
            $extension = $file_info['extension'];
            $tmp_name = $file['tmp_name'];
            //$tmp_name_new = sha1($tmp_name);
            $tmp_name_new = "furu_" . ceil(microtime(true) * 1000) . "_org";
            if($extension != "")
            {
                    $tmp_name_new .= ".".$extension;
            }
            // アップロードされたファイルをupload以下に保存
            $result = move_uploaded_file($tmp_name, 'upload/' . $tmp_name_new);
            $file['new_tmp'] = $tmp_name_new;
            $file['new_tmp_path'] = 'upload/'.$tmp_name_new;
            return $file;
        }
        else
        {
            return null;
        }
    }

    /**
     * DB登録日時の日本語化
     *
     * @param STRING            $date
     * @return object   日本語化日時
     */
    function change_to_j_date($date)
    {
        //日時を日本語化
        $exp = explode(" ",$date);
        $created = "";
        $date_num = explode("-",$exp[0]);
        $created = $date_num[0]."年".$date_num[1]."月".$date_num[2]."日";
        $created .= " ";
        $time_num = explode(":",$exp[1]);
        $created .= $time_num[0]."時".$time_num[1]."分";

        return $created;
    }

    /**
     * デバッグ用
     */
    function debug()
    {
        echo "<hr>デバッグ情報<br />";
        echo "送信日時 : ";
        var_dump($this->datetime);
        echo "<br /><br />";
        echo "宛先ファイル ：";
        var_dump($this->list);
        echo "<br /><br />";
        echo "原稿ファイル ：";
        var_dump($this->article);
        echo "<br /><br />";
        echo "差し込み印字 ：";
        var_dump($this->insert);
    }

    /**
     * マッチング件数取得
     *
     * @param ARRAY          $data	//リストの値格納配列
     * @param INT            $header_line	//ヘッダー行数
     * @param ARRAY          $column_num	//選択項目
     * @param STRING         $property_key	//選択項目
     * @return object        マッチング件数及び付与項目数
     */
    function get_matching_row_count($data,$header_line,$column_num,$property_key,$use_matching_phase)
    {

        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();

	$prefecture_list = Opensite_Dao_Furu::get_tableval_array("prefectures","id","name");

	if($use_matching_phase == ""){
		//マッチ件数及び付与件数格納配列
		$matching_count_all = array();
		$property_array = explode(",",$property_key);
//print_r($property_array)		;
		foreach($property_array as $val){
			$trn_inspect_column_array[] = $this->trn_inspect_column_name[$val];
			if($trn_inspect_column_key == ""){
				$trn_inspect_column_key = $this->trn_inspect_column_name[$val];
			}else{
				$trn_inspect_column_key .= ",".$this->trn_inspect_column_name[$val];
			}
		}
//echo "<br>===".$trn_inspect_column_key;
		//マッチング時参照リスト
		$matching_phase_array = $this->matching_phase;

		//予め件数用配列を定義しておく
		foreach($matching_phase_array as $phase){
			$matching_count_all[$phase]['matching_row'] = 0;
			foreach($trn_inspect_column_array as $property){
				$matching_count_all[$phase][$property] = 0;
			}
		}
	}else{
		$trn_inspect_column_key = "*";
		//ファイル生成時の参照リスト
		$matching_phase_array = array($use_matching_phase);
	}

	for($i = 0; $i < count($data); $i++){
		$material = array();

		//文字コード変換
		//mb_language("Japanese");

		//列番号格納
		/*
		foreach($this->matching_target_column as $column){
			$data[$i][$column_num[$column] - 1] = mb_convert_encoding($data[$i][$column_num[$column] - 1], "UTF-8", "auto");
		}
		*/

		//マッチング用会社名抽出
		$material['name'] = $this->get_matching_name($data[$i][$column_num['name'] - 1]);
///echo "<br>name=" . $material['name'];
		//mb_language("Japanese");
		//$material['name'] = mb_convert_encoding($this->get_matching_name($data[$i][$column_num['name'] - 1]), "UTF-8", "auto");
		//マッチング用住所情報抽出
		$material['address'] = $this->get_matching_address($prefecture_list,$data[$i][$column_num['address'] - 1]);


		//マッチング用電話番号抽出
		//$material['tel'] = $this->get_matching_tel($data[$i][$column_num['tel'] - 1]);
        //電話番号は完全一致に変更 add 2016.11.01
        $material['tel'] = $this->get_allcolumn_matching_tel($data[$i][$column_num['tel'] - 1]);

		//メール_ドメイン変換
		if($data[$i][$column_num['I_URL'] - 1] != ""){
			$material['domain'] = $this->get_domain_by_url($data[$i][$column_num['I_URL'] - 1]);
		}

		//URL_ドメイン変換
		if($data[$i][$column_num['I_MAILADDR'] - 1] != "" && $material['domain'] == ""){
			$material['domain'] = $this->get_domain_by_email($data[$i][$column_num['I_MAILADDR'] - 1]);
		}

		
        //echo("material:<pre>");var_dump($material);echo("</pre><br>");
        //$logger->log(LOG_DEBUG, "material:".print_r($material));

        
        //マッチングループ
		foreach($matching_phase_array as $phase){
			if($phase == "matching_three"){
				//
				//	ドメインマッチングは不要なので、下記をコメントアウト　2017-03-08 中村
				//
				/*
				if($material['domain'] != ""){
					$arr = Opensite_Dao_Furu::trn_inspect_matching("matching_domain",$trn_inspect_column_key,$material);
				}else{
					$arr = array();
				}
				*/
				//
				//	以上まで
				//
//				if(count($arr) == 0){
					if($material['name'] != "" && $material['address']['longname'] != "" && $material['tel'] != ""){
						// 3項目マッチングの電話番号は下二桁を削除し、LIKE検索
                        $material['tel'] = $this->get_matching_tel($data[$i][$column_num['tel'] - 1]);
                        $arr = Opensite_Dao_Furu::trn_inspect_matching($phase,$trn_inspect_column_key,$material);
					}else{
						$arr = array();
					}
//				}
			}else if($phase == "matching_name_addr"){
				if($material['name'] != "" && $material['address']['longname'] != ""){
					$arr = Opensite_Dao_Furu::trn_inspect_matching($phase,$trn_inspect_column_key,$material);
				}else{
					$arr = array();
				}
			}else if($phase == "matching_name_tel"){
                //電話番号は完全一致に変更 add 2016.11.01
                $material['tel'] = $this->get_allcolumn_matching_tel($data[$i][$column_num['tel'] - 1]);

                if($material['name'] != "" && $material['tel'] != ""){
					$arr = Opensite_Dao_Furu::trn_inspect_matching($phase,$trn_inspect_column_key,$material);
				}else{
					$arr = array();
				}
			}else if($phase == "matching_addr_tel"){
                //電話番号は完全一致に変更 add 2016.11.01
                $material['tel'] = $this->get_allcolumn_matching_tel($data[$i][$column_num['tel'] - 1]);

				if($material['address']['longname'] != "" && $material['tel'] != ""){
					$arr = Opensite_Dao_Furu::trn_inspect_matching($phase,$trn_inspect_column_key,$material);

				}else{
					$arr = array();
				}
			}
			if($use_matching_phase == ""){
				if(count($arr) > 0){
					$matching_count_all[$phase]['matching_row']++;
					foreach($trn_inspect_column_array as $property){
						if($property == "I_LISTED_TYPE"){
//							if($arr[$property] != ""){
							//	2016-12-27 下記のように修正　中村
//$fp = fopen("nakamura.txt", "a+");
//fwrite($fp, "step1\n");
							if($arr[$property] != "" && $arr[$property] != "00" && ! is_null($arr[$property])){
//fwrite($fp, "step2\n");
								$matching_count_all[$phase][$property]++;
							}
//fclose($fp);							
						}else{
							if($arr[$property] != "" && $arr[$property] != "0"){
								$matching_count_all[$phase][$property]++;
							}
						}
					}
				}
			}else{
				//CSVファイル生成用
				if(count($arr) > 0){
					return $arr;
				}else{
					return 0;
				}
			}
		}
	}
//echo "<br>mca=";print_r($matching_count_all);
	return $matching_count_all;
    }

    /**
     * マッチング用会社名抽出
     *
     * @param STRING	$name
     * @return object	マッチング用会社名
     */
    function get_matching_name($name)
    {
	//	2017-03-21 下記１行を修正　中村
//	$remove_list = array(" ","　","株式会社","(株)","（株）","有限会社","(有)","（有）","合同会社","(同)","（同）","合資会社","(資)","（資）");
	$remove_list = array("株式会社","(株)","（株）","有限会社","(有)","（有）","合同会社","(同)","（同）","合資会社","(資)","（資）", "合名会社", "(名)","（名）",
						"（一般財団法人）", "（公益財団法人）", "（一般社団法人）", "（公益社団法人）", "一般財団法人", "公益財団法人", "一般社団法人", "公益社団法人", "財団法人", "社団法人"	);
        foreach ($remove_list as $remove_item)
        {
            $name = str_replace($remove_item, "", $name);
        }
	$name = ereg_replace("\xe2\x88\x92", "\xef\xbc\x8d", $name);
	$name = ereg_replace("\xe3\x80\x9c", "\xef\xbd\x9e", $name);

	//
	//	2017-03-21　マルチバイトトリミングのため、下記２行追加　中村
	//
	$whitespace = '[\s\0\x0b\p{Zs}\p{Zl}\p{Zp}]';
	$name = preg_replace(sprintf('/(^%s+|%s+$)/u', $whitespace, $whitespace),'', $name);
	//
	//	2017-03-21　下記１行をコメントアウト　中村
	//
	//	$name = mb_convert_kana($name,"a","UTF-8");
        return $name;
    }

    /**
     * マッチング用電話番号抽出
     *
     * @param STRING	$tel
     * @return object	マッチング用電話番号
     */
    function get_matching_tel($tel)
    {
	$tel = mb_convert_kana($tel,"a","UTF-8");
        $remove_list = array("(",")","（","）","　", " ","-","ー","―","－","～", "－");
        foreach ($remove_list as $remove_item)
        {
            $tel = str_replace($remove_item, "", $tel);
        }
        $tel = mb_convert_kana($tel, "n");
		//
		//	2017-03-21　以下４行コメントアウト　中村
		//
		/*
        if (strlen($tel) > 2)
        {
            $tel = substr($tel, 0, -2);
        }
		*/
		
        return $tel;
    }

    /**
     * マッチング用電話番号抽出（半角数値のみに変換）
     *
     * @param STRING    $tel
     * @return object   マッチング用電話番号
     */
    function get_allcolumn_matching_tel($tel)
    {
        $tel = mb_convert_kana($tel,"a","UTF-8");
        $remove_list = array("(",")","（","）","　", " ","-","ー","―","－","～", "－");
        foreach ($remove_list as $remove_item)
        {
            $tel = str_replace($remove_item, "", $tel);
        }
        $tel = mb_convert_kana($tel, "n");

        return $tel;
    }

    /**
     * マッチング用住所抽出
     *
     * @param STRING	$address
     * @return object	マッチング用住所配列
     */
    function get_matching_address($prefecture_list,$address)
    {
        $rtn = array();
        foreach($prefecture_list as $key => $val){
	    //$val = mb_convert_encoding($val, "SJIS", "auto");
            $pos = strpos($address, $val);
            if ($pos === false) {

            }else{
                $rtn['code'] = $key;
                break;
            }
        }
	if($rtn['code'] != ""){
	    $longname = Opensite_Dao_Furu::get_longname_array($rtn['code']);
	    foreach($longname as $name){
		//$name = mb_convert_encoding($name, "SJIS", "auto");
		//
		//	2017-03-21 以下の１行を修正　中村	
		//
		//	$pos = strpos($address, $name);
		$pos = strpos(str_replace("　", "", $address), $name);
		if ($pos === false) {

		}else{
		    $rtn['longname'] = $name;
                    break;
        	}
	    }
	}
	return $rtn;
    }

    /**
     * マッチング用ドメイン抽出
     *
     * @param STRING	$url
     * @return object	マッチング用ドメイン配列
     */
    function get_domain_by_url($url)
    {
        $domain = null;
        if ($url != "") {
            //URLからドメインを切り出し
            $tmp = str_replace("https://", "", $url);
            $tmp = str_replace("http://", "", $tmp);
            $tmp = str_replace("www.", "", $tmp);
            $tmp_arr = explode("/", $tmp);
            $domain = $tmp_arr[0];
        }
        return $domain;
    }

    /**
     * マッチング用ドメイン抽出
     *
     * @param STRING	$email
     * @return object	マッチング用ドメイン配列
     */
    function get_domain_by_email($email)
    {
        $domain = null;
        if ($email != "" ) {
            //メールアドレスからドメインを切り出し
            $tmp_arr = explode("@", $email) or array();
            if (count($tmp_arr) > 1) {
                $domain = $tmp_arr[1];
            }
        }
        return $domain;
    }

    /**
     * autocreateCsv
     * 手動付与リスト作成
     *
     * @param
     * @return
     */
    function autocreateCsv($request_id,$file_path,$header_line,$property,$column_num,$use_matching_phase)
    {

        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();

        //$logger->log(LOG_DEBUG, "property:".print_r($property));
        //$logger->log(LOG_DEBUG, "column_num:".print_r($column_num));
        //$logger->log(LOG_DEBUG, "use_matching_phase:".$use_matching_phase);

        // リストが存在しない場合
        if($file_path == ""){
            return 0;
        }

	//各リストの値を抽出
	$business_l_list = Opensite_Dao_Furu::get_tableval_array("MST_BUSINESS_CD_L","BUSINESS_CD_L","BUSINESS_CD_L_NAME");
	$business_m_list = Opensite_Dao_Furu::get_tableval_array("MST_BUSINESS_CD_M","BUSINESS_CD_M","BUSINESS_CD_M_NAME");
	$business_s_list = Opensite_Dao_Furu::get_tableval_array("MST_BUSINESS_CD_S","BUSINESS_CD_S","BUSINESS_CD_S_NAME");
	$listed_type_list = Opensite_Dao_Furu::get_tableval_array("MST_LISTED_TYPE","LISTED_TYPE","LISTED_TYPE_NAME");
	$capital_div_list = Opensite_Dao_Furu::get_tableval_array("MST_CAPITAL_DIV","CAPITAL_DIV_ID","CAPITAL_DIV_NAME");
	$employee_div_list = Opensite_Dao_Furu::get_tableval_array("MST_EMPLOYEE_DIV","EMPLOYEE_DIV_ID","EMPLOYEE_DIV_NAME");
	$sale_div_list = Opensite_Dao_Furu::get_tableval_array("MST_SALE_DIV","SALE_DIV_ID","SALE_DIV_NAME");

        if (($handle = fopen($file_path, "r")) !== false){
            $i = 0;

            //文字コード変換
            mb_language("Japanese");

            while (($line = fgetcsv($handle, MAX_LIST_LENGTH , ",")) !== false){
        		$i++;
        		$record = "";
        		$data = array();
                if($header_line == "" || $header_line == 0){
        		    //ヘッダー行がない場合は、ヘッダー行を1行追加して、付与項目の項目名を記載する
                    if($i == 1){
                        for($c = 0;$c < count($line);$c++){
                            if($c == 0){
                                $record = "";
                            }else{
                                $record .= ",";
                            }
                        }
                        foreach($property as $key => $val){
                            if($key == "latlon"){
                                $record .= ",緯度,経度";
                            }else if($key == "business"){
                                $record .= ",大業種,中業種";
                                for($g = 1;$g <= 10;$g++){
                                    $record .= ",小業種".$g;
                                }
                            }else{
                                $record .= ",".$val;
                            }
                        }
                        $contents .= $record."\n";
                        $record = "";
                    }
                }else{
                    if($i <= $header_line){
                        foreach($line as $val){
                            if($record == ""){
                                $record = mb_convert_encoding($val,"UTF-8","auto");
                            }else{
                                $record .= ",".mb_convert_encoding($val,"UTF-8","auto");
                            }
                        }
                        foreach($property as $key => $val){
                            if($i != $header_line){
                                //ヘッダー行の最終行以外は、再修行に追加される付与項目名分の空セルを追加する
                                $record .= ",";
                            }else{
                                //ヘッダー行の最終行には、付与項目名を追加する
                                if($key == "latlon"){
                                    $record .= ",緯度,経度";
                                }else if($key == "business"){
                                    $record .= ",大業種,中業種";
                                    for($g = 1;$g <= 10;$g++){
                                        $record .= ",小業種".$g;
                                    }
                                }else{
                                    $record .= ",".$val;
                                }
                            }
                        }
                        $contents .= $record."\n";
                        continue;
                    }
                }

                //データ部作成
                foreach($line as $val){
                    if($record == ""){
                        $record = mb_convert_encoding($val,"UTF-8","auto");
                    }else{
                        $record .= ",".mb_convert_encoding($val,"UTF-8","auto");
                    }
                }
                $data[] = $line;
                $arr = $this->get_matching_row_count($data,$header_line,$column_num,"",$use_matching_phase);
                //echo('arr：<pre>');var_dump($arr);echo('</pre><br>');
                if($arr != 0){
                    foreach($property as $key => $val){
                        if($key == "latlon"){
                            if($arr['I_LAT'] != ""){
                                $record .= ",".$arr['I_LAT'].",".$arr['I_LON'];
                            }else{
                                $record .= ",,";
                            }
                        }else if($key == "business"){
                            if($arr['I_BUSINESS_CD_S_1'] != ""){
                                $record .= ",".$business_l_list[$arr['I_BUSINESS_CD_L_1']];
                                $record .= ",".$business_m_list[$arr['I_BUSINESS_CD_M_1']];
                                for($b = 1;$b <= 10;$b++){
                                    $record .= ",".$business_s_list[$arr['I_BUSINESS_CD_S_'.$b]];
                                }
                            }else{
                                $record .= ",,,,,,,,,,,,";
                            }
                        }else if($key == "listed_type"){
                            if ( $arr[$this->trn_inspect_column_name[$key]] == '00' ) {
                                $record .= ",";
                            } else {
                                $record .= ",".$listed_type_list[$arr[$this->trn_inspect_column_name[$key]]];
                            }
                        }else if($key == "capital_div"){
                            $record .= ",".$capital_div_list[$arr[$this->trn_inspect_column_name[$key]]];
                        }else if($key == "employee_div"){
                            $record .= ",".$employee_div_list[$arr[$this->trn_inspect_column_name[$key]]];
                        }else if($key == "sale_div"){
                            $record .= ",".$sale_div_list[$arr[$this->trn_inspect_column_name[$key]]];
                        }else if($key == "corp_num"){
                            if ( $arr[$this->trn_inspect_column_name[$key]] == 0 ) {
                                $record .= ",";
                            } else {
                                $record .= ",".$arr[$this->trn_inspect_column_name[$key]];
                            }
                        }else{
                            $record .= ",".$arr[$this->trn_inspect_column_name[$key]];
                        }
                    }
                }else{
                    foreach($property as $key => $val){
                        if($key == "latlon"){
                            $record .= ",,";
                        }else if($key == "business"){
                            $record .= ",,,,,,,,,,,,";
                        }else{
                            $record .= ",";
                        }
                    }
                }
                $contents .= $record."\n";
            }

            fclose($handle);

            $delivery_filepath = "download/furu_". $request_id . "_delivery.csv";
            $contents = mb_convert_encoding($contents,"sjis-win","UTF8");

            $stream = fopen($_SERVER['DOCUMENT_ROOT']."/service/furufuru/" . $delivery_filepath, 'w');
            fwrite($stream, $contents);
            fclose($stream);

            return $delivery_filepath;

        }else{
            // エラーの場合
            return 0;
        }
    }
    /**
     * autocreateCsvBizcp
     * 手動付与リスト作成（Biz Compass用。固定で全カラム列をつける）
     *
     * @param
     * @return
     */
    function autocreateCsvBizcp($request_id,$file_path,$header_line,$property,$column_num,$use_matching_phase)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();

        //$logger->log(LOG_DEBUG, "column_num:".print_r($column_num));
        $logger->log(LOG_DEBUG, "use_matching_phase:".$use_matching_phase);

        // リストが存在しない場合
        if($file_path == "") {
            return 0;
        }

        //各リストの値を抽出
        $business_l_list   = Opensite_Dao_Furu::get_tableval_array("MST_BUSINESS_CD_L","BUSINESS_CD_L","BUSINESS_CD_L_NAME");
        $business_m_list   = Opensite_Dao_Furu::get_tableval_array("MST_BUSINESS_CD_M","BUSINESS_CD_M","BUSINESS_CD_M_NAME");
        $business_s_list   = Opensite_Dao_Furu::get_tableval_array("MST_BUSINESS_CD_S","BUSINESS_CD_S","BUSINESS_CD_S_NAME");
        $listed_type_list  = Opensite_Dao_Furu::get_tableval_array("MST_LISTED_TYPE","LISTED_TYPE","LISTED_TYPE_NAME");
        $capital_div_list  = Opensite_Dao_Furu::get_tableval_array("MST_CAPITAL_DIV","CAPITAL_DIV_ID","CAPITAL_DIV_NAME");
        $employee_div_list = Opensite_Dao_Furu::get_tableval_array("MST_EMPLOYEE_DIV","EMPLOYEE_DIV_ID","EMPLOYEE_DIV_NAME");
        $sale_div_list     = Opensite_Dao_Furu::get_tableval_array("MST_SALE_DIV","SALE_DIV_ID","SALE_DIV_NAME");

        if (($handle = fopen($file_path, "r")) !== false) {
            //文字コード変換
            mb_language("Japanese");

            $row = 0;
            $rec = array();

            while (($line = fgetcsv($handle, MAX_LIST_LENGTH , ",")) !== false) {
                $data = array();
                if($row == 0) {
                    // ヘッダ部
                    $h_arr = $line;
                    if(count($h_arr) != count($this->ht_header)) {
                        // 入力ファイルフォーマットエラー
                        return -1;
                    }

                    foreach($GLOBALS['hojin_request_property_list'] as $key => $val) {
                        if($key == "business") {
                            foreach($this->business_str as $k => $v) {
                                $h_arr[] = $k;
                            }
                        } else {
                            $h_arr[] = $val;
                        }
                    }
                    $str = implode(",", $h_arr);
                    $rec[] = mb_convert_encoding($str, "UTF-8", "auto");

                } else {
                    // データ部
                    $in_arr = $line;
                    $data[] = $line;
                    $out_arr = array();

                    $arr = $this->get_matching_row_count($data, $header_line, $column_num, "", $use_matching_phase);
                    
                    //echo('arr：<pre>');var_dump($arr);echo('</pre><br>');
                    if($arr != 0) {

                        // full column
                        foreach($GLOBALS['hojin_request_property_list'] as $item => $jpn_name) {
                            
                            //echo "item: ".$item."<br>\n";
                            if((array_key_exists($item, $property)) == true) {
                                if($item == "business") {
                                    foreach($this->business_str as $k => $v) {
                                        if($arr[$v] != "") {
                                            if($v == "I_BUSINESS_CD_L_1") {
                                                $list = $business_l_list;
                                            } elseif($v == "I_BUSINESS_CD_M_1") {
                                                $list = $business_m_list;
                                            } else {
                                                $list = $business_s_list;
                                            }

                                            $out_arr[] = $list[$arr[$v]];
                                        } else {
                                            $out_arr[] = "";
                                        }
                                    }
                                } elseif($item == "listed_type") {
                                    $value = $arr[$this->trn_inspect_column_name[$item]];
                                    if($value != "00") {
                                        $out_arr[] = $listed_type_list[$value];
                                    } else {
                                        $out_arr[] = "";
                                    }

                                } elseif($item == "capital_div") {
                                    $value = $arr[$this->trn_inspect_column_name[$item]];
                                    $out_arr[] = $capital_div_list[$value];

                                } elseif($item == "employee_div") {
                                    $value = $arr[$this->trn_inspect_column_name[$item]];
                                    $out_arr[] = $employee_div_list[$value];

                                } elseif($item == "sale_div") {
                                    $value = $arr[$this->trn_inspect_column_name[$item]];
                                    $out_arr[] = $sale_div_list[$value];

                                } elseif($item == "corp_num") {
                                    $value = $arr[$this->trn_inspect_column_name[$item]];
                                    if($value != "0") {
                                        $out_arr[] = $value;
                                    } else {
                                        $out_arr[] = "";
                                    }
                                } else {
                                    $out_arr[] = $arr[$this->trn_inspect_column_name[$item]];
                                }
                            } else {
                                // 依頼なし
                                if($item == "business") {
                                    $out_arr = array_fill(0, count($this->business_str), "");

                                } else {
                                    $out_arr[] = "";
                                }
                            }
                        }
                        $mrg_arr = array_merge($in_arr, $out_arr);
                        $str = implode(",", $mrg_arr);
                        $rec[] = mb_convert_encoding($str, "UTF-8", "auto");
                    } else {
                        // 該当データなし
                        $out_arr = array_fill(0, count($h_arr)-count($in_arr), "");
                        $mrg_arr = array_merge($in_arr, $out_arr);
                        $str = implode(",", $mrg_arr);
                        $rec[] = mb_convert_encoding($str, "UTF-8", "auto");
                    }

                }

                $row++; 
            }

            fclose($handle);

            $delivery_filepath = "download/furu_". $request_id . "_delivery.csv";
            $contents = implode("\n", $rec)."\n";
            $contents = mb_convert_encoding($contents,"sjis-win","UTF8");

            $stream = fopen($_SERVER['DOCUMENT_ROOT']."/service/furufuru/" . $delivery_filepath, 'w');
            fwrite($stream, $contents);
            fclose($stream);

            return $delivery_filepath;

        } else {
            // エラーの場合
            return 0;
        }

    }

    function create_trial_csv($filepath,$data){
	$contents = "name,address,tel";
	$list_row = 0;

	foreach($data as $key => $item){
		if($item['corp'] != ""){
			$contents .= "\n".$item['corp'].",".$item['address'].",".$item['tel'];
			$list_row++;
		}else{
			break;
		}
	}

	$contents = mb_convert_encoding($contents,"sjis-win","UTF8");

	$stream = fopen($_SERVER['DOCUMENT_ROOT']."/service/furufuru/" . $filepath, 'wb');
	fwrite($stream, $contents);
	fclose($stream);

	return $list_row;;
    }
}
?>
