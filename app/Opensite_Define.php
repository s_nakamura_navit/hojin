<?php
/**
 *  Opensite_Define.php
 *
 *  @package   Opensite
 *
 *  $Id$
 */

/*--- Application Site Definition ---*/
//todo:// サイト関係の定数をコントローラから分離する

// システム共通識別コード(コンテンツタイプ)
define('SYSTEM_PREFIX',         "hojin");
define('SERVICE_NAME',         "法人番号検索");

$GLOBALS['hojin_request_property_list'] = array(
	//"latlon" => "緯度・経度(日本測地)"
	"business" => "業種（大分類）"
	,"business_m" => "業種（中分類）"
	,"business_s" => "業種（小分類）"
	,"fax" => "FAX番号"
	,"listed_type" => "上場区分"
	//,"account_close" => "決算月"
	,"employee_div" => "従業員数規模"
	,"sale_div" => "売上高規模"
	,"capital_div" => "資本金規模"
	,"founding_dt" => "設立年月"
	//,"dept_position" => "代表者役職名"
	//,"dept_name" => "代表者氏名"
	,"url" => "URL"
	,"corp_num" => "法人番号（マイナンバー）"
	,"mailaddr" => "メールアドレス"
	,"tel" => "電話番号"
);
$GLOBALS['matching_phase_list'] = array(
	"matching_three" => "3項目マッチ"
	,"matching_name_addr" => "会社名・住所マッチ"
	,"matching_name_tel" => "会社名・電話番号マッチ"
	,"matching_addr_tel" => "住所・電話番号マッチ"
);
//項目単価(手動付与)
$GLOBALS['hojin_request_property_unit_point_list'] = array(
	"latlon" => 12
	,"url" => 10
	,"fax" => 15
	,"listed_type" => 10
	,"account_close" => 10
	,"founding_dt" => 20
	,"capital_div" => 20
	,"employee_div" => 20
	,"sale_div" => 20
	,"dept_position" => 10
	,"dept_name" => 10
	,"business" => 10
	,"corp_num" => 10
	,"mailaddr" => 10
);
//項目単価(自動付与)
$GLOBALS['hojin_request_auto_property_unit_point_list'] = array(
	"latlon" => 24
	,"url" => 20
	,"fax" => 37
	,"listed_type" => 20
	,"account_close" => 20
	,"founding_dt" => 40
	,"capital_div" => 40
	,"employee_div" => 40
	,"sale_div" => 40
	,"dept_position" => 20
	,"dept_name" => 20
	,"business" => 20
	,"corp_num" => 50
	,"mailaddr" => 40
	,"tel" => 10
);
//項目単価
$GLOBALS['hojin_request_property_unit_point'] = 10;
//基本ポイント
$GLOBALS['hojin_request_base_point'] = 0;
//キャンセルポイント
$GLOBALS['hojin_request_cancel_point'] = 0;
//消費税20150328
$GLOBALS['hojin_request_tax'] = 0.08;
//新規登録特典ポイント付与
$GLOBALS['hojin_request_privilege'] = 0;
//サービスメール署名
$GLOBALS['mail_signature'] = "
**************************************************************
株式会社 ナビット 法人番号検索事務局
TEL:0120-937-781 　FAX:03-5215-5702
・東京本社
住所：〒102-0074　東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F
・大阪支社
住所：〒530-0001　大阪府大阪市北区梅田1-11-4-1100
　　　　　　　　　 大阪駅前第四ビル10階1-1号室
TEL 06-4799-9201　　FAX 06-4799-9011
※本サービスについては、「法人番号検索」で検索して下さい。
**************************************************************
";
?>
