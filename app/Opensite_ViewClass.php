<?php
// vim: foldmethod=marker
/**
 *  Opensite_ViewClass.php
 *
 *  @author     {$author}
 *  @package    Opensite
 *  @version    $Id$
 */

// {{{ Opensite_ViewClass
/**
 *  View class.
 *
 *  @author     {$author}
 *  @package    Opensite
 *  @access     public
 */
class Opensite_ViewClass extends Ethna_ViewClass
{
   /**
     *  @var    string  $layout      レイアウトテンプレートファイル名
     */
    var $layout = 'layout.tpl';
    
    /**
     *  @var    bool  $is_layout    レイアウトテンプレートを使うかどうかのフラグ
     */
    var $is_layout = true;
    
    /**
     * @var     bool  $is_gnavi     グローバルメニューを表示するかどうか
     */
    var $is_gnavi = true;

    /**
     * @var     bool  $is_himg      ヘッダーイメージを表示するかどうか
     */
    var $is_himg = true;
    
    /**
     * @var     bool  $is_topbtn    ページ上部に移動するボタンを表示するかどうか
     */
    var $is_topbtn = true;
    
    /**
     * @var     bool  $is_google    Googleアクセス解析コードを差し込むかどうか
     */
    var $is_google = true;
    
    /**
     * @var     bool  $is_conv_google    Googleアクセス解析コードを差し込むかどうか
     */
    var $is_conv_google = false;

    /**
     * @var     bool  $is_conv_google      Googleコンバージョンタグを差し込むかどうか
     */
    var $is_conv_google_prov = false;
    var $is_conv_google_real = false;

    /**
     * @var     bool  $is_conv_yahoo      Yahooコンバージョンタグを差し込むかどうか
     */
    var $is_conv_yahoo_prov = false;
    var $is_conv_yahoo_real = false;

    /**
     * @var     bool  $is_conv_yahoo    Yahooアクセス解析コードを差し込むかどうか
     */
    var $is_conv_yahoo = false;
    
    /**
     * @var     bool  $is_conv_lf    ListFinderアクセス解析コードを差し込むかどうか
     */
    var $is_conv_lf = true;
   
    /**
     * @var     string $logined_id       ログイン中のユーザーアカウント名 
     */
    var $logined_id = "";

    /**
     * @var     string $logined_name    ログイン中のユーザー氏名
     */
    var $logined_name = "";
    
    /**
     * @var     bool    $logined_serial ログイン中のユーザーID
     */
    var $logiend_serial = "";
    
    /**
     *  set common default value.
     *
     *  @access protected
     *  @param  object  Opensite_Renderer  Renderer object.
     */
    function _setDefault(&$renderer)
    {
        $smarty =& $renderer->getEngine();
        $action = $this->backend->ctl->getCurrentActionName();
        $view_name = $this->forward_name;

        // ログイン情報取得
        $this->logined_id = $this->session->get('id');
        $this->logined_name = $this->session->get('name');
        $this->logined_serial = $this->session->get('serial');

        $this->logger->log(LOG_INFO, "[".$this->logined_id."] Action:".$action." View:".$view_name);
    }
    
    //{{{forward
    /**
     *  遷移処理：レイアウトテンプレートに関する処理を追加
     *  
     *  @access public
     */
    function forward()
    {
        $renderer =& $this->_getRenderer();
        $this->_setDefault($renderer);

        // Todo:// ログイン後の新規登録ボタンをポイント購入ボタンに変更

        // using layout.tpl flag
        if ($this->is_layout) {
            // check : layout file existance
            if ((is_absolute_path($this->layout) && is_readable($this->layout))
                ||is_readable($renderer->template_dir . $this->layout)) {

                // 共通プロパティ定義
                $renderer->setPropArray(
                        array(
                            'logined_id'    => $this->logined_id,
                            'logined_name'  => $this->logined_name,
                            'logined_serial'=> $this->logined_serial,
                            'is_gnavi'      => $this->is_gnavi,
                            'is_himg'       => $this->is_himg,
                            'is_topbtn'     => $this->is_topbtn,
                            'is_conv_yahoo' => $this->is_conv_yahoo,
                            'is_google'     => $this->is_google,
                            'is_conv_google'=> $this->is_conv_google,
                            'is_conv_google_prov'=> $this->is_conv_google_prov,
                            'is_conv_google_real'=> $this->is_conv_google_real,
                            'is_conv_yahoo_prov'=> $this->is_conv_yahoo_prov,
                            'is_conv_yahoo_real'=> $this->is_conv_yahoo_real,
                            'is_conv_lf'    => $this->is_conv_lf,
                        ));
                // コンテンツセット
                $renderer->setProp('content',  $renderer->perform($this->forward_path, true) );
                $renderer->display($this->layout);
            } else {
                return Ethna::raiseWarning('file "'.$this->layout.'" not found');
            }
        } else {
            $renderer->perform($this->forward_path);
        }
    }
    //}}}
}
// }}}

?>
