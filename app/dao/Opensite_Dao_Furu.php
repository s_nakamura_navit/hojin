<?php
/**
 *  Opensite_Dao_Furu.php
 */

/**
 *  Opensite Dao_Furu Class.
 */
class Opensite_Dao_Furu
{
    /**
     *  insert_hojin_request
     *  ふるふる依頼管理
     *
     * @param string  user_id  [ログインid]
     * @return array
     */
    function insert_hojin_request($id,$user_serial,$user_id,$list_name,$list_row,$list_header_line,$select_property,$filePath,$process,$created_by)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();

        // SQL定義
        $sql    =  '
insert into
    hojin_request
(
    id,
    user_serial,
    user_id,
    list_name,
    list_row,
    list_header_line,
    select_property,
    list_filepath,
    created_estimate_by,
    flg_processed,
    created
)
values
(
    ?,?,?,?,?,?,?,?,?,?,?
)
';

       $cond = array(
           $id,
           $user_serial,
           $user_id,
           $list_name,
           $list_row,
           $list_header_line,
           $select_property,
           $filePath,
	   $created_by,
           $process,
           date("Y-m-d H:i:s")
        );

//echo $sql."<br />";
//var_dump($cond);

        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            $db->rollback();
            Ethna::raiseNotice("ユーザID:[".$user_id."] ふるふる依頼元ファイルパス:[".$filePath."]",E_FURU_DB_POINT_USE_FAILD);
            return Ethna::raiseError("ふるふる依頼情報の登録に失敗しました。",E_FURU_DB_POINT_USE_FAILD);
        }

        $db->commit();
        return;
    }

    /**
     *  update_hojin_request
     *  ふるふる依頼管理
     *
     * @param string  user_id  [ログインid]
     * @return array
     */
    function update_hojin_request($id,$use_property,$inspect_filepath,$flg_processed,$use_matching_phase)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();

        // SQL定義
        $sql    =  '
update hojin_request set
    use_property = ?
    ,inspect_filepath = ?
    ,client_order = ?
    ,flg_processed = ?
    ,updated = now()
';
if($use_matching_phase != ""){
        $sql   .=  '
    ,created_estimate = now()
    ,created_estimate_by = "'.$use_matching_phase.'"';
}
        $sql   .=  '
where
    id = ?
';

       $cond = array(
           $use_property
           ,$inspect_filepath
           ,date("Y-m-d H:i:s")
           ,$flg_processed
           ,$id
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            $db->rollback();
            Ethna::raiseNotice("ID:[".$id."] 選択ふるふる項目:[".$use_property."]",E_FURU_DB_POINT_USE_FAILD);
            return Ethna::raiseError("ふるふる依頼情報の更新に失敗しました。",E_FURU_DB_POINT_USE_FAILD);
        }

        $db->commit();
        return;
    }

    /**
     *  get_hojin_request_data
     *  ふるふる依頼管理
     *
     * @param string  id  [依頼id]
     * @return array
     */
    function get_hojin_request_data($id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        // SQL定義
        $sql    =  '
select
    *
from
    hojin_request
where
    id = ?
';

       $cond = array(
           $id
        );

        // クエリ実施
        $result = $db->getRow($sql, $cond);

        // エラーが起きた場合
        if(false === $result)
        {
            return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
        }

        // ユーザー情報が見つからない場合
        if( 0 === ($affect = $db->db->Affected_Rows()) )
        {
            return Ethna::raiseError('注文情報がありません。(ID:['.$user_id.'])',E_FURU_DB_USER_NOT_FOUND);
        }

        return $result;
    }

    /**
     *  get_longname_array
     *  市区町村名配列格納
     *
     * @param string  prefecture_id  [都道府県コード]
     * @return array
     */
    function get_longname_array($prefecture_id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

	$longname = array();

        // SQL定義
        $sql    =  '
select
    *
from
    citylist
where
    prefecture_id = ?
';

       $cond = array(
           $prefecture_id
        );

        // クエリ実施
        $result = $db->query($sql, $cond);

        // エラーが起きた場合
        if(false === $result)
        {
            return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
        }

        // ユーザー情報が見つからない場合
        if( 0 === ($affect = $db->db->Affected_Rows()) )
        {
            return Ethna::raiseError('注文情報がありません。(ID:['.$user_id.'])',E_FURU_DB_USER_NOT_FOUND);
        }
	while($arr = $result->fetchRow()){
		$longname[] = $arr['longname'];
	}
        return $longname;
    }

	/**
	 *  trn_inspect_matching
	 *  統合データマッチング
	 *
	 * @param string  phase  [マッチングフェーズ]
	 * @param string  property  [選択項目]
	 * @param array   material [マッチング素材]
	 * @return array
	 */
	function trn_inspect_matching($phase,$property,$material)
	{
		$ctl    =& Ethna_Controller::getInstance();
		$logger =  $ctl->getLogger();
		$back   =  $ctl->getBackend();
		$db     =  $back->getDb();

		$sql = "select use_trn_inspect from mst_use_trn_inspect order by created desc limit 1";
		$result = $db->query($sql);
		$arr = $result->fetchRow();
		$t_name = $arr['use_trn_inspect'];

		//if($phase == "matching_domain" || $phase == "matching_name_tel"){
//echo "<br>material['address']['code'] = " . $material['address']['code'];
		if($material['address']['code'] != "" && $phase != "matching_domain"){
			$table = $t_name."_".$material['address']['code'];

			// SQL定義
			$sql =' SELECT '
					. $property . '
					FROM ' . $table . '
					WHERE ID > 0';
			if($phase != "matching_addr_tel" && $phase != "matching_domain"){
				$sql .= ' AND name = "'.$material['name'].'"';
			}
			if($phase != "matching_name_tel" && $phase != "matching_domain"){
				$sql .= ' AND address = "'.$material['address']['longname'].'"';
			}
			/*
			if($phase != "matching_address_name" && $phase != "matching_domain"){
				$sql .= ' AND tel = "'.$material['tel'].'"';
			}
			*/
			if($phase != "matching_name_addr" && $phase != "matching_domain"){
                //$sql .= ' AND tel = "'.$material['tel'].'"';
                if($phase == "matching_three") {
					//
					//	2017-03-21 以下１行修正　中村
					//
                    //$sql .= ' AND REPLACE(I_TEL, "-", "") like "'.$material['tel'].'%"';
                    $sql .= ' AND REPLACE(I_TEL, "-", "") = "'.$material['tel'].'"';
                } else {
                    $sql .= ' AND REPLACE(I_TEL, "-", "") = "'.$material['tel'].'"';
                }
                
			}
				if($phase == "matching_domain"){
					$sql .= ' AND I_DOMAIN = "'.$material['domain'].'"';
				}
				if ( $property == 'I_LISTED_TYPE') {
					$sql .= ' AND I_LISTED_TYPE <> "00"';
				}
			$sql .= ' LIMIT 1 ';
			/*
			$sql    =  '
select
'.$property.'
from
'.$table.'
where
    ID > 0
';
			if($phase != "matching_addr_tel" && $phase != "matching_domain"){
				$sql .= '
and
    name = "'.$material['name'].'"';
			}
			if($phase != "matching_name_tel" && $phase != "matching_domain"){
				$sql .= '
and
    address = "'.$material['address']['longname'].'"';
			}
			if($phase != "matching_address_name" && $phase != "matching_domain"){
				$sql .= '
and
    tel = "'.$material['tel'].'"';
			}
			if($phase == "matching_domain"){
				$sql .= '
and
    I_DOMAIN = "'.$material['domain'].'"';
			}
			if ( $property == 'I_LISTED_TYPE') {
				$sql .= '
and
    I_LISTED_TYPE <> "00"';
			}
			$sql .= ' limit 1 ';
			*/

//echo "<br><br>" . $sql;

			// クエリ実施
			$result = $db->query($sql);
//echo "<br><br>stp1:" . $sql;

			// エラーが起きた場合
			if(false === $result)
			{
				return array();
			}

			// 情報が見つからない場合
			if( 0 === ($affect = $db->db->Affected_Rows()) )
			{
				return array();
			}else{
                $logger->log(LOG_DEBUG, "material['address']['code']".$material['address']['code']);
                $logger->log(LOG_DEBUG, "phase: ".$phase);
                $logger->log(LOG_DEBUG, "SQL: ".$sql);
				return $result->fetchRow();
			}

		}else{

			for($i=1;$i<48;$i++){
				$table = $t_name."_".$i;

				// SQL定義
				$sql ='SELECT '
						. $property . '
					   FROM ' . $table . '
					   WHERE ID > 0';

				if($phase != "matching_addr_tel" && $phase != "matching_domain"){
					$sql .= ' AND name = "'.$material['name'].'"';
				}
				if($phase != "matching_name_tel" && $phase != "matching_domain"){
					$sql .= ' AND address = "'.$material['address']['longname'].'"';
				}
				/*
				if($phase != "matching_address_name" && $phase != "matching_domain"){
					$sql .= ' AND tel = "'.$material['tel'].'"';
				}
				*/
				if($phase != "matching_name_addr" && $phase != "matching_domain"){
					//$sql .= ' AND tel = "'.$material['tel'].'"';
                    if($phase == "matching_three") {
						//
						//	2017-03-21 以下１行修正　中村
						//
//                        $sql .= ' AND REPLACE(I_TEL, "-", "") like "'.$material['tel'].'%"';
                        $sql .= ' AND REPLACE(I_TEL, "-", "") = "'.$material['tel'].'"';
                    } else {
                        $sql .= ' AND REPLACE(I_TEL, "-", "") = "'.$material['tel'].'"';
                    }
				}
				if($phase == "matching_domain"){
					$sql .= ' AND I_DOMAIN = "'.$material['domain'].'"';
				}
				if ( $property == 'I_LISTED_TYPE') {
					$sql .= ' AND I_LISTED_TYPE <> "00"';
				}
				$sql .= ' LIMIT 1 ';
				/*
				$sql    =  '
select
'.$property.'
from
'.$table.'
where
    ID > 0
';
				if($phase != "matching_addr_tel" && $phase != "matching_domain"){
					$sql .= '
and
    name = "'.$material['name'].'"';
				}
				if($phase != "matching_name_tel" && $phase != "matching_domain"){
					$sql .= '
and
    address = "'.$material['address']['longname'].'"';
				}
				if($phase != "matching_address_name" && $phase != "matching_domain"){
					$sql .= '
and
    tel = "'.$material['tel'].'"';
				}
				if($phase == "matching_domain"){
					$sql .= '
and
    I_DOMAIN = "'.$material['domain'].'"';
				}
				if ( $property == 'I_LISTED_TYPE') {
					$sql .= '
and
    I_LISTED_TYPE <> "00"';
				}
				$sql .= ' limit 1 ';
				*/

// var_dump( $sql );
				// クエリ実施
				$result = $db->query($sql);
//echo "<br><br>stp2:" . $sql;

				// エラーが起きた場合
				if($result === false)
				{
				    //return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
				    return array();
				}

				// 情報が見つからない場合
				if( 0 === ($affect = $db->db->Affected_Rows()) )
				{
					continue;
				}else{
                    $logger->log(LOG_DEBUG, "ELSE material['address']['code']".$material['address']['code']);
                    $logger->log(LOG_DEBUG, "phase: ".$phase);
                    $logger->log(LOG_DEBUG, "SQL: ".$sql);
                    return $result->fetchRow();
				}
			}

		}

		return array();
	}

    /**
     *  insert_hojin_searches
     *  ふるふるマッチング方法毎の情報管理
     *
     * @param string  user_id  [ログインid]
     * @return array
     */
    function insert_hojin_searches($request_id,$phase,$row,$point,$user_serial)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();

        // SQL定義
        $sql    =  '
insert into
    hojin_searches
(
    request_id,
    matching_phase,
    matching_row,
    point,
    created,
    created_by
)
values
(
    ?,?,?,?,?,?
)
';

       $cond = array(
           $request_id,
           $phase,
           $row,
           $point,
           date("Y-m-d H:i:s"),
           $user_serial
        );

//echo $sql."<br />";
//var_dump($cond);

        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            $db->rollback();
            Ethna::raiseNotice("ユーザID:[".$user_id."] ふるふる依頼ID:[".$request_id."]",E_FURU_DB_POINT_USE_FAILD);
            return Ethna::raiseError("ふるふるマッチング方法毎情報の登録に失敗しました。",E_FURU_DB_POINT_USE_FAILD);
        }

        $db->commit();
	return;
    }

    /**
     *  insert_hojin_searches_childs
     *  ふるふる付与項目毎の情報管理
     *
     * @param string  user_id  [ログインid]
     * @return array
     */
    function insert_hojin_searches_childs($searches_id,$attribute,$row,$point,$unit_point,$user_serial)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();

        // SQL定義
        $sql    =  '
insert into
    hojin_searches_childs
(
    searches_id,
    attribute,
    row,
    point,
    unit_point,
    created,
    created_by
)
values
(
    ?,?,?,?,?,?,?
)
';

       $cond = array(
           $searches_id,
           $attribute,
           $row,
           $point,
	   $unit_point,
           date("Y-m-d H:i:s"),
           $user_serial
        );

//echo $sql."<br />";
//var_dump($cond);

        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            $db->rollback();
            Ethna::raiseNotice("ユーザID:[".$user_id."] マッチング方法ID:[".$searches_id."]",E_FURU_DB_POINT_USE_FAILD);
            return Ethna::raiseError("ふるふる付与項目毎情報の登録に失敗しました。",E_FURU_DB_POINT_USE_FAILD);
        }

        $db->commit();
	return;
    }

    /**
     *  get_searches_childs_info
     *  付与件数等配列格納
     *
     * @param string  request_id  [依頼ID]
     * @param string  phase  [選択マッチング方法]
     * @return array
     */
    function get_searches_childs_info($request_id,$phase)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

	$data = array();

        // SQL定義
        $sql    =  '
select
    *
from
    hojin_searches
where
    request_id = ?
and
    matching_phase = ?
limit 1
';

       $cond = array(
           $request_id,
	   $phase
        );

        // クエリ実施
        $result = $db->query($sql, $cond);

        // エラーが起きた場合
        if(false === $result)
        {
            return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
        }

        // マッチング情報が見つからない場合
        if( 0 === ($affect = $db->db->Affected_Rows()) )
        {
            return Ethna::raiseError('マッチング情報がありません。(ID:['.$request_id.'])',E_FURU_DB_USER_NOT_FOUND);
        }

	$arr = $result->fetchRow();
	$searches_id = $arr['id'];
	$data['matching_row']['row'] = $arr['matching_row'];

//各項目事の情報抽出
        // SQL定義
        $sql    =  '
select
    *
from
    hojin_searches_childs
where
    searches_id = ?
';

        $cond = array(
           $searches_id
        );

        // クエリ実施
        $result = $db->query($sql, $cond);

        // エラーが起きた場合
        if(false === $result)
        {
            return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
        }

        // マッチング情報が見つからない場合
        if( 0 === ($affect = $db->db->Affected_Rows()) )
        {
            return Ethna::raiseError('マッチング情報がありません。(ID:['.$request_id.'])',E_FURU_DB_USER_NOT_FOUND);
        }

	while($arr = $result->fetchRow()){
		$data[$arr['attribute']] = array("row" => $arr['row'],"point" => $arr['point'],"unit_point" => $arr['unit_point']);
	}

        return $data;
    }

    /**
     *  get_hojin_request_info
     *  依頼情報取得（自動付与）
     *
     * @param string  request_id  [依頼ID]
     * @return array
     */
    function get_hojin_request_info($request_id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        // SQL定義
        $sql    =  '
select
    *
from
    hojin_request
where
    id = ?
';

        $cond = array(
           $request_id
        );

        // クエリ実施
        $result = $db->getRow($sql, $cond);

        // エラーが起きた場合
        if(false === $result)
        {
            return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
        }

        // ユーザー情報が見つからない場合
        if( 0 === ($affect = $db->db->Affected_Rows()) )
        {
            return Ethna::raiseError('注文情報がありません。(ID:['.$user_id.'])',E_FURU_DB_USER_NOT_FOUND);
        }

        return $result;
    }

    /**
     *  get_tableval_array
     *  テーブルの値を配列で返す
     *
     * @param string  table  [テーブル名]
     * @return array
     */
    function get_tableval_array($table,$key,$val)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

	$data = array();

        // SQL定義
        $sql    =  '
select
    *
from '.$table;

        // クエリ実施
        $result = $db->query($sql);

        // エラーが起きた場合
        if(false === $result)
        {
            return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
        }

        // 情報が見つからない場合
        if( 0 === ($affect = $db->db->Affected_Rows()) )
        {
            return Ethna::raiseError('情報がありません。(table:['.$table.'])',E_FURU_DB_USER_NOT_FOUND);
        }
	while($arr = $result->fetchRow()){
		$data[$arr[$key]] = $arr[$val];
	}

        return $data;
    }

    /**
     *  get_usepoint_for_request
     *  テーブルの値を配列で返す
     *
     * @param string  request_id  [依頼ID]
     * @param int  status  [0=使用,2=戻し]
     * @return array
     */
    function get_usepoint_for_request($request_id,$status)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        // SQL定義
        $sql    =  '
select
    *
from
    mst_use_point_logs
where
    process_id = ?
and
    status = ?
';

        $cond = array(
            $request_id
            ,$status
        );

        // クエリ実施
        $result = $db->query($sql,$cond);

        // エラーが起きた場合
        if(false === $result)
        {
            return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
        }

        // 情報が見つからない場合
        if( 0 === ($affect = $db->db->Affected_Rows()) )
        {
            return Ethna::raiseError('情報がありません。(table:['.$table.'])',E_FURU_DB_USER_NOT_FOUND);
        }
	$arr = $result->fetchRow();

        return $arr;
    }

}
?>
