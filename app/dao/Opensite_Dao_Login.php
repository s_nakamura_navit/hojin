<?php
/**
 *  Opensite_Dao_Login.php
 */

/**
 *  Opensite_Dao_Login Class.
 */
class Opensite_Dao_Login
{
    /**
     *  delete_user_session
     *  ユーザーのログイン状態の解除
     * 
     * @param string  user_id  ユーザーID
     * @return boolean 既読フラグ
     */
    function delete_user_session($user_id)
    {
       $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();
        $flg_delete = 1;
        
        $sql    =  '
update
    user_session
set    
    flg_delete = ?
where
    id = ?
';

        $cond = array(
            $flg_delete,
            $user_id
        );

        // クエリ実行
        $result = $db->query($sql, $cond);
        
        if(Ethna::isError($result))
        {
            $db->rollback;
            return $result;
        }
        $db->commit();

        return;
    }

    /**
     *  check_user_session
     *  接続状態の確認
     * 
     * @param string  user_id  ユーザーID
     * @return boolean 既読フラグ
     */
    function check_user_session()
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

	$sessid = session_id();
        
        $sql    =  '
select
    flg_delete
from
    user_session
where
    sessid = ?
';
        $cond = array(
	    $sessid
        );


        // クエリ実行
	$result = $db->getOne($sql, $cond);    

	if($result){
		// セッション削除
		$this->session->destroy();
		header('Location: index.php?action_login=true');
        }
        return;
    }

    /**
     *  check_cookie_newuser
     *  クッキー情報の突合
     *
     * @param string  user_id  ユーザーID
     * @return boolean 既読フラグ
     */
    function check_cookie_newuser($cookie)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
select
    serial
from
    mst_user
where
    flg_delete = 0
';

        // クエリ実行
        $result = $db->query($sql);
        $data = array();
        while ( $buf = $result->fetchRow()) {
            if( $buf != false)
            {
		$value = md5($buf['serial'] . "opensite_new_user");
		if($value == $cookie){
		    return true;
		}
            }
        }
        return false;
    }
}
?>
