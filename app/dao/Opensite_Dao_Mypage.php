<?php
/**
 *  Opensite_Dao_Mypage.php
 */

/**
 *  Opensite Dao_Mypage Class.
 */
class Opensite_Dao_Mypage
{
    /**
     * init_user_point
     * ユーザーのポイント情報を追加する
     *
     * @param string $user_id
     * @return object
     */
    function init_user_point($user_id,$init_point)
    {
        // 既存のレコードが存在するかチェック
        $result = Opensite_Dao_Mypage::get_current_point($user_id,true);
        if(!Ethna::isError($result))
        {
            return Ethna::raiseNotice("既存のレコードが存在します。(ID:[".$user_id."])",E_FURU_DB_USER_POINT_ALREADY_EXIST);
        }

        // ユーザー情報取得
        $user = Opensite_Dao_Mypage::get_user_info_by_id($user_id);
        if(Ethna::isError($user))
        {
            return $user;
        }

        // 初期レコード作成
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();

        $flg_delete = 0;
        // SQL定義
        $sql    =  '
insert into
    mst_user_point
(
    user_serial,
    user_id,
    point,
    flg_delete,
    updated,
    created
)
values
(
    ?,?,?,?,?,?
)
';

       $cond = array(
           $user['user_id'],
           $user['account'],
           $init_point,
           $flg_delete,
           date("Y-m-d H:i:s"),
           date("Y-m-d H:i:s"),
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            $db->rollback();
            return Ethna::raiseError("ポイント情報の作成に失敗しました。(ID:[".$user_id."])",E_FURU_DB_POINT_INIT_FAILD);
        }

	if($init_point != 0){
	    $status = 4;	//特典付与ステータス
	    // ログ書き込み
            $result = Opensite_Dao_Mypage::add_point_log($user['user_id'],$user['account'],SYSTEM_PREFIX,"",$init_point,$status);
            if(Ethna::isError($result))
            {
            	$db->rollback();
                Ethna::raiseNotice("ユーザID:[".$user_id."] 消費ポイント:[".$use_point."]",E_FURU_DB_POINT_LOG_FAILD);
                return Ethna::raiseError("ポイントログの書き込みに失敗しました。",E_FURU_DB_POINT_LOG_FAILD);
            }
	}

        $db->commit();
        return;
    }

    /**
     *  get_current_point
     *  対象ユーザの保有ポイントを取得
     *
     *  @access protected
     *  @param  string  user_id  [ユーザーID]
     */
    function get_current_point($user_id,$init = false)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $flg_delete = 0;

        $sql    =  '
select
    pt.point as point
from
    mst_user_point pt
where
   pt.flg_delete = ?
and
    pt.user_serial = ?

';

        $cond = array(
            $flg_delete,
            $user_id,
        );

        // クエリ実行
        $result = $db->getOne($sql, $cond);
        if(false === $result || $result == "")
        {
            if($init === false)
            {
                // 初期化時以外はErrorで
                return Ethna::raiseError("ポイント情報取得失敗",E_FURU_DB_USER_POINT_FAILD);
            }
            else
            {
                // 初期化時はNoticeで
                return Ethna::raiseNotice("ポイント情報は存在しません",E_FURU_DB_USER_POINT_NOT_EXIST);
            }
        }
        return $result;

        //$affect = $db->db->Affected_Rows();
        //return $affect;
    }

        /**
     * use_point
     * ポイント消費
     *
     * @param type $user_id     ポイントを使用したユーザID
     * @param type $use_point   消費したポイント数
     * @return object
     */
    function  use_point($id,$user_id,$use_point,$request_id,$status)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();

        $flg_delete = 0;

        // SQL定義
        $sql    =  '
update
    mst_user_point
set';
if($status == 0){
        $sql    .= " point = point - ?,";
}else if($status == 2 || $status == 3){
        $sql    .= " point = point + ?,";
}
        $sql    .='
    updated = ?
where
    flg_delete = ?
and
    user_serial = ?
';

       $cond = array(
           $use_point,
           date("Y-m-d H:i:s"),
           $flg_delete,
           $user_id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            $db->rollback();
            Ethna::raiseNotice("ユーザID:[".$user_id."] 消費ポイント:[".$use_point."]",E_FURU_DB_POINT_USE_FAILD);
            return Ethna::raiseError("ポイント消費処理に失敗しました。",E_FURU_DB_POINT_USE_FAILD);
        }

	// ログ書き込み
        $result = Opensite_Dao_Mypage::add_point_log($user_id,$id,SYSTEM_PREFIX,$request_id,$use_point,$status);
        if(Ethna::isError($result))
        {
            $db->rollback();
            Ethna::raiseNotice("ユーザID:[".$user_id."] 消費ポイント:[".$use_point."]",E_FURU_DB_POINT_LOG_FAILD);
            return Ethna::raiseError("ポイントログの書き込みに失敗しました。",E_FURU_DB_POINT_LOG_FAILD);
        }

        $db->commit();
        return;
    }

    /**
     * add_point_log
     * ポイント消費ログを書き込む
     *
     * @param string $user_id       ユーザーID
     * @param string $contents      コンテンツ名(設定ファイルのSYSTEM_PREFIX)
     * @param string $process_id    提供コンテンツによって異なるプロセスID
     * @param string $use_point     消費するポイント
     * @return object
     */
    function add_point_log($serial,$id,$contents,$process_id,$use_point,$status)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();

        $flg_delete = 0;

/*
        // ユーザ名からアカウント名を取得
        $result = Opensite_Dao_Mypage::get_user_info_by_id($user_id);
        if(Ethna::isError($result))
        {
            $db->rollback();
            return $result;
        }

        // アカウント名
        $account = $result['account'];
*/
        // SQL定義
        $sql    =  '
insert into
    mst_use_point_logs
(
    user_serial,
    user_id,
    select_contents,
    process_id,
    use_point,
    status,
    flg_delete,
    created,
    created_by
)
values
(
    ?,?,?,?,?,?,?,?,?
)
';

       $cond = array(
	   $serial,
           $id,
           $contents,
           $process_id,
           $use_point,
	   $status,
           $flg_delete,
           date("Y-m-d H:i:s"),
	   $serial
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            // ロールバック
            $db->rollback();
            return $result;
        }

        // コミット
        $db->commit();

        return $result;
    }

    /**
     *  get_current_point
     *  対象ユーザの情報を取得
     *
     *  @access protected
     *  @param  string  id  [ログインid]
     */
    function get_current_data($serial)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
select
        *
from
        mst_user
where
        serial = ?
';

        $cond = array(
            $serial
        );

        // クエリ実行
        $result = $db->query($sql, $cond);
        $data   = $result->fetchRow();
        return $data;
    }

    /**
     *  get_current_point
     *  対象ユーザの購入履歴を取得
     *
     *  @access protected
     *  @param  string  id  [ログインid]
     *          int     start_row 開始行
     *          int     fetch_row 取得行数
     */
    function get_current_purchase_history($serial, $start_row, $fetch_row)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

		$flg_processed = 2;

        $sql    =  '
select
    *
from
    hojin_request
where
    flg_delete = 0
and
    flg_processed = ?
and
    user_serial = ?
order by
    created desc
';
        $sql_c    =  '
select
    count(id) as counts 
from
    hojin_request
where
    flg_delete = 0
and
    flg_processed = ?
and
    user_serial = ?
';

        $cond = array(
	    $flg_processed,
            $serial
        );
        // クエリ実行
        $result = $db->query($sql_c, $cond);
        $counts   = $result->fetchRow();
        $result = $db->SelectLimit($sql, $fetch_row, $start_row, $cond);
        return array($counts['counts'], $result);
    }

     /**
     *  get_request_use_point
     *  依頼案件での移動ポイント
     *
     *  @access protected
     *  @param  int  id  [geo_request id]
     *          int status 取得行数
     */
    function get_request_use_point($id, $status)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
select
    use_point
from
    mst_use_point_logs
where
    flg_delete = 0
and
    process_id = ?
and
    status = ?
';

        $cond = array(
            $id,
            $status
        );
        // クエリ実行
        $result = $db->query($sql, $cond);
        $num   = $result->fetchRow();
        return $num['use_point'];
    }

    /**
     * get_user_info_by_id
     * ユーザーIDからユーザー情報を取得する
     *
     * @param string $user_id
     * @return array ユーザー情報
     */
    function get_user_info_by_id($user_id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $flg_delete = 0;

        // SQL定義
        $sql    =  '
select
    serial as user_id,
    id as account,
    company_name as company_name,
    company_kana as company_kana,
    branch_name as branch_name,
    zip1 as zip1,
    zip2 as zip2,
    address as address,
    phone_no1 as phone_no1,
    phone_no2 as phone_no2,
    phone_no3 as phone_no3,
    fax_no1 as fax_no1,
    fax_no2 as fax_no2,
    fax_no3 as fax_no3,
    k_phone_no1 as k_phone_no1,
    k_phone_no2 as k_phone_no2,
    k_phone_no3 as k_phone_no3,
    department_name as department_name,
    post_name as post_name,
    contractor_lname as contractor_lname,
    contractor_fname as contractor_fname,
    contractor_lkana as contractor_lkana,
    contractor_fkana as contractor_fkana,
    email as email,
    come_from as come_from,
    mailmaga as mailmaga,
    updated as updated,
    created as created
from
    mst_user
where
    flg_delete = ?
and
    serial = ?
';

       $cond = array(
           $flg_delete,
           $user_id,
        );
        // クエリ実施
        $result = $db->getRow($sql, $cond);

        // エラーが起きた場合
        if(false === $result)
        {
            return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
        }

        // ユーザー情報が見つからない場合
        if( 0 === ($affect = $db->db->Affected_Rows()) )
        {
            return Ethna::raiseError('ユーザー情報がありません。(ID:['.$user_id.'])',E_FURU_DB_USER_NOT_FOUND);
        }

        return $result;
    }

    /**
     * withdraw_mst_user
     * ユーザーを論理削除する
     *
     * @param string $user_id
     * @return array ユーザー情報
     */
    function withdraw_mst_user($serial)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $flg_delete = 0;

        // SQL定義
        $sql    =  '
update mst_user set
    flg_withdraw = 1
    ,updated = now()
where
    serial = ?
';

        $cond = array(
            $serial
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        //エラーはaction側で判断している
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     * delete_mst_user_point
     * ユーザーポイントを論理削除する
     *
     * @param string $user_id
     * @return array ユーザー情報
     */
    function delete_mst_user_point($serial)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $flg_delete = 0;

        // SQL定義
        $sql    =  '
update mst_user_point set
    flg_delete = 1
    ,updated = now()
where
    flg_delete = 0
and
    user_serial = ?
';

        $cond = array(
            $serial
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        //エラーはaction側で判断している
        $affect = $db->db->Affected_Rows();

        return $affect;

    }

    /**
     * edit_mst_user
     * ユーザー情報を更新する
     *
     * @param string $user_id
     * @return array ユーザー情報
     */
    function edit_mst_user($user_serial,$in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $flg_delete = 0;

if($in{'pw_edit'} != "none"){
        //パスワードの暗号化
        $in{'password'} = md5($in{'password'});
        // SQL定義
        $sql    =  '
update mst_user set
    id = ?
    ,pw = ?
    ,company_name = ?
    ,department_name = ?
    ,post_name = ?
    ,contractor_lname = ?
    ,contractor_fname = ?
    ,zip1 = ?
    ,zip2 = ?
    ,address = ?
    ,phone_no1 = ?
    ,phone_no2 = ?
    ,phone_no3 = ?
    ,email = ?
    ,updated = now()
where
    serial = ?
';

        $cond = array(
            $in{'user_id'}
            ,$in{'password'}
            ,$in{'company_name'}
            ,$in{'department_name'}
            ,$in{'post_name'}
            ,$in{'contractor_lname'}
            ,$in{'contractor_fname'}
            ,$in{'zip1'}
            ,$in{'zip2'}
            ,$in{'address'}
            ,$in{'phone_no1'}
            ,$in{'phone_no2'}
            ,$in{'phone_no3'}
            ,$in{'email'}
            ,$user_serial
        );
}else{
        // SQL定義
        $sql    =  '
update mst_user set
    id = ?
    ,company_name = ?
    ,department_name = ?
    ,post_name = ?
    ,contractor_lname = ?
    ,contractor_fname = ?
    ,zip1 = ?
    ,zip2 = ?
    ,address = ?
    ,phone_no1 = ?
    ,phone_no2 = ?
    ,phone_no3 = ?
    ,email = ?
    ,updated = now()
where
    serial = ?
';

        $cond = array(
            $in{'user_id'}
            ,$in{'company_name'}
            ,$in{'department_name'}
            ,$in{'post_name'}
            ,$in{'contractor_lname'}
            ,$in{'contractor_fname'}
            ,$in{'zip1'}
            ,$in{'zip2'}
            ,$in{'address'}
            ,$in{'phone_no1'}
            ,$in{'phone_no2'}
            ,$in{'phone_no3'}
            ,$in{'email'}
            ,$user_serial
        );
}

        // クエリ実施
        $result = $db->query($sql, $cond);
        //エラーはaction側で判断している
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     * edit_mst_user_point
     * ユーザーポイントを更新する
     *
     * @param string $user_id
     * @return array ユーザー情報
     */
    function edit_mst_user_point($user_serial,$user_id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $flg_delete = 0;

        // SQL定義
        $sql    =  '
update mst_user_point set
    user_id = ?
    ,updated = now()
where
    user_serial = ?
';

        $cond = array(
            $user_id
            ,$user_serial
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        //エラーはaction側で判断している
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     * check_mst_user_id
     * ユーザーIDの重複チェックをする
     *
     * @param string $user_id
     * @return array ユーザー情報
     */
    function check_mst_user_id($user_id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $flg_delete = 0;

        // SQL定義
        $sql    =  '
select
    *
from mst_user
where
    flg_delete = 0
and
    flg_withdraw = 0
and
    id = ?
';
        $cond = array(
            $user_id
        );

        // クエリ実施
        $result = $db->getRow($sql, $cond);
        //エラーはaction側で判断している
        $affect = $db->db->Affected_Rows();

        return array($affect,$result['serial']);
    }

    /**
     * get_request_info_by_id
     * request_idから依頼情報を取得する
     *
     * @param int $request_id
     * @return result 依頼情報
     */
    function get_request_info_by_id($request_id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $flg_delete = 0;

        // SQL定義
        $sql    =  '
select
    log.use_point as use_point
    ,request.*
from
    mst_use_point_logs log
left join
    hojin_request request
on(
    log.process_id = request.id
)
where
    log.flg_delete = ?
and
    log.status = 0
and
    id = ?
';

      $cond = array(
           $flg_delete,
           $request_id,
        );
        // クエリ実施
        $result = $db->getRow($sql, $cond);

        // エラーが起きた場合
        if(false === $result)
        {
            return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
        }

        // 依頼情報が見つからない場合
        if( 0 === ($affect = $db->db->Affected_Rows()) )
        {
            return Ethna::raiseError('依頼情報がありません。(ID:['.$request_id.'])',E_FURU_DB_REQUEST_NOT_FOUND);
        }

        return $result;
    }
}
?>
