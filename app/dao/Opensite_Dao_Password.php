<?php
/**
 *  Opensite_Dao_Password.php
 */

/**
 *  Opensite Dao_Password Class.
 */
class Opensite_Dao_Password
{
    /**
     *  search_user_data
     *  入力情報と一致するユーザデータの検索
     */
    function get_user_info_by_c_t_e($company_name, $phone_no1, $phone_no2, $phone_no3, $email)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $flg_delete = 0;

        // SQL定義
        $sql    =  '
select
    serial as serial,
    id as id,
    pw as pw,
    company_name as company_name,
    contractor_lname as contractor_lname,
    contractor_fname as contractor_fname,
    email as email
from
    mst_user
where
    flg_delete = ?
and
    email = ?
and
    company_name = ?
and
    phone_no1 = ?
and
    phone_no2 = ?
and
    phone_no3 = ?
';

        $cond = array(
            $flg_delete,
            $email,
            $company_name,
            $phone_no1,
            $phone_no2,
            $phone_no3
        );
        // クエリ実施
        $result = $db->getRow($sql, $cond);

        // エラーが起きた場合
        if(false === $result)
        {
            return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
        }

        // ユーザー情報が見つからない場合
        if( 0 === ($affect = $db->db->Affected_Rows()) )
        {
            return Ethna::raiseError('入力情報に対応するユーザー情報がありません。',E_FURU_DB_USER_NOT_FOUND);
        }

        return $result;
    }

    /**
     *  update_id_and_password
     *  ID、パスワード、およびユーザIDを使用しているテーブルを更新
     */
    function update_user_id_password($id, $password, $updated, $serial, $ver)
    {
    	$ctl    =& Ethna_Controller::getInstance();
    	$logger =  $ctl->getLogger();
    	$back   =  $ctl->getBackend();
    	$db     =  $back->getDb();

    	$sql    =  '
update
    mst_user as a,
    mst_user_point as b
set
    a.id = ?,
    a.pw = ?,
    a.updated = ?,
    b.user_id = ?,
    b.updated = ?
where
    a.serial = ?
and
    b.user_serial = ?
';
    	$cond = array(
    			$id,
    			$password,
    			$updated,
    			$id,
    			$updated,
    			$serial,
    			$serial
    	);
    	// クエリ実施
    	$result = $db->query($sql, $cond);
    	if(Ethna::isError($result))
    	{
    		$ver_name = ($ver == 'new')? '新規' : '旧' ;
    		return Ethna::raiseError($ver_name."ID・パスワードの登録に失敗しました。(serial:[".$serial."])",E_FURU_DB_UPDATE_MST_USER_FAILD);
    	}
    	$affect = $db->db->Affected_Rows();

    	return $affect;
    }
}
?>
