<?php
/**
 *  Opensite_Dao_Search.php
 */

/**
 *  Opensite Dao_Search Class.
 */
class Opensite_Dao_Search
{
    function select_government_contents($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $rows   = array();
        
        $area_cond = '';
        $field_cond = '';
        $kind_cond = '';

        if($in{"kind_1000"} != "") {$kind_cond .= " ".$in{"kind_1000"};}
        if($in{"kind_2000"} != "") {$kind_cond .= " ".$in{"kind_2000"};}
        if($in{"kind_3000"} != "") {$kind_cond .= " ".$in{"kind_3000"};}
        $kind_cond = trim($kind_cond);

        if($in{"field_90"} != "") {$field_cond .= " ".$in{"field_90"};}
        if($in{"field_91"} != "") {$field_cond .= " ".$in{"field_91"};}
        if($in{"field_92"} != "") {$field_cond .= " ".$in{"field_92"};}
        if($in{"field_93"} != "") {$field_cond .= " ".$in{"field_93"};}
        if($in{"field_94"} != "") {$field_cond .= " ".$in{"field_94"};}
        if($in{"field_95"} != "") {$field_cond .= " ".$in{"field_95"};}
        if($in{"field_96"} != "") {$field_cond .= " ".$in{"field_96"};}
        if($in{"field_97"} != "") {$field_cond .= " ".$in{"field_97"};}
        if($in{"field_98"} != "") {$field_cond .= " ".$in{"field_98"};}
        if($in{"field_99"} != "") {$field_cond .= " ".$in{"field_99"};}
        if($in{"field_100"} != "") {$field_cond .= " ".$in{"field_100"};}
        if($in{"field_101"} != "") {$field_cond .= " ".$in{"field_101"};}
        $field_cond = trim($field_cond);
   
        $sql    =  '
select
    *
from
    government_contents
where
    flg_delete =0 
 ';
        if($kind_cond != ''){
          $sql .= "and match( kind ) against ( '".$kind_cond."' in boolean mode ) "; 
        }
        
        if($in{"area1"} != ''){
          $sql .= "and ((match( pref_area ) against ( '".$in{"area1"}."' in boolean mode )) or (nation_area = '1')) "; 
        }
        
        if($in{"area2"} != ''){
          $sql .= "and ((match( city_area ) against ( '".$in{"area2"}."' in boolean mode )) or (nation_area = '1')) "; 
        }
        
        if($field_cond != ''){
          $sql .= "and match( field ) against ( '".$field_cond."' in boolean mode )"; 
        }
//echo $sql;
        $cond = array(
            $in{"area1"},
            $in{"area2"}
        );

        // クエリ実施
        $result = $db->query($sql);
        $result_counts = $result->RecordCount();
        $result_ary = array();
         for($j=0;$j<$result_counts;$j++) {
             $result_ary[$j] = $result->fetchRow(DB_FETCHMODE_ASSOC, $j);
         }
        
        return array($result_counts, $result_ary);
        /*
        $result_data = array();
        $i = 0;
        while ($result_data[$i] = $result->fetchRow()) {
            $i++;
        }
echo "i=".$i;        
        return $result_data;
         * */

        //return $result;
    
    }
    
    function select_foundation_contents($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $rows   = array();
        $ary_keyword= array();
        $area_cond = '';
        $field_cond = '';
        $field_cond2 = '';
        $keyword_cond = '';
        
        if($in{"field2_1"} != "") {$field_cond .= " ".$in{"field2_1"};}
        if($in{"field2_2"} != "") {$field_cond .= " ".$in{"field2_2"};}
        if($in{"field2_3"} != "") {$field_cond .= " ".$in{"field2_3"};}
        if($in{"field2_4"} != "") {$field_cond .= " ".$in{"field2_4"};}
        if($in{"field2_5"} != "") {$field_cond .= " ".$in{"field2_5"};}
        if($in{"field2_6"} != "") {$field_cond .= " ".$in{"field2_6"};}
        if($in{"field2_7"} != "") {$field_cond .= " ".$in{"field2_7"};}
        if($in{"field2_8"} != "") {$field_cond .= " ".$in{"field2_8"};}
        if($in{"field2_9"} != "") {$field_cond .= " ".$in{"field2_9"};}
        if($in{"field2_10"} != "") {$field_cond .= " ".$in{"field2_10"};}
        if($in{"field2_11"} != "") {$field_cond .= " ".$in{"field2_11"};}
        $field_cond = trim($field_cond);
        
        if($in{"field3_1"} != "") {$field_cond2 .= " ".$in{"field3_1"};}
        if($in{"field3_2"} != "") {$field_cond2 .= " ".$in{"field3_2"};}
        if($in{"field3_3"} != "") {$field_cond2 .= " ".$in{"field3_3"};}
        if($in{"field3_4"} != "") {$field_cond2 .= " ".$in{"field3_4"};}
        if($in{"field3_5"} != "") {$field_cond2 .= " ".$in{"field3_5"};}
        if($in{"field3_6"} != "") {$field_cond2 .= " ".$in{"field3_6"};}
        if($in{"field3_7"} != "") {$field_cond2 .= " ".$in{"field3_7"};}
        if($in{"field3_8"} != "") {$field_cond2 .= " ".$in{"field3_8"};}
        if($in{"field3_9"} != "") {$field_cond2 .= " ".$in{"field3_9"};}
        if($in{"field3_10"} != "") {$field_cond2 .= " ".$in{"field3_10"};}
        if($in{"field3_11"} != "") {$field_cond2 .= " ".$in{"field3_11"};}
        if($in{"field3_12"} != "") {$field_cond2 .= " ".$in{"field3_12"};}
        if($in{"field3_13"} != "") {$field_cond2 .= " ".$in{"field3_13"};}
        if($in{"field3_14"} != "") {$field_cond2 .= " ".$in{"field3_14"};}
        if($in{"field3_15"} != "") {$field_cond2 .= " ".$in{"field3_15"};}
        $field_cond2 = trim($field_cond2);
   
        
        //キーワード検索条件
        if($in{"keyword"} != "") {
          
          $keyword = mb_convert_kana($in{"keyword"}, 's');
          $ary_keyword = preg_split('/[\s]+/', $keyword, -1, PREG_SPLIT_NO_EMPTY);
          $keyword_cond .= "and ( 1=2 ";
          $cnt = 0;
          foreach( $ary_keyword as $val ){
            // 検索条件を設定
            $keyword_cond .= " or comments like ('%".$val."%')";
          }
          $keyword_cond .= ")";
          
          
        }
        
        
        $sql    =  '
select
    *
from
    foundation_contents
where flg_delete =0
and flg_delete =0 ';
        if($field_cond != ''){
          $sql .= "and match( field ) against ( '".$field_cond."' in boolean mode )"; 
        }
        if($field_cond != ''){
          $sql .= "and match( businessform ) against ( '".$field_cond2."' in boolean mode )"; 
        }
        if($keyword_cond != ''){
          $sql .= $keyword_cond;
        }


        $cond = array(
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $result_counts = $result->RecordCount();
        $result_ary = array();
         for($j=0;$j<$result_counts;$j++) {
             $result_ary[$j] = $result->fetchRow(DB_FETCHMODE_ASSOC, $j);
         }
        
        return array($result_counts, $result_ary);
        /*
        $result_data = array();
        $i = 0;
        while ($result_data[$i] = $result->fetchRow()) {
            $i++;
        }
echo "i=".$i;        
        return $result_data;
         * */

        //return $result;
    
    }
  
    
    /**
     *  select_mail_exist
     *  メールアドレスが登録済みか？
     */
    function select_mail_exist($email)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $rows   = array();

        $sql    =  '
select
    *
from
    mst_user
where
    flg_delete = 0
and ((auth_is_ok = 1) or (auth_is_ok = 0 and authlimit > now()))
and email = ?
';

        $cond = array(
            $email,
        );


        // クエリ実施
        $result = $db->query($sql, $cond);
        $result_counts = $result->RecordCount();
        $result_ary = array();
         for($j=0;$j<$result_counts;$j++) {
             $result_ary[$j] = $result->fetchRow(DB_FETCHMODE_ASSOC, $j);
         }
        
        return array($result_counts, $result_ary);
        
    }
    
    /**
     *  insert_new_user
     *  会員新規登録
     */
    function insert_new_user($in, $id, $pw, $authkey, $authlimit, $rank, $contents)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
insert into mst_user
(
  id,
  pw,
  company_name,
  company_kana,
  branch_name,
  zip1,
  zip2,
  address,
  phone_no1,
  phone_no2,
  phone_no3,
  fax_no1,
  fax_no2,
  fax_no3,
  k_phone_no1,
  k_phone_no2,
  k_phone_no3,
  department_name,
  post_name,
  contractor_lname,
  contractor_fname,
  contractor_lkana,
  contractor_fkana,
  email,
  come_from,
  mailmaga,
  contents,
  target_businessform,
  target_businessform_str,
  authkey,
  authlimit,
  auth_is_ok,
  rank,
  flg_delete,
  updated,
  created
)
values
(
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);
';

        $cond = array(
            $id,
            $pw,
            $in{'company_name'},
            $in{'company_kana'},
            $in{'branch_name'},
            $in{'zip1'},
            $in{'zip2'},
            $in{'address'},
            $in{'phone_no1'},
            $in{'phone_no2'},
            $in{'phone_no3'},
            $in{'fax_no1'},
            $in{'fax_no2'},
            $in{'fax_no3'},
            $in{'k_phone_no1'},
            $in{'k_phone_no2'},
            $in{'k_phone_no3'},
            $in{'department_name'},
            $in{'post_name'},
            $in{'contractor_lname'},
            $in{'contractor_fname'},
            $in{'contractor_lkana'},
            $in{'contractor_fkana'},
            $in{'email'},
            $in{'from'},
            $in{'mailmaga'},
            $contents,        
            $in{'field3'},
            $in{'field3_string'},        
            $authkey,
            $authlimit,
            1,
            $rank,
            0,
            $in{'updated'},
            $in{'created'}
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();  
        $last_insert_serial = mysql_insert_id();

        return array($affect, $last_insert_serial);

    }
    
    
    /**
     *  delete_user_data
     *  仮登録ユーザデータを論理削除
     */
    function delete_user_data($serial)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    mst_user 
set 
    flg_delete   = 1,
    updated = now()
where 
    serial = ?
';
        $cond = array(
            $serial
        );
        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    
    
    
    /**
     *  select_auth_exist
     *  メールアドレスが登録済みか？
     */
    function select_auth_exist($authkey)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $rows   = array();

        $sql    =  '
select
    *
from
    mst_user
where
    flg_delete = 0
and (auth_is_ok = 0 and authlimit >= now())
and authkey = ?
';

        $cond = array(
            $authkey,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $result_counts = $result->RecordCount();
        $result_ary = array();
         for($j=0;$j<$result_counts;$j++) {
             $result_ary[$j] = $result->fetchRow(DB_FETCHMODE_ASSOC, $j);
         }
        
        return array($result_counts, $result_ary);
        
    }
    
    /**
     *  select_loginid_exist
     *  ログインIDが登録済みか？
     */
    function select_loginid_exist($login_id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $rows   = array();

        $sql    =  '
select
    *
from
    mst_user
where
    flg_delete = 0
and id = ?
';

        $cond = array(
            $login_id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $result_counts = $result->RecordCount();
        $result_ary = array();
         for($j=0;$j<$result_counts;$j++) {
             $result_ary[$j] = $result->fetchRow(DB_FETCHMODE_ASSOC, $j);
         }
        
        return array($result_counts, $result_ary);
        
    }
    
    function select_content($contents_id,$kind)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $rows   = array();

        if($kind=="autonomy"){
          $table_name = "government_contents";
        }else if($kind=="foundation"){
          $table_name = "foundation_contents";
        }else{
          return array(0, null);
        }
        
        $sql    =  '
select
    *
from '.$table_name.
'
where
    flg_delete = 0
and contents_org_id = ?
';

        $cond = array(
            $contents_id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $result_counts = $result->RecordCount();
        $result_ary = array();
         for($j=0;$j<$result_counts;$j++) {
             $result_ary[$j] = $result->fetchRow(DB_FETCHMODE_ASSOC, $j);
         }
        
        return array($result_counts, $result_ary);
        
    }
    
    
    
    /**
     *  update_user_regcomplete
     *  仮登録から本登録へユーザデータをUPDATE
     */
    function update_user_regcomplete($authkey, $id ,$pw, $rank)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    mst_user 
set 
    id   = ?,
    pw = ?,
    auth_is_ok = ?,
    rank = ?,
    updated = now()
where 
    authkey = ?
';
        $cond = array(
            $id,
            $pw,
            1,
            $rank,
            $authkey
        );
        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    
    
    function insert_interest_offer($in, $id, $data)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        
        $sql    =  '
insert into log_interest_offer
(
  id,
  company_name,
  company_kana,
  branch_name,
  zip1,
  zip2,
  address,
  phone_no1,
  phone_no2,
  phone_no3,
  fax_no1,
  fax_no2,
  fax_no3,
  k_phone_no1,
  k_phone_no2,
  k_phone_no3,
  department_name,
  post_name,
  contractor_lname,
  contractor_fname,
  contractor_lkana,
  contractor_fkana,
  email,
  target_field1_str,
  target_field2_str,
  target_businessform_str,
  rank,
  contents_org_id,
  kind,
  flg_delete,
  updated,
  created
)
values
(
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
)
';
        
        
        $cond = array(
            $id,
            $in{'company_name'},
            $in{'company_kana'},
            $in{'branch_name'},
            $in{'zip1'},
            $in{'zip2'},
            $in{'address'},
            $in{'phone_no1'},
            $in{'phone_no2'},
            $in{'phone_no3'},
            $in{'fax_no1'},
            $in{'fax_no2'},
            $in{'fax_no3'},
            $in{'k_phone_no1'},
            $in{'k_phone_no2'},
            $in{'k_phone_no3'},
            $in{'department_name'},
            $in{'post_name'},
            $in{'contractor_lname'},
            $in{'contractor_fname'},
            $in{'contractor_lkana'},
            $in{'contractor_fkana'},
            $in{'email'},
            $in{'target_field1_str'},
            $in{'target_field2_str'},        
            $in{'target_businessform_str'},
            $in{'rank'},       
            $data{'contents_id'},
            $data{'callkind'},        
            0,
            $data{'updated'},
            $data{'created'}
        );
        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();  
        $last_insert_serial = mysql_insert_id();

        return array($affect, $last_insert_serial);
    }
    
    /**
     *  update_user_profile
     *  回答者基本情報データ更新
     */
    function update_user_profile($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_user 
set 
    area = ?,
    marriage_status = ?,
    job_kind = ?,
    yearly_income = ?,
    age = ?,
    sex = ?,
    updated = now()
where 
    id = ?
';
        
        $cond = array(
            $in{"area"},
            $in{"marriage_status"},
            $in{"job_kind"},
            $in{"yearly_income"},
            $in{"age"},
            $in{"sex"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
      
    }
    
    /**
     *  check_answer_exist
     *  回答件数取得
     */
    function check_answer_exist($id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $rows   = array();

        $sql    =  '
select
    count(enquete_id) counts
from
    enquete_person_answer
where
    id = ?
and flg_delete = 0
';

        $cond = array(
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $data   = $result->fetchRow();
        $counts = $data{"counts"};

        return $counts;
    }

    
    /**
     *  select_answer
     *  回答一覧取得
     */
    function select_answer($id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $rows   = array();

        $sql    =  '
select
    *
from
    enquete_person_answer
where
    id = ?
and flg_delete = 0
';

        $cond = array(
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $data   = $result->fetchRow();

        return $data;

    }

    /**
     *  update_user_profile
     *  回答データクリア(q3,q4)
     */
    function delete_answer_q3q4($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q3          = NULL,
    q3_no13_text = NULL,
    q4          = NULL,
    q4_no8_text = NULL,
    updated = now()
where 
    id = ?
';
        $cond = array(
            $in{"last_answer"},
            $id,
        );
        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    
    /**
     *  update_user_profile
     *  回答データクリア(q4_1)
     */
    function delete_answer_q41($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer   = ?,
    q4_1          = NULL,
    q4_1_no7_text = NULL,
    updated = now()
where 
    id = ?
';
        $cond = array(
            $in{"last_answer"},
            $id,
        );
        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    
    /**
     *  insert_answer
     *  回答データ新規登録
     */
    function insert_enquete_person_answer($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
insert into enquete_person_answer
(
    id,
    last_answer,
    flg_delete,
    updated,
    created
)
values
(
    ?,
    ?,
    0,
    now(),
    now()
);
';

        $cond = array(
            $id,
            $in{"last_answer"},
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;

    }
    
    /**
     *  update_answer_e03
     */
    function update_answer_e03($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q1          = ?,
    q2          = ?,
    updated = now()
where 
    id = ?
';
        $cond = array(
            $in{"last_answer"},
            $in{"q1"},
            $in{"q2"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e04
     */
    function update_answer_e04($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q3          = ?,
    q3_no13_text = ?,
    q4          = ?,
    q4_no8_text = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q3"},
            $in{"q3_no13_text"},
            $in{"q4"},
            $in{"q4_no8_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e05
     */
    function update_answer_e05($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q5          = ?,
    q5_no11_text = ?,
    q6          = ?,
    q6_no8_text = ?,
    updated = now()
where 
    id = ?
';
        $cond = array(
            $in{"last_answer"},
            $in{"q5"},
            $in{"q5_no11_text"},
            $in{"q6"},
            $in{"q6_no8_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e06
     */
    function update_answer_e06($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer   = ?,
    q7          = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q7"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e07
     */
    function update_answer_e07($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer         = ?,
    q8_person_kifu = ?,
    q8_person_kifu_cnt        = ?,
    q8_all_kifu = ?,
    q9        = ?,
    q9_gaitobokin        = ?,
    q9_tewatashi        = ?,
    q9_bokinbako        = ?,
    q9_hurikomi        = ?,
    q9_card        = ?,
    q9_gift        = ?,
    q9_syouhin        = ?,
    q9_genbutsu        = ?,
    q9_other        = ?,
    q9_no9_text        = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q8_person_kifu"},
            $in{"q8_person_kifu_cnt"},
            $in{"q8_all_kifu"},
            $in{"q9"},
            $in{"q9_gaitobokin"},
            $in{"q9_tewatashi"},
            $in{"q9_bokinbako"},
            $in{"q9_hurikomi"},
            $in{"q9_card"},
            $in{"q9_gift"},
            $in{"q9_syouhin"},
            $in{"q9_genbutsu"},
            $in{"q9_other"},
            $in{"q9_no9_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e08
     */
    function update_answer_e08($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer         = ?,
    q10 = ?,
    q10_no10_text        = ?,
    q11 = ?,
    q11_no13_text        = ?,
    q12        = ?,
    q12_no8_text        = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q10"},
            $in{"q10_no10_text"},
            $in{"q11"},
            $in{"q11_no13_text"},
            $in{"q12"},
            $in{"q12_no8_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  update_answer_e09
     */
    function update_answer_e09($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer             = ?,
    q13         = ?,
    q13_no7_text          = ?,
    updated                 = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q13"},
            $in{"q13_no7_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e10
     */
    function update_answer_e10($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q14    = ?,
    q15    = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q14"},
            $in{"q15"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e11
     *  問合せの一覧取得
     *
     *  @access protected
     *  @param  string  id  [ログインid]
     */
    function update_answer_e11($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q16    = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q16"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e12
     *  問合せの一覧取得
     *
     *  @access protected
     *  @param  string  id  [ログインid]
     */
    function update_answer_e12($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q17         = ?,
    q17_no4_text         = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q17"},
            $in{"q17_no4_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e13
     */
    function update_answer_e13($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q18 = ?,
    q18_no7_text = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q18"},
            $in{"q18_no7_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e14
     */
    function update_answer_e14($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q9         = ?,
    q9_no6_text = ?,
    q9_important = ?,
    q10_kojin_syain_cnt = ?,
    q10_kojin_syain_kaihisyunyu = ?,
    q10_dantai_syain_cnt = ?,
    q10_dantai_syain_kaihisyunyu = ?,
    q10_kojin_sanjo_cnt = ?,
    q10_kojin_sanjo_kaihisyunyu = ?,
    q10_dantai_sanjo_cnt = ?,
    q10_dantai_sanjo_kaihisyunyu = ?,
    q10_kojin_special_cnt = ?,
    q10_kojin_special_kaihisyunyu = ?,
    q10_dantai_special_cnt = ?,
    q10_dantai_special_kaihisyunyu = ?,
    q10_kojin_other_cnt = ?,
    q10_kojin_other_kaihisyunyu = ?,
    q10_dantai_other_cnt = ?,
    q10_dantai_other_kaihisyunyu = ?,
    q11_num_syokuin_all = ?,
    q11_num_syokuin_woman = ?,
    q11_num_yukyusyokuin_all = ?,
    q11_num_yukyusyokuin_woman = ?,
    q11_jinkenhi_yukyusyokuin = ?,
    q11_num_regular_yukyusyokuin_all = ?,
    q11_num_regular_yukyusyokuin_woman = ?,
    q11_jinkenhi_regular_yukyusyokuin = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q9"},
            $in{"q9_no6_text"},
            $in{"q9_important"},
            $in{"q10_kojin_syain_cnt"},
            $in{"q10_kojin_syain_kaihisyunyu"},
            $in{"q10_dantai_syain_cnt"},
            $in{"q10_dantai_syain_kaihisyunyu"},
            $in{"q10_kojin_sanjo_cnt"},
            $in{"q10_kojin_sanjo_kaihisyunyu"},
            $in{"q10_dantai_sanjo_cnt"},
            $in{"q10_dantai_sanjo_kaihisyunyu"},
            $in{"q10_kojin_special_cnt"},
            $in{"q10_kojin_special_kaihisyunyu"},
            $in{"q10_dantai_special_cnt"},
            $in{"q10_dantai_special_kaihisyunyu"},
            $in{"q10_kojin_other_cnt"},
            $in{"q10_kojin_other_kaihisyunyu"},
            $in{"q10_dantai_other_cnt"},
            $in{"q10_dantai_other_kaihisyunyu"},
            $in{"q11_num_syokuin_all"},
            $in{"q11_num_syokuin_woman"},
            $in{"q11_num_yukyusyokuin_all"},
            $in{"q11_num_yukyusyokuin_woman"},
            $in{"q11_jinkenhi_yukyusyokuin"},
            $in{"q11_num_regular_yukyusyokuin_all"},
            $in{"q11_num_regular_yukyusyokuin_woman"},
            $in{"q11_jinkenhi_regular_yukyusyokuin"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e15
     */
    function update_answer_e15($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer   = ?,
    q12           = ?,
    q12_no9_text  = ?,
    q13_num       = ?,
    q13_days      = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q12"},
            $in{"q12_no9_text"},
            $in{"q13_num"},
            $in{"q13_days"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e16
     */
    function update_answer_e16($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q14 = ?,
    q14_important = ?,
    q15 = ?,
    q15_no8_text = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q14"},
            $in{"q14_important"},
            $in{"q15"},
            $in{"q15_no8_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e17
     */
    function update_answer_e17($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q16 = ?,
    q16_no8_text = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q16"},
            $in{"q16_no8_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_answer_e18
     */
    function update_answer_e18($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q17 = ?,
    q17_no6_text = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q17"},
            $in{"q17_no6_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  update_answer_e19
     */
    function update_answer_e19($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q18_1 = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q18_1"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  update_answer_e20
     */
    function update_answer_e20($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q18_2 = ?,
    q18_2_no5_text = ?,
    q18_2_no7_text = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q18_2"},
            $in{"q18_2_no5_text"},
            $in{"q18_2_no7_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  update_answer_e21
     */
    function update_answer_e21($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q19_1 = ?,
    q19_1_no8_text = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q19_1"},
            $in{"q19_1_no8_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  update_answer_e22
     */
    function update_answer_e22($id, $in)
    { 
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q19_2 = ?,
    q19_2_no10_text = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q19_2"},
            $in{"q19_2_no10_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  update_answer_e23
     */
    function update_answer_e23($id, $in)
    {     
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q20 = ?,
    q20_no5_text = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $in{"q20"},
            $in{"q20_no5_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
 
    /**
     *  update_answer_e28
     */
    function update_answer_e28($id, $in)
    {     
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    
    
    
    
    
    
    
    /**
     *  delete_answer_e04
     *  問3回答を削除
     */
    function delete_answer_e04($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q3_1        = NULL,
    q3_1_fa     = NULL,
    q3_2        = NULL,
    q3_2_fa     = NULL,
    q4          = NULL,
    q4_fa       = NULL,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  delete_answer_q8q9q10q11q12
     *  問8-12回答を削除
     */
    function delete_answer_q8q9q10q11q12($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q8_person_kifu          = ?,
    q8_person_kifu_cnt    = ?,
    q8_all_kifu = ?,
    q9          = ?,
    q9_gaitobokin    = ?,
    q9_tewatashi = ?,
    q9_bokinbako          = ?,
    q9_hurikomi    = ?,
    q9_card = ?,
    q9_gift          = ?,
    q9_syouhin    = ?,
    q9_genbutsu = ?,
    q9_other = ?,
    q10    = ?,
    q10_no10_text = ?,
    q11 = ?,
    q11_no13_text = ?,
    q12 = ?,
    q12_no8_text = ?,
    updated = now()
where 
    id = ?
';
        $cond = array(
            $in{"last_answer"},
            $in{"q8_person_kifu"},
            $in{"q8_person_kifu_cnt"},
            $in{"q8_all_kifu"},
            $in{"q9"},
            $in{"q9_gaitobokin"},
            $in{"q9_tewatashi"},
            $in{"q9_bokinbako"},
            $in{"q9_hurikomi"},
            $in{"q9_card"},
            $in{"q9_gift"},
            $in{"q9_syouhin"},
            $in{"q9_genbutsu"},
            $in{"q9_other"},
            $in{"q10"},
            $in{"q10_no10_text"},
            $in{"q11"},
            $in{"q11_no13_text"},
            $in{"q12"},
            $in{"q12_no8_text"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  delete_answer_e10
     *  問7-2回答を削除
     */
    function delete_answer_e17($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q17   = NULL,
    q17_no4_text   = NULL,
    updated = now()
where 
    id = ?
';      
        $cond = array(
            $in{"last_answer"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  delete_answer_e10
     *  問17回答を削除
     */
    function delete_answer_e12($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer = ?,
    q8_2 = NULL,
    q8_2_kojin_kariire = NULL,
    q8_2_seifu_kariire = NULL,
    q8_2_ginko_kariire = NULL,
    q8_2_sinyokinko_kariire = NULL,
    q8_2_rodokinko_kariire = NULL,
    q8_2_sinyokumiai_kariire = NULL,
    q8_2_tihojititai_kariire = NULL,
    q8_2_npo_kariire = NULL,
    q8_2_other_kariire = NULL,
    q8_2_no9_text = NULL,
    updated = now()
where 
    id = ?
';      
        $cond = array(
            $in{"last_answer"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  delete_answer_e08e09e10
     *  問合せの一覧取得
     *
     *  @access protected
     *  @param  string  id  [ログインid]
     */
    function delete_answer_e08e09e10($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    ENQUETE_ANSWER 
set 
    last_answer = ?,
    q9_1        = NULL,
    q9_2        = NULL,
    q9_3        = NULL,
    q9_4        = NULL,
    q9_5        = NULL,
    q9_6        = NULL,
    q10         = NULL,
    q10_fa      = NULL,
    q11         = NULL,
    q11_fa      = NULL,
    q12_1       = NULL,
    q12_1_fa    = NULL,
    q12_2       = NULL,
    q12_2_fa    = NULL,
    q13         = NULL,
    q13_fa      = NULL,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  delete_answer_e15
     *  問合せの一覧取得
     *
     *  @access protected
     *  @param  string  id  [ログインid]
     */
    function delete_answer_e15($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    ENQUETE_ANSWER 
set 
    last_answer = ?,
    q19         = NULL,
    q19_fa      = NULL,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  delete_answer_e18
     */
    function delete_answer_e18($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer     = ?,
    q18_1           = NULL,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  delete_answer_e19
     */
    function delete_answer_e19($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer     = ?,
    q18_2           = NULL,
    q18_2_no5_text  = NULL,
    q18_2_no7_text  = NULL,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  delete_answer_e20
     */
    function delete_answer_e20($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer     = ?,
    q19_1           = NULL,
    q19_1_no8_text  = NULL,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  delete_answer_e21
     */
    function delete_answer_e21($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer       = ?,
    q19_2             = NULL,
    q19_2_no10_text   = NULL,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    /**
     *  delete_answer_e22
     */
    function delete_answer_e22($id, $in)
    { 
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update 
    enquete_person_answer 
set 
    last_answer       = ?,
    q20             = NULL,
    q20_no5_text   = NULL,
    updated = now()
where 
    id = ?
';

        $cond = array(
            $in{"last_answer"},
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;
    }
    
    
    /**
     *  insert_mail
     *  問合せメール保存
     *
     *  @access protected
     *  @param  string  id  [ログインid]
     */
    function insert_mail($id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
insert into enquete_person_mail
(
    id,
    email,
    title,
    mailbody,
    flg_delete,
    updated,
    created
)
values
(
    ?,
    ?,
    ?,
    ?,
    0,
    now(),
    now()
);
';

        $cond = array(
            $id,
            $in{"in_email"}, 
            $in{"in_title"},
            $in{"in_mailbody"},
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;

    }

    /**
     *  select_mail
     *  問合せの一覧取得
     *
     *  @access protected
     *  @param  string  sort[ソートカラム]
     *          string  order[ソートオーダ値 ](DESC/ASC)
     *          int     start_row 開始行
     *          int     fetch_row 取得行数
     *          array   in        パラメータ連想配列
     *  @return array   row  [結果配列]
     */
    function select_mail($id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $rows   = array();

        $sql    =  '
select
    *
from
    enquete_person_mail
where
    id = ?
and flg_delete = 0
order by
    mail_id
';

        $cond = array(
            $id,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $data   = $result->fetchRow();

        return $data;

    }



    /**
     *  total_input
     *  問合せの一覧取得
     *
     *  @access protected
     *  @param  string  sort[ソートカラム]
     *          string  order[ソートオーダ値 ](DESC/ASC)
     *          int     start_row 開始行
     *          int     fetch_row 取得行数
     *          array   in        パラメータ連想配列
     *  @return array   row  [結果配列]
     */
    function total_input($list_mode)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $rows   = array();

        $sql    =  '
select DATE_FORMAT(updated,"%Y%m%d") as hiduke, count(*) as CNT 
from ENQUETE_ANSWER 
where enquete_id > 16
and id not in("e4mnp68u","aj4stuxy","fj47rtux","afk24msw","3uzi5cb8","0001","0002","0003","0004","0007","0010","0011")
and last_answer = "e20"
and DATE_FORMAT(updated,"%Y%m%d") < DATE_FORMAT(now(),"%Y%m%d")
';
if($list_mode != 1){// デフォルトは昨日の分だけ
        $sql .=  ' and DATE_FORMAT(updated,"%Y%m%d") = DATE_FORMAT(now() - interval 1 day,"%Y%m%d")';
}
        $sql    .=  '
group by DATE_FORMAT(updated,"%Y%m%d")
';


        // クエリ実施
        $result = $db->query($sql);
//        $data   = $result->fetchRow();

        return $result;

    }

    /**
     *  total_input_notfinish
     *  問合せの一覧取得
     *
     *  @access protected
     *  @param  string  sort[ソートカラム]
     *          string  order[ソートオーダ値 ](DESC/ASC)
     *          int     start_row 開始行
     *          int     fetch_row 取得行数
     *          array   in        パラメータ連想配列
     *  @return array   row  [結果配列]
     */
    function total_input_notfinish()
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $rows   = array();

        $sql    =  '
select count(*) as CNT 
from ENQUETE_ANSWER 
where enquete_id > 16
and id not in("e4mnp68u","aj4stuxy","fj47rtux","afk24msw","3uzi5cb8","0001","0002","0003","0004","0007","0010","0011")
and last_answer != "e20"
';


        // クエリ実施
        $result = $db->query($sql);
        $data   = $result->fetchRow();

        return $data;

    }




    function insert_user($rand_val)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
insert into ENQUETE_USER
(
    id,
    pw,
    role,
    flg_delete,
    updated,
    created
)
values
(
    ?,
    "67f6e7eee503c7bc52ee901fbb6c6c86",
    1,
    0,
    now(),
    now()
);
';

        $cond = array(
            $rand_val,
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();

        return $affect;

    }

    /**
     *  select_u_data
     *  利用者データを取得
     *
     *  @access protected
     *  @param  array   in        パラメータ連想配列
     *  @return array   result    結果オブジェクト
     */
    function mst_user_data($id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    = 'select * from mst_user where id = "'.$id.'"';

        $result = $db->query($sql);
        $data   = $result->fetchRow();
        return $data;
    }


}
?>
