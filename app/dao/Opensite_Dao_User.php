<?php
/**
 *  Opensite_Dao_User.php
 */

/**
 *  Opensite_Dao_User Class.
 */
class Opensite_Dao_User
{
    /**
     *  is_toc_consent
     *  利用規約既読確認
     * 
     * @param string  user_id  ユーザーID
     * @return boolean 既読フラグ
     */
    function is_tos_consent($user_id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $flg_delete = 0;
        
        $sql    =  '
select
    flg_tos_consent as flg_tos_consent
from
    mst_user
where
    flg_delete = ?
and
    serial = ?
';

        $cond = array(
            $flg_delete,
            $user_id,
        );

        // クエリ実行
        $result = $db->getOne($sql, $cond);
        
        if(Ethna::isError($result))
        {
            return $result;
        }
        
        if(false === $result)
        {
            return Ethna::raiseError("既読フラグを読み込めませんでした。",E_FURU_DB_USER_FLG_VALID);
        }
        
        return $result;
    }
    
    /**:
     * update_tos_consent
     * 既読フラグの更新
     * 
     * @param string $user_id
     * @param bool $flg
     * @return object
     */
    function update_tos_consent($user_id,$flg = true)
    {
       $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();
        $flg_delete = 0;
        
        $sql    =  '
update
    mst_user
set    
    flg_tos_consent = ?
where
    flg_delete = ?
and
    serial = ?
';

        $cond = array(
            $flg,
            $flg_delete,
            $user_id,
        );

        // クエリ実行
        $result = $db->query($sql, $cond);
        
        if(Ethna::isError($result))
        {
            $db->rollback;
            return $result;
        }
        $db->commit();

        return;
    }
}
?>
