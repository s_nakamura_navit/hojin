<?php
/**
 *  Opensite_Dao_Alliance.php
 */

/**
 *  Opensite_Dao_Alliance Class.
 */
class Opensite_Dao_Alliance
{
	///
	///
	///		アライアンスが有効かチェックします
	///
	///
	function valid_alliance($allcd){
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  "select * from mst_alliance where allcd = '" . $allcd . "'";
		$sql    .=  " and flag_delete = '0' and start_service <= '" . date("Y-m-d H:i:s") . "'";
		$sql    .=  " and end_service >= '" . date("Y-m-d H:i:s") . "'";
        $cond = array( $allcd );
		$result = $db->getAll($sql);	// クエリ実行
       	return $result;
	}
	///
	///
	///		アライアンスからのLPでのアクセスログを登録します
	///
	///
	function add_log($alliance_id){
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  'insert into log_alliance ( alliance_id, ip_address, created ) values (?, ?, now());';
        $cond = array(
            $alliance_id,
            $_SERVER["REMOTE_ADDR"]
        );
        // クエリ実施
        $result = $db->query($sql, $cond);
	}
}
?>
