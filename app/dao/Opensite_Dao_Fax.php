<?php
/**
 *  Opensite_Dao_Fax.php
 */

/**
 *  Opensite Dao_Fax Class.
 */
class Opensite_Dao_Fax
{
    /**
     *  search_send_data
     *  法人電話帳へのマッチング処理(電話番号)
     * 
     * @param string  tel  [電話番号]
     * @return array Fax配信リスト
     */
    function search_send_data($tel)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
select
    ido,
    kdo,
    match_lv
from
    send_data
where
    tel_no = ?
';

        $cond = array(
            $tel,
        );

        // クエリ実行
        $result = $db->query($sql, $cond);
        
        $data = array();
        $data = $result->fetchRow();
        return $data;
    }

    /**
     *  get_fax_list
     *  Fax送信状況一覧取得(過去1年間のデータのみ、最大30件まで)
     * 
     * @param string  user_id  [ログインid]
     * @return array Fax配信リスト
     */
    function get_fax_list($user_id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
select
    fax.id as id,
    fax.user_id as user_id,
    fax.fax_id as fax_id,
    DATE_FORMAT(case when fax.published_at = "1970/01/01 09:00" then fax.created_at else fax.published_at end,"%Y/%m/%d %H:%i") as published_at,
    status.status as status,
    fax.message as message,
    fax.updated_at as updated_at,
    fax.created_at as created_at
from
    trn_fax_transmissions fax
left join mst_fax_status status on fax.status = status.id
where
    fax.user_id = ?
and
    fax.flg_delete = 0
order by published_at desc
limit 30
';

        $cond = array(
            $user_id,
        );

        // クエリ実行
        $result = $db->query($sql, $cond);
        
        $data = array();
        while ( $buf = $result->fetchRow()) {
            if( $buf != false)
            {
                $data[] = $buf;
            }
        }
        return $data;
    }

    function receipt_fax_transmission($fax_id,$flg_delete = 0)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();

        // SQL定義
        $sql    =  '
update 
    trn_fax_transmissions
set
    flg_delete = ?,
    updated_at = ?
where
    fax_id = ?
';
       $cond = array(
           $flg_delete,
           date("Y-m-d H:i:s"),
           $fax_id
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $db->commit();
        
        $affect = $db->db->Affected_Rows(); 
        return;
    }
    
    /**
     * regist_fax_transmission
     * Fax送信情報登録
     * 
     * @param int       $user_id    ログインID
     * @param string    $fax_id     Nexlink FaxID
     * @param string    $datetime   配信日時
     * @param int       $status     配信ステータス
     * @param string    $message    詳細
     * @return boolean
     */
    function regist_fax_transmission($user_id,$fax_id,$datetime,$status,$message,$flg_delete = 0)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();

        // SQL定義
        $sql    =  '
insert into
    trn_fax_transmissions
(
    user_id,
    fax_id,
    published_at,
    status,
    message,
    flg_delete,
    updated_at,
    created_at
)
values
(
    ?,?,?,?,?,?,?,?
)
';
       $cond = array(
           $user_id,
           $fax_id,
           $datetime,
           $status,
           $message,
           $flg_delete,
           date("Y-m-d H:i:s"),
           date("Y-m-d H:i:s"),
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $db->commit();
        
        $affect = $db->db->Affected_Rows(); 
        $last_insert_id = mysql_insert_id();

        return array($affect, $last_insert_id);
    }

    public function update_fax_status($user_id,$fax_id,$status)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();

        // SQL定義
        $sql    =  '
update 
    trn_fax_transmissions
set
    status = ?,
    updated_at = ?
where
    fax_id = ?
and
    user_id = ?
';

//        unsent: 未送信
//        reserved: 送信予約中
//        preparation: 送信準備中
//        sending: 送信中
//        done: 送信完了
//        non_delivery: 送信完了（不達あり）
//        error: 送信完了（エラーあり）
        
        $mst_status = array(
            "unsent"        => 0,
            "reserved"      => 1,
            "preparation"   => 2,
            "sending"       => 3,
            "done"          => 4,
            "non_delivery"  => 5,
            "error"         => 6,
        );

       $cond = array(
           $mst_status[$status],
           date("Y-m-d H:i:s"),
           $fax_id,
           $user_id
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $db->commit();
        
        $affect = $db->db->Affected_Rows(); 
        return;
        
    }
    
}
?>
