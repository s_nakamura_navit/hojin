<?php
/**
 *  Opensite_Dao_Request.php
 */

/**
 *  Opensite_Dao_Request Class.
 */
class Opensite_Dao_Request
{
    /**
     *  insert_request_point
     *  ポイント付与依頼履歴新規作成
     */
    function insert_request_point($serial, $id, $in, $flg_processed)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

	$contents = "hojin";

        $sql    =  '
insert into mst_request_point
(
  serial,
  user_serial,
  user_id,
  company_name,
  zip1,
  zip2,
  address,
  department_name,
  post_name,
  contractor_lname,
  contractor_fname,
  contractor_lkana,
  contractor_fkana,
  contents,
  payment,
  email,
  request_lot,
  flg_processed,
  flg_delete,
  created
)
values
(
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  now()
);
';

        $cond = array(
            $in{'request_point_id'},
            $serial,
            $id,
            $in{'company_name'},
            $in{'zip1'},
            $in{'zip2'},
            $in{'address'},
            $in{'department_name'},
            $in{'post_name'},
            $in{'contractor_lname'},
            $in{'contractor_fname'},
            $in{'contractor_lkana'},
            $in{'contractor_fkana'},
            $contents,
            $in{'payment'},
            $in{'email'},
            $in{'point'},
	    $flg_processed,
            0
        );
//echo $sql."<br />";
//var_dump($cond);
//exit;
        // クエリ実施
        $result = $db->query($sql, $cond);
	//エラーはaction側で判断している
        $affect = $db->db->Affected_Rows();
        $last_insert_serial = mysql_insert_id();

        return array($affect, $last_insert_serial);

    }

    /**
     *  delete_request_point
     *  ポイント付与依頼履歴を論理削除
     */
    function delete_request_point($serial)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update
    mst_request_point
set
    flg_delete   = 1
where
    serial = ?
';
        $cond = array(
            $serial
        );
        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            return Ethna::raiseError("ポイント購入依頼情報の論理削除に失敗しました。(serial:[".$serial."])",E_FURU_DB_DELETE_REQUEST_FAILD);
        }
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  insert_request_point_credit_card
     *  ポイント付与依頼履歴(creditcard用事前登録)新規作成	//20150427クレジットカード決済本実装の為不使用
     */
    function insert_request_point_credit_card($serial, $id, $in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
insert into mst_request_point_creditcard_logs
(
  request_point_serial,
  user_serial,
  user_id,
  company_name,
  zip1,
  zip2,
  address,
  department_name,
  post_name,
  contractor_lname,
  contractor_fname,
  contractor_lkana,
  contractor_fkana,
  email,
  request_lot,
  flg_delete,
  created
)
values
(
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  now()
);
';

        $cond = array(
            $in{'request_id'},
            $serial,
            $id,
            $in{'company_name'},
            $in{'zip1'},
            $in{'zip2'},
            $in{'address'},
            $in{'department_name'},
            $in{'post_name'},
            $in{'contractor_lname'},
            $in{'contractor_fname'},
            $in{'contractor_lkana'},
            $in{'contractor_fkana'},
            $in{'email'},
            $in{'point'},
            0
        );
//echo $sql;
//var_dump($cond);
//exit;
        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            return Ethna::raiseError("ポイント購入依頼(クレジットカード事前履歴)情報の作成に失敗しました。(request_id:[".$request_id."])",E_FURU_DB_INSERT_REQUEST_CC_FAILD);
        }
        $affect = $db->db->Affected_Rows();
        $last_insert_serial = mysql_insert_id();

        return array($affect, $last_insert_serial);

    }

    /**
     *  delete_request_point_credit_card
     *  ポイント付与依頼履歴(creditcard用)を論理削除	//20150427クレジットカード決済本実装の為不使用
     */
    function delete_request_point_credit_card($serial)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update
    mst_request_point_creditcard_logs
set
    flg_delete   = 1
where
   request_point_serial = ?
';
        $cond = array(
            $serial
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            return Ethna::raiseError("ポイント購入依頼(クレジットカード事前履歴)情報の論理削除に失敗しました。(serial:[".$serial."])",E_FURU_DB_DELETE_REQUEST_CC_FAILD);
        }
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  update_request_point
     *  ポイント付与依頼履歴を論理削除
     */
    function update_request_point($serial,$flg_process,$flg_delete)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update
    mst_request_point
set
    flg_processed   = ?,
    flg_delete   = ?
where
    serial = ?
';
        $cond = array(
            $flg_process,
            $flg_delete,
            $serial
        );
//echo $sql."<br />";
//var_dump($cond);
//exit;
        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            return Ethna::raiseError("ポイント購入依頼情報の更新に失敗しました。(serial:[".$serial."])",E_FURU_DB_DELETE_REQUEST_POINT_FAILD);
        }
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  delete_request_point
     *  mst_userに名前かなを更新登録
     */
    function update_mst_user_name_kana($serial,$in)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
update
    mst_user
set
    contractor_lkana = ?,
    contractor_fkana = ?,
    updated = now()
where
   serial = ?
';
        $cond = array(
            $in['contractor_lkana'],
            $in['contractor_fkana'],
            $serial
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            return Ethna::raiseError("ユーザー名前かな情報の更新に失敗しました。(serial:[".$serial."])",E_GEO_UPDATE_MST_USER_FAILD);
        }
        $affect = $db->db->Affected_Rows();

        return $affect;
    }

    /**
     *  get_request_point_data
     *  ポイント付与依頼履歴情報を取得
     */
    function get_request_point_data($serial)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
select
    *
from
    mst_request_point
where
    serial = ?
';
        $cond = array(
            $serial
        );
//echo $sql."<br />";
//var_dump($cond);
//exit;

        // クエリ実施
        $result = $db->getRow($sql, $cond);

        // エラーが起きた場合
        if(false === $result)
        {
            return Ethna::raiseError('Query実行エラー',E_Opensite_DB);
        }

        // ユーザー情報が見つからない場合
        if( 0 === ($affect = $db->db->Affected_Rows()) )
        {
            return Ethna::raiseError('ポイント購入依頼情報がありません。(ID:['.$serial.'])',E_GEO_DB_USER_NOT_FOUND);
        }

        return $result;
    }
}
?>
