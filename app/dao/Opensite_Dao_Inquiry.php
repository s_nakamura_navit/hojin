<?php
/**
 *  Opensite_Dao_Inquiry.php
 */

/**
 *  Opensite Dao_Inquiry Class.
 */
class Opensite_Dao_Inquiry
{
    /**
     *  insert_new_user
     *  お問い合わせ内容登録
     */
    function insert_mst_inquiry($in, $serial ,$id)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

	$contents = "geo";

        $sql    =  '
insert into mst_inquiry
(
  user_serial,
  user_id,
  company_name,
  company_kana,
  branch_name,
  zip1,
  zip2,
  address,
  phone_no1,
  phone_no2,
  phone_no3,
  fax_no1,
  fax_no2,
  fax_no3,
  k_phone_no1,
  k_phone_no2,
  k_phone_no3,
  department_name,
  post_name,
  contractor_lname,
  contractor_fname,
  contractor_lkana,
  contractor_fkana,
  email,
  contents,
  inquiry_body,
  flg_delete,
  updated,
  created
)
values
(
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?,
  ?
);
';

        $cond = array(
            $serial,
            $id,
            $in{'company_name'},
            $in{'company_kana'},
            $in{'branch_name'},
            $in{'zip1'},
            $in{'zip2'},
            $in{'address'},
            $in{'phone_no1'},
            $in{'phone_no2'},
            $in{'phone_no3'},
            $in{'fax_no1'},
            $in{'fax_no2'},
            $in{'fax_no3'},
            $in{'k_phone_no1'},
            $in{'k_phone_no2'},
            $in{'k_phone_no3'},
            $in{'department_name'},
            $in{'post_name'},
            $in{'contractor_lname'},
            $in{'contractor_fname'},
            $in{'contractor_lkana'},
            $in{'contractor_fkana'},
            $in{'email'},
            $contents,
            mysql_real_escape_string($in{'inquiry'}),
            0,
            $in{'updated'},
            $in{'created'}
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        $affect = $db->db->Affected_Rows();  
        $last_insert_serial = mysql_insert_id();

        return array($affect, $last_insert_serial);

    }
}
?>
