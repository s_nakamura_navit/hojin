<?php
/**
 *  Opensite_Dao_Geo.php
 */

/**
 *  Opensite Dao_Geo Class.
 */
class Opensite_Dao_Geo
{
    /**
     *  search_send_data
     *  法人電話帳へのマッチング処理(電話番号)
     * 
     * @param string  tel  [電話番号]
     * @return array リストマッチング処理
     */
    function search_send_data($tel,$lv_check,$latlon_type)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();

        $sql    =  '
select
    admin_no,
    match_lv
from
    send_data_spare
where
    admin_no <> 0
and
    tel_no_matching = ?
';

	$cond = array(
		$tel
	);

        // クエリ実行
        $result = $db->query($sql, $cond);

	//件数出し
        $num = $result->numRows();

	$data = array();

	if($num > 1){ 
	    $i=0;
	    //抽出された件数の中で、最もマッチレベルが高いデータを優先する
            while($arr = $result->fetchRow()){
	        if($arr['match_lv'] == 7){
		    //$data['ido'] = $arr['ido'];
		    //$data['kdo'] = $arr['kdo'];
		    $data['admin_no'] = $arr['admin_no'];
		    $data['match_lv'] = $arr['match_lv'];
		    break;
	        }else if($arr['match_lv'] == 5){
		    //$data['ido'] = $arr['ido'];
		    //$data['kdo'] = $arr['kdo'];
		    $data['admin_no'] = $arr['admin_no'];
		    $data['match_lv'] = $arr['match_lv'];
	        }else if($data['id'] != ""){
		    //$data['ido'] = $arr['ido'];
		    //$data['kdo'] = $arr['kdo'];
		    $data['admin_no'] = $arr['admin_no'];
		    $data['match_lv'] = $arr['match_lv'];
	        }
		$i++;
	    }
	}else{
	    //件数が1件、もしくは0件の場合
	    $data = $result->fetchRow();
	}
	
	if($lv_check != ""){
		$latlon = array();
		foreach($lv_check as $lv){
			$lv_num = str_replace("lv_","",$lv);
			if($lv_num == $data['match_lv']){
				$lat = "lat_".$latlon_type;
				$lon = "lon_".$latlon_type;
				$sql = "select * from send_data_geo_latlon where admin_no = ?";
				$cond = array($data['admin_no']);
				$result = $db->query($sql, $cond);
				$arr = $result->fetchRow();
				$contents['match_lv'] = $data['match_lv'];
				$contents['ido'] = $arr[$lat];
				$contents['kdo'] = $arr[$lon];
				break;
			}
		}
		return $contents;
	}else{
        	return $data;
	}
    }

    /**
     *  get_fax_list
     *  Geo送信状況一覧取得(過去1年間のデータのみ、最大30件まで)
     * 
     * @param string  user_id  [ログインid]
     * @return array Geo配信リスト
     */
    function insert_process_log($id,$user_serial,$user_id,$list_name,$list_row,$latlon_type,$match_lv,$get_row,$filePath)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDb();
        $db->begin();

	foreach($match_lv as $val){
		if($match_lv_str == ""){
			$match_lv_str = $val;
		}else{
			$match_lv_str .= ",".$val;
		}
	}

        // SQL定義
        $sql    =  '
insert into
    geo_process_logs
(
    id,
    user_serial,
    user_id,
    list_name,
    list_row,
    latlon_type,
    use_match_lv,
    get_row,
    csv_filepath,
    created
)
values
(
    ?,?,?,?,?,?,?,?,?,?
)
';

       $cond = array(
           $id,
           $user_serial,
           $user_id,
           $list_name,
           $list_row,
	   $latlon_type,
           $match_lv_str,
           $get_row,
           $filePath,
           date("Y-m-d H:i:s")
        );

        // クエリ実施
        $result = $db->query($sql, $cond);
        if(Ethna::isError($result))
        {
            $db->rollback();
            Ethna::raiseNotice("ユーザID:[".$user_id."] 緯度経度付与処理結果ファイルパス:[".$filePath."]",E_GEO_DB_POINT_USE_FAILD);
            return Ethna::raiseError("緯度経度付与処理履歴の登録に失敗しました。",E_GEO_DB_POINT_USE_FAILD);
        }

        $db->commit();
        return;
    }    
}
?>
