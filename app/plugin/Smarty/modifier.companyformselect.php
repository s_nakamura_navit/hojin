<?php
/*
* Smarty plugin
* -------------------------------------------------------------
* File:     modifier.formselect.php
* Type:     modifier
* Name:     formselect
* Purpose:  セレクトボックス生成
* Author:   Tetsuo Nakanishi
* -------------------------------------------------------------
*/
function smarty_modifier_companyformselect($default_selected=null, $option=0)
{
    // グループマスター
// 20121215 changed by sano
//  $master_company = Enquete_MasterManager::get_master_tsv("company", 1);
/***
    $master_company = array(
                                    "1" => "Navit",
                                    "2" => "NSM",
                                    "3" => "Google",
                                    "4" => "Yahoo!",
                                    "5" => "Microsoft",
                                    "6" => "Mapion",
                                    "7" => "NRI"
    );
***/
    $master_company = Enquete_MasterManager::get_master("CLIENT_ID", "serial", "name");
// 20121215 changed by sano

    $option_flg = false;
    if($option==1){
        $option_flg = true;
    }

    $pulldown = Enquete_Formparts::pulldown($master_company, $default_selected, $option_flg);

    return $pulldown;
}
?>