<?php
/*
* Smarty plugin
* -------------------------------------------------------------
* File:     modifier.formselect2.php
* Type:     modifier
* Name:     formselect2
* Purpose:  セレクトボックス生成
* Author:   Tetsuo Nakanishi
* -------------------------------------------------------------
*/
function smarty_modifier_roleformselect2($default_selected=null, $option=0)
{
    //TSVマスタデータ
    $master_role     = Enquete_MasterManager::get_master_tsv("role2",  1);

    $option_flg = false;
    if($option==1){
        $option_flg = true;
    }

    $pulldown = Enquete_Formparts::pulldown($master_role, $default_selected, $option_flg);

    return $pulldown;
}
?>