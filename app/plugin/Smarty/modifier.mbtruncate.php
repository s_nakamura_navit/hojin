<?php
/*
* Smarty plugin
* -------------------------------------------------------------
* File:     modifier.mbtruncate.php
* Type:     modifier
* Name:     mbtruncate
* Purpose:  マルチバイト文字を文字化けさせずに任意文字数で切り捨て、任意の記号をその後に続ける
* Author:   Tetsuo Nakanishi
* -------------------------------------------------------------
*/
function smarty_modifier_mbtruncate($string, $length = 80, $etc = '') {
    if ($length == 0) {return '';}
    if($etc == '') $etc = '・・・';
    if (mb_strlen($string) > $length) {
        return mb_substr($string, 0, $length).$etc;
    } else {
        return $string;
    }
}

?>