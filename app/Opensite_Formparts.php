<?php
/**
 *  Opensite_Formparts.php
 *  フォーム部品生成クラス
 *
 *  @copyright  (C)2010 Navit co.,ltd.
 *  @author     Tetsuo Nakanishi
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  Opensite Formparts Class.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Formparts
{
    /**
     *  pulldown
     *  プルダウン生成
     *
     *  @access protected
     *  @param  array   arr      プルダウン項目配列
     *          string  selected 選択行
     *          boolean noselect 指定なし行の有無
     *  @return string  pulldown プルダウンHTML
     */
    function pulldown($arr, $selected, $noselect = true, $wordselect = null)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();

        $pulldown = '';
        if($noselect){
            if($wordselect == 1){
                $pulldown = '<option value="">選択してください</option>';
            } else if($wordselect == 2){
                $pulldown = '<option value="">指名する場合選択</option>';
            } else {
                $pulldown = '<option value="'.''.'">'.$wordselect.'</option>';
            }
        }
        foreach($arr as $k => $v){
            if(strcmp($k, $selected) == 0){
                $pulldown.= '<option value="'.$k.'" selected>'.$v.'</option>'.DELIMITTER_CRLF;
            }else{
                $pulldown.= '<option value="'.$k.'">'.$v.'</option>'.DELIMITTER_CRLF;
            }
        }

        return $pulldown;
    }

    /**
     *  radio
     *  ラジオ生成
     *
     *  @access protected
     *  @param  array   arr      ラジオ項目配列
     *          string  checked 選択項目
     *          array option 指定の選択項目の下にHTMLタグを挿入したい場合に指定($option["項目番号(1～)"＝"<textarea>～</textarea>"])
     *  @return string  radio    ラジオHTML
     */
    function radio($name, $arr, $checked, $delimitter, $option=null)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $radio = '';
        $cnt = 0;
        foreach($arr as $k => $v){
            $cnt++;
            if(strcmp($k, $checked) == 0){
                if($option!=null){
                    $append_option_flg = false;
                    foreach($option as $key => $tagstring){
                        if($key==$cnt){
                            $radio.= '<label id="'.'label'.'_'.$name.'_'.$cnt.'"><input type="radio" id="'.$name."_".$cnt.'" name="'.$name.'" value="'.$k.'" checked> '.$v.'</label>'.'<br id="'.'tagBr'.'_'.$name.'_'.$cnt.'"  />'.DELIMITTER_CRLF;
                            $radio.= $tagstring;//指定のHTMLタグを挿入
                            $append_option_flg=true;
                        }
                    }
                    if($append_option_flg==false){
                        $radio.= '<label id="'.'label'.'_'.$name.'_'.$cnt.'"><input type="radio" id="'.$name."_".$cnt.'" name="'.$name.'" value="'.$k.'" checked> '.$v.'</label>'.$delimitter.DELIMITTER_CRLF;
                    }
                }else{
                    $radio.= '<label id="'.'label'.'_'.$name.'_'.$cnt.'"><input type="radio" id="'.$name."_".$cnt.'" name="'.$name.'" value="'.$k.'" checked> '.$v.'</label>'.$delimitter.DELIMITTER_CRLF;
                }
            }else{
                if($option!=null){
                    $append_option_flg = false;
                    foreach($option as $key => $tagstring){
                        if($key==$cnt){
                            $radio.= '<label id="'.'label'.'_'.$name.'_'.$cnt.'"><input type="radio" id="'.$name."_".$cnt.'" name="'.$name.'" value="'.$k.'"> '.$v.'</label>'.'<br id="'.'tagBr'.'_'.$name.'_'.$cnt.'"  />'.DELIMITTER_CRLF;
                            $radio.= $tagstring;//指定のHTMLタグを挿入
                            $append_option_flg=true;
                        }
                    }
                    if($append_option_flg==false){
                        $radio.= '<label id="'.'label'.'_'.$name.'_'.$cnt.'"><input type="radio" id="'.$name."_".$cnt.'" name="'.$name.'" value="'.$k.'"> '.$v.'</label>'.$delimitter.DELIMITTER_CRLF;
                    }
                }else{
                    $radio.= '<label id="'.'label'.'_'.$name.'_'.$cnt.'"><input type="radio" id="'.$name."_".$cnt.'" name="'.$name.'" value="'.$k.'"> '.$v.'</label>'.$delimitter.DELIMITTER_CRLF;
                }
            }
        }
        return $radio;
    }

    /**
     *  radio2
     *  ラジオ生成
     *
     *  @access protected
     *  @param  array   arr      ラジオ項目配列
     *          string  checked 選択項目
     *  @return string  radio    ラジオHTML
     */
    function radio2($name, $arr, $checked, $delimitter)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();

        $radio = '';
        $cnt++;
        foreach($arr as $k => $v){
            if(strcmp($k, $checked) == 0){
                $radio.= '<label><input type="radio" id="'.$name."_".$cnt.'" name="'.$name.'" value="'.$k.'" onClick="javascript:through_mem_fees('.$k.');" checked> '.$v.'</label>'.$delimitter.DELIMITTER_CRLF;
            }else{
                $radio.= '<label><input type="radio" id="'.$name."_".$cnt.'" name="'.$name.'" value="'.$k.'" onClick="javascript:through_mem_fees('.$k.');"> '.$v.'</label>'.$delimitter.DELIMITTER_CRLF;
            }
        }
        return $radio;
    }

    /**
     *  sort_arrow
     *  ソートアロー生成
     *
     *  ソート対象のレコード名の配列が必要
     *  また、画面内に「sort_label」「sort_order」のhiddenを設けること
     *
     *  @access protected
     *  @param  array   targets    対象レコード
     *          string  sort_label ソートラベル
     *          string  sort_order ソート順
     *  @return array   sort       ソートHTML（レコード毎の配列）
     */
    function sort_arrow($targets, $sort_label, $sort_order)
    {
        $sort = array();
        foreach($targets as $v){

            // m.MEMBER_ID 等 prefix 部があれば削りフォーム名に合わせる
            $sort_name = preg_replace("/^[a-z]\./", "", $v);

            if($v == $sort_label && $sort_order == "ASC"){
                $sort{$sort_name} = '<input type="image" src="img/arrow_down_red.gif" onClick="document.getElementById(\'sort_label\').value=\''.$v.'\'; document.getElementById(\'sort_order\').value=\'ASC\'; submit();">';
            }else{
                $sort{$sort_name} = '<input type="image" src="img/arrow_down.gif" onClick="document.getElementById(\'sort_label\').value=\''.$v.'\'; document.getElementById(\'sort_order\').value=\'ASC\'; submit();">';
            }

            if($v == $sort_label && $sort_order == "DESC"){
                $sort{$sort_name}.= '<input type="image" src="img/arrow_up_red.gif" onClick="document.getElementById(\'sort_label\').value=\''.$v.'\'; document.getElementById(\'sort_order\').value=\'DESC\'; submit();">';
            }else{
                $sort{$sort_name}.= '<input type="image" src="img/arrow_up.gif" onClick="document.getElementById(\'sort_label\').value=\''.$v.'\'; document.getElementById(\'sort_order\').value=\'DESC\'; submit();">';
            }
        }

        return $sort;
    }
}
?>
