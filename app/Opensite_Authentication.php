<?php
/**
 *  Opensite_Authentication.php
 *  認証モデル
 *
 *  @copyright  (C)2010 Navit co.,ltd.
 *  @author     Tetsuo Nakanishi
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  Opensite Authentication Class.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Authentication
{
    /**
     *  authentication user
     *
     *  @access protected
     *  @return array   user_id,
     *                  password
     */
    function auth($id, $pw, $captcha_code, $captcha_keystring)
    {
        if ($id == "") {
            return Ethna::raiseNotice('ログインIDが入力されていません', E_Opensite_AUTH);
        }
        if ($pw == "") {
            return Ethna::raiseNotice('パスワードが入力されていません', E_Opensite_AUTH);
        }
        if ($captcha_code == "") {
        	return Ethna::raiseNotice('文字認証が入力されていません', E_Opensite_AUTH);
        }

        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDB();

        // ログイン認証実施
        $sql  = '
select
    *
from
    mst_user
where
    id = ?
and
    flg_delete = 0
and
    flg_withdraw = 0
';

        $ret  = $db->db->query($sql, array($id));
        $row  = $ret->fetchRow();

        if (md5($pw) != $row{"pw"}){
            return Ethna::raiseNotice('ログインIDまたはパスワードが間違っています', E_Opensite_AUTH);
        }

        //他者使用中チェック
        //$sql = 'SELECT COUNT(*) AS CNT FROM user_session WHERE id = ? AND flg_delete = 0';
        //$ret = $db->getOne($sql, array($id));
        //if($ret > 0){
            //Front_DbLogger::logger("login_error", "", $id, $_SERVER["REMOTE_ADDR"]);
        //    return Ethna::raiseNotice('現在使用中のIDのためログインできません。', E_AUTH);
        //}
		//他者使用中チェック
		$sql = 'SELECT COUNT(*) AS CNT FROM user_session WHERE id = ? AND flg_delete = 0 AND TIME_TO_SEC(timediff(now(),user_session.updated)) < ?';
		$ret  = $db->db->query($sql, array($id, 60 * CONF_SESSION_TIMEOUT));
        $CntRow  = $ret->fetchRow();
        //$ret = $db->getOne($sql, array($id, 60 * CONF_SESSION_TIMEOUT));
        if($CntRow{"CNT"} > 0){
            //Front_DbLogger::logger("login_error", "", $id, $_SERVER["REMOTE_ADDR"]);
            //return Ethna::raiseNotice('他のサービスでログイン中の可能性があります。', E_Subsidy_AUTH);
	    return "overlap";
        }

        $serial         = $row{"serial"};
        $name           = $row{"contractor_lname"}." ".$row{"contractor_fname"};
        $rank           = $row{"rank"};
        $company_name   = $row{"company_name"};
        $authkey   	= $row{"authkey"};
        $flg_admin   	= $row{"flg_admin"};
        //$name_sei       = $row{"contractor_lname"};
        //$name_mei       = $row{"contractor_fname"};

        // セッションスタート
        $back->session->start();
        $back->session->set('serial',         $serial);
        $back->session->set('id',             $id);
        $back->session->set('name',           $name);
        $back->session->set('rank',           $rank);
        $back->session->set('company_name',        $company_name);
        $back->session->set('authkey',        $authkey);
        $back->session->set('flg_admin',        $flg_admin);

        $logger->log(LOG_INFO, "login ->".$id."(".$name.")");

        // セッション情報格納
        $sql  = '
insert into user_session(
    id,
    sessid,
    updated,
    created
) values (
    ?,
    ?,
    now(),
    now()
)';
        $db   = $back->getDB();

        $data = array(
                $id,
                session_id()
        );
        $ret  = $db->query($sql, $data);
        if (!$ret){
            $back->session->destroy();
            return Ethna::raiseNotice('システムエラー:セッション登録に失敗しました', E_Opensite_AUTH);
        }

        return "login";
    }
    /**
     *  authentication user autologin
     *
     *  @access protected
     *  @return array   serial,
     */
    function auth_autologin($serial)
    {
	//$serial値の存在の有無は、関数内で判断しない

        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDB();

        // ログイン認証実施
        $sql  = '
select
    *
from
    mst_user
where
    serial = ?
and
    flg_delete = 0
and
    flg_withdraw = 0
';

        $ret  = $db->db->query($sql, array($serial));
        $row  = $ret->fetchRow();

        $serial         = $row{"serial"};
        $id         	= $row{"id"};
        $name           = $row{"contractor_lname"}." ".$row{"contractor_fname"};
        $rank           = $row{"rank"};
        $company_name   = $row{"company_name"};
        $authkey   	= $row{"authkey"};
        $flg_admin   	= $row{"flg_admin"};

        // セッションスタート
        $back->session->start();
        $back->session->set('serial',         $serial);
        $back->session->set('id',             $id);
        $back->session->set('name',           $name);
        $back->session->set('rank',           $rank);
        $back->session->set('company_name',   $company_name);
        $back->session->set('authkey',        $authkey);
        $back->session->set('flg_admin',      $flg_admin);

        $logger->log(LOG_INFO, "login ->".$id."(".$name.")");

        // セッション情報格納
        $sql  = '
insert into user_session(
    id,
    sessid,
    updated,
    created
) values (
    ?,
    ?,
    now(),
    now()
)';
        $db   = $back->getDB();

        $data = array(
                $id,
                session_id()
        );
        $ret  = $db->query($sql, $data);
        if (!$ret){
            $back->session->destroy();
            return Ethna::raiseNotice('システムエラー:セッション登録に失敗しました', E_Opensite_AUTH);
        }

        return "login";
    }
}
?>
