<?php
/**
 *  Opensite_AdminLogger.php
 *  管理者ログ書き出しクラス
 */

/**
 *  Opensite AdminLogger Class.
 */
class Opensite_AdminLogger
{
    /**
     *  logger
     *  管理者ログ書き出し
     *
     *  @access protected
     *  @param  string  action    アクション名
     *  @param  int     app_id    アプリケーションID（あれば）
     *  @param  string  target_id 対象ID（会員ID or 管理者ID）
     *  @param  string  message   ログメッセージ
     *  @return string  ret       true/false
     */
    function logger($action, $app_id = "", $target_id, $message)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();
        $db     =  $back->getDB();

        /**
         *  想定ログ
         *
         *  作業名              作業者     アクション         アプリ  対象       作業内容
         *  会員退会処理        <admin_id> member_withdraw    -       member_id  -
         *  会員情報更新        <admin_id> member_edit        -       member_id  update内容
         *  管理端末追加        <admin_id> member_admin       -       member_id  insert内容
         *  管理者管理(追加)    <admin_id> config_admin_add   -       admin_id   -
         *  管理者管理(更新)    <admin_id> config_admin_upd   -       admin_id   -
         *  管理者管理(削除)    <admin_id> config_admin_del   -       admin_id   -
         *  監視設定変更        <admin_id> obs_regist         -       obs_id     update内容
         *  問い合わせ対応      <admin_id> inquiry_send       -       member_id  update内容
         *  問い合わせｴｽｶﾚ      <admin_id> inquiry_escalation -       member_id  -
         *  問い合わせ要検討    <admin_id> inquiry_agenda     -       member_id  -
         *  問い合わせ棄却      <admin_id> inquiry_reject     -       member_id  -
         *  アプリデータ補正    <admin_id> mainte_edit        app_id  shop_id    update内容
         *  アプリデータ削除    <admin_id> mainte_delete      app_id  shop_id    -
         */

        // メッセージ欄のハッシュをパースする
        $message_parsed = "";
        if(is_array($message)){
            foreach($message as $k => $v){
                $message_parsed.= $k.":".$v.",";
            }
        }else{
            $message_parsed = $message;
        }

        $sql  = '
insert into LOG_ADMIN(
    admin_id,
    action,
    app_id,
    target_id,
    message,
    created
) values (
    ?,
    ?,
    ?,
    ?,
    ?,
    now()
)';
        $ins_data = array(
            $this->session->get('id'),
            $action,
            $app_id,
            $target_id,
            $message_parsed
        );
        $ret  = $db->query($sql, $ins_data);
        if (!$ret){
            return Ethna::raiseNotice('システムエラー:管理者ログの登録に失敗しました', E_Opensite_AUTH);
        }

        return $ret;
    }
}
?>
