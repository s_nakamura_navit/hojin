<?php
/*
 *
 */
class Opensite_Trialone
{
	//企業検索名準備
	function searchWordCorp($search_corp_name)
	{
		//特定文字列を除外
		$word = array("株式会社");
		$change   = array("");
		$sc_name = str_replace($word, $change, $search_corp_name);
		//半角を全角へ
		$sc_name = mb_convert_kana($sc_name, 'ASKV');
		return $sc_name;
	}
	//都道府県切り出し（簡易的な判定用）
	function cutPref($address)
	{
		if(mb_strpos($address, '府' ) > 0) {
			$sc_pref_num = mb_strpos($address, '府' );
			$sc_pref = mb_substr($address, 0, $sc_pref_num + 1);
		} else if(mb_strpos($address, '都' ) > 0) {
			$sc_pref_num = mb_strpos($address, '都' );
			$sc_pref = mb_substr($address, 0, $sc_pref_num + 1);
		} else if(mb_strpos($address, '道' ) > 0) {
			$sc_pref_num = mb_strpos($address, '道' );
			$sc_pref = mb_substr($address, 0, $sc_pref_num + 1);
		} else if(mb_strpos($address, '県' ) > 0) {
			$sc_pref_num = mb_strpos($address, '県' );
			$sc_pref = mb_substr($address, 0, $sc_pref_num + 1);
		} else {
			$sc_pref = false;
		}
		return $sc_pref;

	}
	//使用テーブルの選定(mst_use_trn_inspect)
	function getTrnInspect()
	{
		$db =& $this->backend->getDB();
		$sql = "SELECT USE_TRN_INSPECT FROM mst_use_trn_inspect ORDER BY created DESC LIMIT 1";
		$result =& $db->query($sql);
		$resul_muti = $result->fetchRow();
		$select_table = $resul_muti["USE_TRN_INSPECT"];
		return $select_table;
	}

	//県判別
	function searchPref($adress)
	{
		$db =& $this->backend->getDB();
		$sql = "SELECT
					code,
					name
				FROM mst_pref_code
				WHERE ? like concat('%', name, '%')";
		$cond = array('%' . $adress . '%');
		$result = $db->query($sql, $cond);
		$pref_sch = $result->fetchRow();
		return $pref_sch;
	}
	//市区町村判別
	function searchCity($adress)
	{
		$db =& $this->backend->getDB();
		$sql = "SELECT
					citycode,
					prefecture_id,
					longname
				FROM citylist
				WHERE ? like concat('%', longname, '%')";
		$cond = array('%' . $adress . '%');
		$result = $db->query($sql, $cond);
		$city_sch = $result->fetchRow();
		return $city_sch;
	}
	//企業検索：ヒット件数
	function searchCorpCnt($table_name,$sc_name,$city_num, $pref_num, $sc_name_half)
	{
		$db =& $this->backend->getDB();
		$cond = array();
		$sql = "SELECT COUNT(UTI.ID) AS sc_cnt FROM " . $table_name . " AS UTI
				INNER JOIN mst_pref_code ON UTI.I_ADDR1=mst_pref_code.code
				WHERE UTI.I_CORP_NUM <> 0 ";
		if ($pref_num){
			$sql .= " and UTI.I_ADDR1 = ? ";
			$cond[] = $pref_num;
		}
		if (preg_match("/^[0-9]+$/", $sc_name_half)) {
			$sql .= " and ( UTI.name like ? or UTI.I_CORP_NUM = ? )";
		}else{
			$sql .= " and UTI.name like ? ";
		}
				
		$cond[] = '%' . $sc_name . '%';
		if (preg_match("/^[0-9]+$/", $sc_name_half)) {
			$cond[] = $sc_name_half;
		}
		if(!empty($city_num)) {
			$sql .= " AND UTI.I_ADDRESS_CD = ? ";
			$cond[] = $city_num;
		}
//echo "===".$sql;
//echo "<br>";print_r($cond);
//echo "<br>sc=" . $sc_name;
		$result = $db->query($sql, $cond);
		$result_corp_cnt   = $result->fetchRow();
//echo "++++"	;print_r($result_corp_cnt);
		return $result_corp_cnt;
	}
	//企業検索
	function searchCorp($table_name,$sc_name,$city_num, $pref_num, $sc_name_half)
	{
		$db =& $this->backend->getDB();
		$cond = array();
		$sql = "SELECT
					UTI.I_CORP_NUM AS corp_num,
					UTI.ID AS corp_id,
					UTI.I_CORP AS corp_name,
					UTI.I_ADDR2 AS corp_city,
					mst_pref_code.name AS pref
				FROM " . $table_name . " AS UTI
				INNER JOIN mst_pref_code ON UTI.I_ADDR1=mst_pref_code.code
				WHERE UTI.I_CORP_NUM <> 0 ";
		if ($pref_num){
			$sql .= " and UTI.I_ADDR1 = ? ";
			$cond[] = $pref_num;
		}
		if (preg_match("/^[0-9]+$/", $sc_name_half)) {
			$sql .= " and ( UTI.name like ? or UTI.I_CORP_NUM = ? )";
		}else{
			$sql .= " and UTI.name like ? ";
		}

		$cond[] = '%' . $sc_name . '%';
		if (preg_match("/^[0-9]+$/", $sc_name_half)) {
			$cond[] = $sc_name_half;
		}

		if(!empty($city_num)) {
			$sql .= " AND UTI.I_ADDRESS_CD = ? ";
			$cond[] = $city_num;
		}
		mysql_set_charset('utf8');
		$result = $db->query($sql, $cond);
		return $result;
	}


	//企業詳細（付与属性項目の有無）
	function corpDetailUmu($table_name, $corp_id, $pref_code)
	{
		$db =& $this->backend->getDB();
		$sql="SELECT
					UTI.ID,
					UTI.I_CORP,
					I_ADDR_ALL,
					mst_pref_code.name,
					(CASE WHEN (UTI.I_BUSINESS_CD_L_1 = '' or UTI.I_BUSINESS_CD_L_1 IS NULL) THEN '-' ELSE '○' END) AS I_BUSINESS_CD_L_1,
					(CASE WHEN (UTI.I_FAX = '' or UTI.I_FAX IS NULL ) THEN '-' ELSE '○' END) AS I_FAX,
					(CASE WHEN (UTI.I_LISTED_TYPE = '00' or UTI.I_LISTED_TYPE = '' or UTI.I_LISTED_TYPE IS NULL ) THEN '-'
						ELSE '○' END) AS I_LISTED_TYPE,
					(CASE WHEN (UTI.I_EMPLOYEE_DIV = '' or UTI.I_EMPLOYEE_DIV IS NULL ) THEN '-' ELSE '○' END) AS I_EMPLOYEE_DIV,
					(CASE WHEN (UTI.I_SALE_DIV = '' or UTI.I_SALE_DIV IS NULL ) THEN '-' ELSE '○' END) AS I_SALE_DIV,
					(CASE WHEN (UTI.I_CAPITAL_DIV = '' or UTI.I_CAPITAL_DIV IS NULL) THEN '-' ELSE '○' END) AS I_CAPITAL_DIV,
					(CASE WHEN (UTI.I_FOUNDING_DT = '' or UTI.I_FOUNDING_DT IS NULL) THEN '-' ELSE '○' END) AS I_FOUNDING_DT,
					(CASE WHEN (UTI.I_DEPT_POSITION = '' or UTI.I_DEPT_POSITION IS NULL) THEN '-' ELSE '○' END) AS I_DEPT_POSITION,
					(CASE WHEN (UTI.I_DEPT_NAME = '' or UTI.I_DEPT_NAME IS NULL) THEN '-' ELSE '○' END) AS I_DEPT_NAME,
					(CASE WHEN (UTI.I_URL = '' or UTI.I_URL IS NULL ) THEN '-' ELSE '○' END) AS I_URL,
					(CASE WHEN (UTI.I_TEL = '' ) THEN '-' ELSE '○' END) AS I_TEL,
					(CASE WHEN UTI.I_CORP_NUM = 0 THEN '-' ELSE '○' END) AS I_CORP_NUM,
					(CASE WHEN (UTI.I_MAILADDR = '' or UTI.I_MAILADDR IS NULL ) THEN '-' ELSE '○' END) AS I_MAILADDR
				FROM " . $table_name . " AS UTI
				INNER JOIN mst_pref_code ON UTI.I_ADDR1=mst_pref_code.code
				WHERE ID= ? ";
		if ($pref_code){
			$sql .= " and I_ADDR1 = ? ";
		}
		//(CASE WHEN UTI.I_LISTED_TYPE = '00' THEN '-' ELSE '○' END) AS I_LISTED_TYPE,
		if ($pref_code){
			$cond = array($corp_id, $pref_code);
		}else{
			$cond = array($corp_id);
		}
		$result = $db->query($sql, $cond);
		$corp_detail = $result->fetchRow();
		return $corp_detail;
	}


	//企業詳細
	function corpDetail($table_name,$corp_id)
	{
		$db =& $this->backend->getDB();
		$sql="SELECT
					UTI.ID,
					UTI.I_CORP,
					UTI.I_BUSINESS_CD_L_1,
					UTI.I_FAX,
					UTI.I_LISTED_TYPE,
					UTI.I_EMPLOYEE_DIV,
					UTI.I_SALE_DIV,
					UTI.I_CAPITAL_DIV,
					UTI.I_FOUNDING_DT,
					UTI.I_DEPT_POSITION,
					UTI.I_DEPT_NAME,
					UTI.I_URL,
					UTI.I_CORP_NUM,
					UTI.I_MAILADDR
				FROM " . $table_name . " AS UTI
				WHERE ID= ? ";
		$cond = array($corp_id);

		$result = $db->query($sql, $cond);
		$corp_detail = $result->fetchRow();
		return $corp_detail;
	}
	//リクエスト履歴への書き込み
	function insertFurufuruRequest($prams)
	{
		$ctl    =& Ethna_Controller::getInstance();
		$logger =  $ctl->getLogger();
//echo "=========" ;print_r( $prams );
//echo "=========" ;print_r( $_POST );
		$db =& $this->backend->getDB();
		$sql = 'INSERT INTO hojin_request
				(
					user_serial,
					user_id,
					list_name,
					list_filepath,
					list_row,
					select_property,
					get_property_row,
					created_estimate_by,
					use_property,
					flg_processed,
					created_estimate,
					created
				)

				values
				(
				?,?,?,?,?,?,?,?,?,?,?,?
				)
				';

		$cond = array(
				$prams['user_serial'],
				$prams['user_id'],
//				$prams['list_name'],
				$prams['sc_name'],
				$prams['sc_address'],
				$prams['list_row'],
				$prams['select_property'],
				$prams['get_property_row'],
				$prams['created_estimate_by'],
				$prams['use_property'],
				$prams['flg_processed'],
				$prams['created_estimate'],
				$prams['created']
		);

		$result = $db->query($sql, $cond);
		if(Ethna::isError($result))
		{
			$db->rollback();
			Ethna::raiseNotice("ユーザID:[".$prams['user_id']."] ",E_FURU_DB_INSERT_REQUEST_FAILD);
			return Ethna::raiseError("ふるふる依頼情報の登録に失敗しました。",E_FURU_DB_INSERT_REQUEST_FAILD);
		}
		$db->commit();
		return;

	}

	//二重ポストの場合の最新見積ID取得
	function getFurufuruRequestEstimateId($user_id,$id)
	{
		$db =& $this->backend->getDB();
		$sql="SELECT
				id
			FROM hojin_request
			WHERE user_serial= ?
			AND user_id= ?
			ORDER BY created DESC
			LIMIT 1";
		$cond = array($user_id,$id);
		$result = $db->query($sql, $cond);
		$req_id = $result->fetchRow();
		return $req_id['id'];
	}


	//企業情報取得（購入/購入済）
	function getCorpDetailAoc($table_name,$corp_id)
	{
		$db =& $this->backend->getDB();
		$sql="SELECT
				UTI.ID,
				UTI.I_CORP,
				UTI.I_ZIP_1,
				UTI.I_ZIP_2,
				UTI.I_ADDR_ALL,
				UTI.I_TEL,
				UTI.I_BUSINESS_CD_L_1,
				MST_BUSINESS_CD_L.BUSINESS_CD_L_NAME,
				MST_BUSINESS_CD_M.BUSINESS_CD_M_NAME,
				g3_1.BUSINESS_CD_S_NAME as BUSINESS_CD_S_NAME_1,
				g3_2.BUSINESS_CD_S_NAME as BUSINESS_CD_S_NAME_2,
				g3_3.BUSINESS_CD_S_NAME as BUSINESS_CD_S_NAME_3,
				g3_4.BUSINESS_CD_S_NAME as BUSINESS_CD_S_NAME_4,
				g3_5.BUSINESS_CD_S_NAME as BUSINESS_CD_S_NAME_5,
				g3_6.BUSINESS_CD_S_NAME as BUSINESS_CD_S_NAME_6,
				g3_7.BUSINESS_CD_S_NAME as BUSINESS_CD_S_NAME_7,
				g3_8.BUSINESS_CD_S_NAME as BUSINESS_CD_S_NAME_8,
				g3_9.BUSINESS_CD_S_NAME as BUSINESS_CD_S_NAME_9,
				g3_10.BUSINESS_CD_S_NAME as BUSINESS_CD_S_NAME_10,
				UTI.I_FAX,
				UTI.I_LISTED_TYPE,
				MST_LISTED_TYPE.LISTED_TYPE_NAME,
				UTI.I_EMPLOYEE_DIV,
				MST_EMPLOYEE_DIV.EMPLOYEE_DIV_NAME,
				UTI.I_SALE_DIV,
				MST_SALE_DIV.SALE_DIV_NAME,
				UTI.I_CAPITAL_DIV,
				MST_CAPITAL_DIV.CAPITAL_DIV_NAME,
				UTI.I_FOUNDING_DT,
				UTI.I_DEPT_POSITION,
				UTI.I_DEPT_NAME,
				UTI.I_URL,
				UTI.I_CORP_NUM,
				UTI.I_MAILADDR
			FROM " . $table_name . " AS UTI
			LEFT JOIN MST_BUSINESS_CD_L ON UTI.I_BUSINESS_CD_L_1=MST_BUSINESS_CD_L.BUSINESS_CD_L
			LEFT JOIN MST_BUSINESS_CD_M ON UTI.I_BUSINESS_CD_M_1=MST_BUSINESS_CD_M.BUSINESS_CD_M
			LEFT JOIN MST_BUSINESS_CD_S as g3_1 ON UTI.I_BUSINESS_CD_S_1=g3_1.BUSINESS_CD_S
			LEFT JOIN MST_BUSINESS_CD_S as g3_2 ON UTI.I_BUSINESS_CD_S_2=g3_2.BUSINESS_CD_S
			LEFT JOIN MST_BUSINESS_CD_S as g3_3 ON UTI.I_BUSINESS_CD_S_3=g3_3.BUSINESS_CD_S
			LEFT JOIN MST_BUSINESS_CD_S as g3_4 ON UTI.I_BUSINESS_CD_S_4=g3_4.BUSINESS_CD_S
			LEFT JOIN MST_BUSINESS_CD_S as g3_5 ON UTI.I_BUSINESS_CD_S_5=g3_5.BUSINESS_CD_S
			LEFT JOIN MST_BUSINESS_CD_S as g3_6 ON UTI.I_BUSINESS_CD_S_6=g3_6.BUSINESS_CD_S
			LEFT JOIN MST_BUSINESS_CD_S as g3_7 ON UTI.I_BUSINESS_CD_S_7=g3_7.BUSINESS_CD_S
			LEFT JOIN MST_BUSINESS_CD_S as g3_8 ON UTI.I_BUSINESS_CD_S_8=g3_8.BUSINESS_CD_S
			LEFT JOIN MST_BUSINESS_CD_S as g3_9 ON UTI.I_BUSINESS_CD_S_9=g3_9.BUSINESS_CD_S
			LEFT JOIN MST_BUSINESS_CD_S as g3_10 ON UTI.I_BUSINESS_CD_S_10=g3_10.BUSINESS_CD_S
			LEFT JOIN MST_LISTED_TYPE ON UTI.I_LISTED_TYPE=MST_LISTED_TYPE.LISTED_TYPE
			LEFT JOIN MST_EMPLOYEE_DIV ON UTI.I_EMPLOYEE_DIV=MST_EMPLOYEE_DIV.EMPLOYEE_DIV_ID
			LEFT JOIN MST_SALE_DIV ON UTI.I_SALE_DIV=MST_SALE_DIV.SALE_DIV_ID
			LEFT JOIN MST_CAPITAL_DIV ON UTI.I_CAPITAL_DIV=MST_CAPITAL_DIV.CAPITAL_DIV_ID
			WHERE ID= ? ";
		$cond = array($corp_id);
		$result = $db->query($sql, $cond);
		$corp_detail = $result->fetchRow();
		return $corp_detail;
	}
	//ポイント減算
	function updateUserPointSubtraction($params)
	{
		$ctl    =& Ethna_Controller::getInstance();
		$logger =  $ctl->getLogger();

		$db =& $this->backend->getDB();
		$sql ='UPDATE mst_user_point SET point = ? WHERE user_serial = ?';
		$cond = array(
				$params['user_point_aft'],
				$params['user_serial']
		);
		$result = $db->query($sql, $cond);
		if(Ethna::isError($result))
		{
			$db->rollback();
			Ethna::raiseNotice("ユーザ番号:[". $params['user_serial'] ."] ",E_FURU_DB_POINT_USE_FAILD);
			return Ethna::raiseError("ポイント処理に失敗しました。",E_FURU_DB_POINT_USE_FAILD);
			$message = "ptfalsed：ur:[". $params['user_serial'] ."] br:" . $params['user_point_br'] . "/ nd:" . $params['total'] . "/ af:" . $params['user_point_aft'];
			$logger->log(LOG_NOTICE, $message);
		}
		$db->commit();
		return;
	}
	//ユーザ側履歴を購入に変更
	function updateFurufuruRequestAoc($req_id)
	{
		$ctl    =& Ethna_Controller::getInstance();
		$logger =  $ctl->getLogger();

		$db =& $this->backend->getDB();
		$sql ='UPDATE hojin_request SET
					client_order = ? ,
					flg_processed = ?
					WHERE id = ?';
		$cond = array(
				date("Y-m-d H:i:s"),
				2,
				$req_id
		);
		$result = $db->query($sql, $cond);
		if(Ethna::isError($result))
		{
			$db->rollback();
			Ethna::raiseNotice("依頼番号:[". $req_id ."] ",E_FURU_DB_UPDATE_REQUEST_FAILD);
			return Ethna::raiseError("依頼更新処理に失敗しました。",E_FURU_DB_UPDATE_REQUEST_FAILD);
		}
		$db->commit();
		return;
	}
	//管理側のポイント利用履歴に書き込み
	function insertMstUsePointLogs($params)
	{
		$ctl    =& Ethna_Controller::getInstance();
		$logger =  $ctl->getLogger();

		$db =& $this->backend->getDB();
		$sql = 'INSERT INTO mst_use_point_logs
				(
					user_serial,
					user_id,
					select_contents,
					process_id,
					use_point,
					status,
					flg_delete,
					created,
					created_by
				)
				values
				(
				?,?,?,?,?,?,?,?,?
				)
				';

		$cond = array(
				$params['user_serial'],
				$params['user_id'],
				'hojin',
				$params['process_id'],
				$params['use_point'],
				0,
				0,
				date("Y-m-d H:i:s"),
				$params['created_by']
		);

		$result = $db->query($sql, $cond);
		if(Ethna::isError($result))
		{
			$db->rollback();
			Ethna::raiseNotice("ユーザ番号:[". $params['user_serial'] ."] ",E_FURU_DB_POINT_LOG_FAILD);
			return Ethna::raiseError("ポイント利用履歴更新処理に失敗しました。",E_FURU_DB_POINT_LOG_FAILD);
		}
		$db->commit();
	}



}
