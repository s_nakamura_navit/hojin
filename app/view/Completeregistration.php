<?php
/**
 *  Completeregistration.php
 */

/**
 *  Completeregistration view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_Completeregistration extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_View_Completeregistration preforward()");
        
        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');
        
        // レイアウトビュー表示設定
        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;
        $this->is_conv_jb = true;
        $this->is_conv_google_real = true;
        $this->is_conv_yahoo_real = true;
        
        //入力値取得
        $data{'auth'}           = $this->af->get('auth');
        
        // アクション名取得
        $action = $this->backend->ctl->getCurrentActionName();
        
        // 認証・確認メール送信処理結果を取得
        $auth_result = $this->af->get('auth_result');//認証結果
        $mail_send_result = $this->af->get('mail_send_result');//本登録メール送信結果
        
        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('auth_result',$auth_result);
        $this->af->setApp('mail_send_result',$mail_send_result);
        $this->af->setApp('name',  $name);
    }
}

?>
