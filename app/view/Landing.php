<?php
/**
 *  Landing.php
 */

/**
 *  Landing view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_Landing extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');
        
        
        // マスタデータ
        $master_pref = Opensite_MasterManager::get_master_condition("mst_government_city_code", "code", "name", "flg_pref='1'", "code");
        $master_field = Opensite_MasterManager::get_master_condition("mst_government_field_code", "code", "name", "", "code");

        $master_field2 = Opensite_MasterManager::get_master_condition("mst_foundation_field_code", "code", "name", "", "code");
        $master_field2_desc = Opensite_MasterManager::get_master_condition("mst_foundation_field_code", "code", "description", "", "code");

        $master_field3 = Opensite_MasterManager::get_master_condition("mst_foundation_businessform_code", "code", "name", "", "code");
        $master_field3_desc = Opensite_MasterManager::get_master_condition("mst_foundation_businessform_code", "code", "description", "", "code");
        
        
        // プルダウン生成
        $pulldown = array();
        $pulldown{"area1"} = Opensite_Formparts::pulldown($master_pref, $data{"area1"}, true, "都道府県を選択");
        
        // ラジオ生成
        $master_kind = array();
        $master_kind["1"]="補助金・助成金";
        $master_kind["2"]="金融・税制";
        
        $radio = array();
        $radio{"kind"} = Opensite_Formparts::radio("in_kind", $master_kind, $data{"kind"},"&nbsp;&nbsp;&nbsp;");
       
        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('master_field',  $master_field);
        $this->af->setApp('master_field2',  $master_field2);
        $this->af->setApp('master_field2_desc',  $master_field2_desc);
        $this->af->setApp('master_field3',  $master_field3);
        $this->af->setApp('master_field3_desc',  $master_field3_desc);

        // エスケープ無
        $this->af->setAppNE('radio',     $radio);
        $this->af->setAppNE('pulldown',  $pulldown);


    }
}

?>
