<?php
/**
 *  registration.php
 */

/**
 *  registration view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_CommissionRegistration extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
	$this->is_himg = false;
    }
}

?>
