<?php
/**
 *  Confirm.php
 */

/**
 *  Confirm view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_CommissionConfirm extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
	$this->is_himg = false;
    }
}

?>
