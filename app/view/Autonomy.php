<?php
/**
 *  autonomy.php
 */

/**
 *  autonomy view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_Autonomy extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        // セッション情報取得
        $id = $this->session->get('id');
        
        // フォーム入力値
        $data{"kind"}       = $this->af->get('in_kind');
        $data{"area1"}      = $this->af->get('in_area1');
        $data{"area2"}      = $this->af->get('in_area2');
        $data{"field_90"}   = $this->af->get('in_field_90');
        $data{"field_91"}   = $this->af->get('in_field_91');
        $data{"field_92"}   = $this->af->get('in_field_92');
        $data{"field_93"}   = $this->af->get('in_field_93');
        $data{"field_94"}   = $this->af->get('in_field_94');
        $data{"field_95"}   = $this->af->get('in_field_95');
        $data{"field_96"}   = $this->af->get('in_field_96');
        $data{"field_97"}   = $this->af->get('in_field_97');
        $data{"field_98"}   = $this->af->get('in_field_98');
        $data{"field_99"}   = $this->af->get('in_field_99');
        $data{"field_100"}  = $this->af->get('in_field_100');
        $data{"field_101"}  = $this->af->get('in_field_101');
        
        // デフォルト値設定
        if($this->af->validate() == 0){
            $data{"sex"}        = $answers{"sex"};
            $data{"age"}        = $answers{"age"};
            $data{"marriage_status"}   = $answers{"marriage_status"};
            $data{"area"}       = $answers{"area"};
            $data{"job_kind"} = $answers{"job_kind"};
            $data{"yearly_income"}     = $answers{"yearly_income"};
        }
        
        // マスタデータ
        $master_pref = Opensite_MasterManager::get_master_condition("mst_government_city_code", "code", "name", "flg_pref='1'", "code");
        $master_field = Opensite_MasterManager::get_master_condition("mst_government_field_code", "code", "name", "", "code");
        
        // プルダウン生成
        $pulldown = array();
        $pulldown{"area1"} = Opensite_Formparts::pulldown($master_pref, $data{"area1"}, true, "都道府県");
        
        // ラジオ生成
        $master_kind = array();
        $master_kind["1"]="補助金・助成金";
        $master_kind["2"]="金融・税制";
        
        $radio = array();
        $radio{"kind"} = Opensite_Formparts::radio("in_kind", $master_kind, $data{"kind"},"&nbsp;&nbsp;&nbsp;");
       
        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('master_field',  $master_field);

        // エスケープ無
        $this->af->setAppNE('radio',     $radio);
        $this->af->setAppNE('pulldown',  $pulldown);


    }
}

?>
