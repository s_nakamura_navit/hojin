<?php
/**
 *  Completewithdrew.php
 */

/**
 *  Completewithdrew view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_Completewithdrew extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {

        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;

        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_View_Completewithdrew preforward()");
        
        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');

        // アクション名取得
        $action = $this->backend->ctl->getCurrentActionName();
        
        // 仮登録・確認メール送信処理結果を取得
        $db_regist_result = $this->af->get('db_regist_result');//仮登録結果
        $mail_send_result = $this->af->get('mail_send_result');//仮登録メール送信結果
        
        // エスケープ有
        $this->af->setApp('db_regist_result',$db_regist_result);
        $this->af->setApp('mail_send_result',$mail_send_result);
        $this->af->setApp('name',  $name);
        

        // エスケープ無
        //$this->af->setAppNE('radio',     $radio);
        //$this->af->setAppNE('pulldown',  $pulldown);


    }
}

?>
