<?php
/**
 *  SecureConfirm.php
 */

/**
 *  SecureConfirm view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_SecureConfirm extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_View_SecureConfirm preforward()");
        
        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');

        // レイアウトビュー表示設定
        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;
        $this->is_conv_jb = true;
        $this->is_conv_google = true;
        $this->is_conv_yahoo = true;
        
        
        //入力値取得
        $data{'company_name'}           = $this->af->get('in_company_name');
        $data{'zip1'}                   = $this->af->get('in_zip1');      
        $data{'zip2'}                   = $this->af->get('in_zip2');
        $data{'address'}                = $this->af->get('in_address');
        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
        $data{'phone_no2'}              = $this->af->get('in_phone_no2');            
        $data{'phone_no3'}              = $this->af->get('in_phone_no3');            
        $data{'department_name'}        = $this->af->get('in_department_name'); 
        $data{'post_name'}              = $this->af->get('in_post_name');            
        $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
        $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
        $data{'email'}                  = $this->af->get('in_email');
        $data{'from'}                   = $this->af->get('in_from');
	$data{'act'}                    = $this->af->get('in_act');
        $data{'privacy'}                = $this->af->get('in_privacy');   
        
        // アクション名取得
        $action = $this->backend->ctl->getCurrentActionName();
        
        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('name',  $name);
        
    }
}

?>
