<?php
/**
 *  SecureRegistration.php
 */

/**
 *  SecureRegistration view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_SecureRegistration extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');
        
        // レイアウトビュー表示設定
        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;
        
        //入力値取得
        $data{'company_name'}           = $this->af->get('in_company_name');
        $data{'zip1'}                   = $this->af->get('in_zip1');      
        $data{'zip2'}                   = $this->af->get('in_zip2');
        $data{'address'}                = $this->af->get('in_address');
        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
        $data{'phone_no2'}              = $this->af->get('in_phone_no2');            
        $data{'phone_no3'}              = $this->af->get('in_phone_no3');            
        $data{'department_name'}        = $this->af->get('in_department_name'); 
        $data{'post_name'}              = $this->af->get('in_post_name');            
        $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
        $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
        $data{'email'}                  = $this->af->get('in_email');
        $data{'from'}                   = $this->af->get('in_from');
        $data{'act'}               	= $this->af->get('in_act');
        $data{'privacy'}                = $this->af->get('in_privacy');
        
        // デフォルト値設定
        if($this->af->validate() == 0){
            $data{"mailmaga"}       = "はい";
        }
 
        // マスタデータ
        $master_from = array();
        $master_from{'Googleより検索'} = "Googleより検索";
        $master_from{'Yahooより検索'} = "Yahooより検索";
        $master_from{'Facebook'} = "Facebook";
        $master_from{'ナビットサイト'} = "ナビットサイト";
        $master_from{'ナビット営業より紹介'} = "ナビット営業より紹介";
        $master_from{'販売代理店より紹介'} = "販売代理店より紹介";
        $master_from{'御社営業より紹介'} = "御社営業より紹介";
        $master_from{'御社の社員より紹介'} = "御社の社員より紹介";
        $master_from{'ナビット価格表を見て'} = "ナビット価格表を見て";
        $master_from{'メルマガ（ナビット社）'} = "メルマガ（ナビット社）";
        $master_from{'セミナーを受講して'} = "セミナーを受講して";
        $master_from{'その他'} = "その他";
        
        // プルダウン生成
        $pulldown = array();
        $pulldown{"from"} = Opensite_Formparts::pulldown($master_from, $data{"from"}, true, "選択してください");
        
        //規約
        //ファイル名
        $kiyaku_file = BASE."/etc/kiyaku.txt";
        //ファイル読み込み
        mb_language('Japanese');
        $kiyaku_txt = file_get_contents($kiyaku_file);
        $kiyaku_txt = mb_convert_encoding($kiyaku_txt, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');          
        
        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('name',  $name);
        
        // エスケープ無
        $this->af->setAppNE('kiyaku_txt',  $kiyaku_txt);
        $this->af->setAppNE('pulldown',  $pulldown);


    }
}

?>
