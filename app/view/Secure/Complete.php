<?php
/**
 *  SecureComplete.php
 */

/**
 *  SecureComplete view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_SecureComplete extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_View_SecureComplete preforward()");
        
        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');
        
        // レイアウトビュー表示設定
        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;
        $this->is_conv_jb = true;
        $this->is_conv_google_prov = true;
        $this->is_conv_yahoo_prov = true;
                
        //入力値取得
        $data{'company_name'}           = $this->af->get('in_company_name');
        $data{'zip1'}                   = $this->af->get('in_zip1');      
        $data{'zip2'}                   = $this->af->get('in_zip2');
        $data{'address'}                = $this->af->get('in_address');
        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
        $data{'phone_no2'}              = $this->af->get('in_phone_no2');            
        $data{'phone_no3'}              = $this->af->get('in_phone_no3');            
        $data{'department_name'}        = $this->af->get('in_department_name'); 
        $data{'post_name'}              = $this->af->get('in_post_name');            
        $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
        $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
        $data{'email'}                  = $this->af->get('in_email');
        $data{'from'}                   = $this->af->get('in_from');
	$data{'act'}                    = $this->af->get('in_act');
        $data{'privacy'}                = $this->af->get('in_privacy');   
        
        // アクション名取得
        $action = $this->backend->ctl->getCurrentActionName();
        
        // 登録・確認メール送信処理結果を取得
        $db_regist_result = $this->af->get('db_regist_result');//登録結果
        $mail_send_result = $this->af->get('mail_send_result');//登録メール送信結果
        $auto_login_result = $this->af->get('auto_login_result');//登録メール送信結果
        
        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('db_regist_result',$db_regist_result);
        $this->af->setApp('mail_send_result',$mail_send_result);
        $this->af->setApp('auto_login_result',$auto_login_result);
        $this->af->setApp('name',  $name);
        

    }
}

?>
