<?php
/** view
 *  Trialone.php
*/

/**
 *  Trialone view implementation.
*
*  @access     public
*  @package    Opensite
*/
class Opensite_View_Trialone extends Opensite_ViewClass
{
	/**
	 *  preprocess before forwarding.
	 *
	 *  @access public
	 */
	function preforward()
	{
		$this->is_himg = false;

		// セッション情報取得
		$id = $this->session->get('id');
		$name = $this->session->get('name');

		// エスケープ有
		$this->af->setApp('data',  $data);
		$this->af->setApp('name',  $name);

		// エスケープ無
		//$this->af->setAppNE('radio',     $radio);
		//$this->af->setAppNE('pulldown',  $pulldown);
	}
}

?>
