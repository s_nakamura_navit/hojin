<?php
/**
 *  RequestComplete.php
 */

/**
 *  RequestComplete view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_RequestComplete extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_View_RequestComplete preforward()");
        
        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;
        
        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');

        // アクション名取得
        $action = $this->backend->ctl->getCurrentActionName();
        
    }
}

?>
