<?php
/**
 *  RequestRegistration.php
 */

/**
 *  RequestRegistration view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_RequestRegistration extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {   
        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;
        
        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');

	$user_data = Opensite_Dao_Search::mst_user_data($id);
	//$query_str = "action_request_registration=true";
        
	//if($_SERVER['QUERY_STRING'] == $query_str && $id != ""){
	if($id != "" && $this->af->get('back') != 1){
		$data{'company_name'}           = $user_data{'company_name'}; 
	        $data{'zip1'}                   = $user_data{'zip1'};
	        $data{'zip2'}                   = $user_data{'zip2'};
	        $data{'address'}                = $user_data{'address'};
	        $data{'phone_no1'}              = $user_data{'phone_no1'};
	        $data{'phone_no2'}              = $user_data{'phone_no2'};
	        $data{'phone_no3'}              = $user_data{'phone_no3'};
	        $data{'department_name'}        = $user_data{'department_name'};
	        $data{'post_name'}              = $user_data{'post_name'};
	        $data{'contractor_lname'}       = $user_data{'contractor_lname'};
	        $data{'contractor_fname'}       = $user_data{'contractor_fname'};
	        $data{'contractor_lkana'}       = $user_data{'contractor_lkana'};
	        $data{'contractor_fkana'}       = $user_data{'contractor_fkana'};
	        $data{'email'}                  = $user_data{'email'};
	        $data{'point'}                  = $user_data{'point'};
	        $data{'privacy'}                = $user_data{'privacy'};
	}else{
	        //入力値取得
	        $data{'company_name'}           = $this->af->get('in_company_name');
	        $data{'zip1'}                   = $this->af->get('in_zip1');      
	        $data{'zip2'}                   = $this->af->get('in_zip2');
	        $data{'address'}                = $this->af->get('in_address');
	        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
	        $data{'phone_no2'}              = $this->af->get('in_phone_no2');            
	        $data{'phone_no3'}              = $this->af->get('in_phone_no3');            
	        $data{'department_name'}        = $this->af->get('in_department_name'); 
	        $data{'post_name'}              = $this->af->get('in_post_name');            
	        $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
	        $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
	        $data{'contractor_lkana'}       = $this->af->get('in_contractor_lkana');  
	        $data{'contractor_fkana' }      = $this->af->get('in_contractor_fkana');  
	        $data{'email'}                  = $this->af->get('in_email');
	        $data{'point'}                  = $this->af->get('in_point');
	        $data{'payment'}                = $this->af->get('in_payment');
	        $data{'privacy'}                = $this->af->get('in_privacy');
        }

        //規約
        //ファイル名
        $kiyaku_file = BASE."/etc/kiyaku.txt";
        //ファイル読み込み
        mb_language('Japanese');
        $kiyaku_txt = file_get_contents($kiyaku_file);
        $kiyaku_txt = mb_convert_encoding($kiyaku_txt, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');          
        
        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('name',  $name);
        
        // エスケープ無
        $this->af->setAppNE('kiyaku_txt',  $kiyaku_txt);


    }
}

?>
