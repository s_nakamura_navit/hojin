<?php
/**
 *  RequestConfirm.php
 */

/**
 *  RequestConfirm view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_RequestConfirm extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_View_RequestConfirm preforward()");
        
        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;
        
    }
}

?>
