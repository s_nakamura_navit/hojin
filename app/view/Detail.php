<?php
/**
 *  Detail.php
 */

/**
 *  Detail view implementation.
 */
class Opensite_View_Detail extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');

        //フォーム入力値
        $data{"kind"}    = $this->af->get("in_kind");
        $data{"area1"}     = $this->af->get("in_area1");
        $data{"area2"}     = $this->af->get("in_area2");
        $data{"field_90"}  = $this->af->get("in_field_90");
        $data{"field_91"}  = $this->af->get("in_field_91");
        $data{"field_92"}  = $this->af->get("in_field_92");
        $data{"field_93"}  = $this->af->get("in_field_93");
        $data{"field_94"}  = $this->af->get("in_field_94");
        $data{"field_95"}  = $this->af->get("in_field_95");
        $data{"field_96"}  = $this->af->get("in_field_96");
        $data{"field_97"}  = $this->af->get("in_field_97");
        $data{"field_98"}  = $this->af->get("in_field_98");
        $data{"field_99"}  = $this->af->get("in_field_99");
        $data{"field_100"} = $this->af->get("in_field_100");
        $data{"field_101"} = $this->af->get("in_field_101");
        $data{"field2_1"}  = $this->af->get("in_field2_1");
        $data{"field2_2"}  = $this->af->get("in_field2_2");
        $data{"field2_3"}  = $this->af->get("in_field2_3");
        $data{"field2_4"}  = $this->af->get("in_field2_4");
        $data{"field2_5"}  = $this->af->get("in_field2_5");
        $data{"field2_6"}  = $this->af->get("in_field2_6");
        $data{"field2_7"}  = $this->af->get("in_field2_7");
        $data{"field2_8"}  = $this->af->get("in_field2_8");
        $data{"field2_9"}  = $this->af->get("in_field2_9");
        $data{"field2_10"} = $this->af->get("in_field2_10");
        $data{"field2_11"} = $this->af->get("in_field2_11");
        $data{"field3_1"}  = $this->af->get("in_field3_1");
        $data{"field3_2"}  = $this->af->get("in_field3_2");
        $data{"field3_3"}  = $this->af->get("in_field3_3");
        $data{"field3_4"}  = $this->af->get("in_field3_4");
        $data{"field3_5"}  = $this->af->get("in_field3_5");
        $data{"field3_6"}  = $this->af->get("in_field3_6");
        $data{"field3_7"}  = $this->af->get("in_field3_7");
        $data{"field3_8"}  = $this->af->get("in_field3_8");
        $data{"field3_9"}  = $this->af->get("in_field3_9");
        $data{"field3_10"} = $this->af->get("in_field3_10");
        $data{"field3_11"} = $this->af->get("in_field3_11");
        $data{"field3_12"} = $this->af->get("in_field3_12");
        $data{"field3_13"} = $this->af->get("in_field3_13");
        $data{"field3_14"} = $this->af->get("in_field3_14");
        $data{"field3_15"} = $this->af->get("in_field3_15");
        $data{"keyword"}   = $this->af->get("in_keyword");
        $data{"callkind"}  = $this->af->get("in_callkind");
        $data{"contents_id"}  = $this->af->get("in_contents_id");
     
        // 前後スペース除去
        $data{"keyword"} = trim($data{"keyword"});
        // 禁則文字変換
        $data{"keyword"} = $this->replace_prohibition($data{"keyword"});
        // 禁則文字除去
        $data{"keyword"} = $this->delete_prohibition($data{"keyword"});

        //検索
        $result_data = null;
        $result = null;
        $contents_html = "";
        $raw_contents = "";
        $cnv_contents = "";
        $contents_mod_html = "";
      
        if($data{"callkind"}=="autonomy"){
            //自治体案件検索
            //コンテンツファイル名
            $contents_file = BASE."/etc/contents/autonomy/"."Opensite_".$data{"contents_id"}.".html";
            //ファイル読み込み
            mb_language('Japanese');
            $raw_contents = file_get_contents($contents_file);
            //$cnv_contents = mb_convert_encoding($raw_contents, 'UTF-8', 'ASCII,JIS,SJIS,SJIS-win,UTF-8,eucJP-win,EUC-JP');
            $cnv_contents = mb_convert_encoding($raw_contents, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');
            //$contents_html = str_get_html($cnv_contents);
            $contents_html = str_get_html($cnv_contents, true, true, DEFAULT_TARGET_CHARSET, false);

            //ミラサポ関連のキーワードを除去
            //$pattern = '(https?://www.mirasapo.jp[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)';
            //if(preg_match_all($pattern, $contents_html, $res) !== false){
            //    foreach ($res[0] as $value){
            //        $contents_html = str_replace($value, "", $contents_html);
            //    }
            //}

            //タイトル上部に付く見出し1
            foreach($contents_html->find('div.type2') as $element){
              $element->class = 'ditail_title_head_text';
            }
            $midashi1 = $contents_html->find( 'div[class="type2"]' );

            //メインtitle
            foreach($contents_html->find('h1.hojoblogh1') as $element){
              //classを設定
              $element->class = 'ditail_title_text';
            }
            
            //サブtitle
            foreach($contents_html->find('p.hojoblogp') as $element){
              //classを設定
              $element->class = 'ditail_title_subtext';
            }
            
            //小見出しにclassを設定する
            foreach($contents_html->find('h2') as $element){
                //classを設定
                $element->class = 'ditail_midashitext';
                //<div>で囲む
                $element->outertext = '<div class="ditail_midashiback">'.$element->outertext.'</div>';
            }
            
            //<p>タグを<div>で囲む
            foreach($contents_html->find('p') as $element){
                //classを設定
                //$element->class = 'ditail_midashitext';
                //<div>で囲む
                $element->outertext = '<div style="margin-left:42px;margin-right:78px;">'.$element->outertext.'</div>';
            }
            
            //$contents_html->find( 'ul li',2 ) = null;
            //$contents_html->find( 'ul li',3 ) = null;
            
            $i = 0;
            $a_find = 0;
            $link_outertext1 = "";
            $link_outertext2 = "";
            foreach($contents_html->find('ul li') as $element){
              if($i == 0 or $i == 1){
                foreach($element->find('a') as $a_elem){
                  $a_find = 1;
                  if(!isset( $a_elem->href ) ){
                    //hrefが無ければ表示しない
                    $element->outertext = '';
                  } else {
                    if($a_elem->href != ""){
                      if($i==0){
                        //見出し文言を修正
                        $a_elem->find('span',0)->class = 'link_text';
                        $a_elem->find('span',0)->innertext = 'チラシ/パンフレット';
                        $link_outertext1 = $a_elem->outertext;
                        
                      }else if($i==1){
                        //見出し文言を修正
                        $a_elem->find('span',0)->class = 'link_text';
                        $a_elem->find('span',0)->innertext = 'WEBサイト';
                        $link_outertext2 = $a_elem->outertext;
                        
                      }
                    } else {
                      //hrefが無ければ表示しない
                      $element->outertext = '';
                    }
                  }
                  break;
                }
                if($a_find == 0){
                  //<a>タグ自体無し
                  $element->outertext = '';
                }
              }

              if($i >= 2){
                $element->outertext = '';
              }
              $i++;
            }
            //リンクボタンを改めて作成
            $contents_html->find('ul',0)-> outertext = '<div class="link_text_box">'.$link_outertext1.$link_outertext2.'</div>';
            //TOPへボタンは削除
            $contents_html->find('#goTop',0)-> outertext = '';
            
            $contents_mod_html = $contents_html->outertext;
            
            $contents_html->clear();
            unset($contents_html);
            
        }else{
            //財団案件検索
            //コンテンツファイル名
            //$contents_file = BASE."/etc/contents/foundation/".$data{"contents_id"}.'.html';
            $contents_file = BASE."/etc/contents/foundation/".$data{"contents_id"};
            //ファイル読み込み
            mb_language('Japanese');
            $raw_contents = file_get_contents($contents_file);
            $cnv_contents = mb_convert_encoding($raw_contents, 'UTF-8', 'auto');
            $contents_html = str_get_html($cnv_contents);
          
            //財団センター関連のキーワードを除去
            //$pattern = '(https?://www.jfc.or.jp[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)';
            //if(preg_match_all($pattern, $contents_html, $res) !== false){
            //    foreach ($res[0] as $value){
            //        $contents_html = str_replace($value, "", $contents_html);
            //    }
            //}


            //小見出しにclassを設定する
            foreach($contents_html->find('dt') as $element){
                //加工
                $sub_title = $element->plaintext;
                $sub_title = str_replace("：", "", $sub_title);//余計な記号を削除
                $sub_title = str_replace(":", "", $sub_title);//余計な記号を削除
                //<div>で囲む,classを設定
                $element->outertext = '<div class="ditail_midashiback"><h2 class="ditail_midashitext">'.$sub_title.'</h2></div>';
            }
            foreach($contents_html->find('dd') as $element){
                //内容を保存
                $dd_contents = $element->innertext;
                //<div><p>で囲む,classを設定
                $element->outertext = '<div style="margin-left:42px;margin-right:78px;"><p>'.$dd_contents.'</p></div>';
            }
            

            //余計な表題
            $contents_html->find('h2',0)->outertext = '';
            $contents_html->find('h3',0)->outertext = '';
            $contents_html->find('p',1)->outertext = '';
            //助成団体名・住所など(取得して保存→originalタグは削除)
            $foundation_name = $contents_html->find('h4',0)->plaintext;
            $foundation_info = $contents_html->find('p',0)->outertext;
            $contents_html->find('h4',0)->outertext = '';//originalタグを削除
            $contents_html->find('p',0)->outertext = '';//originalタグを削除
            //メインtitle(取得して保存→originalタグは削除)
            $main_title_plain = $contents_html->find('h5.dataDetail',0)->plaintext;
            $contents_html->find('h5.dataDetail',0)->outertext = '';//originalタグを削除
            
            //メインtitle
            $main_title = str_replace("■", "", $main_title_plain);//余計な記号を削除
            $main_title_tag ='<h1 class="ditail_title_text">'.$main_title.'</h1>';//タグを付与して保存
            $contents_html->outertext = $main_title_tag.$contents_html->outertext;//加工済みタグを付与
            //助成団体名・住所など
            $foundation_midashi = '<div class="ditail_midashiback"><h2 class="ditail_midashitext">助成団体</h2></div>';//タグを付与して保存
            $foundation_contents = '<div style="margin-left:42px;margin-right:78px;">'.'<p>'.$foundation_name.'</p>'.$foundation_info.'</div>';//タグを付与して保存
            $contents_html->outertext = $contents_html->outertext.$foundation_midashi.$foundation_contents;//加工済みタグを付与
            
            
            
            
            $contents_mod_html = $contents_html->outertext;
            
            $contents_html->clear();
            unset($contents_html);
          
        }
        /*
        
        //チェックボックス用のデータに加工
        if($data{"field_90"} == "9000") {$data{"field_90"} = "checked ";}
        if($data{"field_91"} == "9100") {$data{"field_91"} = "checked ";}
        if($data{"field_92"} == "9200") {$data{"field_92"} = "checked ";}
        if($data{"field_93"} == "9300") {$data{"field_93"} = "checked ";}
        if($data{"field_94"} == "9400") {$data{"field_94"} = "checked ";}
        if($data{"field_95"} == "9500") {$data{"field_95"} = "checked ";}
        if($data{"field_96"} == "9600") {$data{"field_96"} = "checked ";}
        if($data{"field_97"} == "9700") {$data{"field_97"} = "checked ";}
        if($data{"field_98"} == "9800") {$data{"field_98"} = "checked ";}
        if($data{"field_99"} == "9900") {$data{"field_99"} = "checked ";}
        if($data{"field_100"} == "10000") {$data{"field_100"} = "checked ";}
        if($data{"field_101"} == "10100") {$data{"field_101"} = "checked ";}
        
        if($data{"field2_1"} == "1000") {$data{"field2_1"} = "checked ";}
        if($data{"field2_2"} == "2000") {$data{"field2_2"} = "checked ";}
        if($data{"field2_3"} == "3000") {$data{"field2_3"} = "checked ";}
        if($data{"field2_4"} == "4000") {$data{"field2_4"} = "checked ";}
        if($data{"field2_5"} == "5000") {$data{"field2_5"} = "checked ";}
        if($data{"field2_6"} == "6000") {$data{"field2_6"} = "checked ";}
        if($data{"field2_7"} == "7000") {$data{"field2_7"} = "checked ";}
        if($data{"field2_8"} == "8000") {$data{"field2_8"} = "checked ";}
        if($data{"field2_9"} == "9000") {$data{"field2_9"} = "checked ";}
        if($data{"field2_10"} == "10000") {$data{"field2_10"} = "checked ";}
        if($data{"field2_11"} == "11000") {$data{"field2_11"} = "checked ";}
        
        if($data{"field3_1"} == "1000") {$data{"field3_1"} = "checked ";}
        if($data{"field3_2"} == "2000") {$data{"field3_2"} = "checked ";}
        if($data{"field3_3"} == "3000") {$data{"field3_3"} = "checked ";}
        if($data{"field3_4"} == "4000") {$data{"field3_4"} = "checked ";}
        if($data{"field3_5"} == "5000") {$data{"field3_5"} = "checked ";}
        if($data{"field3_6"} == "6000") {$data{"field3_6"} = "checked ";}
        if($data{"field3_7"} == "7000") {$data{"field3_7"} = "checked ";}
        if($data{"field3_8"} == "8000") {$data{"field3_8"} = "checked ";}
        if($data{"field3_9"} == "9000") {$data{"field3_9"} = "checked ";}
        if($data{"field3_10"} == "10000") {$data{"field3_10"} = "checked ";}
        if($data{"field3_11"} == "11000") {$data{"field3_11"} = "checked ";}
        if($data{"field3_12"} == "12000") {$data{"field3_12"} = "checked ";}
        if($data{"field3_13"} == "13000") {$data{"field3_13"} = "checked ";}
        if($data{"field3_14"} == "14000") {$data{"field3_14"} = "checked ";}
        if($data{"field3_15"} == "15000") {$data{"field3_15"} = "checked ";}
        
         * 
         * 
         * 
         */
        // デフォルト値設定
        // 初期表示時、回答データ取得
        if($this->af->validate() == 0){
            
        }

        
        
        //print_r($result);
                //$result_data = $result->fetchRow();
        //var_dump($result_data[0]);
        //echo $result_data['kind'].'<br />';
        //echo $result_data[1]['field'].'<br />';
        //echo $result_data[2]['contents_org_id'].'<br />';
        /*
        while ($result_data[$i] = $result->fetchRow()) {
            $i++;
        }
        */
        /*
        foreach ($result_data as $row) {
            echo $row['main_title'] . ",";
            echo $row['field'] . ",";
            echo $row['contents_org_id'] . "<br />";
        }
*/
        
        
        
        //結果を配列へ
        //$result_data[$i] =$result->fetch();
//        while ($result_data[$i] = $result->fetchRow()) {
//            $i++;
//        }
                
                
        /*
        print "finish<br /><table border=1>";
        while ($total_data[$i] = $result->fetchRow()) {
            print "<tr><td>";
            print $total_data[$i]{"hiduke"};
            print "</td><td>";
            print $total_data[$i]{"CNT"};
            print "</td></tr>";
            $i++;
        }
        print "</table>";
        */
        
        
        // TSVマスタデータ
        
        // ラジオ生成
        //$radio = array();
        // プルダウン生成
        //$pulldown = array();
        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('result_data',  $result_data);
        $this->af->setApp('result_count',  $result_count);
        $this->af->setApp('name',  $name);
        $this->af->setApp('contents_mod_html_escape',  $contents_mod_html);

        // エスケープ無
        $this->af->setAppNE('contents_mod_html',     $contents_mod_html);
        //$this->af->setAppNE('pulldown',  $pulldown);

    }
    
    function replace_prohibition($str)
    {
        $str = Opensite_Sanitization::replace_prohibition($str, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1);
        return $str;
    }

    function delete_prohibition($str)
    {
        $str = Opensite_Sanitization::replace_prohibition($str, "", "", "", "", "", 0, "", "", "", "");
        return $str;
    }

}

?>
