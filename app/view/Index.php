<?php
/**
 *  index.php
 */

/**
 *  index view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_Index extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
	$this->is_loading_eff = true;
    }
}

?>
