<?php
/**
 *  mypage.php
 */

/**
 *  mypage view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_Mypage extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
	$this->is_himg = false;
    }
}

?>
