<?php
/**
 *  Login.php
 *  ログイン後トップ
 *
 *  @copyright  (C)2010 Navit co.,ltd.
 *  @author     Tetsuo Nakanishi
 *  @package    Opensite
 *  @version    $Id$
 */

/**
 *  login view implementation.
 *
 *  @author     Tetsuo Nakanishi
 *  @access     public
 *  @package    Opensite
 */
class Opensite_View_Login extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        // 表示設定
        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;

        // セッション値取得
        //$serial = $this->session->get("serial");
        //$id     = $this->session->get("id");
        //$name   = $this->session->get("name");
        //$roll   = $this->session->get("roll");

        // エスケープ有
        //$this->af->setApp('serial', $serial);
        //$this->af->setApp('id',     $id);
        //$this->af->setApp('name',   $name);
        //$this->af->setApp('roll',   $roll);

        // 共通
        //$this->af->setAppNE('menu', Opensite_Menu::menu('login'));
        //$this->af->setAppNE('header', TEXT_HEADER);
        //$this->af->setAppNE('footer', TEXT_FOOTER);
    }
}

?>
