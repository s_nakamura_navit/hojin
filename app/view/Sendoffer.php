<?php
/**
 *  SecureComplete.php
 */

/**
 *  Sendoffer view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_Sendoffer extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_View_Sendoffer preforward()");
        
        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');
        
        // メール送信処理結果を取得
        $mail_send_result = $this->af->get('mail_send_result');//メール送信結果
        
        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('mail_send_result',$mail_send_result);
        $this->af->setApp('name',  $name);
        

        // エスケープ無
        //$this->af->setAppNE('radio',     $radio);
        //$this->af->setAppNE('pulldown',  $pulldown);


    }
}

?>
