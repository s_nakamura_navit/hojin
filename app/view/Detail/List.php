<?php
/**
 *  detail_list.php
 */

/**
 *  registration view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_DetailList extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
	$this->is_himg = false;
    }
}

?>
