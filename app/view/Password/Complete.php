<?php
/**
 *  PasswordComplete.php
 */

/**
 *  PasswordComplete view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_PasswordComplete extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {

        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;

        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_View_PasswordComplete preforward()");

        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');

        //入力値取得
        $data{'company_name'}           = $this->af->get('in_company_name');
        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
        $data{'phone_no2'}              = $this->af->get('in_phone_no2');
        $data{'phone_no3'}              = $this->af->get('in_phone_no3');
//      $data{'k_phone_no1'}            = $this->af->get('in_k_phone_no1');
//      $data{'k_phone_no2'}            = $this->af->get('in_k_phone_no2');
//      $data{'k_phone_no3'}            = $this->af->get('in_k_phone_no3');
        $data{'email'}                  = $this->af->get('in_email');

        // アクション名取得
        $action = $this->backend->ctl->getCurrentActionName();

        // 確認メール送信処理結果を取得
        $db_regist_result = $this->af->get('db_regist_result');//仮登録結果
        $mail_send_result = $this->af->get('mail_send_result');//仮登録メール送信結果

        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('db_regist_result',$db_regist_result);
        $this->af->setApp('mail_send_result',$mail_send_result);
        $this->af->setApp('name',  $name);
    }
}

?>
