<?php
/**
 *  PasswordRegistration.php
 */

/**
 *  PasswordRegistration view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_PasswordRegistration extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;

        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');
        $serial = $this->session->get('serial');

        //入力値取得
        $data{'company_name'}           = $this->af->get('in_company_name');
        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
        $data{'phone_no2'}              = $this->af->get('in_phone_no2');
        $data{'phone_no3'}              = $this->af->get('in_phone_no3');
//      $data{'k_phone_no1'}            = $this->af->get('in_k_phone_no1');
//      $data{'k_phone_no2'}            = $this->af->get('in_k_phone_no2');
//      $data{'k_phone_no3'}            = $this->af->get('in_k_phone_no3');
        $data{'email'}                  = $this->af->get('in_email');

        // アクション名取得
        $action = $this->backend->ctl->getCurrentActionName();

        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('name',  $name);

    }
}

?>
