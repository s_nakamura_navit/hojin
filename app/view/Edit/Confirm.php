<?php
/**
 *  EditConfirm.php
 */

/**
 *  EditConfirm view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_EditConfirm extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {

        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;

        $this->logger->log(LOG_INFO, "[".$this->session->get("id")."] Opensite_View_EditConfirm preforward()");
        
        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');
        
        //入力値取得
        $data{'user_id'}           	= $this->af->get('in_user_id');
        $data{'password'}           	= $this->af->get('in_password');
        $data{'company_name'}           = $this->af->get('in_company_name');
        $data{'zip1'}                   = $this->af->get('in_zip1');      
        $data{'zip2'}                   = $this->af->get('in_zip2');
        $data{'address'}                = $this->af->get('in_address');
        $data{'phone_no1'}              = $this->af->get('in_phone_no1');
        $data{'phone_no2'}              = $this->af->get('in_phone_no2');            
        $data{'phone_no3'}              = $this->af->get('in_phone_no3');            
        $data{'department_name'}        = $this->af->get('in_department_name'); 
        $data{'post_name'}              = $this->af->get('in_post_name');            
        $data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
        $data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
        $data{'email'}                  = $this->af->get('in_email');
        
        // アクション名取得
        $action = $this->backend->ctl->getCurrentActionName();
        
        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('name',  $name);
        
    }
}

?>
