<?php
/**
 *  Error.php
 */

/**
 *  thanks view implementation.
 *
 *  @access     public
 *  @package    Opensite
 */
class Opensite_View_SystemError extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {

	$this->is_himg = false;

    }
}

?>
