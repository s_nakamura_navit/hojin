<?php
/**
 *  InquiryRegistration.php
 */

/**
 *  InquiryRegistration view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_InquiryRegistration extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        $this->is_gnavi = false;
        $this->is_himg = false;
        $this->is_topbtn = false;

        // セッション情報取得
        $id = $this->session->get('id');
        $name = $this->session->get('name');
        $serial = $this->session->get('serial');

	$user_data = Opensite_Dao_Mypage::get_user_info_by_id($serial);

        if($id != "" && $this->af->get('back') != 1 && $this->af->validate() == 0){
                $data{'company_name'}           = $user_data{'company_name'};
                $data{'zip1'}                   = $user_data{'zip1'};
                $data{'zip2'}                   = $user_data{'zip2'};
                $data{'address'}                = $user_data{'address'};
                $data{'phone_no1'}              = $user_data{'phone_no1'};
                $data{'phone_no2'}              = $user_data{'phone_no2'};
                $data{'phone_no3'}              = $user_data{'phone_no3'};
                $data{'department_name'}        = $user_data{'department_name'};
                $data{'post_name'}              = $user_data{'post_name'};
                $data{'contractor_lname'}       = $user_data{'contractor_lname'};
                $data{'contractor_fname'}       = $user_data{'contractor_fname'};
                $data{'email'}                  = $user_data{'email'};
                $data{'point'}                  = $user_data{'point'};
                $data{'mailmaga'}               = $user_data{'mailmaga'};
        }else{
        
        //入力値取得
        	$data{'company_name'}           = $this->af->get('in_company_name');
        	$data{'zip1'}                   = $this->af->get('in_zip1');      
        	$data{'zip2'}                   = $this->af->get('in_zip2');
        	$data{'address'}                = $this->af->get('in_address');
        	$data{'phone_no1'}              = $this->af->get('in_phone_no1');
        	$data{'phone_no2'}              = $this->af->get('in_phone_no2');            
        	$data{'phone_no3'}              = $this->af->get('in_phone_no3');            
        	$data{'department_name'}        = $this->af->get('in_department_name'); 
        	$data{'post_name'}              = $this->af->get('in_post_name');            
        	$data{'contractor_lname'}       = $this->af->get('in_contractor_lname');  
        	$data{'contractor_fname' }      = $this->af->get('in_contractor_fname');  
        	$data{'email'}                  = $this->af->get('in_email');
        	$data{'inquiry'}                = $this->af->get('in_inquiry');
	        $data{'agree_privacy'}          = $this->af->get('in_agree_privacy');
	}
        
        //個人情報同意書
        //ファイル名
        $agree_file = BASE."/etc/agree.txt";
        //ファイル読み込み
        mb_language('Japanese');
        $agree_txt = file_get_contents($agree_file);
        $agree_txt = mb_convert_encoding($agree_txt, 'UTF-8', 'SJIS-win,eucJP-win,SJIS,EUC-JP,UTF-8,ASCII,JIS');          

        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('name',  $name);
        
        // エスケープ無
        $this->af->setAppNE('agree_txt',  $agree_txt);
        
    }
}

?>
