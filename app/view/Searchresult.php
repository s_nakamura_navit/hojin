<?php
/**
 *  Searchresult.php
 */

/**
 *  Searchresult view implementation.
 */
class Opensite_View_Searchresult extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        // セッション情報取得
        $id = $this->session->get('id');
        $rank = $this->session->get('rank');
        $name = $this->session->get('name');

        //フォーム入力値
        //$data{"kind"}    = $this->af->get("in_kind");
        $data{"kind_1000"}  = $this->af->get("in_kind_1000");
        $data{"kind_2000"}  = $this->af->get("in_kind_2000");
        $data{"kind_3000"}  = $this->af->get("in_kind_3000");
        
        $data{"area1"}     = $this->af->get("in_area1");
        $data{"area2"}     = $this->af->get("in_area2");
        $data{"field_90"}  = $this->af->get("in_field_90");
        $data{"field_91"}  = $this->af->get("in_field_91");
        $data{"field_92"}  = $this->af->get("in_field_92");
        $data{"field_93"}  = $this->af->get("in_field_93");
        $data{"field_94"}  = $this->af->get("in_field_94");
        $data{"field_95"}  = $this->af->get("in_field_95");
        $data{"field_96"}  = $this->af->get("in_field_96");
        $data{"field_97"}  = $this->af->get("in_field_97");
        $data{"field_98"}  = $this->af->get("in_field_98");
        $data{"field_99"}  = $this->af->get("in_field_99");
        $data{"field_100"} = $this->af->get("in_field_100");
        $data{"field_101"} = $this->af->get("in_field_101");
        $data{"field2_1"}  = $this->af->get("in_field2_1");
        $data{"field2_2"}  = $this->af->get("in_field2_2");
        $data{"field2_3"}  = $this->af->get("in_field2_3");
        $data{"field2_4"}  = $this->af->get("in_field2_4");
        $data{"field2_5"}  = $this->af->get("in_field2_5");
        $data{"field2_6"}  = $this->af->get("in_field2_6");
        $data{"field2_7"}  = $this->af->get("in_field2_7");
        $data{"field2_8"}  = $this->af->get("in_field2_8");
        $data{"field2_9"}  = $this->af->get("in_field2_9");
        $data{"field2_10"} = $this->af->get("in_field2_10");
        $data{"field2_11"} = $this->af->get("in_field2_11");
        $data{"field3_1"}  = $this->af->get("in_field3_1");
        $data{"field3_2"}  = $this->af->get("in_field3_2");
        $data{"field3_3"}  = $this->af->get("in_field3_3");
        $data{"field3_4"}  = $this->af->get("in_field3_4");
        $data{"field3_5"}  = $this->af->get("in_field3_5");
        $data{"field3_6"}  = $this->af->get("in_field3_6");
        $data{"field3_7"}  = $this->af->get("in_field3_7");
        $data{"field3_8"}  = $this->af->get("in_field3_8");
        $data{"field3_9"}  = $this->af->get("in_field3_9");
        $data{"field3_10"} = $this->af->get("in_field3_10");
        $data{"field3_11"} = $this->af->get("in_field3_11");
        $data{"field3_12"} = $this->af->get("in_field3_12");
        $data{"field3_13"} = $this->af->get("in_field3_13");
        $data{"field3_14"} = $this->af->get("in_field3_14");
        $data{"field3_15"} = $this->af->get("in_field3_15");
        $data{"keyword"}   = $this->af->get("in_keyword");
        $data{"callkind"}  = $this->af->get("in_callkind");

        // 前後スペース除去
        $data{"keyword"} = trim($data{"keyword"});
        // 禁則文字変換
        $data{"keyword"} = $this->replace_prohibition($data{"keyword"});
        // 禁則文字除去
        $data{"keyword"} = $this->delete_prohibition($data{"keyword"});

        //検索
        $result_data = null;
        $result = null;
        
        if($data{"callkind"}=="autonomy"){
            //自治体案件検索
            list($result_count, $result_data) = Opensite_Dao_Search::select_government_contents($id, $data);
            $i = 0;
            $pattern = '(https?://www.mirasapo.jp[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)';
            foreach ($result_data as $row) {
                //ミラサポ関連のURLを除去
                if(preg_match_all($pattern, $result_data[$i]['comments'], $res) !== false){
                    foreach ($res[0] as $value){
                        //URLを除去
                        $result_data[$i]['comments'] = str_replace($value, "", $result_data[$i]['comments']);
                    }
                }

                $result_data[$i]['main_title'] = mb_strimwidth($result_data[$i]['main_title'], 0, 72, "...");
                $result_data[$i]['comments'] = mb_strimwidth($result_data[$i]['comments'] , 0, 320, "...");
                $i++;
            }
        }else{
            //財団案件検索
            list($result_count, $result_data) = Opensite_Dao_Search::select_foundation_contents($id, $data);
            $i = 0;
            $pattern = '(https?://www.jfc.or.jp[-_.!~*\'()a-zA-Z0-9;/?:@&=+$,%#]+)';
            foreach ($result_data as $row) {
                //財団センター関連のキーワードを除去
                if(preg_match_all($pattern, $result_data[$i]['comments'], $res) !== false){
                    foreach ($res[0] as $value){
                        //URL表示
                        $result_data[$i]['comments'] = str_replace($value, "", $result_data[$i]['comments']);
                    }
                }

                $result_data[$i]['main_title'] = mb_strimwidth($result_data[$i]['main_title'], 0, 72, "...");
                $result_data[$i]['comments'] = mb_strimwidth($result_data[$i]['comments'] , 0, 320, "...");
                $i++;
            }
        }
        
        
        //チェックボックス用のデータに加工
        if($data{"kind_1000"} == "1000") {$data{"kind_1000"} = "checked ";}
        if($data{"kind_2000"} == "2000") {$data{"kind_2000"} = "checked ";}
        if($data{"kind_3000"} == "3000") {$data{"kind_3000"} = "checked ";}
        
        if($data{"field_90"} == "9000") {$data{"field_90"} = "checked ";}
        if($data{"field_91"} == "9100") {$data{"field_91"} = "checked ";}
        if($data{"field_92"} == "9200") {$data{"field_92"} = "checked ";}
        if($data{"field_93"} == "9300") {$data{"field_93"} = "checked ";}
        if($data{"field_94"} == "9400") {$data{"field_94"} = "checked ";}
        if($data{"field_95"} == "9500") {$data{"field_95"} = "checked ";}
        if($data{"field_96"} == "9600") {$data{"field_96"} = "checked ";}
        if($data{"field_97"} == "9700") {$data{"field_97"} = "checked ";}
        if($data{"field_98"} == "9800") {$data{"field_98"} = "checked ";}
        if($data{"field_99"} == "9900") {$data{"field_99"} = "checked ";}
        if($data{"field_100"} == "10000") {$data{"field_100"} = "checked ";}
        if($data{"field_101"} == "10100") {$data{"field_101"} = "checked ";}
        
        if($data{"field2_1"} == "1000") {$data{"field2_1"} = "checked ";}
        if($data{"field2_2"} == "2000") {$data{"field2_2"} = "checked ";}
        if($data{"field2_3"} == "3000") {$data{"field2_3"} = "checked ";}
        if($data{"field2_4"} == "4000") {$data{"field2_4"} = "checked ";}
        if($data{"field2_5"} == "5000") {$data{"field2_5"} = "checked ";}
        if($data{"field2_6"} == "6000") {$data{"field2_6"} = "checked ";}
        if($data{"field2_7"} == "7000") {$data{"field2_7"} = "checked ";}
        if($data{"field2_8"} == "8000") {$data{"field2_8"} = "checked ";}
        if($data{"field2_9"} == "9000") {$data{"field2_9"} = "checked ";}
        if($data{"field2_10"} == "10000") {$data{"field2_10"} = "checked ";}
        if($data{"field2_11"} == "11000") {$data{"field2_11"} = "checked ";}
        
        if($data{"field3_1"} == "1000") {$data{"field3_1"} = "checked ";}
        if($data{"field3_2"} == "2000") {$data{"field3_2"} = "checked ";}
        if($data{"field3_3"} == "3000") {$data{"field3_3"} = "checked ";}
        if($data{"field3_4"} == "4000") {$data{"field3_4"} = "checked ";}
        if($data{"field3_5"} == "5000") {$data{"field3_5"} = "checked ";}
        if($data{"field3_6"} == "6000") {$data{"field3_6"} = "checked ";}
        if($data{"field3_7"} == "7000") {$data{"field3_7"} = "checked ";}
        if($data{"field3_8"} == "8000") {$data{"field3_8"} = "checked ";}
        if($data{"field3_9"} == "9000") {$data{"field3_9"} = "checked ";}
        if($data{"field3_10"} == "10000") {$data{"field3_10"} = "checked ";}
        if($data{"field3_11"} == "11000") {$data{"field3_11"} = "checked ";}
        if($data{"field3_12"} == "12000") {$data{"field3_12"} = "checked ";}
        if($data{"field3_13"} == "13000") {$data{"field3_13"} = "checked ";}
        if($data{"field3_14"} == "14000") {$data{"field3_14"} = "checked ";}
        if($data{"field3_15"} == "15000") {$data{"field3_15"} = "checked ";}
        
        // デフォルト値設定
        // 初期表示時、回答データ取得
        if($this->af->validate() == 0){
            
        }

        
        
        //print_r($result);
                //$result_data = $result->fetchRow();
        //var_dump($result_data[0]);
        //echo $result_data['kind'].'<br />';
        //echo $result_data[1]['field'].'<br />';
        //echo $result_data[2]['contents_org_id'].'<br />';
        /*
        while ($result_data[$i] = $result->fetchRow()) {
            $i++;
        }
        */
        /*
        foreach ($result_data as $row) {
            echo $row['main_title'] . ",";
            echo $row['field'] . ",";
            echo $row['contents_org_id'] . "<br />";
        }
*/
        
        
        
        //結果を配列へ
        //$result_data[$i] =$result->fetch();
//        while ($result_data[$i] = $result->fetchRow()) {
//            $i++;
//        }
                
                
        /*
        print "finish<br /><table border=1>";
        while ($total_data[$i] = $result->fetchRow()) {
            print "<tr><td>";
            print $total_data[$i]{"hiduke"};
            print "</td><td>";
            print $total_data[$i]{"CNT"};
            print "</td></tr>";
            $i++;
        }
        print "</table>";
        */
        
        
        // TSVマスタデータ
        
        // ラジオ生成
        //$radio = array();
        // プルダウン生成
        //$pulldown = array();
        //
        //
        
        
        //rankによる表示件数制限
        $disp_count = $result_count;
        if($rank=="0"){
            //無料会員の場合は10件まで
            if($disp_count>10){
                $disp_count = 10;
            }
        }
        // エスケープ有
        $this->af->setApp('data',  $data);
        $this->af->setApp('result_data',  $result_data);
        $this->af->setApp('result_count',  $result_count);
        $this->af->setApp('disp_count',  $disp_count);
        $this->af->setApp('name',  $name);
        $this->af->setApp('rank',  $rank);

        // エスケープ無
        //$this->af->setAppNE('radio',     $radio);
        //$this->af->setAppNE('pulldown',  $pulldown);

    }


    function replace_prohibition($str)
    {
        $str = Opensite_Sanitization::replace_prohibition($str, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1);
        return $str;
    }

    function delete_prohibition($str)
    {
        $str = Opensite_Sanitization::replace_prohibition($str, "", "", "", "", "", 0, "", "", "", "");
        return $str;
    }

}

?>
