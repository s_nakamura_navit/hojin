<?php
/**
 *  message.php
 */

/**
 *  message view implementation.
 *
 *  @access     public
 *  @package    Enquete
 */
class Opensite_View_Message extends Opensite_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
        // 表示設定
        $this->is_himg = false;
    }
}

?>
