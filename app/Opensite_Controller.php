<?php
/**
 *  Opensite_Controller.php
 *
 *  @author     {$author}
 *  @package    Opensite
 *  @version    $Id$
 */

/** Application base directory */
define('BASE', dirname(dirname(__FILE__)));

/** include_path setting (adding "/app" and "/lib" directory to include_path) */
$app = BASE . "/app";
$lib = BASE . "/lib";
$pb = BASE . "/pb";
set_include_path(implode(PATH_SEPARATOR, array($app, $lib, $pb)));

/** including application library. */
require_once 'Ethna/Ethna.php';
require_once 'Opensite_Error.php';
require_once 'Opensite_ActionClass.php';
require_once 'Opensite_ActionForm.php';
require_once 'Opensite_AdminLogger.php';
require_once 'Opensite_Authentication.php';
require_once 'Opensite_Formparts.php';
require_once 'Opensite_MasterManager.php';
require_once 'Opensite_Menu.php';
require_once 'Opensite_Pager.php';
require_once 'Opensite_User.php';
require_once 'Opensite_ViewClass.php';
require_once 'Opensite_Sanitization.php';

require_once 'Opensite_Session.php';
require_once 'Opensite_Define.php';
//require_once 'dao/Opensite_Dao_Config.php';
//require_once 'dao/Opensite_Dao_History.php';
require_once 'dao/Opensite_Dao_Inquiry.php';
require_once 'dao/Opensite_Dao_Request.php';
require_once 'dao/Opensite_Dao_User.php';
require_once 'dao/Opensite_Dao_Search.php';
require_once 'dao/Opensite_Dao_Mypage.php';
require_once 'dao/Opensite_Dao_Furu.php';
require_once 'dao/Opensite_Dao_Password.php';
require_once 'dao/Opensite_Dao_Login.php';
require_once 'dao/Opensite_Dao_Alliance.php';
require_once 'simple_html_dom.php';
require_once 'Opensite_Mail.php';

define('CONF_SESSION_TIMEOUT', 30);
define('ONE_PAGE_ROWS',        10);
define('HISTORY_PAGE_ROWS',    50);
define('DELIMITTER_LF',        "\n");
define('DELIMITTER_CRLF',      "\r\n");
define('DELIMITTER_COMMA',     ",");
define('DELIMITTER_TAB',       "\t");
define('ENCRYPT_KEYB',         'WqslpQa');
define('TEXT_HEADER',          '<table width="740" border="0" cellspacing="0" cellpadding="0"><tr><td width="665" height="40" align="left" valign="middle"><h3>ナビットFAX配信サービス</h3></td><td width="75" align="left" valign="middle"></td></tr></table>');
define('TEXT_HEADER_LOGIN',    '<table width="740" border="0" cellspacing="0" cellpadding="0"><tr><td width="665" height="40" align="left" valign="middle"><h3>ナビットFAX配信サービス</h3></td><td width="75" align="left" valign="middle"><img src="images/btn_logout.gif" border="0" onClick="location.href=\'index.php?action_logout_do=true\'" style="cursor:pointer;"></td></tr></table>');
define('TEXT_FOOTER',          '');
define('SYSTEM_START_DATE',    '2015/01/01');
// 権限
define('ROLE_ADMIN',    9);
define('ROLE_SUBADMIN', 5);
define('ROLE_WS',       3);
define('ROLE_NORMAL',   1);

define('FILE_DIRECTORY',       "../www/files/");
define('FILE_DIRECTORY2',      "../www/files2/");
define('DIRECTORY',            "/");
define('PROPOSER',             "proposer/");
define('RESPONDER',            "responder/");

// 宛先リストの1行あたりの長さ（fgetcsvの読み込み長さ： 0:無制限だが処理重め）
define('MAX_LIST_LENGTH',       0);

/**
 *  Opensite application Controller definition.
 *
 *  @author     {$author}
 *  @access     public
 *  @package    Opensite
 */
class Opensite_Controller extends Ethna_Controller
{
    /**#@+
     *  @access private
     */

    /**
     *  @var    string  Application ID(appid)
     */
    var $appid = 'OPENSITE';

    /**
     *  @var    array   forward definition.
     */
    var $forward = array(
        /*
         *  TODO: write forward definition here.
         *
         *  Example:
         *
         *  'index'         => array(
         *      'view_name' => 'Opensite_View_Index',
         *  ),
         */
    );

    /**
     *  @var    array   action definition.
     */
    var $action = array(
        /*
         *  TODO: write action definition here.
         *
         *  Example:
         *
         *  'index'     => array(
         *      'form_name' => 'Sample_Form_SomeAction',
         *      'form_path' => 'Some/Action.php',
         *      'class_name' => 'Sample_Action_SomeAction',
         *      'class_path' => 'Some/Action.php',
         *  ),
         */
    );

    /**
     *  @var    array   SOAP action definition.
     */
    var $soap_action = array(
        /*
         *  TODO: write action definition for SOAP application here.
         *  Example:
         *
         *  'sample'            => array(),
         */
    );

    /**
     *  @var    array       application directory.
     */
    var $directory = array(
        'action'        => 'app/action',
        'action_cli'    => 'app/action_cli',
        'action_xmlrpc' => 'app/action_xmlrpc',
        'app'           => 'app',
        'plugin'        => 'app/plugin',
        'bin'           => 'bin',
        'etc'           => 'etc',
        'filter'        => 'app/filter',
        'locale'        => 'locale',
        'log'           => 'log',
        'lib'           => 'lib',
        'plugins'       => array('app/plugin/Smarty',),
        'template'      => 'template',
        'template_c'    => 'tmp',
        'tmp'           => 'tmp',
        'view'          => 'app/view',
        'www'           => 'www',
        'test'          => 'app/test',
        'mail'          => 'mail',
    );

    /**
     *  @var    array       database access definition.
     */
    var $db = array(
        ''              => DB_TYPE_RW,
    );

    /**
     *  @var    array       extention(.php, etc) configuration.
     */
    var $ext = array(
        'php'           => 'php',
        'tpl'           => 'tpl',
    );

    /**
     *  @var    array   class definition.
     */
    var $class = array(
        /*
         *  TODO: When you override Configuration class, Logger class,
         *        SQL class, don't forget to change definition as follows!
         */
        'class'         => 'Ethna_ClassFactory',
        'backend'       => 'Ethna_Backend',
        'config'        => 'Ethna_Config',
    //    'db'            => 'Ethna_DB_PEAR',
        'db'            => 'Opensite_DB_ADOdb',
        'error'         => 'Ethna_ActionError',
        'form'          => 'Opensite_ActionForm',
        'i18n'          => 'Ethna_I18N',
        'logger'        => 'Ethna_Logger',
        'plugin'        => 'Ethna_Plugin',
//        'session'       => 'Ethna_Session',
        'session'       => 'Opensite_Session',
        'sql'           => 'Ethna_AppSQL',
        'view'          => 'Opensite_ViewClass',
        'renderer'      => 'Ethna_Renderer_Smarty',
        'url_handler'   => 'Opensite_UrlHandler',
    );

    /**
     *  @var    array       list of application id where Ethna searches plugin.
     */
    var $plugin_search_appids = array(
        /*
         *  write list of application id where Ethna searches plugin.
         *
         *  Example:
         *  When there are plugins whose name are like "Common_Plugin_Foo_Bar" in
         *  application plugin directory, Ethna searches them in the following order.
         *
         *  1. Common_Plugin_Foo_Bar,
         *  2. Opensite_Plugin_Foo_Bar
         *  3. Ethna_Plugin_Foo_Bar
         *
         *  'Common', 'Opensite', 'Ethna',
         */
        'Opensite', 'Ethna',
    );

    /**
     *  @var    array       filter definition.
     */
    var $filter = array(
        /*
         *  TODO: when you use filter, write filter plugin name here.
         *  (If you specify class name, Ethna reads filter class in
         *   filter directory)
         *
         *  Example:
         *
         *  'ExecutionTime',
         */
    );

    /**#@-*/

    /**
     *  Get Default language and locale setting.
     *  If you want to change Ethna's output encoding, override this method.
     *
     *  @access protected
     *  @return array   locale name(e.x ja_JP, en_US .etc),
     *                  system encoding name,
     *                  client encoding name(= template encoding)
     *                  (locale name is "ll_cc" format. ll = language code. cc = country code.)
     */
    function _getDefaultLanguage()
    {
        return array('ja_JP', 'UTF-8', 'UTF-8');
    }

    /**
     *  テンプレートエンジンのデフォルト状態を設定する
     *
     *  @access protected
     *  @param  object  Ethna_Renderer  レンダラオブジェクト
     *  @obsolete
     */
    function _setDefaultTemplateEngine(&$renderer)
    {
    }
}

?>
