<?php
/**
 *  Opensite_Sanitization.php
 *
 *  @copyright  (C)2012 Navit co.,ltd.
 *  @author     Masaaki Satake <satake@navit-j.com>
 *  @package    Maitoku
 *  @version    $Id$
 */

/**
 *  Opensite Sanitization Class.
 *
 *  @author     Masaaki Satake <satake@navit-j.com>
 *  @access     public
 *  @package    Maitoku
 */
class Opensite_Sanitization
{
    /**
     *  replace_prohibition
     *  サニタイズ（引数自体の検査は行わない）
     *  replace_prohibition ($origin_char, $r_comma, $r_single_quotation, $r_double_quotation, $r_back_slash, $r_tab, $r_new_line, $r_dollar, $r_carriage_return);
     *  
     *  $origin_char         元の文字列
     *  $r_comma            「,」     を置換するか　0:置換しない 1:「，」に置換する それ以外:指定された文字列に置換
     *  $r_single_quotation 「'」     を置換するか　0:置換しない 1:「’」に置換する それ以外:指定された文字列に置換
     *  $r_double_quotation 「"」     を置換するか　0:置換しない 1:「”」に置換する それ以外:指定された文字列に置換
     *  $r_back_slash       「\」     を置換するか　0:置換しない 1:「￥」に置換する それ以外:指定された文字列に置換
     *  $r_tab              「	」    を置換するか　0:置換しない 1:  nvl に置換する それ以外:指定された文字列に置換
     *  $r_new_line         「(改行)」を置換するか　0:置換しない 1:  nvl に置換する それ以外:指定された文字列に置換
     *  $r_dollar           「$」     を置換するか　0:置換しない 1:「＄」に置換する それ以外:指定された文字列に置換
     *  $r_carriage_return  「(復帰)」を置換するか　0:置換しない 1:  nvl に置換する それ以外:指定された文字列に置換
     *  $r_angle_bracket_l  「<」     を置換するか　0:置換しない 1:「＜」に置換する それ以外:指定された文字列に置換
     *  $r_angle_bracket_r  「>」     を置換するか　0:置換しない 1:「＞」に置換する それ以外:指定された文字列に置換
     *  $result_char         変換後の文字列
     *
     *  戻り値
     *  $result_char
     *  
     *  @access protected
     */
    function replace_prohibition($origin_char, $r_comma, $r_single_quotation, $r_double_quotation, $r_back_slash, $r_tab, $r_new_line, $r_dollar, $r_carriage_return, $r_angle_bracket_l, $r_angle_bracket_r)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();

        $result_char = $origin_char;
        $in_arguments = array($origin_char, $r_comma, $r_single_quotation, $r_double_quotation, $r_back_slash, $r_tab, $r_new_line, $r_dollar, $r_carriage_return, $r_angle_bracket_l, $r_angle_bracket_r);
        $target_cahr = array("", ",", "'", "\"", "\\", "\t", "\n", "\$", "\r", "<", ">");
        $substitute = array("", "，", "’", "”", "￥", "", "", "＄", "", "＜", "＞");

        for($i = 1; $i < 11; $i++){
            if ($in_arguments[$i] == "0"){                                                       //何もしない
            } elseif ($in_arguments[$i] == "1"){
                $result_char = str_replace($target_cahr[$i], $substitute[$i], $result_char);     //規定の文字に置換
            } else {
                $result_char = str_replace($target_cahr[$i], $in_arguments[$i], $result_char);   //指定文字(列)に置換
            };
        }
        return $result_char;
    }

    /**
     *  replace_sv_prohibition
     *  サニタイズ（csv、tsv用）
     *  replace_prohibition ($origin_char, $mode);
     *  
     *  $origin_char          元の文字列
     *  $mode                 変換モード　　　　 1:csv        2:tsv
     *  $r_single_quotation   「'」を置換するか　0:置換しない 1:nvlに置換する
     *  $r_double_quotation   「"」を置換するか　0:置換しない 1:nvlに置換する
     *  $result_char          変換後の文字列
     *
     *  戻り値
     *  $result_char
     *  
     *  @access protected
     */
    function replace_sv_prohibition($origin_char, $mode, $r_single_quotation, $r_double_quotation)
    {
        $ctl    =& Ethna_Controller::getInstance();
        $logger =  $ctl->getLogger();
        $back   =  $ctl->getBackend();

        $result_char = $origin_char;

        // 禁則文字排除
        if ($mode == 1){
            $result_char = str_replace(",", "", $result_char);    //csvの場合、カンマを削除
        } elseif ($mode == 2) {
            $result_char = str_replace("\t", "", $result_char);   //tsvの場合、タブを削除
        }
        if ($r_single_quotation == 1){
            $result_char = str_replace("'", "", $result_char);
        }
        if ($r_double_quotation == 1){
            $result_char = str_replace("\"", "", $result_char);
        }
        $result_char = str_replace("\n", "", $result_char);
        $result_char = str_replace("\r", "", $result_char);

        return $result_char;
    }

}
?>
