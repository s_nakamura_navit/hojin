<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>緯度経度情報提供サービス「ジオコードなう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="株式会社ナビットが提供する緯度経度情報提供サービス「ジオコードなう」">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="緯度,経度,ジオコード,ジオコーディング,付与,位置情報,地理情報">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="shortcut icon" href="">
{literal} 
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<script type="text/javascript" src="js/jquery.jStageAligner.js"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->

<script>
    $(window).load(function(){
      $("#gnavi").sticky({ topSpacing: 0, center:true, className:"hey" });
    });
</script>
<script type="text/javascript">
//吹き出し
$('label').balloon();


$(function(){
      changeCity(document.f2.in_area1);
});  
  	
//全選択・全解除
$(function() {
    $('#all_check').on("click",function(){
        $('.CheckList').prop("checked", true);
    });
});
$(function() {
    $('#all_clear').on("click",function(){
        $('.CheckList').prop("checked", false);
    });
});

$(function() {
    $('#all_check2').on("click",function(){
        $('.CheckList2').prop("checked", true);
    });
});
$(function() {
    $('#all_clear2').on("click",function(){
        $('.CheckList2').prop("checked", false);
    });
});

$(function() {
    $('#all_check3').on("click",function(){
        $('.CheckList3').prop("checked", true);
    });
});
$(function() {
    $('#all_clear3').on("click",function(){
        $('.CheckList3').prop("checked", false);
    });
});

//リセット(自治体)
$(function() {
    $('#a_reset').on("click",function(){
        //ラジオボタン初期値セット
        $('#in_kind_1').prop("checked", true);
        $('#in_kind_2').prop("checked", false);
        //プルダウン初期値セット
        $('select[name="in_area1"]').val("");
        $('#in_area2').html('');//一度select内を空に
        $('#in_area2').append('<option id="city00000" value="'+''+'">'+'市区町村を選択'+'</option>');
        
        //チェックボックスクリア
        $('.CheckList').prop("checked", false);
        
        $("#a_reset").blur();
    });
});
//リセット(財団)
$(function() {
    $('#f_reset').on("click",function(){
        //チェックボックスクリア
        $('.CheckList2').prop("checked", false);
        $('.CheckList3').prop("checked", false);
        $('#in_keyword').val("");
        $("#f_reset").blur();
    });
});

function changeCity(sel){

    var pref_code = sel.options[sel.selectedIndex].value;
    if(pref_code==""){
        $('#in_area2').html('');//一度select内を空に
        $('#in_area2').append('<option id="city00000" value="'+''+'">'+'市区町村を選択'+'</option>');
    }
    
    
    $.ajax({
        type: "GET",
        url: "lib/citydata.php",
        dataType : 'json',
        data: "in_prefcode="+pref_code,
        success: function( res )
        {
            if(pref_code!=""){
                $('#in_area2').html('');//一度select内を空に
                for(var i=0; i<res.length; ++i){
                    $('#in_area2').append('<option id="city'+res[i].city_code+'" value="'+res[i].city_code+'">'+res[i].city_name+'</option>');
                }
            }
        },
        error:function(XMLHttpRequest, textStatus, errorThrown)
        {
            alert('Error : '+ errorThrown);
        }
        
    });
}

function doblur() { 
    var element = document.getElementById("name"); 
    element.blur(); 
} 

function do_submit_with_param(param){
    document.f2.in_callkind.value = param;
    document.f2.submit();
}

$(function(){
    // id="filter"のオブジェクトが無かったらbodyに追加
    if(!document.getElementById("filter")){
        //$("body").append("<div id=\"filter\"></div>");
        $("div#conteinar").append("<div id=\"filter\"></div>");
    }else{
        $("#filter").show();
    }
    // フィルターをクリックするとフィルタをOFFに
    //$("#filter").click(function(){
    //    $(this).hide();
    //});

});

$(function() {
  //$("#box1").jStageAligner("CENTER_BOTTOM", {time: 500, easing: "swing", callback: function() {
    //alert("ok"); 
  //}});
  //$("#fixedbox").jStageAligner("CENTER_MIDDLE", {time: 200, easing: "swing"});
  $("#fixedbox").jStageAligner("CENTER_MIDDLE");
  //$("#fixedbox").jStageAligner("RIGHT_BOTTOM");
  //$("#box4").jStageAligner("LEFT_BOTTOM", {time: 100});
  //$("#box5").jStageAligner("RIGHT_MIDDLE", {time: 1500});
  //$("#box6").jStageAligner("CENTER_TOP", {marginTop: 200});
});


</script>
{/literal} 
</head>
<body>


<!-- ここから表示されるBox -->
<div id="center_box">
<div id="fixedbox">

<div style="position:relative; top:-12px; left:670px; width:24px;">
<a href="index.php"><img src="img/close_round.png" width="20px" height="20px" alt="閉じる" /></a>
</div>

<table style="margin-left:11%;margin-top: -3%">

    <tr>
        <td colspan="3">
                <div id="submit_btn">
                    <span style="font-size:20px;color:#22a90c;">『助成金なう』のご利用には会員登録が必要となります。<br />まずは無料でお試しください！</span><br />
                        <a class="button5" href="./index.php?action_secure_registration=true"  id="registration">今すぐ無料会員登録！</a>     
                </div>
        </td>
    </tr>
    
    <tr>
        <td colspan="3">
                <div id="submit_btn">
                    <span style="font-size:20px;color:#22a90c">すでに会員の方はこちらからログインしてください。</span><br />
                        <a class="button5" href="./index.php?action_login=true" onclick="" id="login_btn">すでに会員の方はこちら</a>     
                </div>
        </td>
    </tr>
    
     <tr>
        <td colspan="3">
                <div id="submit_btn">
                        <a class="button5" href="./lp.html" onclick="" id="login_btn"><img src='img/wakaba_mark.png' width="17px" height="25px" style="display:inline; margig-top:10px;"/> 助成金なうについて詳しくはこちら</a>     
                </div>
        </td>
    </tr>
    
    
</table>

</div>
</div>
<!-- ここまで表示されるBox -->


<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_searchresult" value="true">
<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
	<div id="wrapper">

<!-- ここからheader --><div id="01"></div>
		<div id="header">

			<div id="logo">
				<a href="index.php" ><img src="img/logo.jpg" alt="助成金なう" /></a>
			</div>

			<div id="h1">
				<h1>FAX送信代行サービス「FAXなう」</h1>
			</div>

			<div id="mini_contact">
				<img src="img/m_contact.gif" alt="お問合せ" />
			</div>

			<div id="mini_contact_txt">
				<a href="https://www.navit-j.com/contactus/" target="_blank">お問合せ</a>
			</div>

<!--			<div id="mini_sitemap">
				<img src="img/m_sitemap.gif" alt="サイトマップ" />
			</div>

			<div id="mini_sitemap_txt">
				<a href="#">サイトマップ</a>
			</div>
-->
			<div id="freedial">
				<img src="img/freedial.jpg" alt="0120-937-781" />
			</div>

			{if $app.name !=""}
                        <div style="display:inline-block;width:360px;">
                            <table  border="1" cellspacing="0" cellpadding="0" style="width:100%; border-color:#f00">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <span id="logininfo">{$app.name} 様　ログイン中</span>   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <a href="./index.php?action_logout_do=true" id="logout_btn">ログアウト</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>    
                        </div>
                    {else}
                        <div id="newaccount">
                            <a class="button3" href="./index.php?action_secure_registration=true"  id="registration">新規会員登録</a>
                        </div>
                        <div id="login">
                            <a class="button3" href="./index.php?action_login=true" onclick="" id="login_btn">ログイン</a>
                        </div>
                    {/if}

		</div>

		<div class="clear">
		</div>

<!-- ここまでheader -->

<!-- ここからG NAVI -->


<div id="gnavi">
	<ul>
		<li style="height:93px;">
			<a href="#01"><img src="img/gnavi_home.jpg" alt="ホーム" width="143px" height="94px"></a>
		</li>
		<li style="height:93px;">
			<a href="#02"><input type="image" onClick="" src="img/gnavi_news.jpg" onMouseOver="this.src='img/gnavi_news_d.jpg'" onMouseOut="this.src='img/gnavi_news.jpg'" alt="お知らせ" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="#03"><input type="image" onClick="" src="img/gnavi_a_search.jpg" onMouseOver="this.src='img/gnavi_a_search_d.jpg'" onMouseOut="this.src='img/gnavi_a_search.jpg'" alt="自治体案件検索" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="#04"><input type="image" onClick="" src="img/gnavi_f_search.jpg" onMouseOver="this.src='img/gnavi_f_search_d.jpg'" onMouseOut="this.src='img/gnavi_f_search.jpg'" alt="財団案件検索" width="142px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="#05"><input type="image" onClick="" src="img/gnavi_sem.jpg" onMouseOver="this.src='img/gnavi_sem_d.jpg'" onMouseOut="this.src='img/gnavi_sem.jpg'" alt="セミナー・イベント" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="#06"><input type="image" onClick="" src="img/gnavi_com.jpg" onMouseOver="this.src='img/gnavi_com_d.jpg'" onMouseOut="this.src='img/gnavi_com.jpg'" alt="会社概要" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="https://www.navit-j.com/contactus/" target="_blank"><input type="image" onClick="" src="img/gnavi_ccontact.jpg" onMouseOver="this.src='img/gnavi_ccontact_d.jpg'" onMouseOut="this.src='img/gnavi_ccontact.jpg'" alt="お問合せ" width="143px" height="93px"></a>
		</li>
	</ul>

</div>
		<div class="clear">
		</div>

<!-- ここまでG NAVI -->

<!-- ここからメインビジュアル -->

	<img src="img/main_vis01.jpg" alt="助成金なうの特徴" />

<!-- ここまでメインビジュアル -->

<!-- ここからメインコンテンツ -->
<!-- ここからお知らせ -->
<!--
<div id="02"></div>

	<img src="img/title_news_line.jpg" alt="" />
	<img src="img/title_news.jpg" alt="お知らせ" />


<div class="topics_maincont_news">
		<div class="space">
			<div class="news">
				2014.11.21
			</div>
			<div class="text">
				<a href="http://nyusatsu-now.com/column201421.html">行政書士斎藤優子の入札ワンポイント講座の第21回を掲載いたしました。
「第21回　問題点」</a>
			</div>
		</div>
		<div class="clear">
		</div>

		<div class="space">
			<div class="news">
				2014.11.21
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/kouen_index.html#koen141128" target="_blank">11月25日(火)～28日(金)の間【北海道/東北/関東/関西/中部・北陸/九州/中国・四国エリア行政書士向け】入札申請代行パートナーオンライン説明会を開催いたします。</a>
			</div>
		</div>
		<div class="clear">
		</div>


		<div class="space">
			<div class="media">
				2014.10.19
			</div>
			<div class="text">
				<a href="http://www.navit-j.com/press/info.html#info141120" target="_blank">日本流通新聞のサイト上で「物流なう」のサービスがスタートいたしました。毎朝、物流に特化した入札情報が会員様のメールに送られる、「入札なうクラウド/メル太君」でアライアンスがスタートいたしました。</a>
			</div>
		</div>
		<div class="clear">
		</div>


		<div class="space">
			<div class="news">
				2014.11.17
			</div>
			<div class="text">
				<a href="https://www.facebook.com/nyusatsunow" target="_blank">入札なうのFacebookページがスタートいたしました。入札についての基礎知識やお得な情報などが、週に１、２回のペースで更新されます。是非、この機会にサイトのページに「いいね！」を押して、情報を有効にご活用下さい。</a>
			</div>
		</div>
		<div class="clear">
		</div>


		<div class="space">
			<div class="news">
				2014.11.17
			</div>
			<div class="text">
				<a href="https://www.facebook.com/nyusatsunow" target="_blank">入札なうのFacebookページがスタートいたしました。入札についての基礎知識やお得な情報などが、週に１、２回のペースで更新されます。是非、この機会にサイトのページに「いいね！」を押して、情報を有効にご活用下さい。</a>
			</div>
		</div>
		<div class="clear">
		</div>

		<div class="space">
			<div class="news">
				2014.11.17
			</div>
			<div class="text">
				<a href="https://www.facebook.com/nyusatsunow" target="_blank">入札なうのFacebookページがスタートいたしました。入札についての基礎知識やお得な情報などが、週に１、２回のペースで更新されます。是非、この機会にサイトのページに「いいね！」を押して、情報を有効にご活用下さい。</a>
			</div>
		</div>
		<div class="clear">
		</div>

		<div class="space">
			<div class="news">
				2014.11.17
			</div>
			<div class="text">
				<a href="https://www.facebook.com/nyusatsunow" target="_blank">入札なうのFacebookページがスタートいたしました。入札についての基礎知識やお得な情報などが、週に１、２回のペースで更新されます。是非、この機会にサイトのページに「いいね！」を押して、情報を有効にご活用下さい。</a>
			</div>
		</div>
		<div class="clear">
		</div>


		<div class="space">
			<div class="news">
				2014.11.17
			</div>
			<div class="text">
				<a href="https://www.facebook.com/nyusatsunow" target="_blank">入札なうのFacebookページがスタートいたしました。入札についての基礎知識やお得な情報などが、週に１、２回のペースで更新されます。是非、この機会にサイトのページに「いいね！」を押して、情報を有効にご活用下さい。</a>
			</div>
		</div>
		<div class="clear">
		</div>

</div>

-->

<!-- ここまでお知らせ -->

<!-- ここから自治体フォーム --><div id="03"></div>

	<img src="img/title_a_search_line.jpg" alt="" />
	<img src="img/title_a_search.jpg" alt="自治体案件検索" />

<div class="table">

		<table border="0" cellpadding="5" cellspacing="1">

		<tr>
			<td class="l_Cel"><img src="img/midashi_syubetu.jpg" alt="種別" /></td>
			<td colspan="2" class="r_Cel">
                            {$app_ne.radio.kind}
                            {if is_error('in_kind')}<br /><br /><span class="error">{message name="in_kind"}</span>{/if}
                        </td>
		  </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_area.jpg" alt="エリア" /></td>
			<td colspan="2" class="r_Cel">
                            
                            <select id='in_area1' name="in_area1" class="select_font" onchange="changeCity(this);">{$app_ne.pulldown.area1}</select>
                            <select id='in_area2' name="in_area2" class="select_font"><option value="">市区町村を選択</option></select>
                            {if is_error('in_area1')}<br /><br /><span class="error">{message name="in_area1"}</span>{/if}
                            {if is_error('in_area2')}<br /><br /><span class="error">{message name="in_area2"}</span>{/if}
                            
                        </td>
                            
		  </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_bunya.jpg" alt="分野" />
                        <a href="javascript:void(0)" id="all_check" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
                        <a href="javascript:void(0)" id="all_clear" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a>
                        </td>
			<td class="r_Cel">
                            <table border="0" style="text-align:left;width:640px;">
                                <tr>
                                    <td style="width:320px;"><label><input class="CheckList" type="checkbox" name="in_field_90" id="in_field_90" value="9000" {$app.data.field_90} />{$app.master_field.9000}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_91" id="in_field_91" value="9100" {$app.data.field_91} />{$app.master_field.9100}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_92" id="in_field_92" value="9200" {$app.data.field_92} />{$app.master_field.9200}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_93" id="in_field_93" value="9300" {$app.data.field_93} />{$app.master_field.9300}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_94" id="in_field_94" value="9400" {$app.data.field_94} />{$app.master_field.9400}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_95" id="in_field_95" value="9500" {$app.data.field_95} />{$app.master_field.9500}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_96" id="in_field_96" value="9600" {$app.data.field_96} />{$app.master_field.9600}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_97" id="in_field_97" value="9700" {$app.data.field_97} />{$app.master_field.9700}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_98" id="in_field_98" value="9800" {$app.data.field_98} />{$app.master_field.9800}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_99" id="in_field_99" value="9900" {$app.data.field_99} />{$app.master_field.9900}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_100" id="in_field_100" value="10000" {$app.data.field_100} />{$app.master_field.10000}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_101" id="in_field_101" value="10100" {$app.data.field_101} />{$app.master_field.10100}</label></td>
                                </tr>
                            </table>
                            
                            {if is_error('in_field')}<br /><span class="error">{message name="in_field"}</span>{/if}
                            
                            
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td colspan="4">
				<div id="submit_btn">
					<!--<input type="image" onclick="return false;" id="a_reset" src="img/subbtn_reset.png" onMouseOver="this.src='img/subbtn_reset_d.png'" onMouseOut="this.src='img/subbtn_reset.png'" alt="リセット">-->
                                        <a class="button" href="javascript:void(0)" onclick="return false;" id="a_reset">リセット</a>
					<!--<input type="image" id="a_search" onClick="javascript:do_submit_with_param('autonomy');" src="img/subbtn_submit.png" onMouseOver="this.src='img/subbtn_submit_d.png'" onMouseOut="this.src='img/subbtn_submit.png'" alt="一覧表示">-->
                                        <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit_with_param('autonomy');" id="a_search">一覧表示</a>     
				</div>
			</td>
		  </tr>

    </table>
</div>
	<!-- ここまで自治体フォーム -->

<!-- ここからトップへ戻る -->

	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>

<!-- ここまでトップへ戻る -->

<!-- ここから財団フォーム --><div id="04"></div>

	<img src="img/title_f_search_line.jpg" alt="" />
	<img src="img/title_f_search.jpg" alt="財団案件検索" />

<div class="table">
		<table border="0" cellpadding="5" cellspacing="1">

		<tr>
			<td class="l_Cel"><img src="img/midashi_bunya.jpg" alt="分野" />
                        <a href="javascript:void(0)" id="all_check2" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
                        <a href="javascript:void(0)" id="all_clear2" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a>
                        </td>
			<td class="r_Cel">
                            <table  border="0" style="text-align:left;width:640px;">
                                <tr>
                                    <td style="width:320px;"><label title="{$app.master_field2_desc.1000}"><input class="CheckList2" type="checkbox" name="in_field2_1" id="in_field2_1" value="1000" {$app.data.field2_1} />{$app.master_field2.1000}</label></td>
                                    <td><label title="{$app.master_field2_desc.2000}"><input  class="CheckList2" type="checkbox" name="in_field2_2" id="in_field2_2" value="2000" {$app.data.field2_2} />{$app.master_field2.2000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.3000}"><input  class="CheckList2" type="checkbox" name="in_field2_3" id="in_field2_3" value="3000" {$app.data.field2_3} />{$app.master_field2.3000}</label></td>
                                    <td><label title="{$app.master_field2_desc.4000}"><input  class="CheckList2" type="checkbox" name="in_field2_4" id="in_field2_4" value="4000" {$app.data.field2_4} />{$app.master_field2.4000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.5000}"><input  class="CheckList2" type="checkbox" name="in_field2_5" id="in_field2_5" value="5000" {$app.data.field2_5} />{$app.master_field2.5000}</label></td>
                                    <td><label title="{$app.master_field2_desc.6000}"><input  class="CheckList2" type="checkbox" name="in_field2_6" id="in_field2_6" value="6000" {$app.data.field2_6} />{$app.master_field2.6000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.7000}"><input  class="CheckList2" type="checkbox" name="in_field2_7" id="in_field2_7" value="7000" {$app.data.field2_7} />{$app.master_field2.7000}</label></td>
                                    <td><label title="{$app.master_field2_desc.8000}"><input  class="CheckList2" type="checkbox" name="in_field2_8" id="in_field2_8" value="8000" {$app.data.field2_8} />{$app.master_field2.8000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.9000}"><input  class="CheckList2" type="checkbox" name="in_field2_9" id="in_field2_9" value="9000" {$app.data.field2_9} />{$app.master_field2.9000}</label></td>
                                    <td><label title="{$app.master_field2_desc.10000}"><input class="CheckList2" type="checkbox" name="in_field2_10" id="in_field2_10" value="10000" {$app.data.field2_10} />{$app.master_field2.10000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field2_desc.11000}"><input class="CheckList2" type="checkbox" name="in_field2_11" id="in_field2_11" value="11000" {$app.data.field2_11} />{$app.master_field2.11000}</label></td>
                                </tr>
                            </table>
                            
                            {if is_error('in_field2')}<br /><span class="error">{message name="in_field2"}</span>{/if}
                            
                            
			</td>

			
		</tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_keitai.jpg" alt="形態" />
                        <a href="javascript:void(0)" id="all_check3" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 0px 0px 0px;text-align:left;">全て選択</a>
                        <a href="javascript:void(0)" id="all_clear3" class="AllorClearButton" style="padding:5px 15px 5px 15px;margin:0px 28px 0px 0px;text-align:left;">全て解除</a>
                        </td>
			<td class="r_Cel">
                            <table  border="0" style="text-align:left;width:640px;">
                                <tr>
                                    <td style="width:320px;"><label title="{$app.master_field3_desc.1000}"><input class="CheckList3" type="checkbox" name="in_field3_1" id="in_field3_1" value="1000" {$app.data.field3_1} />{$app.master_field3.1000}</label></td>
                                    <td><label title="{$app.master_field3_desc.2000}"><input  class="CheckList3" type="checkbox" name="in_field3_2" id="in_field3_2" value="2000" {$app.data.field3_2} />{$app.master_field3.2000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.3000}"><input  class="CheckList3" type="checkbox" name="in_field3_3" id="in_field3_3" value="3000" {$app.data.field3_3} />{$app.master_field3.3000}</label></td>
                                    <td><label title="{$app.master_field3_desc.4000}"><input  class="CheckList3" type="checkbox" name="in_field3_4" id="in_field3_4" value="4000" {$app.data.field3_4} />{$app.master_field3.4000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.5000}"><input  class="CheckList3" type="checkbox" name="in_field3_5" id="in_field3_5" value="5000" {$app.data.field3_5} />{$app.master_field3.5000}</label></td>
                                    <td><label title="{$app.master_field3_desc.6000}"><input  class="CheckList3" type="checkbox" name="in_field3_6" id="in_field3_6" value="6000" {$app.data.field3_6} />{$app.master_field3.6000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.7000}"><input  class="CheckList3" type="checkbox" name="in_field3_7" id="in_field3_7" value="7000" {$app.data.field3_7} />{$app.master_field3.7000}</label></td>
                                    <td><label title="{$app.master_field3_desc.8000}"><input  class="CheckList3" type="checkbox" name="in_field3_8" id="in_field3_8" value="8000" {$app.data.field3_8} />{$app.master_field3.8000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.9000}"><input  class="CheckList3" type="checkbox" name="in_field3_9" id="in_field3_9" value="9000" {$app.data.field3_9} />{$app.master_field3.9000}</label></td>
                                    <td><label title="{$app.master_field3_desc.10000}"><input class="CheckList3" type="checkbox" name="in_field3_10" id="in_field3_10" value="10000" {$app.data.field3_10} />{$app.master_field3.10000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.11000}"><input class="CheckList3" type="checkbox" name="in_field3_11" id="in_field3_11" value="11000" {$app.data.field3_11} />{$app.master_field3.11000}</label></td>
                                    <td><label title="{$app.master_field3_desc.12000}"><input class="CheckList3" type="checkbox" name="in_field3_12" id="in_field3_12" value="12000" {$app.data.field3_12} />{$app.master_field3.12000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.13000}"><input class="CheckList3" type="checkbox" name="in_field3_13" id="in_field3_13" value="13000" {$app.data.field3_13} />{$app.master_field3.13000}</label></td>
                                    <td><label title="{$app.master_field3_desc.14000}"><input class="CheckList3" type="checkbox" name="in_field3_14" id="in_field3_14" value="14000" {$app.data.field3_14} />{$app.master_field3.14000}</label></td>
                                </tr>
                                <tr>
                                    <td><label title="{$app.master_field3_desc.15000}"><input class="CheckList3" type="checkbox" name="in_field3_15" id="in_field3_15" value="15000" {$app.data.field3_15} />{$app.master_field3.15000}</label></td>
                                </tr>
                            </table>
                            
                            {if is_error('in_field3')}<br /><span class="error">{message name="in_field3"}</span>{/if}
                            
                            
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><img src="img/midashi_keyword.jpg" alt="キーワード" /></td>
			<td colspan="2" class="r_Cel">
                            <input type="text" id="in_keyword" name="in_keyword" value="{$app.data.keyword}" size="40" maxlength="20" value="案件名などのワードを入力してください" style="font-size:20px;" />
                            <label for="keyword"></label>
                            {if is_error('in_keyword')}<br /><span class="error">{message name="in_keyword"}</span>{/if}
			</td>
		  </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td colspan="3">
				<div id="submit_btn">
					<!--<input type="image" onClick="return false;" id="f_reset" src="img/subbtn_reset.png" onMouseOver="this.src='img/subbtn_reset_d.png'" onMouseOut="this.src='img/subbtn_reset.png'" alt="リセット">-->
                                        <a class="button" href="javascript:void(0)" onclick="return false;" id="f_reset">リセット</a> 
					<!--<input type="image" onClick="javascript:do_submit_with_param('foundation');" src="img/subbtn_submit.png" onMouseOver="this.src='img/subbtn_submit_d.png'" onMouseOut="this.src='img/subbtn_submit.png'" alt="リセット">-->
                                        <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit_with_param('foundation');" id="f_search">一覧表示</a>     
				</div>
			</td>
		  </tr>

	</table>
<input type="hidden" id = "in_callkind" name="in_callkind" value="">                        
</form>
</div>

<!-- ここまで財団フォーム -->

<!-- ここからトップへ戻る -->

	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>

<!-- ここまでトップへ戻る -->

<!-- ここからセミナー -->
<!--
<div id="05"></div>

	<img src="img/title_sem_line.jpg" alt="" />
	<img src="img/title_sem.jpg" alt="セミナー・イベント" />
	<img src="img/con_sem.jpg" alt="セミナー・イベント" />
-->
<!-- ここまでセミナー -->

<!-- ここからトップへ戻る -->
<!-- 
	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>
-->
<!-- ここまでトップへ戻る -->

<!-- ここから会社概要 -->
<!--

<div id="06"></div>

	<img src="img/title_com_line.jpg" alt="" />
	<img src="img/title_com.jpg" alt="会社概要" />

<div id="company">

<div id="company_left">

	<div id="com_msg">

		<div id="navit_logo">
			<a href="http://www.navit-j.com/" target="_blank" /><img src="img/navit_logo.jpg" alt="株式会社ナビット"></a>
		</div>

		<div id="msg">
			入札なうを運営する「株式会社ナビット」は、様々なサービスを<br />提供しております。その実績をご評価いただき、テレビ他、<br />多様なメディアに取り上げていただいております。
		</div>

		<div class="clear">
		</div>
		<hr />
	</div>

	<div id="pre_msg">

		<div id="pre_photo">
			<img src="img/com_fukui.jpg" alt="福井泰代">
		</div>

		<div id="pmsg"><span style="font-size:1.5em; font-weight:bold; color:#FF4000;">のりかえ便利マップをご存じですか？</span><br />
ひとりの主婦が、小さな子供を抱えて駅で困った経験、階段やエスカレータ<br />ーがどこにあって、出口はどこなのか？重いベビーカーを押しながら思いつ<br />いた答えがのりかえ便利マップのきっかけでした。<br />その主婦がナビット代表の福井です。
		</div>

		<div class="clear">
		</div>

	</div>

</div>

	<div id="company_right">
		<img src="img/com_noriben.jpg" alt="のりかえ便利マップ">
	</div>

	<div class="clear">
	</div>

	<div id="company_bottom">
	<hr />
		<h3 style="padding:0 0 0 5px; font-size:19px; font-weight:bold; float:left;">
			株式会社ナビット
		</h3>

		<div style="padding:5px 150px 0 0; font-size:15px; line-height:15px; float:right;">
			本　　社：〒102-0074 東京都東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F<br />
	　　　　　TEL：03-5215-5713　FAX：03-5215-5702　URL：<a href="http://www.navit-j.com/" target="_blank" />http://www.navit-j.com/</a><br />
		</div>
		<div style="padding:5px 121px 0 0; font-size:15px; line-height:15px; float:right;">
			大阪支社：〒530-0001 大阪府大阪市北区梅田1丁目11番4-1100　大阪駅前第四ビル10階1-1号室<br />
	　　　　　TEL：06-4799-9201　FAX：06-4799-9011
		</div>
	</div>
	</div>
-->
<!-- ここまで会社概要 -->

<!-- ここからトップへ戻る -->
<!--
	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>
-->
<!-- ここまでトップへ戻る -->
</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">
            <ul style="list-style-type: none; display: table; margin-left: auto; margin-right: auto; font-size: 12px;">
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="http://www.navit-j.com/privacy/index.html" target="_blank">個人情報保護方針</a>
                </li>
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="regional.html">利用規約</a>
                </li>
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="attention.html">特定商取引法</a>
                </li>
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="http://www.navit-j.com/company/profile.html" target="_blank">会社概要</a>
                </li>
            </ul>          
</div>

<!-- ここまでfooter -->


</div>


<!-- スムーズスクロール -->
{literal}
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 700; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});
</script>
<!-- スムーズスクロール -->

<!-- グローバルナビ -->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->

<!-- グローバルナビ -->

<!-- googleanalytics TAG -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-12', 'auto');
  ga('send', 'pageview');

</script>
<!-- googleanalytics TAG -->
{/literal}


</body>
</html>
