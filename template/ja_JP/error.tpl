<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>FAX送信サービス</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="株式会社ナビットが提供するFAX送信サービスサイト">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="FAX,FAXDM,配信,送信,代行">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/ >

<link rel="shortcut icon" href="">
{literal} 
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script type="text/javascript" src="js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
$( document ).ready(function(){
    jQuery('#datetimepicker').datetimepicker({
        format:'Y/m/d H:i',
        inline:true, lang:'ja',
        minDate:0,
        onChangeDateTime:function(dp,$input){
                $("#datetimepicker").text($input.val());
        }
    });
});
	
//吹き出し
$('label').balloon();

function doblur() { 
    var element = document.getElementById("name"); 
    element.blur(); 
} 

function do_submit_with_param(param){
    document.f2.in_callkind.value = param;
    document.f2.submit();
}



</script>
{/literal} 
</head>
<body>
<!-- ここからconteinar -->
    <div id="conteinar">

<!-- ここからwrapper -->
        <div id="wrapper">

<!-- ここからheader -->
        <div id="01"></div>
        <div id="header">
            <div id="logo" style="width:357px;height:98px;border:1px;">
                <a href="index.php" ><img src="img/logo.jpg" alt="FAX送信代行サービス" /></a>
            </div>
            <div id="h1">
                <h1>株式会社ナビットが提供するFAX送信サービス</h1>
            </div>
            <div id="mini_contact">
                <img src="img/m_contact.gif" alt="お問合せ" />
            </div>
            <div id="mini_contact_txt">
                <a href="https://www.navit-j.com/contactus/" target="_blank">お問合せ</a>
            </div>
            <div id="freedial">
                <img src="img/freedial.jpg" alt="0120-937-781" />
            </div>
            {if $app.name !=""}
            <div style="display:inline-block;width:360px;">
                <table  border="1" cellspacing="0" cellpadding="0" style="width:100%; border-color:#f00">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <span id="logininfo">{$app.name} 様　ログイン中</span>   
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <a href="./index.php?action_logout_do=true" id="logout_btn">ログアウト</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>    
            </div>
            {else}
            <div id="newaccount">
                <a class="button3" href="./index.php?action_secure_registration=true"  id="registration">新規会員登録</a>
            </div>
            <div id="login">
                <a class="button3" href="./index.php?action_login=true" onclick="" id="login_btn">ログイン</a>
            </div>
            {/if}
        </div>
        <div class="clear"></div>
    <!-- ここまでheader -->

    <!-- ここからG NAVI -->
        <div id="gnavi">
            <ul>
                <li style="height:82px;">
                    <a href="#01"><img src="img/gnavi01.jpg" alt="ホーム" width="201px" height="82px"></a>
                </li>
                <li style="height:82px;">
                    <a href="lp.html" target="_blank"><img src="img/gnavi02.jpg" onmouseover="this.src='img/gnavi02.jpg'" onmouseout="this.src='img/gnavi02.jpg'" /></a>
                </li>
                <li style="height:82px;">
                    <a href="#03"><input type="image" onClick="" src="img/gnavi03.jpg" onMouseOver="this.src='img/gnavi03.jpg'" onMouseOut="this.src='img/gnavi03.jpg'" alt="" width="200px" height="82px"></a>
                </li>
                <li style="height:82px;">
                    <a href="#04"><input type="image" onClick="" src="img/gnavi04.jpg" onMouseOver="this.src='img/gnavi04.jpg'" onMouseOut="this.src='img/gnavi04.jpg'" alt="" width="200px" height="82px"></a>
                </li>
                <li style="height:82px;">
                    <a href="http://www.navit-j.com/press/sem150203.html" target="_blank"><img src="img/gnavi05.jpg" onmouseover="this.src='img/gnavi05.jpg'" onmouseout="this.src='img/gnavi05.jpg'" alt="" width="200px" height="82px"></a>
                </li>
            </ul>
        </div>
        <div class="clear"></div>
    <!-- ここまでG NAVI -->

    <!-- ここからメインビジュアル -->
        <img src="img/main_vis01.jpg" alt="FAX送信代行サービスの特徴" border="0" />
    <!-- ここまでメインビジュアル -->

    <!-- ここからメインコンテンツ -->
        <div id="03"></div>
        <div id="contents">
            <hr></hr>
            <br />
            <div class="head_title_bgcolor" style="text-align:left;">お知らせ／メッセージ</div>
            <br />
            <div style="margin-left: 50px;">エラーが発生しました。<br />
            </div>
    <!-- ここからトップへ戻る -->
            <div class="re_top">
                <a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
            </div>
            <div class="clear"></div>
    <!-- ここまでトップへ戻る -->
            <input type="hidden" id = "in_callkind" name="in_callkind" value="">                        
<!-- ここまで財団フォーム -->
        </div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
        <div id="footer">
            <ul style="list-style-type: none; display: table; margin-left: auto; margin-right: auto; font-size: 12px;">
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="http://www.navit-j.com/privacy/index.html" target="_blank">個人情報保護方針</a>
                </li>
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="#">利用規約</a>
                </li>
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="#">特定商取引法</a>
                </li>
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="http://www.navit-j.com/company/profile.html" target="_blank">会社概要</a>
                </li>
            </ul>          
        </div>
<!-- ここまでfooter -->
    </div>


<!-- スムーズスクロール -->
{literal}
    <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->
    <script>
    $(function(){
       // #で始まるアンカーをクリックした場合に処理
       $('a[href^=#]').click(function() {
          // スクロールの速度
          var speed = 700; // ミリ秒
          // アンカーの値取得
          var href= $(this).attr("href");
          // 移動先を取得
          var target = $(href == "#" || href == "" ? 'html' : href);
          // 移動先を数値で取得
          var position = target.offset().top;
          // スムーススクロール
          $('body,html').animate({scrollTop:position}, speed, 'swing');
          return false;
       });
    });
    </script>
<!-- スムーズスクロール -->

<!-- グローバルナビ -->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->

<!-- グローバルナビ -->

<!-- googleanalytics TAG -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-45128510-12', 'auto');
      ga('send', 'pageview');

    </script>
<!-- googleanalytics TAG -->
{/literal}


</body>
</html>