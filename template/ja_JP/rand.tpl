{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<style type="text/css">
.pass {
	text-align: center;
}
</style>
{/literal}
</head>
<body>


<!--エラーメッセージ-->
{if count($errors)}
<br />
<table class="error" align="center">
{foreach from=$errors item=error}
<tr class="error"><td>{$error}</td></tr>
{/foreach}
</table>
<br />
{/if}
<!--エラーメッセージ-->

<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->


<!--フッター-->
{$app_ne.footer}
<!--フッター-->

</body>
</html>
