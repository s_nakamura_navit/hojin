<link rel="stylesheet" href="./css/ditail.css" type="text/css">

<!-- ここからログイン制限ダイアログ -->
<div id="forced_login_dialog" style="display: none;">
    <div style="width:100%;background-color: #ffffff;">
        <img src="./img/logo.jpg" alt="法人番号検索" style="width:178px;height:48px;"  />
        <div style="width:24px;">
            <a href="javascript:void(0);" style="position:relative; top:-60px; left:540px;"><img id="forced_login_dialog_close" src="img/close_round.png" width="20px" height="20px" alt="閉じる" /></a>
        </div>
    </div>
    <hr />
    <div id="submit_btn">
	<form action="{$script}" name="forced" method="POST" id="forced">
	<input type="hidden" name="action_login_do" value="true" />
	<input type="hidden" name="in_act" value="overlap" />
	<input type="hidden" name="in_id" value="{$app.data.id}" />
	<input type="hidden" name="in_pw" value="{$app.data.pw}" />
	</form>
        <span style="font-size:20px;color:#3A2409;">他のサービスでログイン中の可能性があります。<br />強制ログインを行いますか？</span><br />
        <span style="font-size:14px;color:#3A2409;">※「強制ログイン」は、先のログインを強制的に切断します。</span><br /><br />
	<a class="button5_en" href="javascript:void(0)"  id="get_list_close">戻る</a>&nbsp;&nbsp;
        <a class="button5" href="javascript:void(0)"  id="registration" style="width:250px;" onclick="document.forced.submit();" >強制ログイン</a>
    </div>
</div>
<!-- ここまでログイン制限ダイアログ -->

{if $app.act == "overlap"}
{literal}
<script type="text/javascript">
     window.onload = function(){
         $("#forced_login_dialog").lightbox_me({centered: true,closeSelector:'#forced_login_dialog_close',overlayCSS:{background:'#D3C9BA',opacity: .8}});
     }
</script>
{/literal}
{/if}
