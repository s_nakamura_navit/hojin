<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>助成金・補助金検索結果一覧｜助成金・補助金の検索サービス「助成金なう」</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="自治体、財団から公示される助成金、補助金の情報検索サービス「助成金なう」の助成金・補助金検索結果一覧">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="助成金,補助金,財団,セミナー,助成金なう自治体案件,財団案件">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="stylesheet" href="css/first.css" type="text/css">
<link rel="shortcut icon" href="">
{literal}
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>

<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script>
    $(window).load(function(){
      $("#gnavi").sticky({ topSpacing: 0, center:true, className:"hey" });
    });
</script>

<script type="text/javascript">
function do_submit_with_param(contents_id){
    document.f2.in_contents_id.value = contents_id;
    document.f2.submit();
}
</script>
{/literal}
</head>

<body>

<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
	<div id="wrapper">

<!-- ここからheader --><div id="01"></div>
		<div id="header">

			<div id="logo">
				<a href="index.php" ><img src="img/logo.jpg" alt="助成金なう" /></a>
			</div>

			<div id="h1">
				<h1>助成金・補助金の検索サービス「助成金なう」</h1>
			</div>

			<div id="mini_contact">
				<img src="img/m_contact.gif" alt="お問合せ" />
			</div>

			<div id="mini_contact_txt">
				<a href="https://www.navit-j.com/contactus/" target="_blank">お問合せ</a>
			</div>

			<div id="mini_sitemap">
				<img src="img/m_sitemap.gif" alt="サイトマップ" />
			</div>

			<div id="mini_sitemap_txt">
				<a href="#">サイトマップ</a>
			</div>

			<div id="freedial">
				<img src="img/freedial.jpg" alt="0120-937-781" />
			</div>

			{if $app.name !=""}
                        <div style="display:inline-block;width:360px;">
                            <table  border="1" cellspacing="0" cellpadding="0" style="width:100%; border-color:#f00">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <span id="logininfo">{$app.name} 様　ログイン中</span>   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <a href="./index.php?action_logout_do=true" id="logout_btn">ログアウト</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>    
                        </div>
                    {else}
                        <div id="newaccount">
                            <a class="button3" href="./index.php?action_secure_registration=true"  id="registration">新規会員登録</a>
                        </div>
                        <div id="login">
                            <a class="button3" href="./index.php?action_login=true" onclick="" id="login_btn">ログイン</a>
                        </div>
                    {/if}

		</div>

		<div class="clear">
		</div>

<!-- ここまでheader -->

<!-- ここからG NAVI -->


<div id="gnavi">
	<ul>
		<li style="height:93px;">
			<a href="#01"><img src="img/gnavi_home.jpg" alt="ホーム" width="143px" height="94px"></a>
		</li>
		<li style="height:93px;">
			<a href="lp.html" target="_blank"><img src="img/gnavi_news.jpg" onmouseover="this.src='img/gnavi_news_d.jpg'" onmouseout="this.src='img/gnavi_news.jpg'" /></a>
		</li>
		<li style="height:93px;">
			<a href="index.html#03"><input type="image" onClick="" src="img/gnavi_a_search.jpg" onMouseOver="this.src='img/gnavi_a_search_d.jpg'" onMouseOut="this.src='img/gnavi_a_search.jpg'" alt="自治体案件検索" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="index.html#04"><input type="image" onClick="" src="img/gnavi_f_search.jpg" onMouseOver="this.src='img/gnavi_f_search_d.jpg'" onMouseOut="this.src='img/gnavi_f_search.jpg'" alt="財団案件検索" width="142px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="http://www.navit-j.com/press/sem150203.html" target="_blank"><img src="img/gnavi_sem.jpg" onmouseover="this.src='img/gnavi_sem_d.jpg'" onmouseout="this.src='img/gnavi_sem.jpg'" alt="セミナー・イベント" width="143px" height="93px"></a>
		</li>
		<li style="height:93px;">
			<a href="http://www.navit-j.com/" target="_blank"><img src="img/gnavi_com.jpg" onmouseover="this.src='img/gnavi_com_d.jpg'" onmouseout="this.src='img/gnavi_com.jpg'" /></a>
		</li>
		<li style="height:93px;">
			<a href="http://www.navit-j.com/press/info.html" target="_blank"><img src="img/gnavi_ccontact.jpg" onmouseover="this.src='img/gnavi_ccontact_d.jpg'" onmouseout="this.src='img/gnavi_ccontact.jpg'" /></a>
		</li>
	</ul>

</div>
		<div class="clear">
		</div>

<!-- ここまでG NAVI -->


<!-- ここからメインコンテンツ -->
<!-- ここから検索結果 -->

<img src="img/title_search_result.jpg" alt="案件検索結果" />
<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_detail" value="true">
<div id="searchresult">

    {if $app.data.callkind == "autonomy"}
        <!-- 自治体案件検索結果 -->
        {if $app.result_count > 0}

            <!-- ３案件ごとに改行 -->
            {assign var="count" value=1}
            {foreach from=$app.result_data key=k item=v}
                    <div class="box_left">
                        <div class="matter_title">
                                {$v.main_title}
                        </div>
                        <div class="matter_ditail">
                                {$v.comments}
                        </div>
                        <div class="ditail_btn">
                                <a href="javascript:void(0);"><input type="image" onClick="javascript:do_submit_with_param('{$v.contents_org_id}');" src="img/result_btn.png" onMouseOver="this.src='img/result_btn_d.png'" onMouseOut="this.src='img/result_btn.png'" alt="詳細はこちら"></a>
                        </div>
                    </div>
                    {if $count % 3 == 0}<!-- ３案件ごとに改行 -->
                        <div class="clear">
                        </div>   
                    {/if}   
                    {assign var="count" value=$count+1}
                    {if $app.rank == '0'}
                        {if $count > 10}
                            {php}break;{/php}
                        {/if}
                    {/if}
            {/foreach}

        {else}
            <br />
            <div style="text-align:center;">対象の案件はありません。</div>
            <br />
        {/if}
            
    {else}
        <!-- 財団案件検索結果 -->
        {if $app.result_count > 0}

            <!-- ３案件ごとに改行 -->
            {assign var="count" value=1}
            {foreach from=$app.result_data key=k item=v}
                    <div class="box_left">
                        <div class="matter_title">
                                {$v.main_title}
                        </div>
                        <div class="matter_ditail">
                                {$v.comments}
                        </div>
                        <div class="ditail_btn">
                                <a href="javascript:void(0);"><input type="image" onClick="javascript:do_submit_with_param('{$v.contents_org_id}');" src="img/result_btn.png" onMouseOver="this.src='img/result_btn_d.png'" onMouseOut="this.src='img/result_btn.png'" alt="詳細はこちら"></a>
                        </div>
                    </div>
                    {if $count % 3 == 0}<!-- ３案件ごとに改行 -->
                        <div class="clear">
                        </div>   
                    {/if}   
                    {assign var="count" value=$count+1}
                    {if $app.rank == '0'}
                        {if $count > 10}
                            {php}break;{/php}
                        {/if}
                    {/if}
            {/foreach}

        {else}
            <br />
            <div style="text-align:center;">対象の案件はありません。</div>
            <br />
        {/if}
        
    {/if}

<!-- ここからトップへ戻る -->

	<div class="re_top_first">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>
<br />
<span style="font-size:70%;margin-left:20px;">※無料会員版は一度に表示できる件数を10件までとさせていただいております</span>
<!-- ここまでトップへ戻る -->

<!-- ここから会社概要 -->
<!--
	<div id="com_info">
	<hr />
		<h3 style="padding:0 0 0 5px; font-size:19px; font-weight:bold; float:left;">
			株式会社ナビット
		</h3>

		<div style="padding:5px 150px 0 0; font-size:15px; line-height:15px; float:right;">
			本　　社：〒102-0074 東京都東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F<br />
	　　　　　TEL：03-5215-5713　FAX：03-5215-5702　URL：<a href="http://www.navit-j.com/" target="_blank" />http://www.navit-j.com/</a><br />
		</div>
		<div style="padding:5px 121px 0 0; font-size:15px; line-height:15px; float:right;">
			大阪支社：〒530-0001 大阪府大阪市北区梅田1丁目11番4-1100　大阪駅前第四ビル10階1-1号室<br />
	　　　　　TEL：06-4799-9201　FAX：06-4799-9011
		</div>
	</div>
	</div>
-->
<!-- ここまで会社概要 -->
<input type="hidden" id = "in_callkind" name="in_callkind" value="{$app.data.callkind}">
<input type="hidden" id = "in_contents_id" name="in_contents_id" value="{$app.data.contents_id}">
</form>
</div>
<!-- ここまでメインコンテンツ -->




</div>

<!-- ここからfooter -->
<br />
<div id="footer">
	
</div>

<!-- ここまでfooter -->




{literal}
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 700; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});
</script>

<!-- googleanalytics TAG -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-12', 'auto');
  ga('send', 'pageview');

</script>
<!-- googleanalytics TAG -->
{/literal}


</body>
</html>
  
