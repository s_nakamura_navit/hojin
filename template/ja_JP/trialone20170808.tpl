{if $app.corp_detail != ""}

<table class="result_table2">
	<tr>
		<th class="result_table_th2">
			企業名
		</th>
		<th class="result_table_th2">
			所在地
		</th>
	</tr>
	<tr>
		<td>
			{$app.corp_detail.I_CORP}
		</td>
		<td>
			{$app.corp_detail.I_ADDR_ALL}
		</td>
	</tr>
</table>

<div class="search_result2">
			<b>付与属性項目の有無</b>
</div>

<table class="result_table3">

	<tr>
		<th class="result_table_th2" width="50%">
			業種
		</th>
		<td class="result_table_td2" width="50%">
			{$app.corp_detail.I_BUSINESS_CD_L_1}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			FAX番号
		</th>
		<td class="result_table_td2">
			{$app.corp_detail.I_FAX}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			上場区分
		</th>
		<td class="result_table_td2">
			{$app.corp_detail.I_LISTED_TYPE}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			従業員数規模
		</th>
		<td class="result_table_td2">
			{$app.corp_detail.I_EMPLOYEE_DIV}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			売上高規模
		</th>
		<td class="result_table_td2">
			{$app.corp_detail.I_SALE_DIV}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			資本金規模
		</th>
		<td class="result_table_td2">
			{$app.corp_detail.I_CAPITAL_DIV}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			設立年月
		</th>
		<td class="result_table_td2">
			{$app.corp_detail.I_FOUNDING_DT}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			URL
		</th>
		<td class="result_table_td2">
			{$app.corp_detail.I_URL}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			法人番号（マイナンバー）
		</th>
		<td class="result_table_td2">
			{$app.corp_detail.I_CORP_NUM}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			メールアドレス
		</th>
		<td class="result_table_td2">
			{$app.corp_detail.I_MAILADDR}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			金額
		</th>
		<td class="result_table_td2">
		{$app.corp_detail.total}ポイント [ &yen;{$app.corp_detail.total}(税込) ]
		</td>
	</tr>
</table>

<form action="{$script}" name="f_back" method="POST" id="f_back" enctype="multipart/form-data">
    <input type="hidden" name="action_index" value="ture" />
    <input type="hidden" name="sc_name" value="{$app.sc_name}" />
    <input type="hidden" name="sc_address" value="{$app.sc_address}" />
    <input type="hidden" name="in_act" value="search_corp" />
</form>
<form action="{$script}" name="f_get" method="POST" id="f_get" enctype="multipart/form-data">
    {if $app.name != ""}
		<input type="hidden" name="action_trialoneconf" value="true" />
		<input type="hidden" name="corp_id" value="{$app.corp_detail.ID}" />
		{uniqid}
		<input type="hidden" name="sc_name" value="{$app.sc_name}" />
    	<input type="hidden" name="sc_address" value="{$app.sc_address}" />
    	<input type="hidden" name="pref_cd" value="{$app.pref_cd}">
		<input type="hidden" name="city_cd" value="{$app.city_cd}">
    	<input type="hidden" name="in_act" value="trialoneconf" />
    {else}
		<input type="hidden" name="action_secure_registration" value="true" />
    {/if}
</form>

	<div class="search_btn">
		<a class="button" href="javascript:void(0)" onclick="document.f_back.submit();" id="back_button">戻る</a>
				    &nbsp;&nbsp;&nbsp;&nbsp;
		{if $app.corp_detail.total > 0}
		<a class="button" href="javascript:void(0)" onclick="document.f_get.submit();" id="a_search">購入する</a>
		{/if}
	</div>
{/if}

