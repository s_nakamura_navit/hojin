{literal}
<!--
<script type="text/javascript" src="js/jquery.loading.js"></script>
-->
{/literal}

{literal}
<script type="text/javascript">
	function header_line(){
		check = document.f_get.in_header_check.checked;
		if(check == true){
			document.getElementById("disp").style.display = "block";
		}else{
			document.getElementById("disp").style.display = "none";
		}
	}

//参照ボタンデザイン変更
$(function(){
    fileUploader();
});
var fileUploader = function(){
    var target = $('.fileUploder');

    target.each(function(){
        var txt = $(this).find('.txt');
        var btn = $(this).find('.btn');
        var uploader = $(this).find('.uploader');

        uploader.bind('change',function(){
            txt.val($(this).val());
        });

        btn.bind('click',function(event){
            event.preventDefault();
            return false;
        });

        $(this).bind('mouseover',function(){
            btn.css('background-position','0 100%');
        });
        $(this).bind('mouseout',function(){
            btn.css('background-position','0 0');
        });

    });
}

function header_num_close(){
	document.getElementById("auto_disp").style.display = "none";
	document.getElementById("action_confirmation").disabled = false;
	document.getElementById("action_commission_registrationauto").disabled = true;
	document.getElementById("in_auto_check").value = 'off';
	document.getElementById("manual_order_button").style.display = "block";
	document.getElementById("manual_order_button_n").style.display = "none";
	document.getElementById("auto_order_button").style.display = "none";
	document.getElementById("auto_order_button_n").style.display = "block";
}
function header_num_view(){
	document.getElementById("auto_disp").style.display = "block";
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = false;
	document.getElementById("in_auto_check").value = 'on';
	document.getElementById("manual_order_button").style.display = "none";
	document.getElementById("manual_order_button_n").style.display = "block";
	document.getElementById("auto_order_button").style.display = "block";
	document.getElementById("auto_order_button_n").style.display = "none";
}

{/literal}{if $app.auto_check == "off" || $app.auto_check == ""}{literal}
window.onload = function(){
	document.getElementById("auto_disp").style.display = "none";
	document.getElementById("action_confirmation").disabled = false;
	document.getElementById("action_commission_registrationauto").disabled = true;
	document.getElementById("in_auto_check").value = 'off';
	document.getElementById("manual_order_button").style.display = "block";
	document.getElementById("manual_order_button_n").style.display = "none";
	document.getElementById("auto_order_button").style.display = "none";
	document.getElementById("auto_order_button_n").style.display = "block";
};
{/literal}{else if $app.auto_check == "on"}{literal}
window.onload = function(){
	document.getElementById("auto_disp").style.display = "block";
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = false;
	document.getElementById("in_auto_check").value = 'on';
	document.getElementById("manual_order_button").style.display = "none";
	document.getElementById("manual_order_button_n").style.display = "block";
	document.getElementById("auto_order_button").style.display = "block";
	document.getElementById("auto_order_button_n").style.display = "none";
};
{/literal}{/if}{literal}

function do_trial(){
	document.getElementById("action_trial").disabled = false;
	document.getElementById("in_act").disabled = false;
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = true;
	document.f_get.submit();
}
function do_search_corp(){
	document.s_corp.submit();
}


window.onload = function () {
	if ($("#start_search").val()){
		var $target = $('#search_corp');
		var pos = $target.offset().top - 10;
		$("html, body").animate({scrollTop:pos}, 400);//スクロールスピード
	}

 	document.getElementById("auto_disp").style.display = "block";
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = false;
	document.getElementById("in_auto_check").value = 'on';
/*
	document.getElementById("manual_order_button").style.display = "none";
	document.getElementById("manual_order_button_n").style.display = "block";
	document.getElementById("auto_order_button").style.display = "block";
	document.getElementById("auto_order_button_n").style.display = "none";
*/	
	if (getParam() == "1d12NGnK" || $("#id_allcd").val() == "1d12NGnK") {
		$("#bulk_grant").css("display", "inline");
	}
};
function display_bulk_grant(){
	if ($("#bulk_grant").css("display") == "none"){
		$("#bulk_grant").css("display", "inline");
	}else{
		$("#bulk_grant").css("display", "none");
	}
}

function getParam() {
	var url   = location.href;
	parameters    = url.split("?");
	
	if (parameters[1]){
		params   = parameters[1].split("&");
		var paramsArray = [];
		for ( i = 0; i < params.length; i++ ) {
			neet = params[i].split("=");
			paramsArray.push(neet[0]);
			paramsArray[neet[0]] = neet[1];
		}
		var categoryKey = paramsArray["allcd"];
		return categoryKey;
	}else{
		return "";
	}
}
</script>
{/literal}

<!------------------------------------------------------------------------------>

<div align="center" style="margin:20px 0 0px 0;font-size:11px;">

<a href="https://www.navit-j.com">データ・リストの販売、調査代行ならナビット</a> > 法人番号から業種・売上高・従業員数など企業情報取得なら法人番号検索
</div>

<p style="font-size:12px; text-align:center; margin:5px auto;">
法人番号又は企業名から業種・売上高・従業員数などの企業情報取得なら、ナビットの「法人番号検索」にお任せください。<br />
法人番号か企業名を入れて検索すると、法人番号、業種、FAX番号、上場区分、従業員数規模、売上高規模、<br />資本金規模、設立年月、URL、メールアドレス、電話番号まで企業情報を取得することができます。
</p>
<!--<hr color="#017680">-->


        <!-- ここからバナー 
<div style="margin:0 auto 0 auto;text-align:center;">
	<a href="https://www.navit-j.com/blog/?p=24464" target="_blank"><img src="https://www.navit-j.com/service/openkun/img/0nichikoushin_bnr.jpg" onmouseover="this.src='https://www.navit-j.com/service/openkun/img/0nichikoushin_bnr_d.jpg'" onmouseout="this.src='https://www.navit-j.com/service/openkun/img/0nichikoushin_bnr.jpg'" alt="0日更新記念キャンペーン" width="800px"></a></div>
         ここまでバナー -->

<!--<div class="input_demo">
<div style="margin:30px auto 0;text-align:center;">
<a href="https://youtu.be/SlCrFajv_oI" data-lity="data-lity"><img src="img/movie_bnr.gif" onmouseover="this.src='img/movie_bnr_d.gif'" onmouseout="this.src='img/movie_bnr.gif'" alt="法人番号検索サービス動画" border="0"></a>
</div>-->



<!--<div style="float:left; margin:0 0 0 130px; padding:15px;">
 <span style="font-size:30px; font-weight:bold; color:#F00;">NEW!</span>
<img src="img/new.png" />
</div>-->

<form action="{$script}" name="s_corp" method="POST" id="search_corp">
<input type="hidden" name="in_act" value="search_corp">
<input type="hidden" name="allcd" value="{$smarty.cookies.opensite_alliance}" id="id_allcd">
<input type="hidden" name="start_search" value="{$app.start_search}" id="start_search">
	<table class="input_table" style="width:100%;height:100px;clear:both;margin-top:0px;margin:20px 0 30px 0;background-color:#008246; border-radius: 10px; -webkit-border-radius: 10px; -moz-border-radius: 10px;">
		<tr>
			<td class="input_td" style="color:#fff;text-align:right font-size:80%;padding:20px;">法人番号<span style="font-size:60%; color:#fff;">又は</span><br>企業名</td>
			<td>
				<input type="text" name="sc_name" value="{$app.sc_name}" style="width:280px;" /><br /><span style="font-size:55%; color:#FFF;">株式会社や（株）のような法人格は入力しないでください。</span>
			</td>
			<td class="input_td"  align="right" style="color:#fff;padding-right:30px;">所在地<br><span style="font-size:60%; color:#fff;">あれば</span></td>
			<td>
				<input type="text" name="sc_address" value="{$app.sc_address}" placeholder="東京都、東京都千代田区など" style="width:280px;" />
			</td>
		</tr>
		<!--<tr>
			<td colspan=2 style="text-align:center;"><span style="margin:0 0 0 0;font-size:12px;color:#fff;font-weight:bold;">※(株)などの省略表記ではなく、株式会社と入れてください。</span></td>
			<td colspan=2 style="text-align:center;"><span style="margin:0 0 0 0;font-size:12px;color:#fff;font-weight:bold;">※都道府県名、もしくは都道府県名＋市区町村名を１つ入れて下さい。</span></td>
		</tr>-->

		{if isset($app.search_check_err) || isset($app.search_check_err_address)}
		<tr>
			<td colspan=4 align="center">
				{if isset($app.search_check_err)}<br />
				<span class="error">{$app.search_check_err}</span>
				{/if}
				{if isset($app.search_check_err_address)}
				<span class="error">　{$app.search_check_err_address}</span>
				{/if}
			</td>
		</tr>
		{/if}
	</table>
<div class="input_demo">
<!--<hr />-->

	<div class="search_btn" style="text-align:center;margin:15px auto 15px auto;">
		<a class="button" style="background-color:#F55700; border:2px solid #F55700;" href="javascript:void(0);" id="search_corp_button">検索</a>
	</div>
</form>

<div style="margin:30px auto 0;text-align:center;">
<!--<a href="https://youtu.be/SlCrFajv_oI" data-lity="data-lity"><img src="img/movie_bnr.gif" onmouseover="this.src='img/movie_bnr_d.gif'" onmouseout="this.src='img/movie_bnr.gif'" alt="法人番号検索サービス動画" border="0"></a>-->
</div>


	<div class="search_result" align="center">
			{if $app.result_corp != ""}
			<span style="font-size:25px; font-weight-bold; color:#CE4C51;">{$app.sc_name}</span>の検索結果　<span style="font-size:25px; font-weight-bold; color:#CE4C51;">{$app.result_corp_cnt.sc_cnt}社</span>ヒットしました。
			<br />
			<span style="font-size:18px; color:#F00;">
				掲載名をクリックする事で詳細をご確認いただけます。
			</span>
	</div>

	<table class="result_table">
		<tr>
			<th class="result_table_th" width="70%">
				掲載名
			</th>
			<th class="result_table_th" width="30%">
				所在地
			</th>
		</tr>


		{foreach from=$app.result_corp key=k item=v}
		<form action="{$script}" name="f_{$v.corp_id}" method="POST" id="trial_v2" target="_blank">
		<input type="hidden" id="action_trialone" name="action_trialone" value="true"/>
		<input type="hidden" id="in_act" name="in_act" value="trialone"/>
		<input type="hidden" name="corp_id" value="{$v.corp_id}">
		<input type="hidden" name="sc_name" value="{$app.sc_name}">
		<input type="hidden" name="sc_address" value="{$app.sc_address}">
		<input type="hidden" name="pref_cd" value="{$app.pref_code}">
		<input type="hidden" name="city_cd" value="{$app.city_code}">
		<input type="hidden" name="hit_type" value="{$app.hit_type}">

		<tr>
			<td class="result_table_td" width="70%">
				<img src="img/link_point.png" />
				<a href="javaScript:document.f_{$v.corp_id}.submit();">
					{$v.corp_name}
				</a>
			</td>
			<td class="result_table_td" width="30%">
				<a href="javaScript:document.f_{$v.corp_id}.submit();">
					{$v.pref} {$v.corp_city}
				</a>
			</td>
		</tr>


		</form>
{/foreach}
	</table>
{/if}

<div style="margin:15px 0;">
{if $app.search_none != ""}
<span style="font-size:25px; font-weight-bold; color:#CE4C51;">{$app.sc_name}</span>の検索結果　みつかりませんでした。
{/if}
</div>


<div class="search_result" align="center">
	<a href="https://www.navit-j.com/blog/?p=29962">
		<span style="font-size:25px; font-weight:bold; color:#555;text-decoration:underline;">使い方はこちら</span>
	</a>
</div>




<!------------------------------------------------------------------------------>



<form action="{$script}" name="f_get" method="POST" id="f_get" enctype="multipart/form-data">
<input type="hidden" id="action_trial" name="action_trial" value="true" disabled />
<input type="hidden" id="in_act" name="in_act" value="furufuru_trial" disabled />


<!--
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">法人番号検索サービスサービス&nbsp;ご利用タイプの選択</div>
<br />


<div style="float:left; margin:20px 0 30px 175px;">
<img id="manual_order_button" style="display:block;" onmouseover="this.src='img/btn_service_select01_d.png'" onmouseout="this.src='img/btn_service_select01.png'" alt="手動版で抽出（中2日でお戻し）" src="img/btn_service_select01.png" onclick="header_num_close();">
<img id="manual_order_button_n" alt="手動版で抽出（中2日でお戻し）" src="img/btn_service_select01_n.png" style="display:none;" onclick="header_num_close();">
</div>

<div style="float:right; margin:20px 175px 30px 0px;">
<img id="auto_order_button" style="display:none;" onmouseover="this.src='img/btn_service_select02_d.png'" onmouseout="this.src='img/btn_service_select02.png'" alt="全自動で抽出（オンライン対応）" src="img/btn_service_select02.png" onclick="header_num_view();">
<img id="auto_order_button_n" alt="全自動で抽出（オンライン対応）" src="img/btn_service_select02_n.png" style="display:block;" onclick="header_num_view();">
</div>

-->







<div style="clear:both;"></div>

<div id="03"></div>

<script type="text/javascript" src="js/jquery.loading.js"></script>
<div id="04"></div>

<a name="service"></a>
<div id="bulk_grant" style="display:none;">



<div class="table">
        <input type="hidden" id="action_confirmation" name="action_confirmation" value="true" {if $app.auto_check != ""}disabled{/if}/>
        <input type="hidden" id="action_commission_registrationauto" name="action_commission_registrationauto" value="true" {if $app.auto_check == ""}disabled{/if} />
	<input id="in_auto_check" name="in_auto_check" type="hidden" value="" />
        <table border="0" cellpadding="5" cellspacing="1">
            <tr>
                <td class="l_Cel" style="vertical-align:top;">
                    <div class="middle_midashi">顧客属性付与先リスト</div>
                </td>
                <td colspan="2" class="r_Cel">
                    <p class="note">
                        属性を付与したいデータの記載されたファイルをご指定ください。<br />
                        <a href="ex_furu_article.html" target="_blank" style="text-decoration: underline;font-size:16px;"><span style="font-size:16px;">※ファイル形式：CSV（.csv）ファイル</span></a><br />
		    </p>
                    <div class="fileUploder">
                        <input type="text" class="txt" readonly>
                        <button class="btn">参照</button>
                        <input type="file" class="uploader" name="in_up_list">
                    </div>
                    {if $app.is_HTflg == false}
                    <p class="note">
			ヘッダー(タイトル行)あり<input type="checkbox" name="in_header_check" value="on" style="margin:0 20px;vertical-align:-5px;" onclick="header_line();" {if $app.header_check != ""}checked{/if} >
			<span id="disp" style="display:{if $app.header_check != ""}block{else}none{/if};">ヘッダーの行数<input type="text" name="in_header_line" {if $app.header_line != ""}value="{$app.header_line}"{else}value="1"{/if} style="margin:0 20px;width:50px;vertical-align:3px;"></span>
		    </p>
                    <p class="note">
                    {if $app.up_list_err != ""}<br /><span class="error">{$app.up_list_err}</span>{/if}
                    {if $app.row_err}<br /><span style="font-size:12pt;">（<a href="./index.php?action_inquiry_registration=true">2000件を超える場合はお問合せください</a>）</span>{/if}
                    {if $app.tel_value_line_err != ""}<br /><span class="error">{$app.tel_value_line_err}</span>{/if}
                    {if $app.header_line_err != ""}<br /><span class="error">{$app.header_line_err}</span>{/if}
		    </p>

		    <table id="auto_disp" style="display:{if $app.auto_check == "on"}block{else}none{/if};">
			<tr><td class="note" colspan="6" style="padding-bottom:10px;">列を指定して下さい。</td></tr>
			<tr>
			<td class="note" style="text-align:right;">会社名&nbsp;</td>
			<td class="note" style="padding-right:20px;"><input type="text" style="width:30px;" name="in_name_line_num" value="{$app.name_line_num}" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">郵便番号&nbsp;</td>
			<td class="note" style="padding-right:20px;"><input type="text" style="width:30px;" name="in_zipcode_line_num" value="{$app.zipcode_line_num}" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">住所&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_address_line_num" value="{$app.address_line_num}" />&nbsp;列目</td>
			</tr>
			<tr>
			<td class="note" style="text-align:right;">電話番号&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_tel_line_num" value="{$app.tel_line_num}" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">URL&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_I_URL_line_num" value="{$app.I_URL_line_num}" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">メールアドレス&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_I_MAILADDR_line_num" value="{$app.I_MAILADDR_line_num}" />&nbsp;列目</td>
			</tr>
		    </table>
		    	{else}
                    <p class="note">
			ヘッダー(タイトル行)あり<input type="checkbox" name="in_header_check" value="on" style="margin:0 20px;vertical-align:-5px;" onclick="header_line();" checked disabled="disabled" >

			<span id="disp" style="display: block;">ヘッダーの行数<input type="text" name="in_header_line" value="1" style="margin:0 20px;width:50px;vertical-align:3px;" disabled="disabled" ></span>
		    </p>
                    <p class="note">
                    {if $app.up_list_err != ""}<br /><span class="error">{$app.up_list_err}</span>{/if}
                    {if $app.row_err}<br /><span style="font-size:12pt;">（<a href="./index.php?action_inquiry_registration=true">2000件を超える場合はお問合せください</a>）</span>{/if}
                    {if $app.tel_value_line_err != ""}<br /><span class="error">{$app.tel_value_line_err}</span>{/if}
                    {if $app.header_line_err != ""}<br /><span class="error">{$app.header_line_err}</span>{/if}
		    </p>

		    <table id="auto_disp" style="display:{if $app.auto_check == "on"}block{else}none{/if};">
			<tr><td class="note" colspan="6" style="padding-bottom:10px;">列を指定して下さい。</td></tr>
			<tr>
			<td class="note" style="text-align:right;">会社名&nbsp;</td>
			<td class="note" style="padding-right:20px;"><input type="text" style="width:30px;" name="in_name_line_num" value="2"  disabled="disabled" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">郵便番号&nbsp;</td>
			<td class="note" style="padding-right:20px;"><input type="text" style="width:30px;" name="in_zipcode_line_num" value="3" disabled="disabled"  />&nbsp;列目</td>
			<td class="note" style="text-align:right;">住所&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_address_line_num" value="4"  disabled="disabled" />&nbsp;列目</td>
			</tr>
			<tr>
			<td class="note" style="text-align:right;">電話番号&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_tel_line_num" value="5" disabled="disabled" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">URL&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_I_URL_line_num" value="6"  disabled="disabled" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">メールアドレス&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_I_MAILADDR_line_num" value="{$app.I_MAILADDR_line_num}"  disabled="disabled" />&nbsp;列目</td>
			</tr>
		    </table>

		    <input type="hidden" name="in_header_check" value="on">
		    <input type="hidden" name="in_header_line" value="1">
		    <input type="hidden" name="in_name_line_num" value="2">
		    <input type="hidden" name="in_zipcode_line_num" value="3">
		    <input type="hidden" name="in_address_line_num" value="4">
		    <input type="hidden" name="in_tel_line_num" value="5">
		    <input type="hidden" name="in_I_URL_line_num" value="6">

		    	{/if}

                    <p class="note">
                    {if $app.name_line_num_err != ""}<br /><span class="error">{$app.name_line_num_err}</span>{/if}
                    {if $app.address_line_num_err != ""}<br /><span class="error">{$app.address_line_num_err}</span>{/if}
                    {if $app.tel_line_num_err != ""}<br /><span class="error">{$app.tel_line_num_err}</span>{/if}
		    </p>
                </td>
            </tr>

            <tr>
                <td class="l_Cel" style="vertical-align:top;">
                    <div class="middle_midashi">付与したい属性項目</div>
                </td>
                <td colspan="2" class="r_Cel">
                    <p class="note">

		    {foreach from=$app.property_list key=k item=v}
                        <input type="checkbox" name="in_property_check[]" value="{$k}" {if isset($app.property_checked.$k)}{$app.property_checked.$k}{/if}>{$v}<br/>
		    {/foreach}

                    {if $app.property_check_err != ""}<br /><span class="error">{$app.property_check_err}</span>{/if}

			<!--
                        【記載形式】<br />
                        {if $logined_name != ""}
			<a href="ex_furu_article.html#bunrui" target="_blank"/>業種リストを確認する</a><br />
			<a href="ex_furu_article.html#koumoku" target="_blank"/>項目リストを確認する</a>


                        {else}
			<a href="ex_furu_article.html#bunrui" target="_blank"/>業種リストを確認する</a><br />
			<a href="ex_furu_article.html#koumoku" target="_blank"/>項目リストを確認する</a>
			{/if}
                        </span>
			-->
                    </p>
                </td>
            </tr>

            <tr><td colspan="3" style="border-bottom :1px solid #017680;"></td></tr>

            <tr>
                <td colspan="4">
                    <div id="submit_btn">
                        {if $logined_name != ""}
                        <a class="button" href="javascript:void(0);" onclick="document.f_get.submit();" id="search_button">ご依頼の確認</a>
                        {else}
                        <a class="button" href="javascript:void(0);" onclick="displaySendForm();" id="a_search">ご依頼の確認</a>
                        {/if}
                    </div>
                </td>
            </tr>


            <tr>
                <td colspan="2">

	<!-- ここから採用事例 -->

<div style="text-align:center; margin:0 auto;">111111111111
	<a href="https://www.navit-j.com/blog/?p=29983" target="_blank"><img src="img/btn_faq.png" onmouseover="this.src='img/btn_faq_d.png'" onmouseout="this.src='img/btn_faq.png'"alt="ご依頼前に必ずお読みください" /></a>
</div>


	<!-- ここまで採用事例 -->
                </td>
            </tr>

        </table>

</div>



<!--
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">お試し！&nbsp;法人番号検索サービスサービス</div>
<br />
<div style="margin: 0 50px 0 50px;">
情報付与を行いたい企業データを以下フォームより、情報付与依頼をお試しいただけます！<br />
是非こちらより法人番号検索サービスサービスをご体感下さい！<br />
<br />


<table style="width:900px;text-align:left;margin:10px auto 0px auto;border-collapse: collapse;">
<tr><td colspan="4">
	{if $app.trial_check_err.uninput != ""}<br /><span style="font-size:80%;" class="error">{$app.trial_check_err.uninput}</span>{/if}
	{if $app.trial_check_err.uncolumn != ""}<br /><span style="font-size:80%;" class="error">{$app.trial_check_err.uncolumn|nl2br}</span>{/if}
</td></tr>
<tr style="background-color:#F2F2F2;border-bottom:solid 10px #ffffff;">
	<td></td>
	<td style="padding:10px;font-size:80%;">企業名(必須)<br /><span style="font-size:70%;">※企業名を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">電話番号(必須)<br /><span style="font-size:70%;">※電話番号を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">所在地<br /><span style="font-size:70%;">※所在地を入れて下さい</span></td>
<tr style="background-color:#F2F2F2;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_1" value="{$app.data_corp_1}"  placeholder="株式会社ナビット" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_1" value="{$app.data_tel_1}" placeholder="03-5215-5713" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_1" value="{$app.data_address_1}" placeholder="東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F" style="width:300px;" />
	</td>
</tr>



<tr style="background-color:#F2F2F2;border-bottom:solid 10px #ffffff;">
	<td></td>
	<td style="padding:10px;font-size:80%;">企業名(必須)<br /><span style="font-size:70%;">※企業名を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">電話番号(必須)<br /><span style="font-size:70%;">※電話番号を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">所在地<br /><span style="font-size:70%;">※所在地を入れて下さい</span></td>
<tr style="background-color:#F2F2F2;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_1" value="{$app.data_corp_1}"  placeholder="株式会社ナビット" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_1" value="{$app.data_tel_1}" placeholder="03-5215-5713" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_1" value="{$app.data_address_1}" placeholder="東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F" style="width:300px;" />
	</td>
</tr>


<tr style="background-color:#F2F2F2;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報2</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_2" value="{$app.data_corp_2}"  placeholder="企業名" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_2" value="{$app.data_tel_2}" placeholder="電話番号" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_2" value="{$app.data_address_2}" placeholder="所在地" style="width:300px;" />
	</td>
</tr>
<tr style="background-color:#F2F2F2;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報3</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_3" value="{$app.data_corp_3}"  placeholder="企業名" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_3" value="{$app.data_tel_3}" placeholder="電話番号" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_3" value="{$app.data_address_3}" placeholder="所在地" style="width:300px;" />
	</td>
</tr>

</table>
<div style="text-align:center;margin:30px auto 50px auto;">
	<a class="button" href="javascript:void(0);" onclick="do_trial();" id="trial_button">お試し依頼の確認</a>
</div>
-->
    </form>





<div align="center" style="margin:0px auto 15px;">
	<div style="height:30px;"></div>
	<span style="font-weight:bold; font-size:25px;"><a href="https://www.navit-j.com/service/navit_opensite_20170612.html" target="_blank" style="color:#000000;">資料ダウンロードはこちら<span style="color:red; font-weight:bold;">NEW!</span></a></span>
</div>

<div style="text-align:center; margin:10px auto;"><a href="/service/furufuru/inquiry/sample_dl.html" target="_blank"><img src="img/sample_dl.jpg" alt="サンプルデータダウンロード" /></a></div>

</div> <!--hidden部分の終わり -->




<div class="search_result" align="center" style="margin-top:40px;">
	<a href="https://www.navit-j.com/inquiry/db_hojin.html">
		<span style="font-size:25px; font-weight:bold; color:#000;">複数のデータを一括で処理したい方はこちら</span>
	</a>
</div>


<div style="margin:30px 0 0 0; text-align:center;">
<span style="font-weight:bold; font-size:25px;"><a href="https://www.navit-j.com/service/navit_opensite_20170612.html" target="_blank" style="color:#000;">資料ダウンロードはこちら</a></span>
</div>





<a name="kensu"></a>
<div class="head_title_bgcolor" style="width:100%; text-align:left;margin:40px auto 10px auto;">
	■企業総件数：680万社
</div>
	<table border="1" cellpadding="5" style="width:800px; margin:0 auto; font-size:17px;">

		<tr>
			<td style="background-color:#F2F2F2;">法人番号<br />(マイナンバー)</td>
			<td align="left">　　　約124万社<span style="color:#f00;">NEW</span></td>
			<td style="background-color:#F2F2F2;">設立年月</td>
			<td align="left">　　　約8万社</td>

		</tr>

		<tr>
			<td width="20%" style="background-color:#F2F2F2;">業種</td>
			<td align="left">　　　約624万社</td>
			<td width="20%" style="background-color:#F2F2F2;">売上規模</td>
			<td align="left">　　　約246万社 <!--<span style="color:#f00;">NEW</span>--></td>

		</tr>

		<tr>
			<td style="background-color:#F2F2F2;">FAX番号</td>
			<td align="left">　　　約353万社</td>
			<td style="background-color:#F2F2F2;">従業員数規模</td>
			<td align="left">　　　約117万社</td>

		</tr>

		<tr>
			<!--<td style="background-color:#F2F2F2;">代表者名</td>-->
			<!--<td align="left">　　　約125万社</td>-->
			<td style="background-color:#F2F2F2;">企業URL</td>
			<td align="left">　　　約190万社</td>
			<td style="background-color:#F2F2F2;">資本金規模</td>
			<td align="left">　　　約160万社</td>

		</tr>
		<tr>
			<td style="background-color:#F2F2F2;">企業<br />メールアドレス</td>
			<td align="left">　　　約23万社</td>

			<td style="border-bottom-style: hidden;border-right-style: hidden;">&nbsp;</td>
			<td style="border-bottom-style: hidden;border-left-style: hidden;border-right-style: hidden;">&nbsp;</td>
		</tr>

		<!--<tr>
			<td style="background-color:#F2F2F2;">企業<br />メールアドレス</td>
			<td align="left">　　　約23万社</td>
			<td style="background-color:#F2F2F2;"> </td>
			<td> </td>
		</tr>-->

	</table>

<div class="text" style="margin:3px 0 0 700px;">
	<span style="font-size:80%;">2017年9月現在</span>
</div>

<div align="center" style="margin:10px auto 10px;">
<a name="price"></a>


<div class="head_title_bgcolor" style="text-align:left; margin:30px auto 20px auto;">
	■価格表
	<span style="font-size:60%;">(税別)　</span>
	<!--<span style="font-size:80%;">1件からでもお受けできます</span>-->
</div>
<div style="text-align:center;">
	<span style="font-weight:bold;">ポイント単価：1ポイントあたり1円　</span>
	<span style="color:#F00; font-weight:bold;">初期費用は0円です。</span>
</div>


<div style="width:900px;overflow: hidden;font-size:0.8em;margin:10px 0 0 0;">
<div style="float: left;">
<table width="440" border="1"  cellpadding="5">
  <tbody>
    <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">基本料金</th>
      <td align="center"><b>0ポイント</b></td>
    </tr>
    <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">電話番号</th>
		<td align="center"><b>10ポイント</b><span style="font-size:80%;">〔\10(税別)〕</span></td>
    </tr>
    <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">業　種</th>
		<td align="center"><b>20ポイント</b><span style="font-size:80%;">〔\20(税別)〕</span></td>
    </tr>
    <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">URL</th>
		<td align="center"><b>20ポイント</b><span style="font-size:80%;">〔\20(税別)〕</span></td>
    </tr>
        <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">上場区分</th>
		<td align="center"><b>20ポイント</b><span style="font-size:80%;">〔\20(税別)〕</span></td>
    </tr>
        <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">FAX番号</th>
		<td align="center"><b>37ポイント</b><span style="font-size:80%;">〔\37(税別)〕</span></td>
    </tr>
  </tbody>
</table>
	</div>
	<div style="float: right;">
<table width="440" border="1"  cellpadding="5">
  <tbody>
    <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">従業員規模</th>
		<td align="center"><b>40ポイント</b><span style="font-size:80%;">〔\40(税別)〕</span></td>
    </tr>
    <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">売上高規模</th>
		<td align="center"><b>40ポイント</b><span style="font-size:80%;">〔\40(税別)〕</span></td>
    </tr>
    <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">資本金規模</th>
		<td align="center"><b>40ポイント</b><span style="font-size:80%;">〔\40(税別)〕</span></td>
    </tr>
    <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">設立年月</th>
		<td align="center"><b>40ポイント</b><span style="font-size:80%;">〔\40(税別)〕</span></td>
    </tr>
        <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">メールアドレス</th>
		<td align="center"><b>40ポイント</b><span style="font-size:80%;">〔\40(税別)〕</span></td>
    </tr>
        <tr>
      <th scope="row" style="background-color:#ECFEFF;" align="center">法人番号</th>
		<td align="center"><b>50ポイント</b><span style="font-size:80%;">〔\50(税別)〕</span></td>
    </tr>
  </tbody>
</table>
	</div>
	</div>
                
                
                <!--
                 <table border="1" cellpadding="5" style="width:900px; margin:0 auto 5px;">

                        <tr>
                          <td width="12%" style="background-color:#F2F2F2;" align="center">基本料金 </td>
                          <td width="15%" align="center">0ポイント</td>
                          <td width="12%" style="background-color:#F2F2F2;" align="center">売上高規模 </td>
                          <td width="15%" rowspan="5" align="center">40ポイント<span style="font-size:80%;">〔\40(税別)〕</span><br />
                          <span style="font-size:60%;">※1項目あたりの単価</span></td>
							<td width="12%" style="background-color:#F2F2F2;" align="center" rowspan="3"><span style="background-color:#F2F2F2;" align="center">URL</span></td>
                          <td width="15%" rowspan="3" align="center">20ポイント<span style="font-size:80%;">〔\20(税別)〕</span><br />
                          <span style="font-size:60%;">※1項目あたりの単価</span></td>
                        </tr>


                       <tr>
                        <td style="background-color:#F2F2F2;" align="center">
                        業種
                        </td>
                        <td align="center">
                            20ポイント<span style="font-size:80%;">〔\20(税別)〕</span>
                        </td>
                        <td style="background-color:#F2F2F2;" align="center">資本金規模 </td>

                        </tr>

                        <tr>
                        <td style="background-color:#F2F2F2;" align="center">
                        FAX番号
                        </td>
                        <td align="center">
                            37ポイント<span style="font-size:80%;">〔\37(税別)〕</span>
                        </td>
                        <td style="background-color:#F2F2F2;" align="center">設立年月</td>

                        </tr>

                        <tr>
                        <td style="background-color:#F2F2F2;" align="center">
                        上場区分
                        </td>
                        <td align="center">
                            20ポイント<span style="font-size:80%;">〔\20(税別)〕</span>
                        </td>
                        <td rowspan="2" align="center" style="background-color:#F2F2F2;"><span style="background-color:#F2F2F2;">メール<br>アドレス</span></td>
                        <td rowspan="1" align="center" style="background-color:#F2F2F2;"><span style="background-color:#F2F2F2;">法人番号</span></td>
                        <td width="15%" rowspan="1" align="center">50ポイント<span style="font-size:80%;">〔\50(税別)〕</span></td>
                        </tr>
                        <tr>
                          <td style="background-color:#F2F2F2;" align="center">従業員規模</td>
                          <td align="center">40ポイント<span style="font-size:80%;">〔\40(税別)〕</span></td>
                        <td rowspan="1" align="center" style="background-color:#F2F2F2;"><span style="background-color:#F2F2F2;">電話番号</span></td>
                        <td width="15%" rowspan="1" align="center">10ポイント<span style="font-size:80%;">〔\10(税別)〕</span></td>
                        </tr>

               </table>-->




<div class="head_title_bgcolor" style="width:100%; text-align:left;margin:40px auto 10px auto;">
	■お知らせ＆ニュースリリース
</div>

                                    <iframe class="test_news" src="/service/common/announce/list.php" type="text/html" name="notice_test" width="90%" height="270px" style="text-algin:center;margin:10px 0 20px 20px;border:solid 1px #ccc;">
</iframe>
<p style="margin:-17px 0 0 720px;font-size:0.8em;"><a href="../common/announce/list_archive.php">全記事はこちら</a></p>


<!--<div style="margin:0 auto 30px auto;text-align:center;">
<span style="font-weight:bold; font-size:24px;"><a href="https://www.navit-j.com/blog/?p=27751" target="_blank">オープンサイト採用事例・便利な使い方</a></span>
</div>-->

</div>

</div>