{include file=getlist_dialog.tpl}
<script type="text/javascript" src="js/jquery.getlist_loading.js"></script>
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">お試しサービス&nbsp;&nbsp;ご依頼内容の確認</div>
<br />

<div class="table">
    <table border="0" cellpadding="5" cellspacing="1" style="width:900px;margin-left:auto;margin-right:auto;font-size:80%">
	<tr>
            <td class="l_Cel">
                <div class="sub_head">企業情報</div>
            </td>

            <td colspan="2" class="r_Cel">
		{$app.data_corp_1}<br/>
		{$app.data_tel_1}<br/>
		{if $app.data_address_1 != ""}{$app.data_address_1}{/if}
            </td>
	</tr>
	{if $app.data_corp_2 != ""}
	<tr>
	    <td colspan="3">
                <hr />
            </td>
	</tr>
	<tr>

            <td class="l_Cel">
                <div class="sub_head">企業情報2</div>
            </td>

            <td colspan="2" class="r_Cel">
		{$app.data_corp_2}<br/>
		{$app.data_tel_2}<br/>
		{if $app.data_address_2 != ""}{$app.data_address_2}{/if}
            </td>
	</tr>
	{/if}
	{if $app.data_corp_3 != ""}
	<tr>
	    <td colspan="3">
                <hr />
            </td>
	</tr>
	<tr>

            <td class="l_Cel">
                <div class="sub_head">企業情報3</div>
            </td>

            <td colspan="2" class="r_Cel">
		{$app.data_corp_3}<br/>
		{$app.data_tel_3}<br/>
		{if $app.data_address_3 != ""}{$app.data_address_3}{/if}
            </td>
	</tr>
	{/if}
	<tr>
	    <td colspan="3">
                <hr />
                <div id="submit_btn">
		<form action="{$script}" name="f_back" method="POST" id="f_back" enctype="multipart/form-data">
		    <input type="hidden" name="action_index" value="ture" />
		    <input type="hidden" name="in_corp_1" value="{$app.data_corp_1}" />
		    <input type="hidden" name="in_address_1" value="{$app.data_address_1}" />
		    <input type="hidden" name="in_tel_1" value="{$app.data_tel_1}" />
		    <input type="hidden" name="in_corp_2" value="{$app.data_corp_2}" />
		    <input type="hidden" name="in_address_2" value="{$app.data_address_2}" />
		    <input type="hidden" name="in_tel_2" value="{$app.data_tel_2}" />
		    <input type="hidden" name="in_corp_3" value="{$app.data_corp_3}" />
		    <input type="hidden" name="in_address_3" value="{$app.data_address_3}" />
		    <input type="hidden" name="in_tel_3" value="{$app.data_tel_3}" />
		    <input type="hidden" name="in_act" value="back" />
		</form>
		<form action="{$script}" name="f_get" method="POST" id="f_get" enctype="multipart/form-data">
		    {if $app.name != ""}
        		<input type="hidden" name="action_thanks" value="true" />
		    {else}
        		<input type="hidden" name="action_secure_registration" value="true" />
		    {/if}
		    <input type="hidden" name="in_act" value="{$app.act}" />
		</form>
                    <a class="button" href="javascript:void(0)" onclick="document.f_back.submit();" id="back_button">戻る</a>     
		    &nbsp;&nbsp;&nbsp;&nbsp;
                    <a class="button" href="javascript:void(0)" onclick="document.f_get.submit();" id="a_search">この情報でお試し依頼する</a>     
<p>※購入しない場合は、見積もり内容確認時に「依頼を取り消す」ボタンをクリックしてください。<br />
お支払いただいているポイントを返却いたします。</p>
                </div>
            </td>
	</tr>
    </table>
</div>

</form>
</div>
