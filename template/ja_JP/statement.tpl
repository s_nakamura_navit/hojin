{include file=getlist_dialog.tpl}
<script type="text/javascript" src="js/jquery.getlist_loading.js"></script>
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">ご依頼内容の確認<span style="font-size:18px;margin-left:50px;">お客様の現在のポイント数：{$app.point}pt</span></div>
<br />

<div class="table">
    <table border="0" cellpadding="5" cellspacing="1" style="width:600px;margin-left:auto;margin-right:auto;">
	<tr>
            <td class="l_Cel">
                <div class="sub_head">データ名</div>
            </td>

            <td colspan="2" class="r_Cel">
                    {$app.list_name}
            </td>
	</tr>

        <tr>
            <td colspan="3">
                    <hr />
            </td>
        </tr>

        <tr>
            <td class="l_Cel">
                <div class="sub_head">件数</div>
            </td>

            <td colspan="2" class="r_Cel">
                    {$app.count.list}&nbsp;件
            </td>
	</tr>

        <tr>
            <td colspan="3">
                    <hr />
            </td>
        </tr>

        <tr>
            <td class="l_Cel">
                <div class="sub_head">属性付与項目</div>
            </td>

            <td colspan="2" class="r_Cel">
                    {$app.latlon_type_name[$app.latlon_type]}
            </td>
	</tr>

        <tr>
            <td colspan="3">
                    <hr />
            </td>
        </tr>

        <tr>
            <td colspan="3">
                    <hr />
            </td>
        </tr>

                                <tr><td colspan="2">
                                <div id="detail">
                                <table width="100%" id="detail" style="border:1px solid #666">
                                        <tr>
                                                <td colspan="3" style="font-size:18px; text-align:left;background-color:#666; font-weight:bold; color:#fff;">必要ポイントの内訳</td>
                                        </tr>
					{foreach name=loop from=$app.lv_check item=lv}
                                        <tr>
                                                <td style="font-size:17px; font-weight:bold; text-align:left;">{if $smarty.foreach.loop.iteration == 1}価格{/if}</td>
                                                <td class="bls" style="font-size:17px; font-weight:bold; text-align:right;">{$app.lv_name.$lv}</td>
						<td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.count.$lv}&nbsp;件&nbsp;&times;&nbsp;{$app.unit_point.$lv}&nbsp;ポイント&nbsp;=&nbsp;{$app.lv_point.$lv}&nbsp;ポイント</th>
                                        </tr>
					{/foreach}
					{if $smarty.foreach.loop.iteration > 1}
                                        <tr>
                                                <td colspan="2" style="font-size:17px; font-weight:bold; text-align:left;">小計</td>
                                                <td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.ask_point}&nbsp;ポイント</td>
                                        </tr>
					{/if}
                                        <tr>
                                                <td colspan="2" style="font-size:17px; font-weight:bold; text-align:left;">消費税８％</td>
                                                <td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.tax_point}&nbsp;ポイント</td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" style="font-size:17px; font-weight:bold; text-align:left;">基本料金</td>
                                                <td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.bace_point}&nbsp;ポイント</td>
                                        </tr>
                                        <tr>
                                                <td colspan="2" style="font-size:17px; font-weight:bold; text-align:left;">計</td>
                                                <td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.spend}&nbsp;ポイント</td>
                                        </tr>
                                </table>
                                </div>
                                </td></tr>

        <tr>
            <td class="l_Cel">
                <div class="sub_head">お客様の現在のポイント数</div>
            </td>

            <td colspan="2" class="r_Cel">
		{$app.point}&nbsp;ポイント
            </td>
	</tr>

        <tr>
            <td class="l_Cel">
                <div class="sub_head">必要ポイント</div>
            </td>

            <td colspan="2" class="r_Cel">
		{$app.spend}&nbsp;ポイント
            </td>
	</tr>

	{if $app.act == "no_point"}
        <tr>
            <td class="l_Cel">
                <div class="sub_head">不足ポイント</div>
            </td>

            <td colspan="2" class="r_Cel">
		{$app.lack}&nbsp;ポイント
            </td>
	</tr>
	{/if}

	<tr>
	    <td colspan="3">
                <hr />
                <div id="submit_btn">
		<form action="{$script}" name="f_back" method="POST" id="f_back" enctype="multipart/form-data">
		    <input type="hidden" name="action_confirmation" value="ture" />
		    <input type="hidden" name="in_tel_value_line" value="{$app.tel_value_line}" />
		    <input type="hidden" name="in_matching_count" value="{$app.count.matching}" />
		    <input type="hidden" name="in_unmatch_count" value="{$app.count.unmatch}" />
		    <input type="hidden" name="in_unmatch_mobile" value="{$app.count.mobile}" />
		    <input type="hidden" name="in_unmatch_num_err" value="{$app.count.num_err}" />
		    <input type="hidden" name="in_unmatch_blank" value="{$app.count.blank}" />
		    <input type="hidden" name="in_list_count" value="{$app.count.list}" />
		    <input type="hidden" name="in_header_check" value="{$app.header_check}" />
		    <input type="hidden" name="in_header_line" value="{$app.header_line}" />
		    <input type="hidden" name="in_list_name" value="{$app.list_name}" />
		    <input type="hidden" name="in_latlon_type" value="{$app.latlon_type}" />
		    <input type="hidden" name="in_act" value="back" />
		    {foreach from=$app.lv_check item=lv}
		    	<input type="hidden" name="in_lv_check[]" value="{$lv}" />
		    {/foreach}
		    <input type="hidden" name="in_up_file_path" value="{$app.up_file_path}" />
		</form>
		<form action="{$script}" name="f_get" method="POST" id="f_get" enctype="multipart/form-data">
		    {if $app.act == "no_point"}
        		<input type="hidden" name="action_request_registration" value="true" />
		    	<input type="hidden" name="in_get_csv" value="yet" />
			<input type="hidden" name="in_matching" value="{$app.count.matching}" />
		        <input type="hidden" name="in_unmatch" value="{$app.count.unmatch}" />
		        <input type="hidden" name="in_unmatch_mobile" value="{$app.count.mobile}" />
		        <input type="hidden" name="in_unmatch_num_err" value="{$app.count.num_err}" />
		        <input type="hidden" name="in_unmatch_blank" value="{$app.count.blank}" />
		    {else}
        		<input type="hidden" name="action_thanks" value="true" />
		    	<input type="hidden" name="in_get_csv" value="start" />
		    {/if}
		    <input type="hidden" name="in_tel_value_line" value="{$app.tel_value_line}" />
		    <input type="hidden" name="in_header_check" value="{$app.header_check}" />
		    <input type="hidden" name="in_header_line" value="{$app.header_line}" />
		    <input type="hidden" name="in_latlon_type" value="{$app.latlon_type}" />
		    <input type="hidden" name="in_list_name" value="{$app.list_name}" />
		    <input type="hidden" name="in_use_point" value="{$app.spend}" />
		    <input type="hidden" name="in_list_count" value="{$app.count.list}" />
		    <input type="hidden" name="in_count_get" value="{$app.count.get}" />
		    {foreach from=$app.lv_check item=lv}
		    	<input type="hidden" name="in_lv_check[]" value="{$lv}" />
		    {/foreach}
		    <input type="hidden" name="in_up_file_path" value="{$app.up_file_path}" />
		</form>
                    <a class="button" href="javascript:void(0)" onclick="document.f_back.submit();" id="back_button">戻る</a>     
			&nbsp;&nbsp;&nbsp;&nbsp;
		    {if $app.act == "no_point"}
                        <a class="button" href="javascript:void(0)" onclick="document.f_get.submit();" id="a_search">ポイント購入</a>     
		    {else}
                        <a class="button" href="javascript:void(0)" onclick="displayGetListForm();" id="a_search">このデータで依頼する</a>     
		    {/if}
                </div>
            </td>
	</tr>
    </table>
</div>

</form>
</div>
