
{if $app.db_regist_result == "-1" || $app.mail_send_result == "-1"}
<div class="top_header_title" ><span class="top_header_title_border">退会手続きエラー</span></div>
<div style="margin-left: 20px;margin-right: 20px;">
    <span style="font-weight:bold;font-size:100%;">退会手続きができませんでした。</span><br />
    大変お手数ではございますが再度こちらから退会手続きをお願い致します。>> 
    <a href="./index.php?action_mypage=true"> マイページへ</a><br />
      <br />
      <br />
      <span style="color:#666;font-size:80%;">
        【ご登録に失敗する場合、以下の原因が考えられます】<br />
        ※通信環境の混雑等の理由によるもの<br />
        <br />
        <br />
      
      </span>  
</div>
{else}
<div class="top_header_title" ><span class="top_header_title_border">退会手続き完了</span></div>
<div style="margin-left: 20px;margin-right: 20px;">
    <span style="font-weight:bold;font-size:100%;">ご利用いただき、ありがとうございました。</span><br />
    ご登録頂いていたメールアドレスに退会手続き完了のメールを送信致しました。メール受信のご確認をお願い致します。<br />
    ご利用いただいていたログインIDおよびパスワード、またご購入いただいたポイントは無効となりましたので、退会手続き完了後はご利用いただけません。<br />
    <br />
      <span style="color:#666;font-size:80%;">
        ※通信環境の混雑等の理由によりメールの到着に少々お時間がかかる場合がございます。<br /><br />
        ※迷惑メール防止機能により当サイトからのメールが迷惑メールと間違えられ、メール受信画面に表示されない場合が<br />
        &nbsp;&nbsp;&nbsp;ございます。迷惑メールフォルダやゴミ箱に自動的に振り分けられている場合がございますので、一度ご確認頂きま<br />
        &nbsp;&nbsp;&nbsp;すようお願い致します。<br />
      </span>  
</div>
{/if}
<div style="font-size:0.7em;margin-left: 120px;">
<!--
<span>
【注意事項】<br />
<span style="color:#64c601;font-size:16px;">■</span>ID・PWはご登録頂いたメールアドレスにご連絡致します。<br />
<span style="color:#64c601;font-size:16px;">■</span>フリーメールアドレスはご利用頂けない場合がございます。<br />
</span>
-->
</div>        
        
<!--        
<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
    <tr>
        <td class="l_Cel_01_01">会社名</td>
        <td class="s_Cel">{$app.data.company_name}</td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">会社名かな</td>
        <td width="550" class="s_Cel">{$app.data.company_kana}</td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">支店名</td>
        <td width="550" class="s_Cel">{$app.data.branch_name}</td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">郵便番号</td>
        <td width="550" class="s_Cel">{$app.data.zip1}－{$app.data.zip2}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">住所</td>
        <td width="550" class="s_Cel">{$app.data.address}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">電話番号</td>
        <td width="550" class="s_Cel">
            {$app.data.phone_no1}{$app.data.phone_no2}{$app.data.phone_no3}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">携帯電話番号</td>
        <td width="550" class="s_Cel">
            {$app.data.k_phone_no1}{$app.data.k_phone_no2}{$app.data.k_phone_no3}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">FAX番号</td>
        <td width="550" class="s_Cel">
            {$app.data.fax_no1}{$app.data.fax_no2}{$app.data.fax_no3}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">部署名</td>
        <td width="550" class="s_Cel">
            {$app.data.department_name}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">役職名</td>
        <td width="550" class="s_Cel">
            {$app.data.post_name}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">担当者名</td>
        <td width="550" class="s_Cel">
            {$app.data.contractor_lname}&nbsp;&nbsp;{$app.data.contractor_fname}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">担当者名かな</td>
        <td width="550" class="s_Cel">
            {$app.data.contractor_lkana}&nbsp;&nbsp;{$app.data.contractor_fkana}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">メールアドレス</td>
        <td width="550" class="s_Cel">
            {$app.data.email}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">利用規約への同意</td>
        <td width="550" class="s_Cel">
            同意します
        </td>
    </tr>
    <tr>
        <td colspan="3">

	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
 
        </td>
    </tr>
</table>
</div>
-->
                <div class="mod_form_btn">
                    
	<div style="margin-top:20px;margin-left:400px;">
            <!--<a class="button" href="javascript:void(0)" onclick="javascript:history.back();" id="">戻る</a>-->
            <a class="button2" href="index.php" id="to_top_btn">TOPへ</a>
        </div>
                </div>
</div>
        
        
<input type="hidden" name="in_company_name" id="in_company_name" value="{$app.data.company_name}">
<input type="hidden" name="in_company_kana" id="in_company_kana" value="{$app.data.company_kana}">
<input type="hidden" name="in_branch_name" id="in_branch_name" value="{$app.data.branch_name}"> 
<input type="hidden" name="in_zip1" id="in_zip1" value="{$app.data.zip1}">
<input type="hidden" name="in_zip2" id="in_zip2" value="{$app.data.zip2}"> 
<input type="hidden" name="in_address" id="in_address" value="{$app.data.address}"> 
<input type="hidden" name="in_phone_no1" id="in_phone_no1" value="{$app.data.phone_no1}">
<input type="hidden" name="in_phone_no2" id="in_phone_no2" value="{$app.data.phone_no2}"> 
<input type="hidden" name="in_phone_no3" id="in_phone_no3" value="{$app.data.phone_no3}"> 
<input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="{$app.data.k_phone_no1}">
<input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="{$app.data.k_phone_no2}">
<input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="{$app.data.k_phone_no3}">
<input type="hidden" name="in_fax_no1" id="in_fax_no1" value="{$app.data.fax_no1}">
<input type="hidden" name="in_fax_no2" id="in_fax_no2" value="{$app.data.fax_no2}"> 
<input type="hidden" name="in_fax_no3" id="in_fax_no3" value="{$app.data.fax_no3}"> 
<input type="hidden" name="in_department_name" id="in_department_name" value="{$app.data.department_name}"> 
<input type="hidden" name="in_post_name" id="in_post_name" value="{$app.data.post_name}"> 
<input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="{$app.data.contractor_lname}">
<input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="{$app.data.contractor_fname}">
<input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="{$app.data.contractor_lkana}">
<input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="{$app.data.contractor_fkana}"> 
<input type="hidden" name="in_email" id="in_" value="{$app.data.email}"> 
<input type="hidden" name="in_privacy" id="in_privacy" value="{$app.data.privacy}">

<input type="hidden" name="back" id="back" value="0"> 
        
</form>        
	<!-- ここまで入力フォーム -->









</div>
<!-- ここまでメインコンテンツ -->
