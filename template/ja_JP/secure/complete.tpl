{literal} 

<script type="text/javascript">
var _trackingid = 'LFT-10394-1';
var _conversionid = '16';
(function() {
  var lft = document.createElement('script'); lft.type = 'text/javascript'; lft.async = true;
  lft.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + '//track.list-finder.jp/js/ja/track.js';
  var snode = document.getElementsByTagName('script')[0]; snode.parentNode.insertBefore(lft, snode);
})();
</script>

<script type="text/javascript">
//吹き出し
$(window).load(function(){
      $('label').balloon();
});


  	
//全選択・全解除
$(function() {
    $('#all_check').on("click",function(){
        $('.CheckList').prop("checked", true);
    });
});
$(function() {
    $('#all_clear').on("click",function(){
        $('.CheckList').prop("checked", false);
    });
});

$(function() {
    $('#all_check2').on("click",function(){
        $('.CheckList2').prop("checked", true);
    });
});
$(function() {
    $('#all_clear2').on("click",function(){
        $('.CheckList2').prop("checked", false);
    });
});

$(function() {
    $('#all_check3').on("click",function(){
        $('.CheckList3').prop("checked", true);
    });
});
$(function() {
    $('#all_clear3').on("click",function(){
        $('.CheckList3').prop("checked", false);
    });
});

//リセット(自治体)
$(function() {
    $('#a_reset').on("click",function(){
        //ラジオボタン初期値セット
        $('#in_kind_1').prop("checked", true);
        $('#in_kind_2').prop("checked", false);
        //プルダウン初期値セット
        $('select[name="in_area1"]').val("");
        $('#in_area2').html('');//一度select内を空に
        $('#in_area2').append('<option id="city00000" value="'+''+'">'+'市区町村を選択'+'</option>');
        
        //チェックボックスクリア
        $('.CheckList').prop("checked", false);
        
        $("#a_reset").blur();
    });
});
//リセット(財団)
$(function() {
    $('#f_reset').on("click",function(){
        //チェックボックスクリア
        $('.CheckList2').prop("checked", false);
        $('.CheckList3').prop("checked", false);
        $('#in_keyword').val("");
        $("#f_reset").blur();
    });
});

function doblur() { 
    var element = document.getElementById("name"); 
    element.blur(); 
} 

function do_submit(kind){
    
    if(kind=="back"){
        document.f2.back.value = "1";
        document.f2.action = "index.php?action_secure_registration=true";
        document.f2.submit();
    }else{
        document.f2.submit();
    }
}

/*
 　　全角->半角変換
 */
jQuery(function(){
 
    // 郵便番号の処理
    $('.zip-number').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
 
        // 半角数字のみ残す
        var zenkakuDel = new String( hankaku ).match(/\d/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }
 
        $(this).val(zenkakuDel);
    });
    // 電話番号の処理
    $('.tel-number').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
 
        // 半角数字と+-のみ残す
        var zenkakuDel = new String( hankaku ).match(/\d|\-|\+/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }
        
        $(this).val(zenkakuDel);
    });
 
    // メールアドレスの処理
    $('.mail-address').change( function(){
        var zenkigou = "＠－ー＋＿．，、";
        var hankigou = "@--+_...";
        var data = $(this).val();
        var str = "";
 
        // 指定された全角記号のみを半角に変換
        for (i=0; i<data.length; i++)
        {
            var dataChar = data.charAt(i);
            var dataNum = zenkigou.indexOf(dataChar,0);
            if (dataNum >= 0) dataChar = hankigou.charAt(dataNum);
            str += dataChar;
        }
        // 定番の、アルファベットと数字の変換処理
        var hankaku = str.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
        $(this).val(hankaku);
    });
 
});

//確認ボタンの有効無効

jQuery(function(){
    $('#privacy_1').change(function(){
            if ($(this).is(':checked')) {
                    $('#confirm_btn').css({opacity:"1",cursor:"pointer"}).removeAttr('disabled');
            } else {
                    $('#confirm_btn').css({opacity:"0.5",cursor:"default"}).attr('disabled','disabled');
            }
    });
});  


</script>
{/literal} 

<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_trial" value="true" />
<input type="hidden" name="in_act" value="{$app.data.act}" />
<input type="hidden" name="in_secure_status" value="back" />

{if $app.db_regist_result == "-1" || $app.mail_send_result == "-1"}
<div class="top_header_title" ><span class="top_header_title_border">登録エラー</span></div>
<div align="center">
    <span style="font-weight:bold;font-size:100%;">ご登録頂いた内容で会員登録ができませんでした。</span><br />
    大変お手数ではございますが再度こちらから登録をお願い致します。<br /><br />
    <a href="index.php?action_secure_registration=true">【新規会員登録へ】</a><br />
      <br />
      <br />
      <span style="color:#666;font-size:80%;">
        【ご登録に失敗する場合、以下の原因が考えられます】<br />
<!--
        ※仮登録時にご連絡したURLの有効期限切れ(有効期限は仮登録メール発行時から24時間となっております)<br />
-->
        ※通信環境の混雑等の理由によるもの<br />
        ※ご登録頂いたメールアドレスへの送信失敗によるもの(ご登録頂いたメールアドレスをご確認ください)<br />
        <br />
        <br />
      
      </span>  
</div>
{elseif $app.auto_login_result == "-1"}
<div class="top_header_title" ><span class="top_header_title_border">システムエラー</span></div>
<div style="margin-left: 20px;margin-right: 20px;">
    <span style="font-weight:bold;font-size:100%;">ご登録頂いたお試し依頼の情報引継ぎに失敗しました。</span><br />
    大変お手数ではございますが、お客さまのメールアドレスに送信したアカウント情報にてログインいただき、トップページより再度企業情報の入力をお願いいたします。>> 
    <a href="index.php?action_login=true"> ログイン画面へ</a><br />
      <br />
      <br />
      <span style="color:#666;font-size:80%;">
        【ご登録に失敗する場合、以下の原因が考えられます】<br />
        ※通信環境の混雑等の理由によるもの<br />
        <br />
        <br />
      
      </span>  
</div>
{else}
<div class="top_header_title" ><span style="border-bottom:1px #610B0B solid;">会員登録完了</span></div>
<div style="margin-left: 20px;margin-right: 20px;">
    <span style="font-weight:bold;font-size:100%;">ご登録ありがとうございます。会員登録が完了致しました。</span><br />
    ご登録頂いたメールアドレスに会員登録情報を送信致しました。メール受信のご確認をお願い致します。<br />
    会員登録情報には「法人番号検索」のサービスをご利用するためのログインIDおよびパスワードが記載されておりますのでお取り扱いには十分にお気をつけください。<br />
    <br />
      <span style="color:#666;font-size:80%;">
        ※通信環境の混雑等の理由によりメールの到着に少々お時間がかかる場合がございます。<br /><br />
        ※迷惑メール防止機能により当サイトからのメールが迷惑メールと間違えられ、メール受信画面に表示されない場合が<br />
        &nbsp;&nbsp;&nbsp;ございます。迷惑メールフォルダやゴミ箱に自動的に振り分けられている場合がございますので、一度ご確認頂きま<br />
        &nbsp;&nbsp;&nbsp;すようお願い致します。<br />
      </span>  
</div>
{/if}
<div style="font-size:0.7em;margin-left: 120px;">
<!--
<span>
【注意事項】<br />
<span style="color:#64c601;font-size:16px;">■</span>ID・PWはご登録頂いたメールアドレスにご連絡致します。<br />
<span style="color:#64c601;font-size:16px;">■</span>フリーメールアドレスはご利用頂けない場合がございます。<br />
</span>
-->
</div>        
                 <div class="mod_form_btn">
                    
	<div style="margin-top:20px;margin-left:400px;">
            <!--<a class="button" href="javascript:void(0)" onclick="javascript:history.back();" id="">戻る</a>-->
	    {if $app.data.act == "furufuru_trial" && $app.db_regist_result != "-1" && $app.mail_send_result != "-1" && $app.auto_login_result != "-1"}
		<a class="button2" href="javascript:void(0)" id="to_thanks_btn" onclick="document.f2.submit();" >お試し依頼を確定する</a>
	    {else}
        	<a class="button2" href="index.php" id="to_top_btn">TOPへ</a>
	    {/if}
        </div>
                </div>
</div>
        
</form>        
	<!-- ここまで入力フォーム -->
</div>
<!-- ここまでメインコンテンツ -->

</div>
