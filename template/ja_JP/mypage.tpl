{literal}
<script type="text/javascript">
function do_dl(no){
    document.dl.in_request_id.value = no;
    document.dl.in_step.value = 'csv_dl';
    document.dl.submit();
}
function go_estimate(no){
    document.estimate_detail.in_request_id.value = no;
    document.estimate_detail.submit();
}
function go_detail(no,list_type){
	document.list_detail.in_request_id.value = no;
	document.list_detail.in_list_type.value = list_type;
	document.list_detail.submit();
}
</script>
{/literal}
{include file=drew_dialog.tpl}
<div class="top_header_title" style="margin-top:20px;"><span class="top_header_title_border">マイページ</span></div>
<div style="font-size:80%;">
<div class="head_title_bgcolor" style="text-align:left;">お客様情報</div>
<table width="100%" border="0" cellpadding="5" cellspacing="1">
    <tr>
	<td colspan="6" style="text-align:right;">
	<a class="change_btn" href="./index.php?action_edit_registration=true" id="delete_button">会員情報編集</a>
	</td>
    </tr>
    <tr>
        <td class="l_Cel_01_02" width="80px;">名前</td>
        <td class="s_Cel_01" colspan="3">
		<table>
		<tr>
		<td style="border:0;text-align:left;"><span style="font-size:60%;">{$app.data.contractor_lkana}</span></td>
		<td style="border:0;text-align:left;"><span style="font-size:60%;">{$app.data.contractor_fkana}</span></td>
		<td style="border:0;text-align:left;"></td>
		</tr>
		<tr>
		<td style="border:0;text-align:left;"><span style="font-size:150%;">{$app.data.contractor_lname}</span></td>
                <td style="border:0;text-align:left;"><span style="font-size:150%;">{$app.data.contractor_fname}</span></td>
		<td style="border:0;text-align:left;"><span style="font-size:150%;">様</span></td>
		</tr>
		</table>
        <td class="l_Cel_01_02" width="80px;">ナビットポイント</td>
        <td class="s_Cel_01"><span style="font-size:120%;">{$app.point|number_format}&nbsp;pt</span></td>
    </tr>
    <tr>
	<td class="l_Cel_01_02" width="80px;">会社名</td>
	<td class="s_Cel_01">{$app.data.company_name}</td>
	<td class="l_Cel_01_02" width="80px;">住所</td>
	<td class="s_Cel_01">{$app.data.zip1}-{$app.data.zip2}<br />{$app.data.address}</td>
	<td class="l_Cel_01_02" width="80px;">E-MAIL</td>
	<td class="s_Cel_01">{$app.data.email}</td>
    </tr>
    <tr>
	<td colspan="6" style="text-align:right;">
	<a class="withdrew_btn" href="javascript:void(0)" onclick="do_delete();" id="delete_button">退会</a>
	</td>
    </tr>
</table>
<br />
<br />
<br />
<div class="head_title_bgcolor" style="text-align:left;">リスト購入履歴</div>
<table width="100%" border="0" cellpadding="5" cellspacing="1">
    <tr>
    <td style="border:0;padding-top:15px;" colspan="4">
<form action="{$script}" name="estimate_detail" id="estimate_detail" method="POST">
    <input type="hidden" name="action_commission_registration" value="true" />
    <input type="hidden" name="in_request_id" id="in_request_id" value="" />
</form>
<form action="{$script}" name="dl" method="POST" id="dl">
    <input type="hidden" name="action_mypage" value="true" />
    <input type="hidden" name="in_request_id" id="in_request_id" value="" />
    <input type="hidden" name="in_step" id="in_step" value="" />
</form>
<form action="{$script}" name="list_detail" id="list_detail" method="POST">
    <input type="hidden" name="action_detail_list" value="true" />
    <input type="hidden" name="in_request_id" id="in_request_id" value="" />
    <input type="hidden" name="in_list_type" id="in_list_type" value="" />
</form>




<form action="{$script}" name="pager_m" method="POST" id="pager_m">
    <input type="hidden" name="action_mypage" value="true" />
    <input type="hidden" name="in_step" id="in_step" value="" />
    <input type="hidden" name="offset"     id="offset"     value="{$form.offset}">
</form>
<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
    </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02" width="130px;">法人番号・企業名</td>
        <td class="l_Cel_01_02" width="700px;">購入日時</td>
        <td class="l_Cel_01_02" width="60px;">付与情報</td>
        <td class="l_Cel_01_02" width="60px;">データ<br>確認可能なのは購入日時から7日以内です</td>
    </tr>
{if $app.counts == 0}
    <tr>
        <td class="s_Cel_01" colspan="5" align="center">対象履歴が見つかりません</td>
    </tr>
{else}
    {foreach from=$app.history key=k item=v name=l}
    {if $v.id != ""}
    <tr>
        <td class="{$v.style}" >
		{if $v.use_matching_phase == "trialone"}
        	{$v.list_name}<br />
	    {else}
		【ファイル名】<br />
        	{$v.list_name}<br />
		【総件数】{$v.list_row}&nbsp;件
	    {/if}
        </td>


        <td class="{$v.style}" >

		{$v.client_order}

        </td>


        <td class="{$v.style}" >
	    {if $v.use_matching_phase != "trial"}
		{foreach from=$v.property_name item=name}
        	    {$app.property_list.$name}<br />
		{/foreach}
	    {/if}
        </td>
        <td class="{$v.style}">
        	{if $v.flg_processed == 0}
				{if $v.use_matching_phase != "trial"}
					情報付与作業中
				{else}
	        	   		見積もり作成中
				{/if}
        	{elseif $v.flg_processed == 1}

    		{if $v.use_matching_phase == "trialone"}
    			{if $v.est_flag}
				<form action="{$script}" name="toc_{$v.id}" id="toc_{$v.id}" method="POST">
					<input type="hidden" name="action_trialoneconf" value="true" />
					<input type="hidden" name="corp_id" value="{$v.corp_id}" />
					<input type="hidden" name="req_id" value="{$v.req_id}" />
					<input type="hidden" name="pref_cd" value="{$v.pref_cd}">
					<input type="hidden" name="in_act" value="trialoneconf_m" />
				</form>
				<input type="button" onclick="document.toc_{$v.id}.submit();" value="見積もり確認" />
				{else}
					キャンセル済み
        	    {/if}
    	    {else}
    	    <input type="button" onclick="go_estimate({$v.id})" value="見積もり確認" />
    	    {/if}


        	{elseif $v.flg_processed == 2}
        		{if $v.use_matching_phase == "trialone"}
					{if $v.dl_flag}
					<form action="{$script}" name="tod_{$v.id}" id="tod_{$v.id}" method="POST">
					    <input type="hidden" name="id" value="{$v.id}" />
					    <input type="hidden" name="action_trlonedetail" value="true" />
					    <input type="hidden" name="in_request_id" id="in_request_id" value="{$v.corp_id}" />
					    <input type="hidden" name="pref_cd" value="{$v.pref_cd}">
					    <input type="hidden" name="in_act" id="in_list_type" value="trlonedetail" />
					</form>

					<input type="button" onclick="document.tod_{$v.id}.submit();" value="詳細確認" />
					{/if}
				{elseif $v.use_matching_phase != "trial"}
					<input type="button" onclick="go_detail({$v.id},{if $v.use_matching_phase == ""}'manual'{else}'auto'{/if});" value="詳細確認" /><br /><br />
				{else}
	        	   		購入済み<br />
				{/if}

			 	{if $v.dl_flag}
			 		{if $v.use_matching_phase != "trialone"}
	            			<input type="button" onclick="do_dl({$v.id});" value="ダウンロード" />
	            	{/if}
				{else}
					{if $v.use_matching_phase != "trial"}
						購入済み
					{/if}
				{/if}
			{elseif $v.flg_processed == 3}
				{if $v.use_matching_phase == "trialone"}
	    			{if $v.est_flag}
					<form action="{$script}" name="toc_{$v.id}" id="toc_{$v.id}" method="POST">
						<input type="hidden" name="action_trialoneconf" value="true" />
						<input type="hidden" name="corp_id" value="{$v.corp_id}" />
						<input type="hidden" name="req_id" value="{$v.req_id}" />
						<input type="hidden" name="pref_cd" value="{$v.pref_cd}">
						<input type="hidden" name="in_act" value="trialoneconf_m" />
					</form>
					<input type="button" onclick="document.toc_{$v.id}.submit();" value="見積もり確認" />
					{else}
						キャンセル済み
	        	    {/if}
    	    	{/if}

        	{elseif $v.flg_processed == 9}
        	    キャンセル済み
        	{/if}
        </td>
    </tr>
    {/if}
    {/foreach}
{/if}
<tr>
<td colspan="4">
<form action="{$script}" name="pager" method="POST" id="pager">
<input type="hidden" name="action_mypage" value="true" />
<input type="hidden" name="step" id="step" value="" />
<input type="hidden" name="offset"     id="offset"     value="{$form.offset}">
<!--ページャ-->
{$app_ne.pager}
<!--ページャ-->
</form>
</td>
</tr>
</table>
</div>
</div>
<!-- ここまでメインコンテンツ -->
</div>
