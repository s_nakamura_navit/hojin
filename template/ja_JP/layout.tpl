<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>法人番号から住所電話番号などの顧客属性付与なら法人番号検索 ｜ データ・リストの販売、調査代行ならナビット</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="法人番号を入れて検索することで、企業名、業種、FAX番号、上場区分、従業員数規模、売上高規模、資本金規模、設立年月、URL、メールアドレスまで情報が付与することができます。企業と企業属性を提供するサービスになります。">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="法人番号,企業属性">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="stylesheet" href="css/furufuru.css" type="text/css">
<link rel="stylesheet" type="text/css" href="css/loading.css" />
{if $smarty.server.REQUEST_URI == "/service/furufuru/index.php?action_secure_registration=true"
    || $smarty.server.REQUEST_URI == "/service/furufuru/index.php?action_login=true"
    || $smarty.server.REQUEST_URI == "/service/furufuru/index.php?action_password_registration=true"
    || $smarty.server.REQUEST_URI == "/service/furufuru/index.php?action_inquiry_registration=true"
}
<meta name="robots" content="noindex" />
{/if}
{literal} 
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/jquery.balloon.min.js"></script>
<script type="text/javascript" src="js/jquery.lightbox_me.js"></script>
<link rel="stylesheet" href="css/lity.css" type="text/css" />
<script type="text/javascript" src="js/lity.js" defer=”defer”></script>

<script type="text/javascript">
//吹き出し
$('label').balloon();

function doblur() { 
    var element = document.getElementById("name"); 
    element.blur(); 
} 

function do_submit(){
    document.f2.submit();
}

function do_submit_with_param(param){
    document.f2.in_callkind.value = param;
    document.f2.submit();
}
function calc_proc(){
	var i;
	var total = 0;
	var s = "";
	
	$("[id^=id_item_]").each(function(i, elem) {
		if ( $(elem).prop('checked') ){
			total = total + Number($(elem).val());
		}
	});
	s = total + "ポイント [ &yen;" + Math.floor(total * 1.08) + "(税込) ]";
	$("#total_price").html(s);
}
</script>
{/literal} 
</head>
<body>
    
{if $logined_name == ""}
    {include file=regist_dialog.tpl}
{/if}

<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
    <div id="wrapper">

<!-- ここからheader --><div id="01"></div>
        <div id="header">
                <div id="h1">
                    <h1>法人番号から住所電話番号などの顧客属性付与なら法人番号検索 ｜ データ・リストの販売、調査代行ならナビット</h1>
                </div>
            <div id="logo">
                <a href="index.php" ><img src="img/logo.jpg" alt="法人番号検索サービス"></a>
            </div>

            <div id="header_contact">

                <div id="mini_home">
				<img src="img/m_sitemap.gif" alt="ホーム" />
				<a href="index.php">ホーム</a>
			</div>

                <div id="mini_contact">
                    <img src="img/m_contact.gif" alt="お問合せ" />
                    <a href="./index.php?action_inquiry_registration=true">お問合せ</a>
                </div>
                    <div id="mini_sitemap">
				<img src="img/m_sitemap.gif" alt="運営会社" />
				<a href="http://www.navit-j.com/" target="_blank">運営会社</a>
			</div>
            </div>

            <div id="freedial">
                <img src="img/freedial.jpg" alt="0120-937-781" />
            </div>

            {if $logined_name !=""}
            <div style="display:inline-block;">
                <table  border="1" cellspacing="0" cellpadding="0" style="width:100%; border-color:#FF7400">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <span id="logininfo">{$logined_name} 様　ログイン中</span>   
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
					<a href="./index.php?action_mypage=true" id="user_contents_btn">マイページ</a>
					<a href="./index.php?action_request_registration=true" id="user_contents_btn">ポイント購入</a>
                                        <a href="./index.php?action_logout_do=true" id="user_contents_btn">ログアウト</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>    
            </div>
            {else}
            <div id="newaccount">
                <a href="./index.php?action_secure_registration=true"  id="registration"><img src="img/newaccount.jpg" onmouseover="this.src='img/newaccount_d.jpg'" onmouseout="this.src='img/newaccount.jpg'" alt="新規会員登録" width="160px" height="50px"></a>
            </div>
            <div id="login">
                <a href="./index.php?action_login=true" onclick="" id="login_btn"><img src="img/login.jpg" onmouseover="this.src='img/login_d.jpg'" onmouseout="this.src='img/login.jpg'" alt="ログイン" width="146px" height="50px"></a>
            </div>
            {/if}
        </div>
        <div class="clear"></div>
        <!-- ここまでheader -->

        {if $is_gnavi == true}
        <!-- ここからG NAVI -->
        <div id="gnavi">
            <ul>
                <li style="height:95px;">
                        <a href="https://www.navit-j.com/blog/?p=29983" target="_blank"><img src="img/gnavi01.jpg" onmouseover="this.src='img/gnavi01_d.jpg'" onmouseout="this.src='img/gnavi01.jpg'" alt="ご利用方法" width="145px" height="95px"></a>
                </li>
                <li style="height:95px;">
                        <a href="/service/attacknumber1/" target="_blank"><img src="img/gnavi02.jpg" onmouseover="this.src='img/gnavi02_d.jpg'" onmouseout="this.src='img/gnavi02.jpg'" alt="アタックナンバーワン" width="143px" height="95px" /></a>
                </li>
                <li style="height:95px;">
                        <a href="/service/openkun/" target="_blank"><img src="img/gnavi03.jpg" onmouseover="this.src='img/gnavi03_d.jpg'" onmouseout="this.src='img/gnavi03.jpg'" alt="オープン君" width="143px" height="95px"></a>
                </li>
                <li style="height:95px;">
                        <a href="/service/denwacho/" target="_blank"><img src="img/gnavi04.jpg" onmouseover="this.src='img/gnavi04_d.jpg'" onmouseout="this.src='img/gnavi04.jpg'" alt="法人電話帳" width="142px" height="95px"></a>
                </li>
                <li style="height:95px;">
                        <a href="/service/cleansing/" target="_blank"><img src="img/gnavi05.jpg" onmouseover="this.src='img/gnavi05_d.jpg'" onmouseout="this.src='img/gnavi05.jpg'" alt="住所チェッカー" width="143px" height="95px"></a>
                </li>
                <li style="height:95px;">
                        <a href="/service/fax/" target="_blank"><img src="img/gnavi06.jpg" onmouseover="this.src='img/gnavi06_d.jpg'" onmouseout="this.src='img/gnavi06.jpg'" alt="FAX番号リスト送信サービス" width="144px" height="95px"></a>
                </li>
                <li style="height:95px;">
                    {if $logined_name == ""}
                        <a href="/service/furufuru/" target="_blank"><img src="img/gnavi07.jpg" onmouseover="this.src='img/gnavi07_d.jpg'" onmouseout="this.src='img/gnavi07.jpg'" alt="" width="140px" height="95px"></a>
                    {else}
                        <a href="/service/furufuru/" target="_blank"><img src="img/gnavi07.jpg" onmouseover="this.src='img/gnavi07_d.jpg'" onmouseout="this.src='img/gnavi07.jpg'" alt="" width="140px" height="95px"></a>
                    {/if}
                </li>
            </ul>
        </div>
        <div class="clear"></div>
        <!-- ここまでG NAVI -->
        {else}
        <hr id="head-hr" color="#017680" noshade="">
        {/if}

        {if $is_himg == true}
        <!-- ここからメインビジュアル -->
<div id="main_vis">
	<a href="https://www.navit-j.com/blog/?p=29962" target="_blank"><img
	src="img/main_vis01.jpg"
	onmouseover="this.src='img/main_vis01_d.jpg'"
	onmouseout="this.src='img/main_vis01.jpg'"
	alt="法人番号検索の特徴"
	 border="0"/></a>
</div>

        <!-- ここまでメインビジュアル -->



        <div align="center" style="margin:0px 0 0px 0;">

<!--
            <a href="index.php?action_secure_registration=true"><img src="img/tokuten.jpg" onmouseover="this.src='img/tokuten_d.jpg'" onmouseout="this.src='img/tokuten.jpg'" alt="今なら新規登録で200ptプレゼント！！"></a>
-->
        </div>

<!--
      <div align="center" style="padding:20px 0 30px 0;">
		<a href="#service"><img src="img/btn_select01.png" onmouseover="this.src='img/btn_select01_d.png'" onmouseout="this.src='img/btn_select01.png'" alt="手動版（中2日）"></a>　　　　　
<img src="img/btn_select02.png" onmouseover="this.src='img/btn_select02.png'" onmouseout="this.src='img/btn_select02.png'" alt="全自動版（その場で付与）Comming Soon">
        </div>
-->

        {/if}

<!-- ここからメインコンテンツ -->

        {$content}
        
        {if $is_topbtn == true}
        <!-- ここからトップへ戻る 
	<div class="re_top">
            <a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear"></div>-->
        <!-- ここまでトップへ戻る -->
        {/if}
    </div>


<!-- 付加サービスバナー 

    <div style="width:1100px; margin:50px auto 30px;">

<a href="/service/fax/" target="_blank" /><img src="img/service_fax.png" onmouseover="this.src='img/service_fax_d.png'" onmouseout="this.src='img/service_fax.png'" alt="FAX送信サービス"></a>

<a href="ex_furu_use.html#price_dm"><img src="img/service_mail.png" onmouseover="this.src='img/service_mail_d.png'" onmouseout="this.src='img/service_mail.png'" alt="ダイレクトメール発送サービス"></a>

<a href="ex_furu_use.html#price_call"><img src="img/service_tel.png" onmouseover="this.src='img/service_tel_d.png'" onmouseout="this.src='img/service_tel.png'" alt="電話営業代行サービス"></a>

<a href="ex_furu_use.html#price_meishi"><img src="img/service_meishi.png" onmouseover="this.src='img/service_meishi_d.png'" onmouseout="this.src='img/service_meishi.png'" alt="名刺入力代行サービス"></a>
</div>
 付加サービスバナー -->


<!--<div style="text-align:center; margin:20px auto;">
	<a href="https://www.navit-j.com/contactus/" target="_blank">
		<img src="img/contact.jpg" onmouseover="this.src='img/contact_d.jpg'" onmouseout="this.src='img/contact.jpg'" alt="お見積り無料！お気軽にお問い合わせください！フリーダイヤル　0120-937-781"/>
	</a>
</div>-->

<p style="font-size:0.5em;text-align:center; margin:20px auto;">※法人番号検索サービスのデータは、弊社が保有する「法人電話帳データ」を元にしています。</br>
弊社ナビットでは名簿リストの買い取り、購入は行なっておりません。ご了承ください。</p>
</div>


<p style="font-size:14px; text-align:center; margin:20px auto 15px;">
法人番号からの、住所・電話番号・メールアドレスなどの顧客属性提供サービスなら、ナビットの「法人番号検索」にお任せください。<br />
法人番号を入れて検索することで、企業名、業種、FAX番号、上場区分、従業員数規模、売上高規模、資本金規模、設立年月、URL、<br />メールアドレスまで情報が付与することができます。企業と企業属性を提供するサービスになります。
</p>


<!-- ここまでメインコンテンツ -->
</div>


<!-- ここからfooter -->
<br />
<div id="footer_border">
<div id="footer">
<div id="footer_pmark">
        <a href="http://privacymark.jp/" target="_blank"><img src="img/10822604_05_75_JP.gif"></a>
</div>
<div id="footer_text">
    <ul>
<li>
	<a href="https://www.navit-j.com/" target="_blank">データ・リストの販売、調査代行ならナビット TOP</a>
	</li>
        <li>
        <a href="/privacy/index.html" target="_blank">個人情報保護方針</a>
        </li>
        <li>
        <a href="regional.html" target="_blank">利用規約</a>
        </li>
        <li>
        <a href="attention.html" target="_blank">特定商取引法</a>
        </li>
        <li>
        <a href="https://www.navit-j.com/" target="_blank">会社概要</a>
        </li>
    </ul>          
    <ul style="font-weight:bold;">
        <li>
        株式会社ナビット
        </li>
        <li>
        東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F
        </li>
    </ul>
</div>
</div>
</div>

<!-- ここまでfooter -->


</div>

<!-- スムーズスクロール -->
{literal}
<script>
$(function(){
	calc_proc();
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^="#"]').click(function() {
      // スクロールの速度
      var speed = 700; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});
</script>
<!-- スムーズスクロール -->
{/literal}

{if $is_google == true}
    {include file=system/google_analytics.tpl}
{/if}

{if $is_conv_lf==true}
    {include file=system/list_finder.tpl}
{/if}

{if $is_conv_jb==true}
{literal}
<!-- J&B conversion TAG -->
{/literal}
{/if}

{if $is_conv_google == true}
    {include file=system/google_conversion.tpl}
{/if}

{if $is_conv_google_prov == true}
    {include file=system/google_conversion_provisional.tpl}
{/if}

{if $is_conv_yahoo_prov == true}
    {include file=system/yahoo_conversion_provisional.tpl}
{/if}

{if $is_conv_google_real == true}
    {include file=system/google_conversion_real.tpl}
{/if}

{if $is_conv_yahoo_real == true}
    {include file=system/yahoo_conversion_real.tpl}
{/if}

{if $is_conv_yahoo == true}
    {include file=system/yahoo_conversion.tpl}
{/if}
<!--YDNサーチターゲティング-->
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'RU861UYHU8';
var yahoo_retargeting_label = '';
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"　defer=”defer”></script>


</body>
</html>
