{literal}
<script type="text/javascript">
<!--
function do_dl(){
    document.dl.in_step.value = 'csv_dl';
    document.dl.submit();
}
//-->
</script>
{/literal}

{include file=commission/dialog.tpl}
<div class="top_header_title" style="margin-top:20px;"><span class="top_header_title_border">依頼内容詳細</span></div>

<div class="table_com">

<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
    <tr>
	<td class="l_Cel_com">お預かりしたファイル</td>
	<td class="v_Cel_com">{$app.data.list_name}</td>
    </tr>
    <tr>
	<td class="l_Cel_com">お預かりした企業数</td>
	<td class="v_Cel_com">{$app.data.list_row}件</td>
    </tr>
    <tr>
	<td class="l_Cel_com">付与形態</td>
	<td class="v_Cel_com">{if $app.list_type == "manual"}手動付与{elseif $app.list_type == "auto"}自動付与（{$app.matching_phase_list[$app.data.created_estimate_by]}）{/if}</td>
    </tr>
</table>
<br />

<form action="{$script}" name="dl" method="POST" id="dl">
    <input type="hidden" name="action_mypage" value="true" />
    <input type="hidden" name="in_request_id" id="in_request_id" value="{$app.request_id}" />
    <input type="hidden" name="in_list_type" id="in_list_type" value="{$app.list_type}" />
    <input type="hidden" name="in_step" id="in_step" value="" />
</form>
     
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
    {foreach from=$app.use_property item=key}
    <tr>
        <td class="l_Cel_com">
		{$app.property_list.$key}<br />
		{$app.unit_point.$key}ポイント
	</td>
        <td class="v_Cel_com">{$app.count.$key|number_format}件</td>
        <td class="v_Cel_com">{$app.point.$key|number_format}ポイント</td>
    </tr>
    {/foreach}
    {if $app.point.base != 0}
    <tr>
        <td class="l_Cel_com">基本料金</td>
        <td class="v_Cel_com"></td>
        <td class="v_Cel_com"><span id="base_point">{$app.point.base|number_format}</span>ポイント</td>
    </tr>
    {/if}
<!--
    <tr>
        <td class="l_Cel_com">消費税{math equation="x * y" x=$app.point.tax_rate y=100}％</td>
        <td class="v_Cel_com"></td>
        <td class="v_Cel_com"><span id="tax_point">{$app.point.tax|number_format}</span>ポイント</td>
    </tr>
-->
    <tr>
        <td class="l_Cel_com">必要ポイント</td>
        <td class="v_Cel_com" style="border-right:0;"><!--span id="total_count">{if $app.total_count == ""}0{else}{$app.total_count|number_format}{/if}</span>件xx--></td>
        <td class="v_Cel_com" style="border-left:0;"><span id="total_point">{$app.point.total|number_format}</span>ポイント</td>
    </tr>
</table>
{if $app.use_property_check_err != ""}<br /><div style="margin-left: 120px; font-size:18px;"><span class="error">{$app.use_property_check_err}</span></div>{/if}
</div>
    <div style="width:750px;margin:20px 0 0 127px;text-align:center;">
		<a class="button" href="./index.php?action_mypage=true" id="back_btn">戻る</a>&nbsp;&nbsp;
{if $app.data.dl_flag}
		<a class="button" href="javascript:void(0)" id="back_btn" onclick="do_dl();">ダウンロード</a>
{/if}
    </div>
<!--
    <div class="mod_form_btn">
	<div style="margin-top:20px;margin-left:300px;">
		<a class="button" href="./index.php?action_mypage=true" id="back_btn">戻る</a>
	</div>
	<div style="margin-top:-46px;margin-left:520px;">
		<a class="button" href="javascript:void(0)" id="back_btn" onclick="do_dl();">ダウンロード</a>
	</div>
    </div>
-->
<br />
