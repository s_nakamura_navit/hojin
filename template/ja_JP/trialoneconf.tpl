{include file=getlist_dialog.tpl}
<script type="text/javascript" src="js/jquery.getlist_loading.js"></script>
<div class="head_title_bgcolor" style="text-align:left;margin-top:50px;">ご依頼内容の確認<span style="font-size:18px;margin-left:50px;">お客様の現在のポイント数：{$app.point.user|number_format}ポイント</span></div>
<br />


<div class="table">
    <table class="result_conf" border="0" cellpadding="5" cellspacing="1">
	<tr height="100">
            <td class="l_Cel">
                <div class="sub_head">
					掲載名
				</div>
            </td>

            <td colspan="2" class="r_Cel">
                    {$app.corp_detail.I_CORP}
            </td>
	</tr>


        <tr height="100">
            <td class="l_Cel">
                <div class="sub_head">属性付与項目</div>
            </td>

            <td colspan="2" class="r_Cel">
		{foreach from=$app.grant_item item=name}
			{$name.ttl}<br />
		{/foreach}
            </td>
	</tr>




<tr>
<td colspan="2">
<div id="detail">
<table width="100%" id="detail" style="border:1px solid #CE4C51">
<tr>
        <td colspan="2" style="font-size:18px; text-align:left;background-color:#008246; font-weight:bold; color:#fff;">必要ポイントの内訳</td>
</tr>
</tr>
        <td style="font-size:17px; font-weight:bold; text-align:left;">価格</td>
        <td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;"></td>
</tr>


{foreach from=$app.grant_item item=v}
<tr>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">{$v.ttl}</td>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$v.point}&nbsp;ポイント</td>
</tr>
{/foreach}


<tr>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">小計</td>
        <td colspan="1" class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.subtotal}&nbsp;ポイント</td>
</tr>
<tr>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">消費税８％</td>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.tax}&nbsp;ポイント</td>
</tr>
<tr>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">計</td>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.total}&nbsp;ポイント</td>
</tr>
</table>
</div>
</td>
</tr>


<tr>
<td class="l_Cel">
<div class="sub_head">お客様の<br />
現在のポイント数</div>
</td>

            <td colspan="2" class="r_Cel" style="text-align:right">
		{$app.point.user|number_format}&nbsp;ポイント
            </td>
	</tr>

        <tr>
            <td class="l_Cel" >
                <div class="sub_head">必要ポイント</div>
            </td>

            <td colspan="2" class="r_Cel" style="text-align:right">
		{$app.total}&nbsp;ポイント
            </td>
	</tr>

	{if $app.act == "no_point"}
        <tr>
            <td class="l_Cel">
                <div class="sub_head">不足ポイント</div>
            </td>

            <td colspan="2" class="r_Cel" style="text-align:right">
				{$app.point_lack}&nbsp;ポイント
            </td>
	</tr>
	{/if}






	<tr>
	    <td colspan="3">
                <hr />
                <div id="submit_btn">
		<form action="{$script}" name="f_back" method="POST" id="f_back" enctype="multipart/form-data">

		    <input type="hidden" id="action_trialone" name="action_trialone" value="true"/>
			<input type="hidden" id="in_act" name="in_act" value="trialone"/>
			<input type="hidden" name="corp_id" value="{$app.corp_detail.ID}">
			<input type="hidden" name="sc_name" value="{$app.sc_name}">
			<input type="hidden" name="sc_address" value="{$app.sc_address}">
			<input type="hidden" name="pref_cd" value="{$app.pref_cd}">
			<input type="hidden" name="city_cd" value="{$app.city_cd}">
			<input type="hidden" name="hit_type" value="{$app.hit_type}">
			{foreach from=$app.items key="idx" item=itm}
				<input type="hidden" name="items[{$idx}]" value="{$itm}">
			{/foreach}
		</form>


		<form action="{$script}" name="f_get" method="POST" id="f_get" enctype="multipart/form-data">
		    {if $app.act == "no_point"}
        		<input type="hidden" name="action_request_registration" value="true" />
		    	<input type="hidden" name="in_act" value="request_point_trialone" />
				<input type="hidden" name="corp_id" value="{$app.corp_detail.ID}">
				<input type="hidden" name="sc_name" value="{$app.sc_name}">
				<input type="hidden" name="sc_address" value="{$app.sc_address}">
				<input type="hidden" name="pref_cd" value="{$app.pref_cd}">
				<input type="hidden" name="city_cd" value="{$app.city_cd}">
		    {else}
        		<input type="hidden" name="action_trialonecomp" value="true" />
        		<input type="hidden" name="in_act" value="trialonecomp">
				{uniqid}
				<input type="hidden" name="corp_id" value="{$app.corp_detail.ID}">
				<input type="hidden" name="req_id" value="{$app.req_id}">
				<input type="hidden" name="sc_name" value="{$app.sc_name}">
				<input type="hidden" name="sc_address" value="{$app.sc_address}">
				<input type="hidden" name="pref_cd" value="{$app.pref_cd}">
				<input type="hidden" name="city_cd" value="{$app.city_cd}">
				<input type="hidden" name="hit_type" value="{$app.hit_type}">
				{foreach from=$app.items key="idx" item=itm}
					<input type="hidden" name="items[{$idx}]" value="{$itm}">
				{/foreach}
			{/if}
		</form>
		{if $app.page_f==1}
		<a class="button" href="javascript:void(0)" onclick="document.f_back.submit();" id="back_button">戻る</a>
		{/if}
			&nbsp;&nbsp;&nbsp;&nbsp;
		    {if $app.act == "no_point"}
                        <a class="button" href="javascript:void(0)" onclick="document.f_get.submit();" id="a_search">ポイント購入</a>
		    {else}
                        <a class="button" href="javascript:void(0)" onclick="document.f_get.submit();" id="a_search">購入する</a>
		    {/if}

                </div>
            </td>
	</tr>
    </table>
</div>

</form>
</div>