{literal}
<script type="text/javascript">
function do_submit_inquiry(kind){
    if(kind=='back'){
        document.inquiry_f2.submit();
    }else{
        document.inquiry_f3.submit();
    }

}
</script>
{/literal}
<div class="top_header_title" ><span class="top_header_title_border">ご入力内容の確認</span></div>
<div style="margin-left: 120px;">以下の内容でよろしければ、『送信』ボタンを押してください。<br />修正する場合は、『戻る』ボタンを押して該当箇所を修正してください。</div>
<div style="font-size:0.7em;margin-left: 120px;">
<!--
<span>
【注意事項】<br />
<span style="color:#64c601;font-size:16px;">■</span>ID・PWはご登録頂いたメールアドレスにご連絡致します。<br />
<span style="color:#64c601;font-size:16px;">■</span>フリーメールアドレスはご利用頂けない場合がございます。<br />
</span>
-->
</div>        
        
        
<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
    <tr>
        <td class="l_Cel_01_02" width="80px;">会社名</td>
        <td class="s_Cel_01">{$app.data.company_name}</td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">部署名</td>
        <td class="s_Cel_01">
            {$app.data.department_name}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">役職名</td>
        <td class="s_Cel_01">
            {$app.data.post_name}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">担当者名</td>
        <td class="s_Cel_01">
            {$app.data.contractor_lname}&nbsp;&nbsp;{$app.data.contractor_fname}
        </td>
    </tr>

<!--
    <tr>
        <td class="l_Cel_01_02">会社名かな</td>
        <td class="s_Cel_01">{$app.data.company_kana}</td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">支店名</td>
        <td class="s_Cel_01">{$app.data.branch_name}</td>
    </tr>
    -->
    <tr>
        <td class="l_Cel_01_02">郵便番号</td>
        <td class="s_Cel_01">{$app.data.zip1}－{$app.data.zip2}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">住所</td>
        <td class="s_Cel_01">{$app.data.address}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">電話番号</td>
        <td class="s_Cel_01">
            {$app.data.phone_no1}{$app.data.phone_no2}{$app.data.phone_no3}
        </td>
    </tr>
    <!--
    <tr>
        <td class="l_Cel_01_02">携帯電話番号</td>
        <td class="s_Cel_01">
            {$app.data.k_phone_no1}{$app.data.k_phone_no2}{$app.data.k_phone_no3}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">FAX番号</td>
        <td class="s_Cel_01">
            {$app.data.fax_no1}{$app.data.fax_no2}{$app.data.fax_no3}
        </td>
    </tr>
    
    <tr>
        <td class="l_Cel_01_02">担当者名かな</td>
        <td class="s_Cel_01">
            {$app.data.contractor_lkana}&nbsp;&nbsp;{$app.data.contractor_fkana}
        </td>
    </tr>
    -->
    <tr>
        <td class="l_Cel_01_02">メールアドレス</td>
        <td class="s_Cel_01">
            {$app.data.email}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">お問い合わせ内容</td>
        <td class="s_Cel_01">
            {$app.data.inquiry|nl2br}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">個人情報取扱い同意書への同意</td>
        <td class="s_Cel_01">
            同意します
        </td>
    </tr>
    <tr>
        <td colspan="3">
                <!-- ここからトップへ戻る -->
	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
                <!-- ここまでトップへ戻る -->
        </td>
    </tr>
</table>
</div>
                <div class="mod_form_btn">
                    
        <div style="margin-top:20px;margin-left:280px;">
            <form action="{$script}" name="inquiry_f2" method="POST" id="inquiry_f2">
                <input type="hidden" name="action_inquiry_registration" value="true">
                <a class="button" href="javascript:void(0)" onclick="javascript:do_submit_inquiry('back');" id="back_btn">戻る</a>
                    <input type="hidden" name="in_company_name" id="in_company_name" value="{$app.data.company_name}">
                    <input type="hidden" name="in_company_kana" id="in_company_kana" value="{$app.data.company_kana}">
                    <input type="hidden" name="in_branch_name" id="in_branch_name" value="{$app.data.branch_name}"> 
                    <input type="hidden" name="in_zip1" id="in_zip1" value="{$app.data.zip1}">
                    <input type="hidden" name="in_zip2" id="in_zip2" value="{$app.data.zip2}"> 
                    <input type="hidden" name="in_address" id="in_address" value="{$app.data.address}"> 
                    <input type="hidden" name="in_phone_no1" id="in_phone_no1" value="{$app.data.phone_no1}">
                    <input type="hidden" name="in_phone_no2" id="in_phone_no2" value="{$app.data.phone_no2}"> 
                    <input type="hidden" name="in_phone_no3" id="in_phone_no3" value="{$app.data.phone_no3}"> 
                    <input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="{$app.data.k_phone_no1}">
                    <input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="{$app.data.k_phone_no2}">
                    <input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="{$app.data.k_phone_no3}">
                    <input type="hidden" name="in_fax_no1" id="in_fax_no1" value="{$app.data.fax_no1}">
                    <input type="hidden" name="in_fax_no2" id="in_fax_no2" value="{$app.data.fax_no2}"> 
                    <input type="hidden" name="in_fax_no3" id="in_fax_no3" value="{$app.data.fax_no3}"> 
                    <input type="hidden" name="in_department_name" id="in_department_name" value="{$app.data.department_name}"> 
                    <input type="hidden" name="in_post_name" id="in_post_name" value="{$app.data.post_name}"> 
                    <input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="{$app.data.contractor_lname}">
                    <input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="{$app.data.contractor_fname}">
                    <input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="{$app.data.contractor_lkana}">
                    <input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="{$app.data.contractor_fkana}"> 
                    <input type="hidden" name="in_email" id="in_email" value="{$app.data.email}">
                    <input type="hidden" name="in_inquiry" id="in_inquiry" value="{$app.data.inquiry}">
                    <input type="hidden" name="back" id="back" value="1"> 
                {if $app.get_csv == "yet"}
                    <input type="hidden" name="in_matching" value="{$app.matching}" />
                    <input type="hidden" name="in_unmatch" value="{$app.unmatch}" />
                    <input type="hidden" name="in_unmatch_mobile" value="{$app.unmatch_mobile}" />
                    <input type="hidden" name="in_unmatch_num_err" value="{$app.unmatch_num_err}" />
                    <input type="hidden" name="in_unmatch_blank" value="{$app.unmatch_blank}" />
                    <input type="hidden" name="in_tel_value_line" value="{$app.tel_value_line}" />
                    <input type="hidden" name="in_header_check" value="{$app.header_check}" />
                    <input type="hidden" name="in_header_line" value="{$app.header_line}" />
                    <input type="hidden" name="in_latlon_type" value="{$app.latlon_type}" />
                    <input type="hidden" name="in_list_name" value="{$app.list_name}" />
                    <input type="hidden" name="in_use_point" value="{$app.use_point}" />
                    <input type="hidden" name="in_list_count" value="{$app.list_count}" />
                    <input type="hidden" name="in_count_get" value="{$app.count_get}" />
                    <input type="hidden" name="in_lv_7_count"value="{$app.lv_7_count}" />
                    <input type="hidden" name="in_lv_5_count"value="{$app.lv_5_count}" />
                    <input type="hidden" name="in_lv_3_count"value="{$app.lv_3_count}" />
                    <input type="hidden" name="in_get_csv" value="{$app.get_csv}" />
                    <input type="hidden" name="in_up_file_path" value="{$app.up_file_path}" />
                {/if}
            </form>
            </div>
        <div style="margin-top:-46px;margin-left:500px;">
            <form action="{$script}" name="inquiry_f3" method="POST" id="inquiry_f3">
                <input type="hidden" name="action_inquiry_complete" value="true">
                <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit_inquiry('send');" id="send_btn">送信</a>
                    <input type="hidden" name="in_company_name" id="in_company_name" value="{$app.data.company_name}">
                    <input type="hidden" name="in_company_kana" id="in_company_kana" value="{$app.data.company_kana}">
                    <input type="hidden" name="in_branch_name" id="in_branch_name" value="{$app.data.branch_name}"> 
                    <input type="hidden" name="in_zip1" id="in_zip1" value="{$app.data.zip1}">
                    <input type="hidden" name="in_zip2" id="in_zip2" value="{$app.data.zip2}"> 
                    <input type="hidden" name="in_address" id="in_address" value="{$app.data.address}"> 
                    <input type="hidden" name="in_phone_no1" id="in_phone_no1" value="{$app.data.phone_no1}">
                    <input type="hidden" name="in_phone_no2" id="in_phone_no2" value="{$app.data.phone_no2}"> 
                    <input type="hidden" name="in_phone_no3" id="in_phone_no3" value="{$app.data.phone_no3}"> 
                    <input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="{$app.data.k_phone_no1}">
                    <input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="{$app.data.k_phone_no2}">
                    <input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="{$app.data.k_phone_no3}">
                    <input type="hidden" name="in_fax_no1" id="in_fax_no1" value="{$app.data.fax_no1}">
                    <input type="hidden" name="in_fax_no2" id="in_fax_no2" value="{$app.data.fax_no2}"> 
                    <input type="hidden" name="in_fax_no3" id="in_fax_no3" value="{$app.data.fax_no3}"> 
                    <input type="hidden" name="in_department_name" id="in_department_name" value="{$app.data.department_name}"> 
                    <input type="hidden" name="in_post_name" id="in_post_name" value="{$app.data.post_name}"> 
                    <input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="{$app.data.contractor_lname}">
                    <input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="{$app.data.contractor_fname}">
                    <input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="{$app.data.contractor_lkana}">
                    <input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="{$app.data.contractor_fkana}"> 
                    <input type="hidden" name="in_email" id="in_email" value="{$app.data.email}">
                    <input type="hidden" name="in_inquiry" id="in_inquiry" value="{$app.data.inquiry}">
                    <input type="hidden" name="back" id="back" value="0"> 
                {if $app.get_csv == "yet"}
                    <input type="hidden" name="in_matching" value="{$app.matching}" />
                    <input type="hidden" name="in_unmatch" value="{$app.unmatch}" />
                    <input type="hidden" name="in_unmatch_mobile" value="{$app.unmatch_mobile}" />
                    <input type="hidden" name="in_unmatch_num_err" value="{$app.unmatch_num_err}" />
                    <input type="hidden" name="in_unmatch_blank" value="{$app.unmatch_blank}" />
                    <input type="hidden" name="in_tel_value_line" value="{$app.tel_value_line}" />
                    <input type="hidden" name="in_header_check" value="{$app.header_check}" />
                    <input type="hidden" name="in_header_line" value="{$app.header_line}" />
                    <input type="hidden" name="in_latlon_type" value="{$app.latlon_type}" />
                    <input type="hidden" name="in_list_name" value="{$app.list_name}" />
                    <input type="hidden" name="in_use_point" value="{$app.use_point}" />
                    <input type="hidden" name="in_list_count" value="{$app.list_count}" />
                    <input type="hidden" name="in_count_get" value="{$app.count_get}" />
                    <input type="hidden" name="in_lv_7_count"value="{$app.lv_7_count}" />
                    <input type="hidden" name="in_lv_5_count"value="{$app.lv_5_count}" />
                    <input type="hidden" name="in_lv_3_count"value="{$app.lv_3_count}" />
                    <input type="hidden" name="in_get_csv" value="{$app.get_csv}" />
                    <input type="hidden" name="in_up_file_path" value="{$app.up_file_path}" />
                {/if}
            </form>
        </div>
                </div>
</div>
        
        

        
        
	<!-- ここまで入力フォーム -->









</div>
<!-- ここまでメインコンテンツ -->
