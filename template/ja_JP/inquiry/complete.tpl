<!-- Google Code for &#12405;&#12427;&#12405;&#12427;&#12469;&#12540;&#12499;&#12473; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 959001386;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "ZF2hCJj5lWcQquakyQM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/959001386/?label=ZF2hCJj5lWcQquakyQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Yahoo Code for your Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var yahoo_conversion_id = 1000164359;
    var yahoo_conversion_label = "2g6WCMWUpmcQjbuDygM";
    var yahoo_conversion_value = 0;
    /* ]]> */
</script>
<script type="text/javascript" src="//s.yimg.jp/images/listing/tool/cv/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//b91.yahoo.co.jp/pagead/conversion/1000164359/?value=0&label=2g6WCMWUpmcQjbuDygM&guid=ON&script=0&disvt=true"/>
    </div>
</noscript>


<!-- YDN Conversion tag -->
<script type="text/javascript" language="javascript">
  /* <![CDATA[ */
  var yahoo_ydn_conv_io = "B7B.fbEOLDUYvHlT5mBp";
  var yahoo_ydn_conv_label = "VOHD08SCS9TN2HEDKLCK15671";
  var yahoo_ydn_conv_transaction_id = "";
  var yahoo_ydn_conv_amount = "0";
  /* ]]> */
</script>
<script type="text/javascript" language="javascript" charset="UTF-8" src="//b90.yahoo.co.jp/conv.js"></script>




<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_secure_complete" value="true">
{if $app.db_regist_result == "-1" || $app.mail_send_result == "-1"}
<div class="top_header_title" ><span style="border-bottom:1px #562E39 solid;">お問い合わせ送信エラー</span></div>
<div style="text-align:center;">
    <span style="font-weight:bold;font-size:100%;">ご入力頂いた内容で送信ができませんでした。</span><br />
    <br />
    大変お手数ではございますが再度こちらから再送信をお願い致します。>> 
    <a href="./index.php?action_inquiry_registration=true"> お問い合わせフォームへ</a><br />
</div>
      <br />
      <br />
<div style="margin-left: 180px;">
      <span style="color:#666;font-size:80%;">
        【ご登録に失敗する場合、以下の原因が考えられます】<br />
        ※通信環境の混雑等の理由によるもの<br /><br />
        ※ご登録頂いたメールアドレスへの送信失敗によるもの(ご登録頂いたメールアドレスをご確認ください)<br />
        <br />
        <br />
      </span>  
</div>
{else}
<div class="top_header_title" ><span class="top_header_title_border">お問い合わせ送信完了</span></div>
<div style="text-align:center;">
    <span style="font-weight:bold;font-size:100%;">お問い合わせを受け付けました。</span><br />
    <br />
</div>
<div style="margin-left: 280px;">
    弊社より、あらためてご連絡させていただきますので、<br />今しばらくお待ちください。<br />
    <br />
    また、ご入力頂いたメールアドレスに確認メールを送信致しました。<br />受信のご確認をお願い致します。<br />
</div>
      <br />
      <br />
<div style="margin-left: 150px;">
      <span style="color:#666;font-size:80%;">
        ※通信環境の混雑等の理由によりメールの到着に少々お時間がかかる場合がございます。<br /><br />
        ※迷惑メール防止機能により当サイトからのメールが迷惑メールと間違えられ、メール受信画面に表示されない場合が<br />
        &nbsp;&nbsp;&nbsp;ございます。迷惑メールフォルダやゴミ箱に自動的に振り分けられている場合がございますので、一度ご確認頂きま<br />
        &nbsp;&nbsp;&nbsp;すようお願い致します。<br />
      
      </span>  
</div>
{/if}
<div style="font-size:0.7em;margin-left: 120px;">
<!--
<span>
【注意事項】<br />
<span style="color:#64c601;font-size:16px;">■</span>ID・PWはご登録頂いたメールアドレスにご連絡致します。<br />
<span style="color:#64c601;font-size:16px;">■</span>フリーメールアドレスはご利用頂けない場合がございます。<br />
</span>
-->
</div>        
        
<!--        
<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
    <tr>
        <td class="l_Cel_01_01">会社名</td>
        <td class="s_Cel">{$app.data.company_name}</td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">会社名かな</td>
        <td width="550" class="s_Cel">{$app.data.company_kana}</td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">支店名</td>
        <td width="550" class="s_Cel">{$app.data.branch_name}</td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">郵便番号</td>
        <td width="550" class="s_Cel">{$app.data.zip1}－{$app.data.zip2}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">住所</td>
        <td width="550" class="s_Cel">{$app.data.address}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">電話番号</td>
        <td width="550" class="s_Cel">
            {$app.data.phone_no1}{$app.data.phone_no2}{$app.data.phone_no3}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">携帯電話番号</td>
        <td width="550" class="s_Cel">
            {$app.data.k_phone_no1}{$app.data.k_phone_no2}{$app.data.k_phone_no3}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">FAX番号</td>
        <td width="550" class="s_Cel">
            {$app.data.fax_no1}{$app.data.fax_no2}{$app.data.fax_no3}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">部署名</td>
        <td width="550" class="s_Cel">
            {$app.data.department_name}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">役職名</td>
        <td width="550" class="s_Cel">
            {$app.data.post_name}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">担当者名</td>
        <td width="550" class="s_Cel">
            {$app.data.contractor_lname}&nbsp;&nbsp;{$app.data.contractor_fname}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">担当者名かな</td>
        <td width="550" class="s_Cel">
            {$app.data.contractor_lkana}&nbsp;&nbsp;{$app.data.contractor_fkana}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">メールアドレス</td>
        <td width="550" class="s_Cel">
            {$app.data.email}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">利用規約への同意</td>
        <td width="550" class="s_Cel">
            同意します
        </td>
    </tr>
    <tr>
        <td colspan="3">

	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
 
        </td>
    </tr>
</table>
</div>
-->
                <div class="mod_form_btn">
                    
	<div style="margin-top:20px;margin-left:400px;">
            <!--<a class="button" href="javascript:void(0)" onclick="javascript:history.back();" id="">戻る</a>-->
        {if $app.get_csv == "yet"}
            <form action="{$script}" name="f2" method="POST" id="f2">
            <input type="hidden" name="action_confirmation" value="true">

            <input type="hidden" name="in_matching_count" value="{$app.matching}" />
            <input type="hidden" name="in_unmatch_count" value="{$app.unmatch}" />
            <input type="hidden" name="in_unmatch_mobile" value="{$app.unmatch_mobile}" />
            <input type="hidden" name="in_unmatch_num_err" value="{$app.unmatch_num_err}" />
            <input type="hidden" name="in_unmatch_blank" value="{$app.unmatch_blank}" />
            <input type="hidden" name="in_tel_value_line" value="{$app.tel_value_line}" />
            <input type="hidden" name="in_header_check" value="{$app.header_check}" />
            <input type="hidden" name="in_header_line" value="{$app.header_line}" />
            <input type="hidden" name="in_latlon_type" value="{$app.latlon_type}" />
            <input type="hidden" name="in_list_name" value="{$app.list_name}" />
            <input type="hidden" name="in_use_point" value="{$app.use_point}" />
            <input type="hidden" name="in_list_count" value="{$app.list_count}" />
            <input type="hidden" name="in_count_get" value="{$app.count_get}" />
            <input type="hidden" name="in_lv_7_count"value="{$app.lv_7_count}" />
            <input type="hidden" name="in_lv_5_count"value="{$app.lv_5_count}" />
            <input type="hidden" name="in_lv_3_count"value="{$app.lv_3_count}" />
            <input type="hidden" name="in_get_csv" value="{$app.get_csv}" />
            <input type="hidden" name="in_up_file_path" value="{$app.up_file_path}" />
            <input type="hidden" name="in_act" value="back" />

            </form>
            <a class="button" href="javascript:void(0)" onclick="document.f2.submit();" id="">戻る</a>
        {else}
            <a class="button2" href="index.php" id="to_top_btn">TOPへ</a>
        {/if}
        </div>
                </div>
</div>
        
        
<input type="hidden" name="in_company_name" id="in_company_name" value="{$app.data.company_name}">
<input type="hidden" name="in_company_kana" id="in_company_kana" value="{$app.data.company_kana}">
<input type="hidden" name="in_branch_name" id="in_branch_name" value="{$app.data.branch_name}"> 
<input type="hidden" name="in_zip1" id="in_zip1" value="{$app.data.zip1}">
<input type="hidden" name="in_zip2" id="in_zip2" value="{$app.data.zip2}"> 
<input type="hidden" name="in_address" id="in_address" value="{$app.data.address}"> 
<input type="hidden" name="in_phone_no1" id="in_phone_no1" value="{$app.data.phone_no1}">
<input type="hidden" name="in_phone_no2" id="in_phone_no2" value="{$app.data.phone_no2}"> 
<input type="hidden" name="in_phone_no3" id="in_phone_no3" value="{$app.data.phone_no3}"> 
<input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="{$app.data.k_phone_no1}">
<input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="{$app.data.k_phone_no2}">
<input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="{$app.data.k_phone_no3}">
<input type="hidden" name="in_fax_no1" id="in_fax_no1" value="{$app.data.fax_no1}">
<input type="hidden" name="in_fax_no2" id="in_fax_no2" value="{$app.data.fax_no2}"> 
<input type="hidden" name="in_fax_no3" id="in_fax_no3" value="{$app.data.fax_no3}"> 
<input type="hidden" name="in_department_name" id="in_department_name" value="{$app.data.department_name}"> 
<input type="hidden" name="in_post_name" id="in_post_name" value="{$app.data.post_name}"> 
<input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="{$app.data.contractor_lname}">
<input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="{$app.data.contractor_fname}">
<input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="{$app.data.contractor_lkana}">
<input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="{$app.data.contractor_fkana}"> 
<input type="hidden" name="in_email" id="in_email" value="{$app.data.email}">
<input type="hidden" name="in_email" id="in_from" value="{$app.data.from}">
<!--<input type="hidden" name="in_mailmaga" id="in_mailmaga" value="{$app.data.mailmaga}">-->
<input type="hidden" name="in_privacy" id="in_privacy" value="{$app.data.privacy}">

<input type="hidden" name="back" id="back" value="0"> 
        
</form>        
	<!-- ここまで入力フォーム -->









</div>
<!-- ここまでメインコンテンツ -->
