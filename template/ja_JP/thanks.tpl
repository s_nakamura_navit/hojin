<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">{if $app.act == "furufuru_trial"}お試しサービスの {/if}ご依頼を受け付けました</div>

<br />

<div class="table">
    <table border="0" cellpadding="5" cellspacing="1" style="width:600px;margin-left:auto;margin-right:auto;">
	<tr>
            <td class="l_Cel">
		<span style="font-size:34px;">ご利用ありがとうございます。</span><br /><br />
		ご依頼内容は、<a href="./index.php?action_mypage=true">マイページ</a>にてご確認いただけます。<br /><br />
		{if $app.act == "furufuru_trial"}
			付与結果に関しましては
		{else}
                	付与可能件数や最終価格は
		{/if}
		マイページにてご連絡いたしますので、お待ちください。<br /><br />
            </td>
	</tr>

	<tr>
	    <td class="l_Cel">
                <hr />
                <div id="submit_btn">
                    <a class="button" href="index.php" id="back_button">TOPページヘ戻る</a>     
                    <!--a class="button" href="index.php?action_send=true" id="a_search">取得する</a-->     
                </div>
            </td>
	</tr>
    </table>
</div>

</div>
