{include file=forced_login_dialog.tpl}
<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_login_do" value="true">

<div class="top_header_title" ><span class="top_header_title_border">ログイン</span></div>
<div style="margin-left: 296px;">ログインIDとパスワードを入力してください。<br /></div>
<div style="font-size:0.7em;margin-left: 120px;">
    <span>
    </span>
</div>
<br />

<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 208px;">
    <tr>
        <td style="text-align:right;">ログインID</td>
        <td>
            <input type="text" name="in_id" value="{$app.data.id}"  size="30" maxlength="64" style="font-size:20px;width:300px;">

        </td>
    </tr>
    <tr>
        <td style="text-align:right;">パスワード</td>
        <td>
            <input type="password" name="in_pw"  value="" size="30" maxlength="64" style="font-size:20px;width:300px;">

        </td>
    </tr>
    <tr>
		<td style="text-align:right;vertical-align top;">文字認証</td>
		<td>
			<img src="index.php?action_captcha=true" style="margin-bottom:5px" id ='captcha'><br />
			<input type="text" name="in_captcha_code" size="15" maxlength="8" style="font-size:20px;width:300px;" /><br />
			<a href="#" onclick="document.getElementById('captcha').src = 'index.php?action_captcha=true' + Math.random(); return false">[画像変更]</a>
		</td>
	</tr>
    <tr>
        <td>
        </td>
        <td style="font-size:14px; text-align:left;">
		<a href="./index.php?action_password_registration=true">ログインIDやパスワードを忘れた場合はこちら</a>
        </td>
    </tr>

<tr>
	<table style="margin-left: 180px;">
		<tr>
			<td align="right" valign="top"><img alt="！" src="img/mark_01.png" width="25px" height="25px"/></td>
			<td><span style="font-size:20px; margin-top:20px;">ログインID、パスワード、ポイントは以下のサービスで共通利用できます。</span></td>
		</tr>
	</table>
</tr>

<tr>
	<table style="margin-left: 180px;">
		<tr>
			<td align="right" valign="top"><img alt="！" src="img/mark_01.png" width="25px" height="25px"/></td>
			<td><span style="font-size:20px; margin-top:20px;">２つ以上のサービスに同時ログインはできません。<br />一旦ログアウトしてからご利用ください。<br />不正データダウンロード防止措置ですのでご了承ください。</span></td>
		</tr>
	</table>
</tr>


    <tr>
        <td colspan=2>
            <div class="mod_form_btn">
                <div style="margin-top:20px;margin-left:410px;"><a class="button2" href="javascript:void(0)" onclick="javascript:do_submit();" id="login_btn">ログイン</a></div>
            </div>
        </td>
    </tr>
</table>
<!--エラーメッセージ-->
{if count($errors)}
<table class="error" align="center">
{foreach from=$errors item=error}
<tr class="error" style="color:red;"><td>{$error}</td></tr>
{/foreach}
</table>
{/if}
<!--エラーメッセージ-->
</div>

</div>
</form>
	<!-- ここまで入力フォーム -->
</div>
<!-- ここまでメインコンテンツ -->
