{$doctype}
{$html}
<head>
{$meta}
{$css}
{$js}
<title>{$title}</title>
{literal}
<link type="text/css" href="css/style.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<style type="text/css">
.pass {
	text-align: center;
	margin-top: 0px;
}
.toptitle {
	text-align: left;
	margin-top: 0px;
}
.title {
	color: #32CD32; 
        font-size: xx-large;
}
.backcolor_text {
    background-color:#191970;
    /*font-weight: bold;*/
    font-size: medium;
    color: #ffffff; 
}



</style>


<script type="text/javascript">
$(function() {
  $('#all_check').on("click",function(){
    $('.CheckList').prop("checked", true);
  });
});
$(function() {
  $('#all_clear').on("click",function(){
    $('.CheckList').prop("checked", false);
  });
});


function changeCity(sel){

    var pref_code = sel.options[sel.selectedIndex].value; 
    $.ajax({
        type: "GET",
        url: "./index.php?action_getcity=true",
        dataType : 'json',
        data: "in_prefcode="+pref_code,
        success: function( res )
        {
            $('#select_city').html('');//一度select内を空に
            for(var i=0; i<res.length; ++i){
                $('#select_city').append('<option id="city'+res[i].city_code+'" value="'+res[i].city_code+'">'+res[i].city_name+'</option>');
            }
        },
        error:function(XMLHttpRequest, textStatus, errorThrown)
        {
            alert('Error : '+ errorThrown);
        }
        
    });
}





</script>
{/literal}
</head>
<body>
<!--ヘッダー-->
{$app_ne.header}
<!--ヘッダー-->

<h3>

<table width="740" border="0" cellspacing="0" cellpadding="1">
  <tr>
    <td><p class="toptitle">助成金・補助金情報配信サービス「助成金なう」<br /><span class="title">助成金なう</span></p></td>
  </tr>
</table>
</h3>
<div></div>
<div class="image">
<div class="backcolor_text">&nbsp;&nbsp;&nbsp;検　索</div>
    <br />
    <div style="text-align:center; color:#32CD32;"><span style="text-align:center; margin:40px auto; font-size:20px;">自治体案件検索</span><br />Autonomy Search</div>
    <div><hr></div>
    
    <table width="740" >
        <tr>
            <form action="{$script}" name="f2" method="{$form_action}">
            <td width="739">
              
                
                
              <table border="0" cellpadding="2" cellspacing="0" id="table3" style="text-align:left; margin:40px auto; font-size:18px;">
                    
                    <tr>
                        <td>
                            種別
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            {$app_ne.radio.kind}
                            {if is_error('in_kind')}<br /><span class="error">{message name="in_kind"}</span>{/if}
                        </td>
                    </tr>
                    <tr><td height="60"></td></tr>
                    <tr>
                        <td>
                            エリア
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            
                            <select name="in_area1" class="select_font" onchange="changeCity(this);">{$app_ne.pulldown.area1}</select>
                            <select id='select_city' name="in_area2" class="select_font"><option value="">市区町村</option></select>
                            {if is_error('in_area1')}<br /><span class="error">{message name="in_area1"}</span>{/if}
                            
                        </td>
                    </tr>
                    <tr><td height="60"></td></tr>
                    <tr>
                        <td>
                            分野
                            <br /><br />
                            <a href="#"  id="all_check" class="css_button_s" style="padding:5px 20px 5px 20px;">全選択</a>
                            <a href="#"  id="all_clear" class="css_button_s" style="padding:5px 20px 5px 20px;">解　除</a>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>
                            <table style="text-align:left;">
                                <tr>
                                    <td style="width:240px;"><label><input class="CheckList" type="checkbox" name="in_field_90" id="in_field_90" value="90" {$app.data.field_90} />{$app.master_field.90}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_91" id="in_field_91" value="91" {$app.data.field_91} />{$app.master_field.91}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_92" id="in_field_92" value="92" {$app.data.field_92} />{$app.master_field.92}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_93" id="in_field_93" value="93" {$app.data.field_93} />{$app.master_field.93}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_94" id="in_field_94" value="94" {$app.data.field_94} />{$app.master_field.94}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_95" id="in_field_95" value="95" {$app.data.field_95} />{$app.master_field.95}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_96" id="in_field_96" value="96" {$app.data.field_96} />{$app.master_field.96}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_97" id="in_field_97" value="97" {$app.data.field_97} />{$app.master_field.97}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_98" id="in_field_98" value="98" {$app.data.field_98} />{$app.master_field.98}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_99" id="in_field_99" value="99" {$app.data.field_99} />{$app.master_field.99}</label></td>
                                </tr>
                                <tr>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_100" id="in_field_100" value="100" {$app.data.field_100} />{$app.master_field.100}</label></td>
                                    <td><label><input  class="CheckList" type="checkbox" name="in_field_101" id="in_field_101" value="101" {$app.data.field_101} />{$app.master_field.101}</label></td>
                                </tr>
                            </table>
                            
                            {if is_error('in_area1')}<br /><span class="error">{message name="in_area1"}</span>{/if}
                            
                        </td>
                    </tr>
                    
              </table>

                <div class="pass">
                    <table border="0" cellpadding="2" cellspacing="0" id="table3" style="text-align:center; margin:40px auto; font-size:18px;">
                    <tr>
                        <td>
                            <div>
                                <a href="#" class="css_button" onClick="javascript:do_submit();" style="padding:10px 80px 10px 80px;">戻る</a>
                            </div>
                        </td>
                        <td>
                            <div>
                                <a href="#" class="css_button" onClick="javascript:do_submit();" style="padding:10px 70px 10px 70px;">リセット</a>
                            </div>
                        </td>
                        <td>
                            <div>
                                <a href="#" class="css_button" onClick="javascript:do_submit();" style="padding:10px 80px 10px 80px;">検索</a>
                            </div>
                        </td>
                        
                    </tr>
                    </table>
                    <br />
                    <br />
              </div>
            </td>
            </form>
        </tr>
    </table>
</td>
</tr></table>



<br />
<br />
<br />
<br />
<br />
<br />
<br />





<div><img src="images/border1.gif" width="740" height="3" /></div>
<div><img src="images/bar.gif" width="740" height="5" /></div>

<table width="740" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="110" align="left" valign="middle"><font size="2"><p class="navit"><a href="http://www.navit-j.com/"><img src="images/navit_logo.gif" width="109" height="40" /></a></p></font></td>
    <td width="130" align="left" valign="middle"><font size="4">株式会社ナビット</font></td>
    <td width="420" align="right" valign="middle"><p>〒102-0074 東京都東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F</p></td>
  </tr>
</table>
<div align="right" valign="top">
        <p class="tel" valign="top"><img src="images/freedial.gif" width="100" height="20" alt="フリーダイアル"/> <font size="5">0120-937-781</font><br />受付時間10：00～19：00<br />（土日祝祭日休み）</p>
        <p></p>
</div>
<br />
<br />
<p align="center">Copyright (C) NAVIT CO.,LTD. All Rights Reserved. </p>
 

{$footer}
<!--フッター-->
{$app_ne.footer}
<!--フッター-->
</body>
</html>
