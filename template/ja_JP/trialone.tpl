{if $app.corp_detail != ""}
{include file=./regist_dialog.tpl}
{literal}
<script type="text/javascript">
     function submit_proc(){
		var checked = false;
		$("[id^=id_item_]").each(function(i, elem) {
			if ( $(elem).prop('checked') ){
				checked = true;
			}
		});
		if (checked){
			document.f_get.submit();
		}else{
			$("#error_dialog").lightbox_me({centered: true,closeSelector:'#send_confirm_close',overlayCSS:{background:'#D3C9BA',opacity: .8}});
		}
     }
</script>
{/literal}


<table class="result_table2">
	<tr>
		<th class="result_table_th2">
			掲載名
		</th>
		<th class="result_table_th2">
			所在地
		</th>
	</tr>
	<tr>
		<td>
			{$app.corp_detail.I_CORP}
		</td>
		<td>
			{$app.corp_detail.I_ADDR_ALL}
		</td>
	</tr>
</table>

<div class="search_result2">
			<b>付与属性項目の選択</b>
</div>
<form action="{$script}" name="f_get" method="POST" id="f_get" enctype="multipart/form-data">

<table class="result_table3">
	<tr>
		{if $app.hit_type == "corp_name"}
			<th class="result_table_th2">
				法人番号（マイナンバー）
			</th>
			<td class="result_table_td2">
				<input type="checkbox" id="id_item_1" name="items[corp_num]" value="{$app.point_list.corp_num}" {if $app.items.corp_num}checked{/if} onclick="calc_proc();" /><label for="id_item_1">{$app.point_list.corp_num}ポイント</label>
			</td>
		{/if}
	<tr>
		<th class="result_table_th2" width="50%">
			業種
		</th>
		<td class="result_table_td2" width="50%">
			{if $app.corp_detail.I_BUSINESS_CD_L_1 == "○"}
				<input type="checkbox" id="id_item_2" name="items[business]" value="{$app.point_list.business}" {if $app.items.business}checked{/if} onclick="calc_proc();" /><label for="id_item_2">{$app.point_list.business}ポイント</label>
			{else}
				-
			{/if}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			FAX番号
		</th>
		<td class="result_table_td2">
			{if $app.corp_detail.I_FAX == "○"}
				<input type="checkbox" id="id_item_3" name="items[fax]" value="{$app.point_list.fax}" {if $app.items.fax}checked{/if} onclick="calc_proc();" /><label for="id_item_3">{$app.point_list.fax}ポイント</label>
			{else}
				-
			{/if}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			上場区分
		</th>
		<td class="result_table_td2">
			{if $app.corp_detail.I_LISTED_TYPE == "○"}
				<input type="checkbox" id="id_item_4" name="items[listed_type]" value="{$app.point_list.listed_type}" {if $app.items.listed_type}checked{/if} onclick="calc_proc();" /><label for="id_item_4">{$app.point_list.listed_type}ポイント</label>
			{else}
				-
			{/if}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			従業員数規模
		</th>
		<td class="result_table_td2">
			{if $app.corp_detail.I_EMPLOYEE_DIV == "○"}
				<input type="checkbox" id="id_item_5" name="items[employee_div]" value="{$app.point_list.employee_div}" {if $app.items.employee_div}checked{/if} onclick="calc_proc();" /><label for="id_item_5">{$app.point_list.employee_div}ポイント</label>
			{else}
				-
			{/if}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			売上高規模
		</th>
		<td class="result_table_td2">
			{if $app.corp_detail.I_SALE_DIV == "○"}
				<input type="checkbox" id="id_item_6" name="items[sale_div]" value="{$app.point_list.sale_div}" {if $app.items.sale_div}checked{/if} onclick="calc_proc();" /><label for="id_item_6">{$app.point_list.sale_div}ポイント</label>
			{else}
				-
			{/if}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			資本金規模
		</th>
		<td class="result_table_td2">
			{if $app.corp_detail.I_CAPITAL_DIV == "○"}
				<input type="checkbox" id="id_item_7" name="items[capital_div]" value="{$app.point_list.capital_div}" {if $app.items.capital_div}checked{/if} onclick="calc_proc();" /><label for="id_item_7">{$app.point_list.capital_div}ポイント</label>
			{else}
				-
			{/if}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			設立年月
		</th>
		<td class="result_table_td2">
			{if $app.corp_detail.I_FOUNDING_DT == "○"}
				<input type="checkbox" id="id_item_8" name="items[founding_dt]" value="{$app.point_list.founding_dt}" {if $app.items.founding_dt}checked{/if} onclick="calc_proc();" /><label for="id_item_8">{$app.point_list.founding_dt}ポイント</label>
			{else}
				-
			{/if}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			URL
		</th>
		<td class="result_table_td2">
			{if $app.corp_detail.I_URL == "○"}
				<input type="checkbox" id="id_item_9" name="items[url]" value="{$app.point_list.url}" {if $app.items.url}checked{/if} onclick="calc_proc();" /><label for="id_item_9">{$app.point_list.url}ポイント</label>
			{else}
				-
			{/if}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			メールアドレス
		</th>
		<td class="result_table_td2">
			{if $app.corp_detail.I_MAILADDR == "○"}
				<input type="checkbox" id="id_item_10" name="items[mailaddr]" value="{$app.point_list.mailaddr}" {if $app.items.mailaddr}checked{/if} onclick="calc_proc();" /><label for="id_item_10">{$app.point_list.mailaddr}ポイント</label>
			{else}
				-
			{/if}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			電話番号
		</th>
		<td class="result_table_td2">
			{if $app.corp_detail.I_TEL == "○"}
				<input type="checkbox" id="id_item_11" name="items[tel]" value="{$app.point_list.tel}" {if $app.items.tel}checked{/if} onclick="calc_proc();" /><label for="id_item_11">{$app.point_list.tel}ポイント</label>
			{else}
				-
			{/if}
		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			金額
		</th>
		<td class="result_table_td2" id="total_price">
			0ポイント [ ¥0(税込) ]
		</td>
	</tr>
</table>

    {if $app.name != ""}
		<input type="hidden" name="action_trialoneconf" value="true" />
		<input type="hidden" name="corp_id" value="{$app.corp_detail.ID}" />
		{uniqid}
		<input type="hidden" name="sc_name" value="{$app.sc_name}" />
    	<input type="hidden" name="sc_address" value="{$app.sc_address}" />
    	<input type="hidden" name="pref_cd" value="{$app.pref_cd}">
		<input type="hidden" name="city_cd" value="{$app.city_cd}">
    	<input type="hidden" name="in_act" value="trialoneconf" />
    {else}
		<input type="hidden" name="action_secure_registration" value="true" />
    {/if}
   	<input type="hidden" name="hit_type" value="{$app.hit_type}" />
</form>
<form action="{$script}" name="f_back" method="POST" id="f_back" enctype="multipart/form-data">
    <input type="hidden" name="action_index" value="ture" />
    <input type="hidden" name="sc_name" value="{$app.sc_name}" />
    <input type="hidden" name="sc_address" value="{$app.sc_address}" />
    <input type="hidden" name="in_act" value="search_corp" />
</form>

	<div class="search_btn">
		<a class="button" href="javascript:void(0)" onclick="document.f_back.submit();" id="back_button">戻る</a>
				    &nbsp;&nbsp;&nbsp;&nbsp;
		{if $app.corp_detail.total > 0}
		<a class="button" href="javascript:void(0)" onclick="submit_proc();" id="a_search">購入する</a>
		{/if}
	</div>
{/if}

