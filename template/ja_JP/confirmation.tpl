{include file=getlist_dialog.tpl}
<script type="text/javascript" src="js/jquery.getlist_loading.js"></script>
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">ご依頼内容の確認<span style="font-size:18px;margin-left:50px;">お客様の現在のポイント数：{$app.point.user}ポイント</span></div>
<br />

<div class="table">
    <table border="0" cellpadding="5" cellspacing="1" style="width:600px;margin-left:auto;margin-right:auto;">
	<tr>
            <td class="l_Cel">
                <div class="sub_head">データ名</div>
            </td>

            <td colspan="2" class="r_Cel">
                    {$app.list_name}
            </td>
	</tr>

        <tr>
            <td colspan="3">
                    <hr />
            </td>
        </tr>

        <tr>
            <td class="l_Cel">
                <div class="sub_head">件数</div>
            </td>

            <td colspan="2" class="r_Cel">
                    {$app.count.list}&nbsp;件
            </td>
	</tr>

        <tr>
            <td colspan="3">
                    <hr />
            </td>
        </tr>

        <tr>
            <td class="l_Cel">
                <div class="sub_head">属性付与項目</div>
            </td>

            <td colspan="2" class="r_Cel">
		{foreach from=$app.property_check item=name}
			{$app.property_list.$name}
			<br />
		{/foreach}
            </td>
	</tr>

        <tr>
            <td colspan="3">
                    <hr />
            </td>
        </tr>

                                <tr><td colspan="2">
                                <div id="detail">
                                <table width="100%" id="detail" style="border:1px solid #666">
                                        <tr>
                                                <td colspan="2" style="font-size:18px; text-align:left;background-color:#666; font-weight:bold; color:#fff;">必要ポイントの内訳</td>
                                        </tr>
                                        </tr>
                                                <td style="font-size:17px; font-weight:bold; text-align:left;">価格</td>
                                                <td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;"></td>
                                        </tr>
					{foreach from=$app.property_check item=name}
                                        <tr>
                                                <td style="font-size:17px; font-weight:bold; text-align:left;">&nbsp;&nbsp;{$app.property_list.$name}</td>
						<td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.count.calculation_list}&nbsp;件&nbsp;&times;&nbsp;{$app.point.unit.$name}&nbsp;ポイント&nbsp;=&nbsp;{$app.point.property_total.$name}&nbsp;ポイント</th>
                                        </tr>
					{/foreach}
					{if $app.point.base != 0}
                                        <tr>
                                                <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">基本料金</td>
                                                <td colspan="1" class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.point.base}&nbsp;ポイント</td>
                                        </tr>
					{/if}
                                        <tr>
                                                <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">小計</td>
                                                <td colspan="1" class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.point.subtotal}&nbsp;ポイント</td>
                                        </tr>
                                        <tr>
                                                <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">消費税８％</td>
                                                <td colspan="1" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.point.tax}&nbsp;ポイント</td>
                                        </tr>
                                        <tr>
                                                <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">計</td>
                                                <td colspan="1" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.point.total}&nbsp;ポイント</td>
                                        </tr>
                                </table>
                                </div>
                                </td></tr>

        <tr>
            <td class="l_Cel">
                <div class="sub_head">お客様の<br />
現在のポイント数</div>
            </td>

            <td colspan="2" class="r_Cel">
		{$app.point.user}&nbsp;ポイント
            </td>
	</tr>

        <tr>
            <td class="l_Cel">
                <div class="sub_head">必要ポイント</div>
            </td>

            <td colspan="2" class="r_Cel">
		{$app.point.total}&nbsp;ポイント
            </td>
	</tr>

	{if $app.act == "no_point"}
        <tr>
            <td class="l_Cel">
                <div class="sub_head">不足ポイント</div>
            </td>

            <td colspan="2" class="r_Cel">
		{$app.point.lack}&nbsp;ポイント
            </td>
	</tr>
	{/if}

	<tr>
	    <td colspan="3">
                <hr />
                <div id="submit_btn">
		<form action="{$script}" name="f_back" method="POST" id="f_back" enctype="multipart/form-data">
		    <input type="hidden" name="action_index" value="ture" />
		    <input type="hidden" name="in_list_count" value="{$app.count.list}" />
		    <input type="hidden" name="in_header_check" value="{$app.header_check}" />
		    <input type="hidden" name="in_header_line" value="{$app.header_line}" />
		    <input type="hidden" name="in_list_name" value="{$app.list_name}" />
		    <input type="hidden" name="in_up_file_path" value="{$app.up_file_path}" />
                    {foreach from=$app.property_check item=property}
                        <input type="hidden" name="in_property_check[]" value="{$property}" />
                    {/foreach}
		    <input type="hidden" name="in_auto_check" value="off" />
		    <input type="hidden" name="in_act" value="back" />
		</form>
		<form action="{$script}" name="f_get" method="POST" id="f_get" enctype="multipart/form-data">
		    {if $app.act == "no_point"}
        		<input type="hidden" name="action_request_registration" value="true" />
		    	<input type="hidden" name="in_get_csv" value="yet" />
                    	{foreach from=$app.property_check item=property}
                            <input type="hidden" name="in_property_check[]" value="{$property}" />
                    	{/foreach}
		    	<input type="hidden" name="in_act" value="request_point" />
		    {else}
        		<input type="hidden" name="action_thanks" value="true" />
		    	<input type="hidden" name="in_get_csv" value="start" />
                        <input type="hidden" name="in_property_key" value="{$app.property_key}" />
                        <input type="hidden" name="in_property_name" value="{$app.property_name}" />
		    	<input type="hidden" name="in_act" value="request_estimate" />
		    {/if}
		    <input type="hidden" name="in_request_id" value="{$app.request_id}" />
		    <input type="hidden" name="in_header_check" value="{$app.header_check}" />
		    <input type="hidden" name="in_header_line" value="{$app.header_line}" />
		    <input type="hidden" name="in_list_name" value="{$app.list_name}" />
		    <input type="hidden" name="in_use_point" value="{$app.point.total}" />
		    <input type="hidden" name="in_list_count" value="{$app.count.list}" />
		    <input type="hidden" name="in_up_file_path" value="{$app.up_file_path}" />
		</form>
                    <a class="button" href="javascript:void(0)" onclick="document.f_back.submit();" id="back_button">戻る</a>     
			&nbsp;&nbsp;&nbsp;&nbsp;
		    {if $app.act == "no_point"}
                        <a class="button" href="javascript:void(0)" onclick="document.f_get.submit();" id="a_search">ポイント購入</a>     
		    {else}
                        <a class="button" href="javascript:void(0)" onclick="displayGetListForm();" id="a_search">このデータで依頼する</a>     
		    {/if}
<p>※購入しない場合は、見積もり内容確認時に「依頼を取り消す」ボタンをクリックしてください。<br />
お支払いただいているポイントを返却いたします。</p>
                </div>
            </td>
	</tr>
    </table>
</div>

</form>
</div>
