
{literal}
<script type="text/javascript">
//吹き出し
$(window).load(function(){
      $('label').balloon();

    if($('#privacy_1').length != 0){
        if ($('#privacy_1').is(':checked')) {
            $('#confirm_btn').css({opacity:"1",cursor:"pointer"}).removeAttr('disabled');
        } else {
            $('#confirm_btn').css({opacity:"0.5",cursor:"default"}).attr('disabled','disabled');
        }
    }

});



//全選択・全解除
$(function() {
    $('#all_check').on("click",function(){
        $('.CheckList').prop("checked", true);
    });
});
$(function() {
    $('#all_clear').on("click",function(){
        $('.CheckList').prop("checked", false);
    });
});

$(function() {
    $('#all_check2').on("click",function(){
        $('.CheckList2').prop("checked", true);
    });
});
$(function() {
    $('#all_clear2').on("click",function(){
        $('.CheckList2').prop("checked", false);
    });
});

$(function() {
    $('#all_check3').on("click",function(){
        $('.CheckList3').prop("checked", true);
    });
});
$(function() {
    $('#all_clear3').on("click",function(){
        $('.CheckList3').prop("checked", false);
    });
});

//リセット(1)
$(function() {
    $('#a_reset').on("click",function(){
        //ラジオボタン初期値セット
        $('#in_kind_1').prop("checked", true);
        $('#in_kind_2').prop("checked", false);
        //プルダウン初期値セット
        $('select[name="in_area1"]').val("");
        $('#in_area2').html('');//一度select内を空に
        $('#in_area2').append('<option id="city00000" value="'+''+'">'+'市区町村を選択'+'</option>');

        //チェックボックスクリア
        $('.CheckList').prop("checked", false);

        $("#a_reset").blur();
    });
});
//リセット(2)
$(function() {
    $('#f_reset').on("click",function(){
        //チェックボックスクリア
        $('.CheckList2').prop("checked", false);
        $('.CheckList3').prop("checked", false);
        $('#in_keyword').val("");
        $("#f_reset").blur();
    });
});

function doblur() {
    var element = document.getElementById("name");
    element.blur();
}

function do_submit(){
    if($("#privacy_1").length != 0){
        if ($('#privacy_1').is(':checked')) {
            document.f2.submit();
        }else{
            return false;
        }
    }else{
        document.f2.submit();
    }
}

/*
 　　全角->半角変換
 */
jQuery(function(){

    // 郵便番号の処理
    $('.zip-number').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});

        // 半角数字のみ残す
        var zenkakuDel = new String( hankaku ).match(/\d/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }

        $(this).val(zenkakuDel);
    });
    // 電話番号の処理
    $('.tel-number').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\－|\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});

        // 半角数字と+-のみ残す
        var zenkakuDel = new String( hankaku ).match(/\d|\-|\+/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }

        $(this).val(zenkakuDel);
    });

    // メールアドレスの処理
    $('.mail-address').change( function(){
        var zenkigou = "＠－ー＋＿．，、";
        var hankigou = "@--+_...";
        var data = $(this).val();
        var str = "";

        // 指定された全角記号のみを半角に変換
        for (i=0; i<data.length; i++)
        {
            var dataChar = data.charAt(i);
            var dataNum = zenkigou.indexOf(dataChar,0);
            if (dataNum >= 0) dataChar = hankigou.charAt(dataNum);
            str += dataChar;
        }
        // 定番の、アルファベットと数字の変換処理
        var hankaku = str.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
        $(this).val(hankaku);
    });

});
</script>


<script type="text/javascript">
//確認ボタンの有効無効
jQuery(function(){
    if($('#privacy_1').length != 0){
        $('#privacy_1').change(function(){
                if ($(this).is(':checked')) {
                        $('#confirm_btn').css({opacity:"1",cursor:"pointer"}).removeAttr('disabled');
                } else {
                        $('#confirm_btn').css({opacity:"0.5",cursor:"default"}).attr('disabled','disabled');
                }
        });
    }
});
</script>
{/literal}
<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_request_confirm" value="true">
{if $app.act == "request_point"}
    <input type="hidden" name="in_request_id" value="{$app.request_id}" />
    <input type="hidden" name="in_act" value="{$app.act}" />
    <input type="hidden" name="in_matching" value="{$app.matching}" />
    <input type="hidden" name="in_unmatch" value="{$app.unmatch}" />
    <input type="hidden" name="in_unmatch_mobile" value="{$app.unmatch_mobile}" />
    <input type="hidden" name="in_unmatch_num_err" value="{$app.unmatch_num_err}" />
    <input type="hidden" name="in_unmatch_blank" value="{$app.unmatch_blank}" />
    <input type="hidden" name="in_tel_value_line" value="{$app.tel_value_line}" />
    <input type="hidden" name="in_header_check" value="{$app.header_check}" />
    <input type="hidden" name="in_header_line" value="{$app.header_line}" />
    <input type="hidden" name="in_latlon_type" value="{$app.latlon_type}" />
    <input type="hidden" name="in_list_name" value="{$app.list_name}" />
    <input type="hidden" name="in_use_point" value="{$app.use_point}" />
    <input type="hidden" name="in_list_count" value="{$app.list_count}" />
    <input type="hidden" name="in_count_get" value="{$app.count_get}" />
    <input type="hidden" name="in_lv_7_count"value="{$app.lv_7_count}" />
    <input type="hidden" name="in_lv_5_count"value="{$app.lv_5_count}" />
    <input type="hidden" name="in_lv_3_count"value="{$app.lv_3_count}" />
    <input type="hidden" name="in_get_csv" value="{$app.get_csv}" />
    {foreach from=$app.lv_check item=lv}
    <input type="hidden" name="in_lv_check[]" value="{$lv}" />
    {/foreach}
    {if $app.use_matching_phase != ""}
	<input type="hidden" name="in_use_matching_phase" value="{$app.use_matching_phase}" />
	<input type="hidden" name="in_use_property_str" value="{$app.use_property_str}" />
	<input type="hidden" name="in_column_num_str" value="{$app.column_num_str}" />
    {/if}
    <input type="hidden" name="in_up_file_path" value="{$app.up_file_path}" />
{/if}

<div class="top_header_title" ><span class="top_header_title_border">ポイントの購入お申込み</span></div>
<div style="margin-left: 120px;">ナビットプリペイドポイントの購入申込みを致します。<br />下記フォームに必要事項を入力後、確認ボタンを押してください。<br /></div>

<div style="font-size:0.7em;margin-left: 120px;">
    <span>
    <span style="color:#562E39;font-size:16px;">■</span>ナビットプリペイドポイントは「ナビットポイント事務局」が管理しています。<br />
    <span style="color:#562E39;font-size:16px;">■</span>「ナビットポイント事務局」より購入依頼確認メールが送信されます。<br />
    </span>
</div>

<br />

<div style="margin-left: 120px;"><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><span style="font-size:80%;"> は必須入力項目です</span></div>
<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
    <tr>
        <td class="l_Cel_01_01">会社名　<!--<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" />--></td>
        <td class="s_Cel"><!--例：株式会社ナビット<br />-->
            <input type="text" id="in_company_name" name="in_company_name" value="{$app.data.company_name}" size="50" maxlength="50"  style="font-size:16px;" readonly="readonly" />
            {if is_error('in_company_name')}<br /><span class="error" style="color:red;">{message name="in_company_name"}</span>{/if}
        </td>
    </tr>
    <!--tr>
        <td class="l_Cel_01_01">会社名かな　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">例：かぶしきがいしゃなびっと<br />
            <input type="text" id="in_company_kana" name="in_company_kana" value="{$app.data.company_kana}" size="50" maxlength="50"  style="font-size:16px;" />
            {if is_error('in_company_kana')}<div class="error" style="color:red;">{message name="in_company_kana"}</div>{/if}
        </td>
    </tr-->
    <!--tr>
        <td class="l_Cel_01_01">支店名</td>
        <td width="550" class="s_Cel">例：東京支店<br />
            <input type="text" id="in_branch_name" name="in_branch_name" value="{$app.data.branch_name}" size="50" maxlength="50"  style="font-size:16px;" />
            {if is_error('in_branch_name')}<div class="error" style="color:red;">{message name="in_branch_name"}</div>{/if}
        </td>
    </tr-->
    <tr>
        <td class="l_Cel_01_01">郵便番号　<!--<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><br />（半角数字）--></td>
        <td width="550" class="s_Cel"><!--例：102-0074<br />-->
            <input type="text" id="in_zip1" name="in_zip1" value="{$app.data.zip1}" size="10" maxlength="3"  style="font-size:16px;width:100px;" class="zip-number" onKeyUp="AjaxZip3.zip2addr('in_zip1','in_zip2','in_address','in_address','in_address');" readonly="readonly"/>
            －<input type="text" id="in_zip2" name="in_zip2" value="{$app.data.zip2}" size="10" maxlength="4"  style="font-size:16px;width:100px;" class="zip-number" onKeyUp="AjaxZip3.zip2addr('in_zip1','in_zip2','in_address','in_address','in_address');" readonly="readonly"/>
            {if is_error('in_zip1')}<div class="error" style="color:red;">{message name="in_zip1"}</div>{/if}
            {if is_error('in_zip2')}<div class="error" style="color:red;">{message name="in_zip2"}</div>{/if}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">住所　<!--<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" />--></td>
        <td width="550" class="s_Cel"><!--例：東京都東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F<br />-->
            <input type="text" id="in_address" name="in_address" value="{$app.data.address}" size="100" maxlength="100"  style="font-size:16px;width:560px;"  readonly="readonly"/>
            {if is_error('in_address')}<div class="error" style="color:red;">{message name="in_address"}</div>{/if}
        </td>
    </tr>
    <!--tr>
        <td class="l_Cel_01_01">電話番号　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><br />（半角数字）</td>
        <td width="550" class="s_Cel">例：03-5215-5713<br />
            <input type="text" id="in_phone_no1" name="in_phone_no1" value="{$app.data.phone_no1}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_phone_no2" name="in_phone_no2" value="{$app.data.phone_no2}" size="10" maxlength="4"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_phone_no3" name="in_phone_no3" value="{$app.data.phone_no3}" size="10" maxlength="4"  style="font-size:16px;width:100px;" class="tel-number" />
            {if is_error('in_phone_no1')}<div class="error" style="color:red;">{message name="in_phone_no1"}</div>{/if}
            {if is_error('in_phone_no2')}<div class="error" style="color:red;">{message name="in_phone_no2"}</div>{/if}
            {if is_error('in_phone_no3')}<div class="error" style="color:red;">{message name="in_phone_no3"}</div>{/if}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">携帯電話番号<br />（半角数字）</td>
        <td width="550" class="s_Cel">例：090-1234-5789<br />
            <input type="text" id="in_k_phone_no1" name="in_k_phone_no1" value="{$app.data.k_phone_no1}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_k_phone_no2" name="in_k_phone_no2" value="{$app.data.k_phone_no2}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_k_phone_no3" name="in_k_phone_no3" value="{$app.data.k_phone_no3}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            {if is_error('in_k_phone_no1')}<div class="error" style="color:red;">{message name="in_k_phone_no1"}</div>{/if}
            {if is_error('in_k_phone_no2')}<div class="error" style="color:red;">{message name="in_k_phone_no2"}</div>{/if}
            {if is_error('in_k_phone_no3')}<div class="error" style="color:red;">{message name="in_k_phone_no3"}</div>{/if}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">FAX番号<br />（半角数字）</td>
        <td width="550" class="s_Cel">例：03-5215-3020<br />
            <input type="text" id="in_fax_no1" name="in_fax_no1" value="{$app.data.fax_no1}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_fax_no2" name="in_fax_no2" value="{$app.data.fax_no2}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_fax_no3" name="in_fax_no3" value="{$app.data.fax_no3}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            {if is_error('in_fax_no1')}<div class="error" style="color:red;">{message name="in_fax_no1"}</div>{/if}
            {if is_error('in_fax_no2')}<div class="error" style="color:red;">{message name="in_fax_no2"}</div>{/if}
            {if is_error('in_fax_no3')}<div class="error" style="color:red;">{message name="in_fax_no3"}</div>{/if}
        </td>
    </tr-->
    <tr>
        <td class="l_Cel_01_01">部署名　<!--<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" />--></td>
        <td width="550" class="s_Cel"><!--例：マーケティング事業部 <span style="font-size:0.8em;">(部署名がない方は 「なし」 とご記入ください)</span><br />-->
            <input type="text" id="in_department_name" name="in_department_name" value="{$app.data.department_name}" size="50" maxlength="50"  style="font-size:16px;"  readonly="readonly"/>
            {if is_error('in_department_name')}<div class="error" style="color:red;">{message name="in_department_name"}</div>{/if}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">役職名　<!--<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" />--></td>
        <td width="550" class="s_Cel"><!--例：事業部長 <span style="font-size:0.8em;">(役職名がない方は 「なし」 とご記入ください)</span><br />-->
            <input type="text" id="in_post_name" name="in_post_name" value="{$app.data.post_name}" size="50" maxlength="50"  style="font-size:16px;"  readonly="readonly"/>
            {if is_error('in_post_name')}<div class="error" style="color:red;">{message name="in_post_name"}</div>{/if}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">担当者名　<!--<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" />--></td>
        <td width="550" class="s_Cel">
            <span>姓&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="in_contractor_lname" name="in_contractor_lname" value="{$app.data.contractor_lname}" size="25" maxlength="50"  style="font-size:16px;width:200px;"  readonly="readonly"/></span>&nbsp;&nbsp;
            <span>名&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="in_contractor_fname" name="in_contractor_fname" value="{$app.data.contractor_fname}" size="25" maxlength="50"  style="font-size:16px;width:200px;"  readonly="readonly"/></span>
            {if is_error('in_contractor_lname')}<div class="error" style="color:red;">{message name="in_contractor_lname"}</div>{/if}
            {if is_error('in_contractor_fname')}<div class="error" style="color:red;">{message name="in_contractor_fname"}</div>{/if}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">担当者名かな　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">
            <span>せい&nbsp;<input type="text" id="in_contractor_lkana" name="in_contractor_lkana" value="{$app.data.contractor_lkana}" size="25" maxlength="50"  style="font-size:16px;width:200px;" /></span>&nbsp;&nbsp;
            <span>めい&nbsp;<input type="text" id="in_contractor_fkana" name="in_contractor_fkana" value="{$app.data.contractor_fkana}" size="25" maxlength="50"  style="font-size:16px;width:200px;" /></span>
            {if is_error('in_contractor_lkana')}<div class="error" style="color:red;">{message name="in_contractor_lkana"}</div>{/if}
            {if is_error('in_contractor_fkana')}<div class="error" style="color:red;">{message name="in_contractor_fkana"}</div>{/if}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">メールアドレス　<!--<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" />--></td>
        <td width="550" class="s_Cel">
            <input type="text" id="in_email" name="in_email" value="{$app.data.email}" size="50" maxlength="50"  style="font-size:16px;"  readonly="readonly"/>
            {if is_error('in_email')}<div class="error" style="color:red;">{message name="in_email"}</div>{/if}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">支払い方法　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">
	<div style="font-size:16px;overflow:hidden;">
	<div style="float:left;margin:15px 0 0 0;" width="350px">
            <input type="radio" id="in_payment" name="in_payment" value="bank_transfer" {if $app.data.payment == "bank_transfer"} checked {/if} />&nbsp;銀行振込
	    &nbsp;&nbsp;&nbsp;&nbsp;


	    <input type="radio" id="in_payment" name="in_payment" value="credit_card" {if $app.data.payment == "credit_card"} checked {/if} />&nbsp;クレジットカード決済</div>
		<div style="margin:0 30px 0 0px;float:right;" width="300px">
			<img src="img/card_visa_b.gif" width="60px"><img src="img/card_master_b.gif" width="60px">
		</div>
	    <div style="font-size:8pt;clear:both;">[クレジットカード決済を選択された方]<br />
弊社では決済のため氏名、住所、電話番号、カード情報等の個人情報を決済代行会社へ委託しております。<br />
取得者：株式会社ナビット 提供先：株式会社ASJ 利用目的：決済のため 保存期間：7年</div>
	</div>
            {if is_error('in_payment')}<div class="error" style="color:red;">{message name="in_payment"}</div>{/if}
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">ポイント購入数　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">
	    <font style="font-size:16px;">2,000pt&nbsp;&times;&nbsp;<input type="text" maxlength="5" id="in_point" name="in_point" value="{$app.data.point}" style="font-size:16px;width:50px;"/>&nbsp;口</font>
                <br />&nbsp;ポイント購入は2,000ポイント単位でのお申込みとなります。
                <br />&nbsp;1ポイントあたり1円となります。
                <br />&nbsp;銀行振り込みの場合、1回の購入は99,999口までとなります。
<!--                <br />&nbsp;クレジットカードの場合、1回の購入は100万円分（500口）までとなります。100万円分を超える購入は複数回に分けて、お手続きください。 -->
                <br />&nbsp;振込手数料はお客様ご負担となりますのでご了承ください。
                <br />&nbsp;ポイントの反映は弊社指定銀行口座へのご入金が確認でき次第となります。
            {if is_error('in_point')}<div class="error" style="color:red;">{message name="in_point"}</div>{/if}
        </td>
    </tr>

    <!--tr>
        <td class="l_Cel_01_01">サイトを知ったきっかけ　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">
            <select id='in_from' name="in_from" class="select_font_s">{$app_ne.pulldown.from}</select>
            {if is_error('in_from')}<div class="error" style="color:red;">{message name="in_from"}</div>{/if}
        </td>
    </tr-->
    <!--
    <tr>
        <td class="l_Cel_01_01">メールマガジンの送付を希望する　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">
            {*$app_ne.radio.mailmaga*}
            {*if is_error('in_mailmaga')*}<br /><br /><span class="error">{*message name="in_mailmaga"*}</span>{*/if*}
        </td>
    </tr>
    -->

    <tr>
        <td colspan="3">
            {if $app.flg_tos_consent == false}
            <div class="kiyaku">
                <div class="title">法人番号検索、ナビットプリペイドポイントの購入・利用規約</div>
                <pre style="font-family:Meiryo;">
{$app_ne.kiyaku_txt}
                </pre>
            </div>
                <div class="mod_form_importance_btn">
                <label for="privacy_1" style="font-size:80%;" id="privacy_label"><input value="{$app.data.privacy}" type="checkbox" name="in_privacy" id="privacy_1">上記「利用規約」に同意します。 </label>
                &nbsp;<span style="font-size:1em"></span>
                {if is_error('in_privacy')}<div class="error" style="color:red;">{message name="in_privacy"}</div>{/if}
                <br />
                </div>
            {/if}
                <!-- ここからトップへ戻る -->
	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
                <!-- ここまでトップへ戻る -->
        </td>
    </tr>
</table>
</div>
</form>


                <div class="mod_form_btn">
	<div style="margin-top:20px;margin-left:280px;">
	{if $app.act == "request_point"}
	    <form action="{$script}" name="back" method="POST" id="back">
		{if $app.use_matching_phase == ""}
		<input type="hidden" name="action_confirmation" value="true">
		{else}
		<input type="hidden" name="action_commission_confirm" value="true">
		<input type="hidden" name="in_use_matching_phase" value="{$app.use_matching_phase}" />
                {foreach from=$app.use_property item=v}
                        <input type="hidden" name="in_use_property[]" value="{$v}" />
                {/foreach}
		<input type="hidden" name="in_column_num_str" value="{$app.column_num_str}" />
		{/if}
		<input type="hidden" name="in_request_id" value="{$app.request_id}" />
		<input type="hidden" name="in_act" value="back" />
		<input type="hidden" name="in_up_file_path" value="{$app.up_file_path}" />
	    </form>
	    <a class="button" href="javascript:void(0)" onclick="document.back.submit();" id="back_btn">戻る</a>
	{elseif $app.act == "request_point_trialone"}
		<form action="{$script}" name="back" method="POST" id="back">
		<input type="hidden" name="action_trialoneconf" value="true">
		<input type="hidden" name="corp_id" value="{$app.corp_id}" />
		<input type="hidden" name="sc_name" value="{$app.sc_name}" />
    	<input type="hidden" name="sc_address" value="{$app.sc_address}" />
    	<input type="hidden" name="pref_cd" value="{$app.pref_cd}">
		<input type="hidden" name="city_cd" value="{$app.city_cd}">
		<input type="hidden" name="in_act" value="trialoneconf_b">
	    </form>
	    <a class="button" href="javascript:void(0)" onclick="document.back.submit();" id="back_btn">戻る</a>

	{else}
            <a class="button" href="index.php" id="back_btn">戻る</a>
	{/if}
	</div>
	<div style="margin-top:-46px;margin-left:520px;">
            <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit();return false;" id="confirm_btn">確認</a>
	</div>
                </div>
<br />

<br />
<div align="center">
<span id="ss_gmo_img_wrapper_100-50_image_ja">
<a href="https://jp.globalsign.com/" target="_blank" rel="nofollow">
<img alt="SSL　GMOグローバルサインのサイトシール" border="0" id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_100-50_ja.gif">
</a>
</span><br />
<span style="font-size:8px;">
<script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_100-50_ja.js" defer="defer"></script>
このサイトはグローバルサインにより認証されています。<br />SSL対応ページからの情報送信は暗号化により保護されます。
</span><br />
</div>

	<!-- ここまで入力フォーム -->



