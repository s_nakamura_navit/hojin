{literal} 
<script type="text/javascript">

</script>
{/literal} 

        <br />
{if $app.db_regist_result == "-1" || $app.mail_send_result == "-1"}
<div class="top_header_title" ><span class="top_header_title_border">ポイント購入 送信エラー</span></div>
<div style="margin-left: 64px;">
    <span style="font-weight:bold;font-size:100%;">ご登録頂いた内容で、ポイント購入ができませんでした。</span><br />
    大変お手数ではございますが再度こちらから購入手続きをお願い致します。>> 
    <a href="/phonebook/index.php?action_request_registration=true"> ポイント購入手続きへ</a><br />
      <br />
      <br />
      <span style="color:#666;font-size:80%;">
        【ご登録に失敗する場合、以下の原因が考えられます】<br />
        ※通信環境の混雑等の理由によるもの<br /><br />
        ※ご登録頂いたメールアドレスへの送信失敗によるもの(ご登録頂いたメールアドレスをご確認ください)<br />
        <br />
        <br />
      
      </span>  
</div>
{else}
<div class="top_header_title" ><span style="border-bottom:1px #562E39 solid;">ポイント購入の完了</span></div>
<div style="margin-left: 64px;">
    <span style="font-weight:bold;font-size:120%;">ご購入のお申込みを承りました。</span><br />
    <br />
    
    <span style="color:#f00;font-size:100%;">ご注文番号：</span><span style="color:#000;font-size:100%;">{$app.data.request_point_id}</span><br />
    <br />
    <span style="color:#000;font-size:100%;">
    ナビットプリペイドポイントご購入のお申し込みを頂き誠にありがとうございます。<br />
    <br />
    {if $app.data.payment == "bank_transfer"}
    ご登録頂いているメールアドレスに確認メールをお送りしましたので内容をご確認の<br />
    うえメール内記載の指定口座へのお振込みをよろしくお願い申し上げます。<br />
    <br />
    指定口座への入金が確認でき次第ポイントの反映をさせて頂きます。
    {else if $app.data.payment == "credit_card"}
    ご登録頂いているメールアドレスに確認メールをお送りしましたので内容をご確認をお願い致します。<br />
    <br />
    ご購入いただいたポイントの反映はマイページよりご確認いただけます。
<!--
    クレジットカード決済が確認でき次第ポイントの反映をさせて頂きます。<br />
    クレジットカードの決済画面に自動的に移動しなかった方は、<a id="credit_card" href="{$app.credit.url}?admin={$app.credit.admin}&amount={$app.credit.amount}&charge={$app.credit.charge}&mail={$app.credit.mail}" target="_blank">こちら</a>より決済画面へお進み下さい。
-->
    {/if}
    </span>
    <br />
      <br />
      <br />
      <span style="color:#666;font-size:80%;">
        ※通信環境の混雑等の理由によりメールの到着に少々お時間がかかる場合がございます。<br /><br />
        ※迷惑メール防止機能により当サイトからのメールが迷惑メールと間違えられ、メール受信画面に表示されない場合が<br />
        &nbsp;&nbsp;&nbsp;ございます。迷惑メールフォルダやゴミ箱に自動的に振り分けられている場合がございますので、一度ご確認頂きま<br />
        &nbsp;&nbsp;&nbsp;すようお願い致します。<br />
      
      </span>  
</div>
{/if}
<div style="font-size:0.7em;margin-left: 120px;">
</div>        

        <div class="mod_form_btn">
                    
	<div style="margin-top:20px;margin-left:400px;">
        
	{if $app.data.act == "request_point"}
    	    <form action="{$script}" name="f2" method="POST" id="f2">
		{if $app.data.use_matching_phase == ""}
	        <input type="hidden" name="action_confirmation" value="true">
		{else}
		<input type="hidden" name="action_commission_confirm" value="true">
		<input type="hidden" name="in_use_matching_phase" value="{$app.data.use_matching_phase}">
		{foreach from=$app.data.use_property item=v}
    			<input type="hidden" name="in_use_property[]" value="{$v}" />
    		{/foreach}
		<input type="hidden" name="in_column_num_str" value="{$app.data.column_num_str}">
		{/if}
	        <input type="hidden" name="in_request_id"value="{$app.data.request_id}" />
	        <input type="hidden" name="in_act" value="back" />
	    </form>        
            <a class="button" href="javascript:void(0)" onclick="document.f2.submit();" id="">戻る</a>
	{else}
            <a class="button2" href="index.php" id="to_top_btn">TOPへ</a>
	{/if}
        </div>
                </div>

        

