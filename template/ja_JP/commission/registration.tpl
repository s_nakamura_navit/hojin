{literal}
<script type="text/javascript">
<!--
//購入項目数カウンター
//個々の単価が違う場合があるので、引数に単価も入れている(第4)
//最終的に表示する数字は一旦文字列化してから（.toString）ナンバーフォーマットしている
function count_entry_number(id,no,total) {
    if(document.getElementById(id).checked){
	//現在の付与総件数を取得
        count = document.getElementById('in_total_count').value;
	//チェックされた項目の件数を総件数に追加
        count = eval(count) + eval(no);//eval関数で整数として認識
	//総件数を反映（不可視値）
        document.getElementById('in_total_count').value = count;
	//総件数に反映
        document.getElementById('total_count').textContent = count.toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
	//現在の小計を取得
        point = document.getElementById('in_subtotal_point').value;
	//チェックされた項目の必要ポイントを小計にに追加
        point = eval(point) + eval(total);
	//小計を反映（不可視値）
        document.getElementById('in_subtotal_point').value = point;
	//消費税計算
        tax_point = eval(Math.floor((point + {/literal}{$app.point.base}{literal}) * {/literal}{$app.point.tax_rate}{literal}));
	//消費税を反映
        document.getElementById('tax_point').textContent = tax_point.toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
	//合計を反映
        document.getElementById('total_point').textContent = (point + {/literal}{$app.point.base}{literal} + tax_point).toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
    }else{
	//上記分岐の処理の引き算版
        count = document.getElementById('in_total_count').value;
        count = eval(count) - eval(no);
        document.getElementById('in_total_count').value = count;
        document.getElementById('total_count').textContent = count.toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
        point = document.getElementById('in_subtotal_point').value;
        point = eval(point) - eval(total);
        document.getElementById('in_subtotal_point').value = point;
        tax_point = eval(Math.floor((point + {/literal}{$app.point.base}{literal}) * {/literal}{$app.point.tax_rate}{literal}));
        document.getElementById('tax_point').textContent = tax_point.toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
        document.getElementById('total_point').textContent = (point + {/literal}{$app.point.base}{literal} + tax_point).toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
    }
}
//-->
</script>
{/literal}

{include file=commission/dialog.tpl}
<div class="top_header_title" style="margin-top:20px;"><span class="top_header_title_border">見積もり内容確認</span></div>
<div style="margin-left: 120px; font-size:18px;">購入する項目を選択し、確認ボタンを押してください。見積もり時のポイントとご購入件数と項目分のポイントの差額は返却されます。<br /></div>
<div style="margin-left: 120px; font-size:18px;">購入しない場合は、依頼を取り消すボタンを押してください。依頼した件数と項目分のポイントが返却されます。<br /></div>
<br />        
     
<div class="table_com">
<form action="{$script}" name="go_confirm" id="go_confirm" method="POST">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
    {foreach from=$app.select_property item=key}
    <tr>
        <td class="l_Cel_com">
		<input type="checkbox" id="in_{$key}_row" name="in_use_property[]" value="{$key}" style="vertical-align:-5px;" onClick="count_entry_number('in_{$key}_row',{$app.count.$key},{$app.point.$key});" {foreach from=$app.use_property item=v}{if $v == $key}checked{/if}{/foreach} {if $app.count.$key == 0}disabled{/if} />
		{$app.property_list.$key}<br />
	</td>
        <td class="v_Cel_com">{$app.count.$key|number_format}件</td>
        <td class="v_Cel_com">{$app.point.$key|number_format}ポイント</td>
    </tr>
    {/foreach}
    {if $app.point.base != 0}
    <tr>
        <td class="l_Cel_com">基本料金</td>
        <td class="v_Cel_com"></td>
        <td class="v_Cel_com"><span id="base_point">{$app.point.base|number_format}</span>ポイント</td>
    </tr>
    {/if}
    <tr>
        <td class="l_Cel_com">消費税{math equation="x * y" x=$app.point.tax_rate y=100}％</td>
        <td class="v_Cel_com"></td>
        <td class="v_Cel_com"><span id="tax_point">{$app.point.tax|number_format}</span>ポイント</td>
    </tr>
    <tr>
        <td class="l_Cel_com"></td>
        <td class="v_Cel_com"><span id="total_count">{if $app.total_count == ""}0{else}{$app.total_count|number_format}{/if}</span>件</td>
        <td class="v_Cel_com"><span id="total_point">{$app.point.total|number_format}</span>ポイント</td>
    </tr>
</table>
    <input type="hidden" name="action_commission_confirm" value="true" />
    <input type="hidden" id="in_request_id" name="in_request_id" value="{$app.request_id}">
    <input type="hidden" id="in_total_count" name="in_total_count" value="{if $app.total_count == ""}0{else}{$app.total_count}{/if}">
    <input type="hidden" id="in_subtotal_point" name="in_subtotal_point" value="{if $app.subtotal_point == ""}0{else}{$app.subtotal_point}{/if}">
</form>
{if $app.use_property_check_err != ""}<br /><div style="margin-left: 120px; font-size:18px;"><span class="error">{$app.use_property_check_err}</span></div>{/if}
</div>
    <div class="mod_form_btn">
	<div style="margin-top:20px;margin-left:300px;">
		<a class="button" href="javascript:void(0)" id="back_btn" onclick="do_cancel();">依頼を取り消す</a>
	</div>
	<div style="margin-top:-46px;margin-left:520px;">
		<a class="button2" href="javascript:void(0)" onclick="javascript:document.go_confirm.submit();" id="confirm_btn">購入する</a>
	</div>
    </div>
<br />

<br />
    <div class="mod_form_btn">
	<div style="margin-top:20px;margin-left:400px;">
		<a class="button" href="./index.php?action_mypage=true" id="back_btn">戻る</a>
	</div>
    </div>


