<link rel="stylesheet" href="css/ditail.css" type="text/css">

<!-- ここから依頼確認 -->
<div id="cancel_dialog" style="display:none;">

    <div style="width:100%;background-color: #ffffff;">
        <img src="img/logo.jpg" alt="法人番号検索" style="width:178px;height:48px;"  />
        <div style="width:24px;">
            <a href="javascript:void(0);" style="position:relative; top:-60px; left:740px;"><img id="cancel_dialog_close" src="img/close_round.png" width="20px" height="20px" alt="閉じる" /></a>
        </div>
    </div>
    <hr />
    <div style="text-align:center;">
    <br />
    <span style="font-size:20px;color:#3A2409;" id="cancel_dialog_msg">
        依頼を取り消します。<br />
        よろしいですか？<br />
    <span style="font-size:12px;color:#3A2409;margin-left:-0px;">※この操作は取り消しができませんのでご注意ください。<br /></span>
    <br />
    <br />
    <form action="{$script}" name="cancel_submit" method="POST" id="cancel_submit">
    	<input type="hidden" name="action_commission_registrationauto" value="true" />
    	<input type="hidden" name="in_request_id" value="{$app.request_id}" />
    	<input type="hidden" name="in_act" value="do_cancel" />
    </form>
        <a class="button5_en" href="javascript:void(0)"  id="cancel_dialog_close">戻る</a>&nbsp;&nbsp;
        <span><a class="button5" href="javascript:void(0)" onclick="document.cancel_submit.submit();" id="send_delete">依頼を取り消す</a></span>
    </div>

</div>
<!-- ここまで依頼確認 -->

{literal}
<script type="text/javascript">
function do_cancel(){
	$("#cancel_dialog").lightbox_me({centered: true,closeSelector:'#cancel_dialog_close',overlayCSS:{background:'#D3C9BA',opacity: .8}});
}

$(function() {
        var alert_flg = true;
	$("span a").bind('click', function (e) {
		alert_flg = false;
	});
        $(window).on("beforeunload",function(e){
                if (alert_flg) {
                        //$("#cancel_dialog").lightbox_me({centered: true,closeSelector:'#cancel_dialog_close',overlayCSS:{background:'#D3C9BA',opacity: .8}});
                        return "マッチング結果が消えますがよろしいですか？";
                } else {
                }
        });
});

</script>
{/literal}
