{if $app.use_matching_phase != ""}
	{include file=./commission/getlist_dialog.tpl}
	<script type="text/javascript" src="js/jquery.getlist_loading.js"></script>
{/if}
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">購入内容の確認<span style="font-size:18px;margin-left:50px;">お客様の現在のポイント数：{$app.point.client|number_format}pt</span></div>
<br />

<div class="table">
    <table border="0" cellpadding="5" cellspacing="1" style="width:600px;margin-left:auto;margin-right:auto;">
	<tr>
            <td class="l_Cel">
                <div class="sub_head">データ名</div>
            </td>

            <td colspan="2" class="r_Cel">
                    {$app.data.list_name}
            </td>
	</tr>

        <tr>
            <td colspan="3">
                    <hr />
            </td>
        </tr>

        <tr>
            <td class="l_Cel">
                <div class="sub_head">依頼件数</div>
            </td>

            <td colspan="2" class="r_Cel">
                    {$app.data.list_row|number_format}&nbsp;件
            </td>
	</tr>

        <tr>
            <td colspan="3">
                    <hr />
            </td>
        </tr>
        <tr>
            <td class="l_Cel">
                <div class="sub_head">属性付与項目</div>
            </td>
            <td colspan="2" class="r_Cel">
		    {foreach from=$app.use_property item=v}
                        {$app.property_list.$v}<br />
		    {/foreach}
            </td>
	</tr>

        <tr>
            <td colspan="3">
                    <hr />
            </td>
        </tr>

                                <tr><td colspan="2">
                                <div id="detail">
                                <table width="100%" id="detail" style="border:1px solid #666">
                                        <tr>
                                                <td colspan="2" style="font-size:18px; text-align:left;background-color:#666; font-weight:bold; color:#fff;">ポイントの内訳</td>
                                        </tr>
                                        <tr>
                                                <td style="font-size:17px; font-weight:bold; text-align:left;">価格</td>
						<td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;"></td>
                                        {foreach from=$app.use_property item=name}
                                        <tr>
                                                <td style="font-size:17px; font-weight:bold; text-align:left;">&nbsp;&nbsp;{$app.property_list.$name}</td>
                                                <td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">
							{$app.count.$name|number_format}&nbsp;件&nbsp;&times;&nbsp;{$app.point.common.$name|number_format}&nbsp;ポイント&nbsp;=&nbsp;{$app.point.$name|number_format}&nbsp;ポイント
						</td>
                                        </tr>
                                        {/foreach}
                                        </tr>
                                        {if $app.point.base != 0}
                                        <tr>
                                                <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">基本料金</td>
                                                <td colspan="1" class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.point.base|number_format}&nbsp;ポイント</td>
                                        </tr>
                                        {/if}
                                        <tr>
                                                <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">小計</td>
                                                <td colspan="1" class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.point.sub_total|number_format}&nbsp;ポイント</td>
                                        </tr>
                                        <tr>
                                                <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">消費税{math equation="x * y" x=$app.point.tax_rate y=100}％</td>
                                                <td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.point.tax|number_format}&nbsp;ポイント</td>
                                        </tr>
                                        <tr>
                                                <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">計</td>
                                                <td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;">{$app.point.total|number_format}&nbsp;ポイント</td>
                                        </tr>
                                </table>
                                </div>
                                </td></tr>
	{if $app.use_matching_phase == ""}
        <tr>
            <td class="l_Cel">
                <div class="sub_head">返却ポイント</div>
            </td>

            <td colspan="2" class="r_Cel">
		{$app.point.back|number_format}&nbsp;ポイント
            </td>
	</tr>
	{else}
        <tr>
            <td class="l_Cel">
                <div class="sub_head">お客様の<br />
現在のポイント数</div>
            </td>

            <td colspan="2" class="r_Cel">
                {$app.point.client|number_format}&nbsp;ポイント
            </td>
        </tr>

        <tr>
            <td class="l_Cel">
                <div class="sub_head">必要ポイント</div>
            </td>

            <td colspan="2" class="r_Cel">
                {$app.point.total|number_format}&nbsp;ポイント
            </td>
        </tr>

        {if $app.point.lack != ""}
        <tr>
            <td class="l_Cel">
                <div class="sub_head">不足ポイント</div>
            </td>

            <td colspan="2" class="r_Cel">
                {$app.point.lack|number_format}&nbsp;ポイント
            </td>
        </tr>
        {/if}
        {/if}
	
    </table>
</div>
    <form action="{$script}" name="back" id="back" method="POST">
    {if $app.use_matching_phase == ""}
    <input type="hidden" name="action_commission_registration" value="true" />
    {else}
    <input type="hidden" name="action_commission_registrationauto" value="true" />
    <input type="hidden" name="in_use_matching_phase" value="{$app.use_matching_phase}" />
    <input type="hidden" name="in_column_num_str" value="{$app.column_num_str}" />
    {/if}
    <input type="hidden" name="in_act" value="back" />
    <input type="hidden" name="in_request_id" value="{$app.request_id}" />
    <input type="hidden" name="in_total_count_send" value="{$app.total_count_send}" />
    <input type="hidden" name="in_total_point" value="{$app.point.total}" />
    {foreach from=$app.use_property item=v}
    <input type="hidden" name="in_use_property[]" value="{$v}" />
    {/foreach}
    </form>

    <form action="{$script}" name="go_complete" id="go_complete" method="POST">
    <input type="hidden" name="action_commission_complete" value="true" />
    <input type="hidden" name="in_request_id" value="{$app.request_id}" />
    <input type="hidden" name="in_total_count" value="{$app.total_count_send}" />
    <input type="hidden" name="in_total_point" value="{$app.point.total}" />
    <input type="hidden" name="in_back_point" value="{$app.point.back}" />
    {if $app.use_matching_phase != ""}
    <input type="hidden" name="in_use_matching_phase" value="{$app.use_matching_phase}" />
    <input type="hidden" name="in_column_num_str" value="{$app.column_num_str}" />
    {/if}
    <input type="hidden" name="in_act" value="get_list" />
    {foreach from=$app.use_property item=v}
    <input type="hidden" name="in_use_property[]" value="{$v}" />
    {/foreach}
    </form>

    <form action="{$script}" name="go_request_point" id="go_request_point" method="POST">
    <input type="hidden" name="action_request_registration" value="true" />
    <input type="hidden" name="in_request_id" value="{$app.request_id}" />
    <input type="hidden" name="in_use_matching_phase" value="{$app.use_matching_phase}" />
    <input type="hidden" name="in_use_property_str" value="{$app.use_property_str}" />
    <input type="hidden" name="in_column_num_str" value="{$app.column_num_str}" />
    <input type="hidden" name="in_act" value="request_point" />
    </form>

    <div class="mod_form_btn">
	<div style="margin-top:20px;margin-left:300px;">
		<a class="button" href="javascript:void(0)" id="back_btn" onclick="document.back.submit();">戻る</a>
	</div>
	<div style="margin-top:-46px;margin-left:520px;">
	{if $app.use_matching_phase == ""}
		<a class="button2" href="javascript:void(0)" onclick="document.go_complete.submit();" id="confirm_btn">購入する</a>
	{else}
		{if $app.point.lack != ""}
			<a class="button" href="javascript:void(0)" onclick="document.go_request_point.submit();" id="a_search">ポイント購入</a>
		{else}
			<a class="button2" href="javascript:void(0)" onclick="displayGetListForm();" id="confirm_btn">購入する</a>
		{/if}
	{/if}
	</div>
    </div>
<br />

    <span style="font-size:12px;color:#3A2409;">※手続き後は自動的にダウンロードが行われます。<br /></span>
