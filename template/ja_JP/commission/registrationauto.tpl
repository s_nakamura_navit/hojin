{literal}
<script type="text/javascript">
<!--
//購入項目数カウンター
//個々の単価が違う場合があるので、引数に単価も入れている(第4)
//最終的に表示する数字は一旦文字列化してから（.toString）ナンバーフォーマットしている
{/literal}
{foreach from=$app.matching_phase key=val item=phase}
{literal}
function count_entry_number_{/literal}{$val}{literal}(id,no,total) {
    if(document.getElementById(id).checked){
	//現在の付与総件数を取得
        //count = document.getElementById('in_total_count').value;
	//チェックされた項目の件数を総件数に追加
        //count = eval(count) + eval(no);//eval関数で整数として認識
	//総件数を反映（不可視値）
        //document.getElementById('in_total_count').value = count;
	//総件数に反映
        //document.getElementById('{/literal}{$val}{literal}_total_count').textContent = count.toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
	//現在の小計を取得
        point = document.getElementById('{/literal}{$val}{literal}_subtotal_point').value;
	//チェックされた項目の必要ポイントを小計にに追加
        point = eval(point) + eval(total);
	//小計を反映（不可視値）
        document.getElementById('{/literal}{$val}{literal}_subtotal_point').value = point;
	//消費税計算
        tax_point = eval(Math.floor((point + {/literal}{$app.point.base}{literal}) * {/literal}{$app.point.tax_rate}{literal}));
	//消費税を反映
        document.getElementById('{/literal}{$val}{literal}_tax_point').textContent = tax_point.toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
	//合計を反映
        document.getElementById('{/literal}{$val}{literal}_total_point').textContent = (point + {/literal}{$app.point.base}{literal} + tax_point).toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
    }else{
	//上記分岐の処理の引き算版
        //count = document.getElementById('in_total_count').value;
        //count = eval(count) - eval(no);
        //document.getElementById('in_total_count').value = count;
        //document.getElementById('{/literal}{$val}{literal}_total_count').textContent = count.toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
        point = document.getElementById('{/literal}{$val}{literal}_subtotal_point').value;
        point = eval(point) - eval(total);
        document.getElementById('{/literal}{$val}{literal}_subtotal_point').value = point;
        tax_point = eval(Math.floor((point + {/literal}{$app.point.base}{literal}) * {/literal}{$app.point.tax_rate}{literal}));
        document.getElementById('{/literal}{$val}{literal}_tax_point').textContent = tax_point.toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
        document.getElementById('{/literal}{$val}{literal}_total_point').textContent = (point + {/literal}{$app.point.base}{literal} + tax_point).toString().replace(/([\d]+?)(?=(?:\d{3})+$)/g, function(t){ return t + ','; });
    }
}
{/literal}
{/foreach}
{literal}

function do_confirmation(phase){
	document.getElementById('in_use_matching_phase').value = phase;
	document.go_confirm.submit();
}

//検索エリアの表示・非表示の切り替え
function show_row_area(){
	if(document.getElementById('header_cell').style.display == "none"){
		document.getElementById('header_cell').style.display = "table-row";
{/literal}{foreach from=$app.property_check item=property}{literal}
		document.getElementById('{/literal}{$property}{literal}_cell').style.display = "table-row";
		document.getElementById('{/literal}{$property}{literal}_header_cell').style.display = "table-row";
{/literal}{/foreach}{literal}
	}else if(document.getElementById('header_cell').style.display == "table-row"){
		document.getElementById('header_cell').style.display = "none";
{/literal}{foreach from=$app.property_check item=property}{literal}
		document.getElementById('{/literal}{$property}{literal}_cell').style.display = "none";
		document.getElementById('{/literal}{$property}{literal}_header_cell').style.display = "none";
{/literal}{/foreach}{literal}
	}
}
//-->
</script>
{/literal}

{include file=commission/dialog_auto.tpl}
<div class="top_header_title" style="margin-top:20px;"><span class="top_header_title_border">自動付与内容確認</span></div>
<div style="margin-left: 120px; font-size:18px;">購入する項目を選択し、確認ボタンを押してください。<font color="#ffffff">{$app.time}</font><br /></div>
<br />        

<div class="table_com">
<form action="{$script}" name="go_confirm" id="go_confirm" method="POST">
<table width="850" border="0" cellpadding="5" cellspacing="1" style="margin-left: 38px;">
    <tr>
	<td colspan="12" class="title_Cel">マッチング方法</td>

    </tr>
    <tr>
        {foreach from=$app.matching_phase key=val item=phase}
      <td colspan="3" class="v_Cel_com">{$phase}</td>
    {/foreach}
    </tr>
    <tr>
	<td colspan="12" class="title_Cel" >マッチングした企業数</td>
    </tr>
    <tr>  {foreach from=$app.matching_phase key=val item=phase}
	<td colspan="3" class="v_Cel_com">{$app.count.$val.matching_row|number_format}件</td>
    {/foreach}
    </tr>

<tr>
<td colspan="12">
</td>
</tr>
    <tr>
	<td colspan="12" style="text-align:center;">
<a href="javascript:void(0)" style="display:block;" onclick="show_row_area();"><img src="img/select_btn.png" onmouseover="this.src='img/select_btn_d.png'" onmouseout="this.src='img/select_btn.png'" /></a></td>
    </tr>
<tr>
<td colspan="12">
</td>
</tr>
    <tr id="header_cell" style="display:none;">
    {foreach from=$app.matching_phase key=val item=phase}
	<td class="l_Cel_com">件数</td>
	<td class="l_Cel_com">ポイント合計</td>
	<td class="l_Cel_com">購入</td>
    {/foreach}
    </tr>
    {foreach from=$app.property_check item=property}

<tr id="{$property}_header_cell" style="display:none;">
<td class="l_Cel_com2" colspan="12">{$app.property_list.$property}</td>
</tr>

    <tr id="{$property}_cell" style="display:none;">
<!--	<td class="l_Cel_com">{$app.property_list.$property}</td>-->
	{foreach from=$app.matching_phase key=val item=phase}
            <td class="v_Cel_com">{$app.count.$val.$property|number_format}件</td>
            <td class="v_Cel_com">{$app.point.$val.$property|number_format}<br />ポイント</td>
            <td class="l_Cel_com">
		<input type="checkbox" id="in_{$val}_{$property}_row" name="in_use_property[{$val}][]" value="{$property}" style="vertical-align:-5px;" onClick="count_entry_number_{$val}('in_{$val}_{$property}_row',{$app.count.$val.$property},{$app.point.$val.$property});" {if $app.count.$val.$property != 0}checked{/if} {if $app.count.$val.$property == 0}disabled{/if} />
	    </td>
	{/foreach}
    </tr>
    {/foreach}
    {if $app.point.base != 0}
    <tr>
        <td class="l_Cel_com">基本料金</td>
        <td class="v_Cel_com" colspan="12"><span id="base_point">{$app.point.base|number_format}</span>ポイント</td>
    </tr>
    {/if}
    <tr>
        <td colspan="12" class="title_Cel">消費税{math equation="x * y" x=$app.point.tax_rate y=100}％</td></tr>
	{foreach from=$app.matching_phase key=val item=phase}
        <td class="v_Cel_com" colspan="3"><span id="{$val}_tax_point">{$app.point.$val.tax|number_format}</span>ポイント</td>
	{/foreach}
    </tr>
    <tr>
        <td class="title_Cel" colspan="12">ご購入に必要なポイント</td>

    </tr>
    <tr>
	{foreach from=$app.matching_phase key=val item=phase}
	<input type="hidden" id="{$val}_subtotal_point" name="in_subtotal_point" value="{$app.point.$val.subtotal}" disabled />
        <td class="v_Cel_com" colspan="3"><span id="{$val}_total_point">{$app.point.$val.total|number_format}</span>ポイント</td>
	{/foreach}
    </tr>
    <tr>
	{foreach from=$app.matching_phase key=val item=phase}
        <td class="v_Cel_com" colspan="3">
		{if $app.count.$val.matching_row != 0}
		<span><a class="button2" href="javascript:void(0)" onclick="do_confirmation('{$val}');" id="confirm_btn">購入する</a></span>
		{else}
		<font class="button2_n" href="javascript:void(0)" id="confirm_btn">購入する</font>
		{/if}
	</td>
	{/foreach}
    </tr>
</table>
    <input type="hidden" name="action_commission_confirm" value="true" />
    <input type="hidden" id="in_use_matching_phase" name="in_use_matching_phase" value="" />
    <input type="hidden" id="in_request_id" name="in_request_id" value="{$app.request_id}">
    <input type="hidden" id="in_column_num_str" name="in_column_num_str" value="{$app.column_num_str}">
</form>
{if $app.use_property_check_err != ""}<br /><div style="margin-left: 120px; font-size:18px;"><span class="error">{$app.use_property_check_err}</span></div>{/if}
</div>
    <div style="width:750px;margin:20px 0 0 120px;text-align:center;">
		<a class="button" href="javascript:void(0)" id="back_btn" onclick="do_cancel();">依頼を取り消す</a>
    </div>
<br />
