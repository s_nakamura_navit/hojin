{if $app.act == "get_list"}
{literal}
<script type="text/javascript">
window.onload = function(){
	document.f_csv.submit();
}
</script>
{/literal}
{/if}
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">購入完了！</div>

<br />

<div class="table">
<form action="{$script}" name="f_csv" method="POST" id="f_csv" enctype="multipart/form-data">
    <input type="hidden" name="action_commission_complete" value="true" />
    <input type="hidden" name="in_act" value="dl_csv" />
    <input type="hidden" name="in_estimate_file_path" value="{$app.dl_file_path}" />
</form>
    <table border="0" cellpadding="5" cellspacing="1" style="width:600px;margin-left:auto;margin-right:auto;">
	<tr>
            <td class="l_Cel">
		<span style="font-size:34px;">ご利用ありがとうございます！</span><br /><br />
		正常にダウンロードいただけなかった場合、<a href="./index.php?action_mypage=true">マイページ</a>より再度ダウンロードをお願いします。<font color="#ffffff">{$app.time}</font><br /><br />
            </td>
	</tr>

	<tr>
	    <td class="l_Cel">
                <hr />
                <div id="submit_btn">
		{if $app.use_matching_phase == ""}
                    <a class="button" href="index.php?action_mypage=true" id="back_button">マイページヘ戻る</a>
		{else}
                    <a class="button" href="index.php" id="back_button">TOPヘ</a>
		{/if}
                    <!--a class="button" href="index.php?action_send=true" id="a_search">取得する</a-->     
                </div>
            </td>
	</tr>
    </table>
</div>

