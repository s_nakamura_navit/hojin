<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>FAX送信サービス</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="description" content="株式会社ナビットが提供するFAX送信サービスサイト">
<meta name="author" content="株式会社ナビット">
<meta name="keywords" content="FAX,FAXDM,配信,送信,代行">
<link rel="stylesheet" href="css/default.css" type="text/css">
<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/ >

<link rel="shortcut icon" href="">
{literal} 
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="js/jquery.balloon.js"></script>
<!--<script type="text/javascript" src="js/jquery.sticky.js"></script>-->
<script type="text/javascript" src="js/jquery.datetimepicker.js"></script>

<script type="text/javascript">
$( document ).ready(function(){
	jQuery('#datetimepicker').datetimepicker({
		format:'Y/m/d H:i',
		inline:true, lang:'ja',
		minDate:0,
		onChangeDateTime:function(dp,$input){
			$("#datetimepicker").text($input.val());
		}
	});
});
	
//吹き出し
$('label').balloon();

function doblur() { 
    var element = document.getElementById("name"); 
    element.blur(); 
} 

function do_submit_with_param(param){
    document.f2.in_callkind.value = param;
    document.f2.submit();
}



</script>
{/literal} 
</head>
<body>
<form action="{$script}" name="f2" method="POST" id="f2" enctype="multipart/form-data">
<input type="hidden" name="action_confirmation" value="true">
<!-- ここからconteinar -->
<div id="conteinar">

<!-- ここからwrapper -->
	<div id="wrapper">

<!-- ここからheader --><div id="01"></div>
		<div id="header">

			<div id="logo" style="width:357px;height:98px;border:1px;">
				<a href="index.php" ><img src="img/logo.jpg" alt="FAX送信代行サービス" /></a>
			</div>

			<div id="h1">
				<h1>株式会社ナビットが提供するFAX送信サービス</h1>
			</div>

			<div id="mini_contact">
				<img src="img/m_contact.gif" alt="お問合せ" />
			</div>

			<div id="mini_contact_txt">
				<a href="https://www.navit-j.com/contactus/" target="_blank">お問合せ</a>
			</div>
<!--
			<div id="mini_sitemap">
				<img src="img/m_sitemap.gif" alt="サイトマップ" />
			</div>

			<div id="mini_sitemap_txt">
				<a href="#">サイトマップ</a>
			</div>
-->
			<div id="freedial">
				<img src="img/freedial.jpg" alt="0120-937-781" />
			</div>

                    {if $app.name !=""}
                        <div style="display:inline-block;width:360px;">
                            <table  border="1" cellspacing="0" cellpadding="0" style="width:100%; border-color:#f00">
                                <tr>
                                    <td>
                                        <table width="100%">
                                            <tr>
                                                <td align="left">
                                                    <span id="logininfo">{$app.name} 様　ログイン中</span>   
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <a href="./index.php?action_logout_do=true" id="logout_btn">ログアウト</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>    
                        </div>
                    {else}
                        <div id="newaccount">
                            <a class="button3" href="./index.php?action_secure_registration=true"  id="registration">新規会員登録</a>
                        </div>
                        <div id="login">
                            <a class="button3" href="./index.php?action_login=true" onclick="" id="login_btn">ログイン</a>
                        </div>
                    {/if}
                                                

		</div>

		<div class="clear">
		</div>

<!-- ここまでheader -->

<!-- ここからG NAVI -->


<div id="gnavi">
	<ul>
		<li style="height:82px;">
			<a href="#01"><img src="img/gnavi01.jpg" alt="ホーム" width="201px" height="82px"></a>
		</li>
		<li style="height:82px;">
			<a href="lp.html" target="_blank"><img src="img/gnavi02.jpg" onmouseover="this.src='img/gnavi02.jpg'" onmouseout="this.src='img/gnavi02.jpg'" /></a>
		</li>
		<li style="height:82px;">
			<a href="#03"><input type="image" onClick="" src="img/gnavi03.jpg" onMouseOver="this.src='img/gnavi03.jpg'" onMouseOut="this.src='img/gnavi03.jpg'" alt="" width="200px" height="82px"></a>
		</li>
		<li style="height:82px;">
			<a href="#04"><input type="image" onClick="" src="img/gnavi04.jpg" onMouseOver="this.src='img/gnavi04.jpg'" onMouseOut="this.src='img/gnavi04.jpg'" alt="" width="200px" height="82px"></a>
		</li>
		<li style="height:82px;">
			<a href="http://www.navit-j.com/press/sem150203.html" target="_blank"><img src="img/gnavi05.jpg" onmouseover="this.src='img/gnavi05.jpg'" onmouseout="this.src='img/gnavi05.jpg'" alt="" width="200px" height="82px"></a>
		</li>
	</ul>

</div>
		<div class="clear">
		</div>

<!-- ここまでG NAVI -->

<!-- ここからメインビジュアル -->

	<img src="img/main_vis01.jpg" alt="FAX送信代行サービスの特徴" border="0" />

<!--
	<img src="img/main_vis01.jpg" alt="助成金なうの特徴" usemap="#Map" border="0" />
    <map name="Map" id="Map">
      <area shape="rect" coords="73,18,301,255" href="#03" />
      <area shape="rect" coords="395,25,622,257" href="http://www.navit-j.com/press/sem150203.html" target="_blank" />
    </map>
-->
<!-- ここまでメインビジュアル -->

<!-- ここからメインコンテンツ -->
<!-- ここからお知らせ -->
<!-- ここまでお知らせ -->

<!-- ここから自治体フォーム -->
<div id="03"></div>

<!--<img src="img/title_a_search_line.jpg" alt="" />-->

<hr></hr>

<br />
<div class="head_title_bgcolor" style="text-align:left;">FAXの送信</div>
<br />
<div style="margin-left: 50px;">FAX送信を行うには、FAX原稿とFAX送信先のリストが必要となります。<br /></div>
<div style="margin-left: 50px;">まずは、FAX原稿データとFAX送信先リストデータをご用意ください。<br /></div>
<br />
<div style="font-size:0.9em;margin-left: 70px;">
    <span>
    【はじめての方はこちらをご覧ください】<br />
    <span style="color:#4060a0;font-size:16px;margin-left: 90px;">■</span><a href="ex_fax_article.html" target="_blank"  style="text-decoration: underline;">FAX原稿データについて</a><br />
    <span style="color:#4060a0;font-size:16px;margin-left: 90px;">■</span><a href="ex_fax_sendlist.html" target="_blank" style="text-decoration: underline;">FAX送信先リストデータについて</a><br />
    </span>
</div>        
<br />               
        

<div class="table">

		<table border="0" cellpadding="5" cellspacing="1">

		<tr>
			<td class="l_Cel">
                            <div class="middle_midashi">FAX送信先リスト</div>
                        </td>
                        
			<td colspan="2" class="r_Cel">
                            <input type="file" name="in_up_list" class="file">
                            <p class="note">
                                FAX送信先が記載されたファイルをご指定ください。<br />
                                &nbsp;&nbsp;※こちらからサンプルをダウンロードしてご利用ください。<br />
                                &nbsp;&nbsp;&nbsp;&nbsp;<a style="text-decoration: underline;">FAX送信先CSV　サンプルファイルダウンロード</a>
                            </p>
                            {if is_error('in_up_list')}<br /><br /><span class="error">{message name="in_up_list"}</span>{/if}
                        </td>
               </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td class="l_Cel"><div class="middle_midashi">FAX原稿</div></td>
			<td colspan="2" class="r_Cel">
                            <input type="file" name="in_up_article" class="file">
                            <p class="note">
                                ※ファイル形式：Officeファイル(.doc、.docx、.xls、.xlsx、.ppt、.pptx)またはPDF（.pdf）ファイル<br>
                                ※原稿サイズ<br>
                                &nbsp;&nbsp;A4サイズ（横：210mm × 縦：297mm）<br>
                                &nbsp;&nbsp;B4サイズ（横：257mm × 縦：364mm）<br>
                            </p>           
				
                            {if is_error('in_up_article')}<br /><br /><span class="error">{message name="in_up_article"}</span>{/if}
                        </td>
                            
		  </tr>

		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
                        <td class="l_Cel"><div class="middle_midashi">送信日時</div></td>
                        <td colspan="2" class="r_Cel">
            				<input type="text" id="datetimepicker" name="datetimepicker"></div>
			               <p class="note">
                                送信日時をご指定ください。<br />
                            </p>
                        </td>
			
		</tr>

                <tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
                        <td class="l_Cel"><div class="middle_midashi">差込み印字</div></td>
                        <td colspan="2" class="r_Cel">
<textarea id="insert" name="insert" cols="60" rows="4"></textarea>
			               <p class="note">
                                差込む印字の内容をご指定ください。<br />
            					宛先リスト(CSV)の値を差込む場合はヘッダ行（1行目）の値を{{}}で囲んで指定します<br/>
            					※書式<br />
            					[]：印字指定なし<br />
								[文字列]：1行印字<br />
								[文字列, 文字列, 文字列, 文字列]：4行印字（空行は空文字列で表記します）<br />
								※記述例<br />
								原稿への差込先頭行に「会社名」、2行目に「部署名」、3行目に「役職名」、4行目に「氏名」を設定する場合
<div style="background-color:#f5f5f5;border:1px solid rgba(0, 0, 0, 0.15);font-size:16px;"><pre>{literal}'["{{会社名}}", "{{部署名}}", "{{役職名}}", "{{氏名}}"]'{/literal}</pre></div>
                            </p>
                        </td>
			
		</tr>         
                        
                        
                        
                        
                        
                        
                        
		<tr>
			<td colspan="3">
				<hr />
			</td>
		</tr>

		<tr>
			<td colspan="4">
				<div id="submit_btn">
					<!--<input type="image" onclick="return false;" id="a_reset" src="img/subbtn_reset.png" onMouseOver="this.src='img/subbtn_reset_d.png'" onMouseOut="this.src='img/subbtn_reset.png'" alt="リセット">-->
                                        <!--<a class="button" href="javascript:void(0)" onclick="return false;" id="a_reset">リセット</a>-->
					<!--<input type="image" id="a_search" onClick="javascript:do_submit_with_param('autonomy');" src="img/subbtn_submit.png" onMouseOver="this.src='img/subbtn_submit_d.png'" onMouseOut="this.src='img/subbtn_submit.png'" alt="一覧表示">-->
					<a class="button" href="javascript:void(0)" onclick="javascript:do_submit_with_param('autonomy');" id="a_search">送信内容の確認へ</a>     
				</div>
			</td>
		  </tr>

    </table>
</div>
	<!-- ここまで自治体フォーム -->

<!-- ここからトップへ戻る -->

	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>

<!-- ここまでトップへ戻る -->

<input type="hidden" id = "in_callkind" name="in_callkind" value="">                        
</form>
</div>

<!-- ここまで財団フォーム -->

<!-- ここからセミナー -->
<!--
<div id="05"></div>

	<img src="img/title_sem_line.jpg" alt="" />
	<img src="img/title_sem.jpg" alt="セミナー・イベント" />
	<img src="img/con_sem.jpg" alt="セミナー・イベント" />
-->
<!-- ここまでセミナー -->

<!-- ここからトップへ戻る -->
<!-- 
	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>
-->
<!-- ここまでトップへ戻る -->

<!-- ここから会社概要 -->
<!--

<div id="06"></div>

	<img src="img/title_com_line.jpg" alt="" />
	<img src="img/title_com.jpg" alt="会社概要" />

<div id="company">

<div id="company_left">

	<div id="com_msg">

		<div id="navit_logo">
			<a href="http://www.navit-j.com/" target="_blank" /><img src="img/navit_logo.jpg" alt="株式会社ナビット"></a>
		</div>

		<div id="msg">
			入札なうを運営する「株式会社ナビット」は、様々なサービスを<br />提供しております。その実績をご評価いただき、テレビ他、<br />多様なメディアに取り上げていただいております。
		</div>

		<div class="clear">
		</div>
		<hr />
	</div>

	<div id="pre_msg">

		<div id="pre_photo">
			<img src="img/com_fukui.jpg" alt="福井泰代">
		</div>

		<div id="pmsg"><span style="font-size:1.5em; font-weight:bold; color:#FF4000;">のりかえ便利マップをご存じですか？</span><br />
ひとりの主婦が、小さな子供を抱えて駅で困った経験、階段やエスカレータ<br />ーがどこにあって、出口はどこなのか？重いベビーカーを押しながら思いつ<br />いた答えがのりかえ便利マップのきっかけでした。<br />その主婦がナビット代表の福井です。
		</div>

		<div class="clear">
		</div>

	</div>

</div>

	<div id="company_right">
		<img src="img/com_noriben.jpg" alt="のりかえ便利マップ">
	</div>

	<div class="clear">
	</div>

	<div id="company_bottom">
	<hr />
		<h3 style="padding:0 0 0 5px; font-size:19px; font-weight:bold; float:left;">
			株式会社ナビット
		</h3>

		<div style="padding:5px 150px 0 0; font-size:15px; line-height:15px; float:right;">
			本　　社：〒102-0074 東京都東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F<br />
	　　　　　TEL：03-5215-5713　FAX：03-5215-5702　URL：<a href="http://www.navit-j.com/" target="_blank" />http://www.navit-j.com/</a><br />
		</div>
		<div style="padding:5px 121px 0 0; font-size:15px; line-height:15px; float:right;">
			大阪支社：〒530-0001 大阪府大阪市北区梅田1丁目11番4-1100　大阪駅前第四ビル10階1-1号室<br />
	　　　　　TEL：06-4799-9201　FAX：06-4799-9011
		</div>
	</div>
	</div>
-->
<!-- ここまで会社概要 -->

<!-- ここからトップへ戻る -->
<!--
	<div class="re_top">
		<a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a>
	</div>
	<div class="clear">
	</div>
-->
<!-- ここまでトップへ戻る -->
</div>
<!-- ここまでメインコンテンツ -->

<!-- ここからfooter -->
<br />
<div id="footer">
            <ul style="list-style-type: none; display: table; margin-left: auto; margin-right: auto; font-size: 12px;">
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="http://www.navit-j.com/privacy/index.html" target="_blank">個人情報保護方針</a>
                </li>
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="#">利用規約</a>
                </li>
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="#">特定商取引法</a>
                </li>
                <li style="display: table-cell; padding: 10px 10px 0 10px;">
                <a href="http://www.navit-j.com/company/profile.html" target="_blank">会社概要</a>
                </li>
            </ul>          
</div>

<!-- ここまでfooter -->


</div>


<!-- スムーズスクロール -->
{literal}
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->
<script>
$(function(){
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 700; // ミリ秒
      // アンカーの値取得
      var href= $(this).attr("href");
      // 移動先を取得
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});
</script>
<!-- スムーズスクロール -->

<!-- グローバルナビ -->
<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>-->

<!-- グローバルナビ -->

<!-- googleanalytics TAG -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45128510-12', 'auto');
  ga('send', 'pageview');

</script>
<!-- googleanalytics TAG -->
{/literal}


</body>
</html>
