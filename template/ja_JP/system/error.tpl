<div class="top_header_title" style="margin-top:30px;"><span class="top_header_title_border">システムエラー</span></div>
<div style="text-align: center;">
    <b>システムにエラーが発生しました。</b><br />
    {$app.error.message}
    {if $app.code != ""}
        コード:{$app.error.code}<br />
    {/if}
</div>
<br />
<div style="text-align: center;">
    <a class="button" href="index.php">トップへ戻る</a>
</div>

