{literal}
<script type="text/javascript">
function displayGetListForm(){
    $("#get_list_confirm").lightbox_me({centered: true,closeSelector:'#get_list_close',overlaySpeed:100,overlayCSS:{background:'#D3C9BA',opacity: .8}});
}
</script>
{/literal}

<!-- ここからリスト取得確認 -->
<div id="get_list_confirm" style="display:none;">
    <div style="width:100%;background-color: #ffffff;">
        <img src="img/logo.jpg" alt="Faxなう" style="width:178px;height:48px;"  />
        <div style="width:24px;">
            <a href="javascript:void(0);" style="position:relative; top:-60px; left:592px;"><img id="get_list_close" src="img/close_round.png" width="20px" height="20px" alt="閉じる" /></a>
        </div>
    </div>
    <hr />
    <div style="text-align:center;">
    <br />
    <span style="font-size:20px;color:#3A2409;margin-left:10px;">ポイントを利用してFAX送信手続きを行います。<br />よろしいですか？<br /></span>
    <span style="font-size:12px;color:#3A2409;margin-left:-0px;">※この操作は取り消しができませんのでご注意ください。<br /></span>
    <br />
	<a class="button5_en" href="javascript:void(0)"  id="get_list_close">戻る</a>&nbsp;&nbsp;
	<a class="button5" href="index.php?action_send=true" id="send_interest">送信する</a>
    </div>
</div>
<!-- ここまでリスト取得確認 -->