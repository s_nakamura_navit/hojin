<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_secure_complete" value="true">
{if $app.db_regist_result == "-1" || $app.mail_send_result == "-1"}
<div class="top_header_title" ><span class="top_header_title_border">新規ID・パスワード発行送信エラー</span></div>
<div style="text-align:center;">
    <span style="font-weight:bold;font-size:100%;">ご入力頂いた内容で送信ができませんでした。</span><br />
    <br />
    大変お手数ではございますが再度こちらから再送信をお願い致します。>>
    <a href="./index.php?action_password_registration=true"> 新規ID・パスワード発行フォームへ</a><br />
</div>
      <br />
      <br />
<div style="margin-left: 180px;">
      <span style="color:#666;font-size:80%;">
        【ご登録に失敗する場合、以下の原因が考えられます】<br />
        ※通信環境の混雑等の理由によるもの<br /><br />
        ※ご登録頂いたメールアドレスへの送信失敗によるもの(ご登録頂いたメールアドレスをご確認ください)<br />
        <br />
        <br />
      </span>
</div>
{else}
<div class="top_header_title" ><span class="top_header_title_border">新規ID・パスワード発行送信完了</span></div>
<div style="text-align:center;">
    <span style="font-weight:bold;font-size:100%;">新規ID・パスワード発行を受け付けました。</span><br />
    <br />
</div>
<div style="margin-left: 280px;">
    ご入力頂いたメールアドレスに、<br />新しく発行したID・パスワードを記載したメールを送信致しました。<br />受信のご確認をお願い致します。<br />
</div>
      <br />
      <br />
<div style="margin-left: 150px;">
      <span style="color:#666;font-size:80%;">
        ※通信環境の混雑等の理由によりメールの到着に少々お時間がかかる場合がございます。<br /><br />
        ※迷惑メール防止機能により当サイトからのメールが迷惑メールと間違えられ、メール受信画面に表示されない場合が<br />
        &nbsp;&nbsp;&nbsp;ございます。迷惑メールフォルダやゴミ箱に自動的に振り分けられている場合がございますので、一度ご確認頂きま<br />
        &nbsp;&nbsp;&nbsp;すようお願い致します。<br />

      </span>
</div>
{/if}
<div style="font-size:0.7em;margin-left: 120px;">
<!--
<span>
【注意事項】<br />
<span style="color:#64c601;font-size:16px;">■</span>ID・PWはご登録頂いたメールアドレスにご連絡致します。<br />
<span style="color:#64c601;font-size:16px;">■</span>フリーメールアドレスはご利用頂けない場合がございます。<br />
</span>
-->
</div>

                <div class="mod_form_btn">

	<div style="margin-top:20px;margin-left:400px;">
        <a class="button2" href="index.php" id="to_top_btn">TOPへ</a>
        </div>
                </div>
</div>

<input type="hidden" name="in_company_name" id="in_company_name" value="{$app.data.company_name}">
<input type="hidden" name="in_phone_no1" id="in_phone_no1" value="{$app.data.phone_no1}">
<input type="hidden" name="in_phone_no2" id="in_phone_no2" value="{$app.data.phone_no2}">
<input type="hidden" name="in_phone_no3" id="in_phone_no3" value="{$app.data.phone_no3}">
<!-- <input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="{$app.data.k_phone_no1}"> -->
<!-- <input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="{$app.data.k_phone_no2}"> -->
<!-- <input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="{$app.data.k_phone_no3}"> -->
<input type="hidden" name="in_email" id="in_email" value="{$app.data.email}">

<input type="hidden" name="back" id="back" value="0">

</form>
	<!-- ここまで入力フォーム -->









</div>
<!-- ここまでメインコンテンツ -->
