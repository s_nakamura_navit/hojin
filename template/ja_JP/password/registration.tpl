{literal}
<script type="text/javascript">
function do_submit_password(){
    document.f2.submit();
}
</script>
{/literal}
<form action="{$script}" name="f2" method="POST" id="f2">
<input type="hidden" name="action_password_complete" value="true">
<div class="top_header_title" ><span class="top_header_title_border">新規ID・パスワード発行画面</span></div>
<div style="margin-left: 120px;">パスワードをお忘れの方は、下記フォームに必要事項を入力後、送信ボタンを押してください。<br /></div>
<div style="margin-left: 120px;">新しく発行したID・パスワードを記載したメールをお届けいたします。<br /></div>
<div style="font-size:0.7em;margin-left: 120px;">
<!--
    <span>
    【注意事項】<br />
    <span style="color:#562E39;font-size:16px;">■</span>パスワードはご登録頂いたメールアドレスにご連絡致します。<br />
    <span style="color:#562E39;font-size:16px;">■</span>フリーメールアドレスはご利用頂けない場合がございます。<br />
    </span>
-->
</div>
<br />


<div style="margin-left: 120px;"><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><span style="font-size:80%;"> は必須入力項目です</span></div>
<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
    <tr>
        <td class="l_Cel_01_01">会社名　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td class="s_Cel">例：株式会社ナビット<!--　※法人のお客様のみご登録いただけます。--><br />
            <input type="text" id="in_company_name" name="in_company_name" value="{$app.data.company_name}" size="50" maxlength="50"  style="font-size:16px;" />
            {if is_error('in_company_name')}<br /><span class="error" style="color:red;">{message name="in_company_name"}</span>{/if}
        </td>

    <tr>
        <td class="l_Cel_01_01">電話番号　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><br />（半角数字）</td>
        <td width="550" class="s_Cel">例：03-5215-5713<br />
            <input type="text" id="in_phone_no1" name="in_phone_no1" value="{$app.data.phone_no1}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_phone_no2" name="in_phone_no2" value="{$app.data.phone_no2}" size="10" maxlength="4"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_phone_no3" name="in_phone_no3" value="{$app.data.phone_no3}" size="10" maxlength="4"  style="font-size:16px;width:100px;" class="tel-number" />
            {if is_error('in_phone_no1')}<div class="error" style="color:red;">{message name="in_phone_no1"}</div>{/if}
            {if is_error('in_phone_no2')}<div class="error" style="color:red;">{message name="in_phone_no2"}</div>{/if}
            {if is_error('in_phone_no3')}<div class="error" style="color:red;">{message name="in_phone_no3"}</div>{/if}
        </td>
    </tr>

<!--
    <tr>
        <td class="l_Cel_01_01">携帯電話番号<br />（半角数字）</td>
        <td width="550" class="s_Cel">例：090-1234-5789<br />
            <input type="text" id="in_k_phone_no1" name="in_k_phone_no1" value="{$app.data.k_phone_no1}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_k_phone_no2" name="in_k_phone_no2" value="{$app.data.k_phone_no2}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_k_phone_no3" name="in_k_phone_no3" value="{$app.data.k_phone_no3}" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            {if is_error('in_k_phone_no1')}<div class="error" style="color:red;">{message name="in_k_phone_no1"}</div>{/if}
            {if is_error('in_k_phone_no2')}<div class="error" style="color:red;">{message name="in_k_phone_no2"}</div>{/if}
            {if is_error('in_k_phone_no3')}<div class="error" style="color:red;">{message name="in_k_phone_no3"}</div>{/if}
        </td>
    </tr>
-->

    <tr>
        <td class="l_Cel_01_01">メールアドレス　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">
            <input type="text" id="in_email" name="in_email" value="{$app.data.email}" size="50" maxlength="50"  style="font-size:16px;" />
            {if is_error('in_email')}<div class="error" style="color:red;">{message name="in_email"}</div>{/if}
            {if is_error('in_user_data')}<br /><span class="error" style="color:red;">{message name="in_user_data"}</span>{/if}
        </td>
    </tr>

    <tr>
        <td colspan="3">
                <!-- ここからトップへ戻る -->
	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
                <!-- ここまでトップへ戻る -->
        </td>
    </tr>
</table>
</div>
</form>
                <div class="mod_form_btn">
	<div style="margin-top:20px;margin-left:280px;">
        <a class="button" href="index.php" id="back_btn">戻る</a>
	</div>
	<div style="margin-top:-46px;margin-left:520px;">
	    <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit_password();" id="send_btn">送信</a>
	</div>
                </div>


        </div>
	<!-- ここまで入力フォーム -->

<br />
<div align="center">
<span id="ss_gmo_img_wrapper_100-50_image_ja">
<a href="https://jp.globalsign.com/" target="_blank" rel="nofollow">
<img alt="SSL　GMOグローバルサインのサイトシール" border="0" id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_100-50_ja.gif">
</a>
</span><br />
<span style="font-size:8px;">
<script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_100-50_ja.js" defer="defer"></script>
このサイトはグローバルサインにより認証されています。<br />SSL対応ページからの情報送信は暗号化により保護されます。
</span><br />
</div>
<!-- ここまでメインコンテンツ -->
