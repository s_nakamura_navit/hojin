{literal}
<!--
<script type="text/javascript" src="js/jquery.loading.js"></script>
-->
{/literal}

{literal}
<script type="text/javascript">
	function header_line(){
		check = document.f_get.in_header_check.checked;
		if(check == true){
			document.getElementById("disp").style.display = "block";
		}else{
			document.getElementById("disp").style.display = "none";
		}
	}

//参照ボタンデザイン変更
$(function(){
    fileUploader();
});
var fileUploader = function(){
    var target = $('.fileUploder');

    target.each(function(){
        var txt = $(this).find('.txt');
        var btn = $(this).find('.btn');
        var uploader = $(this).find('.uploader');

        uploader.bind('change',function(){
            txt.val($(this).val());
        });

        btn.bind('click',function(event){
            event.preventDefault();
            return false;
        });

        $(this).bind('mouseover',function(){
            btn.css('background-position','0 100%');
        });
        $(this).bind('mouseout',function(){
            btn.css('background-position','0 0');
        });

    });
}

function header_num_close(){
	document.getElementById("auto_disp").style.display = "none";
	document.getElementById("action_confirmation").disabled = false;
	document.getElementById("action_commission_registrationauto").disabled = true;
	document.getElementById("in_auto_check").value = 'off';
	document.getElementById("manual_order_button").style.display = "block";
	document.getElementById("manual_order_button_n").style.display = "none";
	document.getElementById("auto_order_button").style.display = "none";
	document.getElementById("auto_order_button_n").style.display = "block";
}
function header_num_view(){
	document.getElementById("auto_disp").style.display = "block";
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = false;
	document.getElementById("in_auto_check").value = 'on';
	document.getElementById("manual_order_button").style.display = "none";
	document.getElementById("manual_order_button_n").style.display = "block";
	document.getElementById("auto_order_button").style.display = "block";
	document.getElementById("auto_order_button_n").style.display = "none";
}

{/literal}{if $app.auto_check == "off" || $app.auto_check == ""}{literal}
window.onload = function(){
	document.getElementById("auto_disp").style.display = "none";
	document.getElementById("action_confirmation").disabled = false;
	document.getElementById("action_commission_registrationauto").disabled = true;
	document.getElementById("in_auto_check").value = 'off';
	document.getElementById("manual_order_button").style.display = "block";
	document.getElementById("manual_order_button_n").style.display = "none";
	document.getElementById("auto_order_button").style.display = "none";
	document.getElementById("auto_order_button_n").style.display = "block";
};
{/literal}{else if $app.auto_check == "on"}{literal}
window.onload = function(){
	document.getElementById("auto_disp").style.display = "block";
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = false;
	document.getElementById("in_auto_check").value = 'on';
	document.getElementById("manual_order_button").style.display = "none";
	document.getElementById("manual_order_button_n").style.display = "block";
	document.getElementById("auto_order_button").style.display = "block";
	document.getElementById("auto_order_button_n").style.display = "none";
};
{/literal}{/if}{literal}

function do_trial(){
	document.getElementById("action_trial").disabled = false;
	document.getElementById("in_act").disabled = false;
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = true;
	document.f_get.submit();
}
function do_search_corp(){
	document.s_corp.submit();
}


window.onload = function () {
 	document.getElementById("auto_disp").style.display = "block";
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = false;
	document.getElementById("in_auto_check").value = 'on';
	document.getElementById("manual_order_button").style.display = "none";
	document.getElementById("manual_order_button_n").style.display = "block";
	document.getElementById("auto_order_button").style.display = "block";
	document.getElementById("auto_order_button_n").style.display = "none";
};
</script>
{/literal}

<!------------------------------------------------------------------------------>


<div class="input_demo">
	<p style="text-align:center; font-size:25px; color:#F55700; margin:10px 0 10px 0;">お試しは無料です。まずは1件お試しください。<br />付与できた項目だけに料金が発生します。</p>
<form action="{$script}" name="s_corp" method="POST" id="search_corp">
<input type="hidden" name="in_act" value="search_corp">

	<table class="input_table">
		<tr>
			<td class="input_td" style="color:#333;"><a href="https://navit-j.com/blog/?p=17255" target="_blank">企業名</a><span style="font-size:80%;">(必須)</span></td>
			<td>
				<input type="text" name="sc_name" value="{$app.sc_name}"  placeholder="株式会社ナビット" style="width:310px;" />
			</td>
			<td class="input_td" style="color:#333;"><a href="https://navit-j.com/blog/?p=17255" target="_blank">所在地</a><span style="font-size:80%;">(必須)</span></td>
			<td>
				<input type="text" name="sc_address" value="{$app.sc_address}" placeholder="東京都、東京都千代田区など" style="width:310px;" />
			</td>
		</tr>
		<tr>
			<td colspan=2></td>
			<td colspan=2 style="text-align:right;"><span style="font-size:12px;">※都道府県名、もしくは都道府県名＋市区町村名を１つ入れて下さい。</span></td>
		</tr>

		<tr>
			<td colspan=4 align="center">
				{if isset($app.search_check_err)}<br />
				<span class="error">{$app.search_check_err}</span>
				{/if}
				{if isset($app.search_check_err_address)}
				<span class="error">　{$app.search_check_err_address}</span>
				{/if}
			</td>
		</tr>
	</table>

<hr />

	<div class="search_btn" style="text-align:center;margin:15px auto 15px auto;">
		<a class="button" style="background-color:#F55700; border:2px solid #F55700;" href="javascript:void(0);" onclick="do_search_corp();" id="search_corp">検索</a>
	</div>
</form>

	<div class="search_result" align="center">
			{if $app.result_corp != ""}
			<span style="font-size:25px; font-weight-bold; color:#F85400;">{$app.sc_name}</span>の検索結果　<span style="font-size:25px; font-weight-bold; color:#F85400;">{$app.result_corp_cnt.sc_cnt}社</span>ヒットしました。<br /><span style="font-size:15px; color:#F00;">企業名、所在地をクリックする事で詳細をご確認いただけます。</span>
	</div>

	<table class="result_table">
		<tr>
			<th class="result_table_th" width="70%">
				企業名
			</th>
			<th class="result_table_th" width="30%">
				所在地
			</th>
		</tr>
		{foreach from=$app.result_corp key=k item=v}
		<form action="{$script}" name="f_{$v.corp_id}" method="POST" id="trial_v2">
		<input type="hidden" id="action_trialone" name="action_trialone" value="true"/>
		<input type="hidden" id="in_act" name="in_act" value="trialone"/>
		<input type="hidden" name="corp_id" value="{$v.corp_id}">
		<input type="hidden" name="sc_name" value="{$app.sc_name}">
		<input type="hidden" name="sc_address" value="{$app.sc_address}">
		<input type="hidden" name="pref_cd" value="{$app.pref_code}">
		<input type="hidden" name="city_cd" value="{$app.city_code}">

		<tr>
			<td class="result_table_td" width="70%">
				<img src="img/link_point.png" /><a href="javaScript:document.f_{$v.corp_id}.submit();">
					{$v.corp_name}
				</a>
			</td>
			<td class="result_table_td" width="30%">
				<a href="javaScript:document.f_{$v.corp_id}.submit();">
					{$v.pref} {$v.corp_city}
				</a>
			</td>
		</tr>


		</form>
{/foreach}
	</table>
{/if}

<div style="margin:15px 0;">
{if $app.search_none != ""}
<span style="font-size:25px; font-weight-bold; color:#F85400;">{$app.sc_name}</span>の検索結果　みつかりませんでした。
{/if}
</div>


<a name="price"></a>

<div class="head_title_bgcolor" style="text-align:left; margin:60px auto 20px auto;">
	■価格表
	<span style="font-size:60%;">(税別)　</span>
	<span style="font-size:80%;">1件からでもお受けできます</span>
</div>
<div style="text-align:center;">
	<span style="font-weight:bold;">ポイント単価：1ポイントあたり1円　</span>
	<span style="color:#F00; font-weight:bold;">初期費用は0円です。</span>
</div>
                 <table border="1" cellpadding="5" style="width:900px; margin:0 auto 5px;">

                        <tr>
                          <td width="12%" style="background-color:#ECFEFF;" align="center">基本料金 </td>
                          <td width="15%" align="center"><b>0ポイント</b></td>
                          <td width="12%" style="background-color:#ECFEFF;" align="center"><span style="background-color:#ECFEFF;" align="center">従業員規模 </span></td>
                          <td width="15%" rowspan="4" align="center"><b>40ポイント</b><br />
                          <span style="font-size:60%;">※1項目あたりの単価</span></td>
                          <td width="15%" style="background-color:#ECFEFF;" align="center"><span style="background-color:#ECFEFF;" align="center">代表者役職 </span></td>
                          <td width="15%" rowspan="4" align="center"><b>20ポイント</b><br />
                          <span style="font-size:60%;">※1項目あたりの単価</span></td>
                        </tr>


                       <tr>
                        <td style="background-color:#ECFEFF;" align="center">
                        業種
                        </td>
                        <td align="center">
                            <b>20ポイント</b>
                        </td>
                        <td style="background-color:#ECFEFF;" align="center"><span style="background-color:#ECFEFF;">売上高規模 </span></td>
                        <td style="background-color:#ECFEFF;" align="center"><span style="background-color:#ECFEFF;">代表者氏名 </span></td>
                        </tr>

                        <tr>
                        <td style="background-color:#ECFEFF;" align="center">
                        FAX番号
                        </td>
                        <td align="center">
                            <b>30ポイント</b>
                        </td>
                        <td style="background-color:#ECFEFF;" align="center"><span style="background-color:#ECFEFF;" align="center">資本金規模 </span></td>
                        <td style="background-color:#ECFEFF;" align="center"><span style="background-color:#ECFEFF;" align="center">URL</span></td>
                        </tr>

                        <tr>
                        <td style="background-color:#ECFEFF;" align="center">
                        上場区分
                        </td>
                        <td align="center">
                            <b>20ポイント</b>
                        </td>
                        <td style="background-color:#ECFEFF;" align="center"><span style="background-color:#ECFEFF;">設立年月 </span></td>
                        <td style="background-color:#ECFEFF;" align="center"><span style="background-color:#ECFEFF;">法人番号 <span style="color:#F00; font-weight:bold;">NEW!</span><br />(マイナンバー)</span></td>
                        </tr>

                        <!--tr>
                        <td style="background-color:#ECFEFF;" align="center">
                        決算月
                        </td>
                        <td align="center">
                            <b>10ポイント</b><br /><span style="font-size:60%;">※1項目あたりの単価になります。</span></td>
                        </td>
                        <td align="center"><b>20ポイント</b><span style="font-size:60%;">※1項目あたりの単価になります。</span></td>
                        </tr-->

                </table>
<div style="text-align:center;">
	<span style="color:#F00; font-weight:bold;">※全項目付与できた場合も、310ポイント(310円)です。</span>
</div>

<div align="center" style="margin:10px auto 10px;">
<img src="img/arrow.gif" /><br />



<!------------------------------------------------------------------------------>



<form action="{$script}" name="f_get" method="POST" id="f_get" enctype="multipart/form-data">
<input type="hidden" id="action_trial" name="action_trial" value="true" disabled />
<input type="hidden" id="in_act" name="in_act" value="furufuru_trial" disabled />


<!--
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">名刺にふるふるサービス&nbsp;ご利用タイプの選択</div>
<br />


<div style="float:left; margin:20px 0 30px 175px;">
<img id="manual_order_button" style="display:block;" onmouseover="this.src='img/btn_service_select01_d.png'" onmouseout="this.src='img/btn_service_select01.png'" alt="手動版で抽出（中2日でお戻し）" src="img/btn_service_select01.png" onclick="header_num_close();">
<img id="manual_order_button_n" alt="手動版で抽出（中2日でお戻し）" src="img/btn_service_select01_n.png" style="display:none;" onclick="header_num_close();">
</div>

<div style="float:right; margin:20px 175px 30px 0px;">
<img id="auto_order_button" style="display:none;" onmouseover="this.src='img/btn_service_select02_d.png'" onmouseout="this.src='img/btn_service_select02.png'" alt="全自動で抽出（オンライン対応）" src="img/btn_service_select02.png" onclick="header_num_view();">
<img id="auto_order_button_n" alt="全自動で抽出（オンライン対応）" src="img/btn_service_select02_n.png" style="display:block;" onclick="header_num_view();">
</div>

-->







<div style="clear:both;"></div>

<div id="03"></div>

<script type="text/javascript" src="js/jquery.loading.js"></script>
<div id="04"></div>


<a name="service"></a>
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">■複数のデータに一括で付与したい方はこちら</div>


<!--
<a href="ex_furu_article.html"><div class="button7" style="margin-top:20px; margin-left:0px; width:150px; height:60px; padding-top:15px; font-size:90%;line-height:1em;valign:middle;">依頼データ<br />について
</div></a>

<a href="ex_furu_article.html"><div class="button7" style="margin-top:20px; margin-left:0px; width:150px; height:60px; padding-top:15px; font-size:90%;line-height:1em;valign:middle;">依頼データ<br />の作成方法
</div></a>


<a href="ex_furu_article.html#koumoku"><div class="button7" style="margin-top:20px; margin-left:0px; width:150px; height:60px; padding-top:15px; font-size:90%;">項目リスト
</div></a>

<a href="ex_furu_article.html#bunrui"><div class="button7" style="margin-top:20px; margin-left:0px; width:150px; height:60px; padding-top:15px; font-size:90%;">
業種分類表
</div></a>

<a href="ex_furu_use.html#qa"><div class="button7" style="margin-top:20px; margin-left:0px; width:150px; height:60px; padding-top:15px; font-size:90%;">
よくある質問
</div></a>
-->


</div>
<div class="table">
        <input type="hidden" id="action_confirmation" name="action_confirmation" value="true" {if $app.auto_check != ""}disabled{/if}/>
        <input type="hidden" id="action_commission_registrationauto" name="action_commission_registrationauto" value="true" {if $app.auto_check == ""}disabled{/if} />
	<input id="in_auto_check" name="in_auto_check" type="hidden" value="" />
        <table border="0" cellpadding="5" cellspacing="1">
<!--
            <tr>
                <td class="l_Cel" style="vertical-align:top;">
                    <div class="middle_midashi">付与サービスタイプ</div>
                </td>
                <td colspan="2" class="r_Cel">
			<input id="in_auto_check" name="in_auto_check" type="hidden" value="" />
			<img id="auto_order_button" style="display:block;margin-right:20px;float:left;" onmouseover="this.src='img/btn_service_select02_d.png'" onmouseout="this.src='img/btn_service_select02.png'" alt="全自動で抽出（オンライン対応）" src="img/btn_service_select02.png" onclick="header_num_view();">
			<img id="auto_order_button_n" alt="全自動で抽出（オンライン対応）" src="img/btn_service_select02_n.png" style="display:none;margin-right:20px;float:left;" onclick="header_num_view();">
			<img id="manual_order_button" style="display:none;" onmouseover="this.src='img/btn_service_select01_d.png'" onmouseout="this.src='img/btn_service_select01.png'" alt="手動版で抽出（中2日でお戻し）" src="img/btn_service_select01.png" onclick="header_num_close();">
			<img id="manual_order_button_n" alt="手動版で抽出（中2日でお戻し）" src="img/btn_service_select01_n.png" style="display:block;" onclick="header_num_close();">
		</td>
            </tr>
-->
            <tr>
                <td class="l_Cel" style="vertical-align:top;">
                    <div class="middle_midashi">顧客属性付与先リスト</div>
                </td>
                <td colspan="2" class="r_Cel">
                    <p class="note">
                        属性を付与したいデータの記載されたファイルをご指定ください。<br />
                        <a href="ex_furu_article.html" target="_blank" style="text-decoration: underline;font-size:16px;"><span style="font-size:16px;">※ファイル形式：CSV（.csv）ファイル</span></a><br />
<!--
                        ※最大件数：2000件（<a href="./index.php?action_inquiry_registration=true">2000件を超える場合はお問合せください</a>）<br />
                        <a href="ex_furu_article.html" target="_blank" style="text-decoration: underline;">■CSV（依頼データ）の作り方はこちら■</a><br />
-->
		    </p>
                    <div class="fileUploder">
                        <input type="text" class="txt" readonly>
                        <button class="btn">参照</button>
                        <input type="file" class="uploader" name="in_up_list">
                    </div>
                    <p class="note">
			ヘッダー(タイトル行)あり<input type="checkbox" name="in_header_check" value="on" style="margin:0 20px;vertical-align:-5px;" onclick="header_line();" {if $app.header_check != ""}checked{/if} >
			<span id="disp" style="display:{if $app.header_check != ""}block{else}none{/if};">ヘッダーの行数<input type="text" name="in_header_line" {if $app.header_line != ""}value="{$app.header_line}"{else}value="1"{/if} style="margin:0 20px;width:50px;vertical-align:3px;"></span>
		    </p>
                    <p class="note">
                    {if $app.up_list_err != ""}<br /><span class="error">{$app.up_list_err}</span>{/if}
                    {if $app.row_err}<br /><span style="font-size:12pt;">（<a href="./index.php?action_inquiry_registration=true">2000件を超える場合はお問合せください</a>）</span>{/if}
                    {if $app.tel_value_line_err != ""}<br /><span class="error">{$app.tel_value_line_err}</span>{/if}
                    {if $app.header_line_err != ""}<br /><span class="error">{$app.header_line_err}</span>{/if}
		    </p>

		    <table id="auto_disp" style="display:{if $app.auto_check == "on"}block{else}none{/if};">
			<tr><td class="note" colspan="6" style="padding-bottom:10px;">列を指定して下さい。</td></tr>
			<tr>
			<td class="note" style="text-align:right;">会社名&nbsp;</td>
			<td class="note" style="padding-right:20px;"><input type="text" style="width:30px;" name="in_name_line_num" value="{$app.name_line_num}" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">郵便番号&nbsp;</td>
			<td class="note" style="padding-right:20px;"><input type="text" style="width:30px;" name="in_zipcode_line_num" value="{$app.zipcode_line_num}" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">住所&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_address_line_num" value="{$app.address_line_num}" />&nbsp;列目</td>
			</tr>
			<tr>
			<td class="note" style="text-align:right;">電話番号&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_tel_line_num" value="{$app.tel_line_num}" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">URL&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_I_URL_line_num" value="{$app.I_URL_line_num}" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">メールアドレス&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_I_MAILADDR_line_num" value="{$app.I_MAILADDR_line_num}" />&nbsp;列目</td>
			</tr>
		    </table>

                    <p class="note">
                    {if $app.name_line_num_err != ""}<br /><span class="error">{$app.name_line_num_err}</span>{/if}
                    {if $app.address_line_num_err != ""}<br /><span class="error">{$app.address_line_num_err}</span>{/if}
                    {if $app.tel_line_num_err != ""}<br /><span class="error">{$app.tel_line_num_err}</span>{/if}
		    </p>
                </td>
            </tr>

            <tr>
                <td class="l_Cel" style="vertical-align:top;">
                    <div class="middle_midashi">付与したい属性項目</div>
                </td>
                <td colspan="2" class="r_Cel">
                    <p class="note">

		    {foreach from=$app.property_list key=k item=v}
                        <input type="checkbox" name="in_property_check[]" value="{$k}" {if isset($app.property_checked.$k)}{$app.property_checked.$k}{/if}>{$v}<br/>
		    {/foreach}

                    {if $app.property_check_err != ""}<br /><span class="error">{$app.property_check_err}</span>{/if}

			<!--
                        【記載形式】<br />
                        {if $logined_name != ""}
			<a href="ex_furu_article.html#bunrui" target="_blank"/>業種リストを確認する</a><br />
			<a href="ex_furu_article.html#koumoku" target="_blank"/>項目リストを確認する</a>


                        {else}
			<a href="ex_furu_article.html#bunrui" target="_blank"/>業種リストを確認する</a><br />
			<a href="ex_furu_article.html#koumoku" target="_blank"/>項目リストを確認する</a>
			{/if}
                        </span>
			-->
                    </p>
                </td>
            </tr>

            <tr><td colspan="3" style="border-bottom :1px solid #017680;"></td></tr>

            <tr>
                <td colspan="4">
                    <div id="submit_btn">
                        {if $logined_name != ""}
                        <a class="button" href="javascript:void(0);" onclick="document.f_get.submit();" id="search_button">ご依頼の確認</a>
                        {else}
                        <a class="button" href="javascript:void(0);" onclick="displaySendForm();" id="a_search">ご依頼の確認</a>
                        {/if}
                    </div>
                </td>
            </tr>


            <tr>
                <td colspan="2">

	<!-- ここから採用事例 -->

<div style="text-align:center; margin:0 auto;">
	<a href="https://navit-j.com/blog/?p=16957" target="_blank"><img src="img/btn_faq.png" onmouseover="this.src='img/btn_faq_d.png'" onmouseout="this.src='img/btn_faq.png'"alt="ご依頼前に必ずお読みください" /></a>
</div>


	<!-- ここまで採用事例 -->
                </td>
            </tr>

        </table>

</div>

<!--
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">お試し！&nbsp;名刺にふるふるサービス</div>
<br />
<div style="margin: 0 50px 0 50px;">
情報付与を行いたい企業データを以下フォームより、情報付与依頼をお試しいただけます！<br />
是非こちらより名刺にふるふるサービスをご体感下さい！<br />
<br />



<table style="width:900px;text-align:left;margin:10px auto 0px auto;border-collapse: collapse;">
<tr><td colspan="4">
	{if $app.trial_check_err.uninput != ""}<br /><span style="font-size:80%;" class="error">{$app.trial_check_err.uninput}</span>{/if}
	{if $app.trial_check_err.uncolumn != ""}<br /><span style="font-size:80%;" class="error">{$app.trial_check_err.uncolumn|nl2br}</span>{/if}
</td></tr>
<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td></td>
	<td style="padding:10px;font-size:80%;">企業名(必須)<br /><span style="font-size:70%;">※企業名を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">電話番号(必須)<br /><span style="font-size:70%;">※電話番号を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">所在地<br /><span style="font-size:70%;">※所在地を入れて下さい</span></td>
<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_1" value="{$app.data_corp_1}"  placeholder="株式会社ナビット" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_1" value="{$app.data_tel_1}" placeholder="03-5215-5713" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_1" value="{$app.data_address_1}" placeholder="東京都千代田区九段南1-5-5 Daiwa九段ビル8F" style="width:300px;" />
	</td>
</tr>



<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td></td>
	<td style="padding:10px;font-size:80%;">企業名(必須)<br /><span style="font-size:70%;">※企業名を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">電話番号(必須)<br /><span style="font-size:70%;">※電話番号を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">所在地<br /><span style="font-size:70%;">※所在地を入れて下さい</span></td>
<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_1" value="{$app.data_corp_1}"  placeholder="株式会社ナビット" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_1" value="{$app.data_tel_1}" placeholder="03-5215-5713" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_1" value="{$app.data_address_1}" placeholder="東京都千代田区九段南1-5-5 Daiwa九段ビル8F" style="width:300px;" />
	</td>
</tr>


<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報2</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_2" value="{$app.data_corp_2}"  placeholder="企業名" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_2" value="{$app.data_tel_2}" placeholder="電話番号" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_2" value="{$app.data_address_2}" placeholder="所在地" style="width:300px;" />
	</td>
</tr>
<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報3</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_3" value="{$app.data_corp_3}"  placeholder="企業名" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_3" value="{$app.data_tel_3}" placeholder="電話番号" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_3" value="{$app.data_address_3}" placeholder="所在地" style="width:300px;" />
	</td>
</tr>

</table>
<div style="text-align:center;margin:30px auto 50px auto;">
	<a class="button" href="javascript:void(0);" onclick="do_trial();" id="trial_button">お試し依頼の確認</a>
</div>
-->
    </form>





<div align="center" style="margin:0px auto 15px;"><a href="http://navit-j.com/furufuru_manga.html" target="_blank"><img src="img/dl_manga.jpg" onmouseover="this.src='img/dl_manga_d.jpg'" onmouseout="this.src='img/dl_manga.jpg'" alt="名刺にふるふるマンガダウンロード" /></a></div>

<div style="text-align:center; margin:10px auto;"><a href="/service/furufuru/inquiry/sample_dl.html" target="_blank"><img src="img/sample_dl.jpg" alt="サンプルデータダウンロード" /></a></div>

<div align="center" style="margin:15px 0 15px 0;">
<span style="font-weight:bold; font-size:23px;"><a href="http://www.navit-j.com/blog/?p=497" target="_blank">ＳＦＡでの採用事例</a></span><br />

<!--
<span style="font-weight:bold; font-size:23px;"><a href="
http://www.navit-j.com/blog/?p=10159" target="_blank">名刺にふるふるサービス全自動版リリース</a></span>
-->
</div>


<a name="kensu"></a>
<div class="head_title_bgcolor" style="width:100%; text-align:left;margin:40px auto 10px auto;">
	■企業総件数：680万社
</div>
	<table border="1" cellpadding="5" style="width:800px; margin:0 auto; font-size:17px;">

		<tr>
			<td width="20%" style="background-color:#D7E0EA;">業種</td>
			<td width="30%" align="center">約624万社</td>
			<td width="20%" style="background-color:#D7E0EA;">売上規模</td>
			<td width="30%" align="center">約119万社</td>
		</tr>

		<tr>
			<td style="background-color:#D7E0EA;">FAX番号</td>
			<td align="center">約340万社</td>
			<td style="background-color:#D7E0EA;">従業員数規模</td>
			<td align="center">約117万社</td>
		</tr>

		<tr>
			<td style="background-color:#D7E0EA;">企業URL</td>
			<td align="center">約190万社</td>
			<td style="background-color:#D7E0EA;">資本金規模</td>
			<td align="center">約23万社</td>
		</tr>

		<tr>
			<td style="background-color:#D7E0EA;">代表者名</td>
			<td align="center">約125万社</td>
			<td style="background-color:#D7E0EA;">法人番号<br />(マイナンバー)</td>
			<td align="center">約80万社 <span style="color:#f00;">NEW</span></td>
		</tr>

		<tr>
			<td style="background-color:#D7E0EA;">企業<br />メールアドレス</td>
			<td align="center">約23万社 <span style="color:#f00;">NEW</span></td>
			<td style="background-color:#D7E0EA;"> </td>
			<td> </td>
		</tr>

	</table>

<div class="text" style="margin:3px 0 0 680px;">
	<span style="font-size:80%;">2016年5月現在</span>
</div>



<div class="head_title_bgcolor" style="width:100%; text-align:left;margin:40px auto 10px auto;">
	■お知らせ＆ニュースリリース
</div>

                                    <object class="test_news" data="/service/common/announce/list.php" type="text/html" name="notice_test" width="90%" height="270px" style="text-algin:center;margin:10px 0 20px 20px;border:solid 1px #ccc;">
</object>
<p style="margin:-17px 0 0 720px;font-size:0.8em;"><a href="../../press/info.html">全記事はこちら</a></p>
</div>