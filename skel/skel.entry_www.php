<?php
/**
 *  {$action_name}.php
 *
 *  @author     {$author}
 *  @package    Subsidy
 *  @version    $Id$
 */
require_once '{$dir_app}/Subsidy_Controller.php';

Subsidy_Controller::main('Subsidy_Controller', '{$action_name}');
?>
