<?php
/**
 *  {$view_path}
 *
 *  @author     {$author}
 *  @package    Subsidy
 *  @version    $Id$
 */

/**
 *  {$forward_name} view implementation.
 *
 *  @author     {$author}
 *  @access     public
 *  @package    Subsidy
 */
class {$view_class} extends Subsidy_ViewClass
{
    /**
     *  preprocess before forwarding.
     *
     *  @access public
     */
    function preforward()
    {
    }
}

?>
