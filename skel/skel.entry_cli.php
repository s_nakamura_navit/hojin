<?php
/**
 *  {$action_name}.php
 *
 *  @author     {$author}
 *  @package    Subsidy
 *  @version    $Id$
 */
chdir(dirname(__FILE__));
require_once '{$dir_app}/Subsidy_Controller.php';

ini_set('max_execution_time', 0);

Subsidy_Controller::main_CLI('Subsidy_Controller', '{$action_name}');
?>
