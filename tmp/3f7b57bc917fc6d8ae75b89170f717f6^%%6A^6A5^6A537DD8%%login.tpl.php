<?php /* Smarty version 2.6.27, created on 2017-07-28 13:07:30
         compiled from login.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "forced_login_dialog.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f2" method="POST" id="f2">
<input type="hidden" name="action_login_do" value="true">

<div class="top_header_title" ><span class="top_header_title_border">ログイン</span></div>
<div style="margin-left: 296px;">ログインIDとパスワードを入力してください。<br /></div>
<div style="font-size:0.7em;margin-left: 120px;">
    <span>
    </span>
</div>
<br />

<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 208px;">
    <tr>
        <td style="text-align:right;">ログインID</td>
        <td>
            <input type="text" name="in_id" value="<?php echo $this->_tpl_vars['app']['data']['id']; ?>
"  size="30" maxlength="64" style="font-size:20px;width:300px;">

        </td>
    </tr>
    <tr>
        <td style="text-align:right;">パスワード</td>
        <td>
            <input type="password" name="in_pw"  value="" size="30" maxlength="64" style="font-size:20px;width:300px;">

        </td>
    </tr>
    <tr>
		<td style="text-align:right;vertical-align top;">文字認証</td>
		<td>
			<img src="index.php?action_captcha=true" style="margin-bottom:5px" id ='captcha'><br />
			<input type="text" name="in_captcha_code" size="15" maxlength="8" style="font-size:20px;width:300px;" /><br />
			<a href="#" onclick="document.getElementById('captcha').src = 'index.php?action_captcha=true' + Math.random(); return false">[画像変更]</a>
		</td>
	</tr>
    <tr>
        <td>
        </td>
        <td style="font-size:14px; text-align:left;">
		<a href="./index.php?action_password_registration=true">ログインIDやパスワードを忘れた場合はこちら</a>
        </td>
    </tr>

<tr>
	<table style="margin-left: 180px;">
		<tr>
			<td align="right" valign="top"><img alt="！" src="img/mark_01.png" width="25px" height="25px"/></td>
			<td><span style="font-size:20px; margin-top:20px;">ログインID、パスワード、ポイントは以下のサービスで共通利用できます。</span></td>
		</tr>
	</table>
</tr>

<tr>
	<table style="margin-left: 180px;">
		<tr>
			<td align="right" valign="top"><img alt="！" src="img/mark_01.png" width="25px" height="25px"/></td>
			<td><span style="font-size:20px; margin-top:20px;">２つ以上のサービスに同時ログインはできません。<br />一旦ログアウトしてからご利用ください。<br />不正データダウンロード防止措置ですのでご了承ください。</span></td>
		</tr>
	</table>
</tr>


    <tr>
        <td colspan=2>
            <div class="mod_form_btn">
                <div style="margin-top:20px;margin-left:410px;"><a class="button2" href="javascript:void(0)" onclick="javascript:do_submit();" id="login_btn">ログイン</a></div>
            </div>
        </td>
    </tr>
</table>
<!--エラーメッセージ-->
<?php if (count ( $this->_tpl_vars['errors'] )): ?>
<table class="error" align="center">
<?php $_from = $this->_tpl_vars['errors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['error']):
?>
<tr class="error" style="color:red;"><td><?php echo $this->_tpl_vars['error']; ?>
</td></tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php endif; ?>
<!--エラーメッセージ-->
</div>

</div>
</form>
	<!-- ここまで入力フォーム -->
</div>
<!-- ここまでメインコンテンツ -->