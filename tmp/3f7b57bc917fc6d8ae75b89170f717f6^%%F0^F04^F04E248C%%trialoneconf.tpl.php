<?php /* Smarty version 2.6.27, created on 2017-07-28 17:34:34
         compiled from trialoneconf.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'uniqid', 'trialoneconf.tpl', 140, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "getlist_dialog.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<script type="text/javascript" src="js/jquery.getlist_loading.js"></script>
<div class="head_title_bgcolor" style="text-align:left;margin-top:50px;">ご依頼内容の確認<span style="font-size:18px;margin-left:50px;">お客様の現在のポイント数：<?php echo $this->_tpl_vars['app']['point']['user']; ?>
ポイント</span></div>
<br />


<div class="table">
    <table class="result_conf" border="0" cellpadding="5" cellspacing="1">
	<tr height="100">
            <td class="l_Cel">
                <div class="sub_head">企業名</div>
            </td>

            <td colspan="2" class="r_Cel">
                    <?php echo $this->_tpl_vars['app']['corp_detail']['I_CORP']; ?>

            </td>
	</tr>


        <tr height="100">
            <td class="l_Cel">
                <div class="sub_head">属性付与項目</div>
            </td>

            <td colspan="2" class="r_Cel">
		<?php $_from = $this->_tpl_vars['app']['grant_item']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['name']):
?>
			<?php echo $this->_tpl_vars['name']['ttl']; ?>
<br />
		<?php endforeach; endif; unset($_from); ?>
            </td>
	</tr>




<tr>
<td colspan="2">
<div id="detail">
<table width="100%" id="detail" style="border:1px solid #666">
<tr>
        <td colspan="2" style="font-size:18px; text-align:left;background-color:#666; font-weight:bold; color:#fff;">必要ポイントの内訳</td>
</tr>
</tr>
        <td style="font-size:17px; font-weight:bold; text-align:left;">価格</td>
        <td class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;"></td>
</tr>


<?php $_from = $this->_tpl_vars['app']['grant_item']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['v']):
?>
<tr>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;"><?php echo $this->_tpl_vars['v']['ttl']; ?>
</td>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;"><?php echo $this->_tpl_vars['v']['point']; ?>
&nbsp;ポイント</td>
</tr>
<?php endforeach; endif; unset($_from); ?>


<tr>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">小計</td>
        <td colspan="1" class="bls" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;"><?php echo $this->_tpl_vars['app']['subtotal']; ?>
&nbsp;ポイント</td>
</tr>
<tr>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">消費税８％</td>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;"><?php echo $this->_tpl_vars['app']['tax']; ?>
&nbsp;ポイント</td>
</tr>
<tr>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:left;">計</td>
        <td colspan="1" style="font-size:17px; font-weight:bold; text-align:right; border-left:1px solid #666;"><?php echo $this->_tpl_vars['app']['total']; ?>
&nbsp;ポイント</td>
</tr>
</table>
</div>
</td>
</tr>


<tr>
<td class="l_Cel">
<div class="sub_head">お客様の<br />
現在のポイント数</div>
</td>

            <td colspan="2" class="r_Cel">
		<?php echo $this->_tpl_vars['app']['point']['user']; ?>
&nbsp;ポイント
            </td>
	</tr>

        <tr>
            <td class="l_Cel">
                <div class="sub_head">必要ポイント</div>
            </td>

            <td colspan="2" class="r_Cel">
		<?php echo $this->_tpl_vars['app']['total']; ?>
&nbsp;ポイント
            </td>
	</tr>

	<?php if ($this->_tpl_vars['app']['act'] == 'no_point'): ?>
        <tr>
            <td class="l_Cel">
                <div class="sub_head">不足ポイント</div>
            </td>

            <td colspan="2" class="r_Cel">
		<?php echo $this->_tpl_vars['app']['point_lack']; ?>
&nbsp;ポイント
            </td>
	</tr>
	<?php endif; ?>






	<tr>
	    <td colspan="3">
                <hr />
                <div id="submit_btn">
		<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f_back" method="POST" id="f_back" enctype="multipart/form-data">

		    <input type="hidden" id="action_trialone" name="action_trialone" value="true"/>
			<input type="hidden" id="in_act" name="in_act" value="trialone"/>
			<input type="hidden" name="corp_id" value="<?php echo $this->_tpl_vars['app']['corp_detail']['ID']; ?>
">
			<input type="hidden" name="sc_name" value="<?php echo $this->_tpl_vars['app']['sc_name']; ?>
">
			<input type="hidden" name="sc_address" value="<?php echo $this->_tpl_vars['app']['sc_address']; ?>
">
			<input type="hidden" name="pref_cd" value="<?php echo $this->_tpl_vars['app']['pref_cd']; ?>
">
			<input type="hidden" name="city_cd" value="<?php echo $this->_tpl_vars['app']['city_cd']; ?>
">
		</form>


		<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f_get" method="POST" id="f_get" enctype="multipart/form-data">
		    <?php if ($this->_tpl_vars['app']['act'] == 'no_point'): ?>
        		<input type="hidden" name="action_request_registration" value="true" />
		    	<input type="hidden" name="in_act" value="request_point_trialone" />
				<input type="hidden" name="corp_id" value="<?php echo $this->_tpl_vars['app']['corp_detail']['ID']; ?>
">
				<input type="hidden" name="sc_name" value="<?php echo $this->_tpl_vars['app']['sc_name']; ?>
">
				<input type="hidden" name="sc_address" value="<?php echo $this->_tpl_vars['app']['sc_address']; ?>
">
				<input type="hidden" name="pref_cd" value="<?php echo $this->_tpl_vars['app']['pref_cd']; ?>
">
				<input type="hidden" name="city_cd" value="<?php echo $this->_tpl_vars['app']['city_cd']; ?>
">
		    <?php else: ?>
        		<input type="hidden" name="action_trialonecomp" value="true" />
        		<input type="hidden" name="in_act" value="trialonecomp">
				<?php echo smarty_function_uniqid(array(), $this);?>

				<input type="hidden" name="corp_id" value="<?php echo $this->_tpl_vars['app']['corp_detail']['ID']; ?>
">
				<input type="hidden" name="req_id" value="<?php echo $this->_tpl_vars['app']['req_id']; ?>
">
				<input type="hidden" name="sc_name" value="<?php echo $this->_tpl_vars['app']['sc_name']; ?>
">
				<input type="hidden" name="sc_address" value="<?php echo $this->_tpl_vars['app']['sc_address']; ?>
">
				<input type="hidden" name="pref_cd" value="<?php echo $this->_tpl_vars['app']['pref_cd']; ?>
">
				<input type="hidden" name="city_cd" value="<?php echo $this->_tpl_vars['app']['city_cd']; ?>
">
		    <?php endif; ?>
		</form>
		<?php if ($this->_tpl_vars['app']['page_f'] == 1): ?>
		<a class="button" href="javascript:void(0)" onclick="document.f_back.submit();" id="back_button">戻る</a>
		<?php endif; ?>
			&nbsp;&nbsp;&nbsp;&nbsp;
		    <?php if ($this->_tpl_vars['app']['act'] == 'no_point'): ?>
                        <a class="button" href="javascript:void(0)" onclick="document.f_get.submit();" id="a_search">ポイント購入</a>
		    <?php else: ?>
                        <a class="button" href="javascript:void(0)" onclick="document.f_get.submit();" id="a_search">購入する</a>
		    <?php endif; ?>

                </div>
            </td>
	</tr>
    </table>
</div>

</form>
</div>