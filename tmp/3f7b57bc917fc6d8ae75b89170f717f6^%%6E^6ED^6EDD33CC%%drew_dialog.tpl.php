<?php /* Smarty version 2.6.27, created on 2017-07-28 17:22:48
         compiled from drew_dialog.tpl */ ?>
<link rel="stylesheet" href="./css/ditail.css" type="text/css">

<!-- ここから退会確認 -->
<div id="drew_dialog" style="display:none;">

    <div style="width:100%;background-color: #ffffff;">
        <img src="./img/logo.jpg" alt="ふるふるサービス" style="width:178px;height:48px;"  />
        <div style="width:24px;">
            <a href="javascript:void(0);" style="position:relative; top:-60px; left:740px;"><img id="drew_dialog_close" src="img/close_round.png" width="20px" height="20px" alt="閉じる" /></a>
        </div>
    </div>
    <hr />
    <div style="text-align:center;">
    <br />
    <span style="font-size:20px;color:#3A2409;" id="drew_dialog_msg">
        退会の手続きを行います。<br />
        会員向けのサービスがすべてご利用いただけなくなり、<br />
        ポイントもすべて無効となります。（ポイントの払い戻しはいたしません）<br />
        <br />
        メルマガなどの利用停止は、こちらではなく、<br />
        メールやお電話でお問い合わせいただき、お手続きください。<br />
    </span>
    <br />
    <br />
    <form action="<?php echo $this->_tpl_vars['script']; ?>
" name="delete_submit" method="POST" id="delete_submit">
    <input type="hidden" name="action_completewithdrew" value="true" />
        <a class="button5_en" href="javascript:void(0)"  id="drew_dialog_close">戻る</a>&nbsp;&nbsp;
        <a class="button5" href="javascript:void(0)" onclick="document.delete_submit.submit();" id="send_delete">退会する</a>
    </div>
    </form>

</div>
<!-- ここまで退会確認 -->

<?php echo '
<script type="text/javascript">
     function do_delete(){
         $("#drew_dialog").lightbox_me({centered: true,closeSelector:\'#drew_dialog_close\',overlayCSS:{background:\'#D3C9BA\',opacity: .8}});
     }
</script>
'; ?>
