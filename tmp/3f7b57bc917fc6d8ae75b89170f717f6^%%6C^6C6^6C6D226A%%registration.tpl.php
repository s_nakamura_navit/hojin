<?php /* Smarty version 2.6.27, created on 2017-07-28 16:46:24
         compiled from secure/registration.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'message', 'secure/registration.tpl', 192, false),)), $this); ?>
<?php echo ' 
<script type="text/javascript">
//吹き出し
$(window).load(function(){
      $(\'label\').balloon();
      
      
    if ($(\'#privacy_1\').is(\':checked\')) {
        $(\'#confirm_btn\').css({opacity:"1",cursor:"pointer"}).removeAttr(\'disabled\');
    } else {
        $(\'#confirm_btn\').css({opacity:"0.5",cursor:"default"}).attr(\'disabled\',\'disabled\');
    }
});


  	
//全選択・全解除
$(function() {
    $(\'#all_check\').on("click",function(){
        $(\'.CheckList\').prop("checked", true);
    });
});
$(function() {
    $(\'#all_clear\').on("click",function(){
        $(\'.CheckList\').prop("checked", false);
    });
});

$(function() {
    $(\'#all_check2\').on("click",function(){
        $(\'.CheckList2\').prop("checked", true);
    });
});
$(function() {
    $(\'#all_clear2\').on("click",function(){
        $(\'.CheckList2\').prop("checked", false);
    });
});

$(function() {
    $(\'#all_check3\').on("click",function(){
        $(\'.CheckList3\').prop("checked", true);
    });
});
$(function() {
    $(\'#all_clear3\').on("click",function(){
        $(\'.CheckList3\').prop("checked", false);
    });
});

//リセット(自治体)
$(function() {
    $(\'#a_reset\').on("click",function(){
        //ラジオボタン初期値セット
        $(\'#in_kind_1\').prop("checked", true);
        $(\'#in_kind_2\').prop("checked", false);
        //プルダウン初期値セット
        $(\'select[name="in_area1"]\').val("");
        $(\'#in_area2\').html(\'\');//一度select内を空に
        $(\'#in_area2\').append(\'<option id="city00000" value="\'+\'\'+\'">\'+\'市区町村を選択\'+\'</option>\');
        
        //チェックボックスクリア
        $(\'.CheckList\').prop("checked", false);
        
        $("#a_reset").blur();
    });
});
//リセット(財団)
$(function() {
    $(\'#f_reset\').on("click",function(){
        //チェックボックスクリア
        $(\'.CheckList2\').prop("checked", false);
        $(\'.CheckList3\').prop("checked", false);
        $(\'#in_keyword\').val("");
        $("#f_reset").blur();
    });
});

function doblur() { 
    var element = document.getElementById("name"); 
    element.blur(); 
} 

function do_submit(){
    
    if ($(\'#privacy_1\').is(\':checked\')) {
        document.f2.submit();
    }else{
        return false;
    }
}

/*
 　　全角->半角変換
 */
jQuery(function(){
 
    // 郵便番号の処理
    $(\'.zip-number\').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\\－|\\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
 
        // 半角数字のみ残す
        var zenkakuDel = new String( hankaku ).match(/\\d/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }
 
        $(this).val(zenkakuDel);
    });
    // 電話番号の処理
    $(\'.tel-number\').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\\－|\\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
 
        // 半角数字と+-のみ残す
        var zenkakuDel = new String( hankaku ).match(/\\d|\\-|\\+/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }
        
        $(this).val(zenkakuDel);
    });
 
    // メールアドレスの処理
    $(\'.mail-address\').change( function(){
        var zenkigou = "＠－ー＋＿．，、";
        var hankigou = "@--+_...";
        var data = $(this).val();
        var str = "";
 
        // 指定された全角記号のみを半角に変換
        for (i=0; i<data.length; i++)
        {
            var dataChar = data.charAt(i);
            var dataNum = zenkigou.indexOf(dataChar,0);
            if (dataNum >= 0) dataChar = hankigou.charAt(dataNum);
            str += dataChar;
        }
        // 定番の、アルファベットと数字の変換処理
        var hankaku = str.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
        $(this).val(hankaku);
    });
 
});

//確認ボタンの有効無効

jQuery(function(){
    $(\'#privacy_1\').change(function(){
            if ($(this).is(\':checked\')) {
                    $(\'#confirm_btn\').css({opacity:"1",cursor:"pointer"}).removeAttr(\'disabled\');
            } else {
                    $(\'#confirm_btn\').css({opacity:"0.5",cursor:"default"}).attr(\'disabled\',\'disabled\');
            }
    });
});  

function back_submit(){
	document.getElementById("action_secure_confirm").disabled = true;
	document.getElementById("action_trial").disabled = false;
	document.getElementById("in_secure_status").disabled = false;
	document.f2.submit();
}
</script>
'; ?>
 

<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f2" method="POST" id="f2">
<input type="hidden" id="action_secure_confirm" name="action_secure_confirm" value="true">
<?php if ($this->_tpl_vars['app']['data']['act'] == 'furufuru_trial'): ?>
	<input type="hidden" id="action_trial" name="action_trial" value="true" disabled />
	<input type="hidden" id="in_secure_status" name="in_secure_status" value="back" disabled />
	<input type="hidden" id="in_act" name="in_act" value="<?php echo $this->_tpl_vars['app']['data']['act']; ?>
" />
<?php endif; ?>

<div class="top_header_title" ><span class="top_header_title_border">新規会員登録</span></div>
<?php if ($this->_tpl_vars['app']['data']['act'] == 'furufuru_trial'): ?>
<div style="margin-left: 120px;">お試しサービスご利用には、以下無料の新規会員登録が必要です。<br /><br /></div>
<?php endif; ?>
        
<div style="margin-left: 120px;"><img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><span style="font-size:80%;"> は必須入力項目です</span></div>
<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
    <tr>
        <td class="l_Cel_01_01">会社名　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td class="s_Cel">例：株式会社ナビット　※法人のお客様のみご登録いただけます。<br />
            <input type="text" id="in_company_name" name="in_company_name" value="<?php echo $this->_tpl_vars['app']['data']['company_name']; ?>
" size="50" maxlength="50"  style="font-size:16px;" />
            <?php if (is_error ( 'in_company_name' )): ?><br /><span class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_company_name'), $this);?>
</span><?php endif; ?>
        </td>
    </tr>

    <tr>
        <td class="l_Cel_01_01">部署名・役職名・担当者名　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">
            例：マーケティング事業部　事業部長　ナビット　たろう<br />

            <span style="font-size:0.8em;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(部署名・役職名がない方は 「なし」 とご記入ください)</span><br />
            <span>部署名&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="in_department_name" name="in_department_name" value="<?php echo $this->_tpl_vars['app']['data']['department_name']; ?>
" size="25" maxlength="50"  style="font-size:16px;width:170px;" /></span>&nbsp;&nbsp;
            <span>役職名&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="in_post_name" name="in_post_name" value="<?php echo $this->_tpl_vars['app']['data']['post_name']; ?>
" size="25" maxlength="50"  style="font-size:16px;width:170px;" /></span>&nbsp;&nbsp;
            <br>
            <br>
            <span>姓&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="in_contractor_lname" name="in_contractor_lname" value="<?php echo $this->_tpl_vars['app']['data']['contractor_lname']; ?>
" size="25" maxlength="50"  style="font-size:16px;width:200px;" /></span>&nbsp;&nbsp;
            <span>名&nbsp;&nbsp;&nbsp;&nbsp;<input type="text" id="in_contractor_fname" name="in_contractor_fname" value="<?php echo $this->_tpl_vars['app']['data']['contractor_fname']; ?>
" size="25" maxlength="50"  style="font-size:16px;width:200px;" /></span>
            <?php if (is_error ( 'in_contractor_lname' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_contractor_lname'), $this);?>
</div><?php endif; ?>
            <?php if (is_error ( 'in_contractor_fname' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_contractor_fname'), $this);?>
</div><?php endif; ?>
        </td>
    </tr>
<!--
    <tr>
        <td class="l_Cel_01_01">会社名かな　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">例：かぶしきがいしゃなびっと<br />
            <input type="text" id="in_company_kana" name="in_company_kana" value="<?php echo $this->_tpl_vars['app']['data']['company_kana']; ?>
" size="50" maxlength="50"  style="font-size:16px;" />
            <?php if (is_error ( 'in_company_kana' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_company_kana'), $this);?>
</div><?php endif; ?>
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">支店名</td>
        <td width="550" class="s_Cel">例：東京支店<br />
            <input type="text" id="in_branch_name" name="in_branch_name" value="<?php echo $this->_tpl_vars['app']['data']['branch_name']; ?>
" size="50" maxlength="50"  style="font-size:16px;" />
            <?php if (is_error ( 'in_branch_name' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_branch_name'), $this);?>
</div><?php endif; ?>
        </td>
    </tr>
    -->
    <tr>
        <td class="l_Cel_01_01">郵便番号　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><br />（半角数字）</td>
        <td width="550" class="s_Cel">例：102-0074<br />
            <input type="text" id="in_zip1" name="in_zip1" value="<?php echo $this->_tpl_vars['app']['data']['zip1']; ?>
" size="10" maxlength="3"  style="font-size:16px;width:100px;" class="zip-number" onKeyUp="AjaxZip3.zip2addr('in_zip1','in_zip2','in_address','in_address','in_address');"/>
            －<input type="text" id="in_zip2" name="in_zip2" value="<?php echo $this->_tpl_vars['app']['data']['zip2']; ?>
" size="10" maxlength="4"  style="font-size:16px;width:100px;" class="zip-number" onKeyUp="AjaxZip3.zip2addr('in_zip1','in_zip2','in_address','in_address','in_address');"/>
            <?php if (is_error ( 'in_zip1' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_zip1'), $this);?>
</div><?php endif; ?>
            <?php if (is_error ( 'in_zip2' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_zip2'), $this);?>
</div><?php endif; ?>
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">住所　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">例：東京都東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F<br />
            <input type="text" id="in_address" name="in_address" value="<?php echo $this->_tpl_vars['app']['data']['address']; ?>
" size="100" maxlength="100"  style="font-size:16px;width:560px;" />
            <?php if (is_error ( 'in_address' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_address'), $this);?>
</div><?php endif; ?>
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">電話番号　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /><br />（半角数字）</td>
        <td width="550" class="s_Cel">例：03-5215-5713<br />
            <input type="text" id="in_phone_no1" name="in_phone_no1" value="<?php echo $this->_tpl_vars['app']['data']['phone_no1']; ?>
" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_phone_no2" name="in_phone_no2" value="<?php echo $this->_tpl_vars['app']['data']['phone_no2']; ?>
" size="10" maxlength="4"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_phone_no3" name="in_phone_no3" value="<?php echo $this->_tpl_vars['app']['data']['phone_no3']; ?>
" size="10" maxlength="4"  style="font-size:16px;width:100px;" class="tel-number" />
            <?php if (is_error ( 'in_phone_no1' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_phone_no1'), $this);?>
</div><?php endif; ?>
            <?php if (is_error ( 'in_phone_no2' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_phone_no2'), $this);?>
</div><?php endif; ?>
            <?php if (is_error ( 'in_phone_no3' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_phone_no3'), $this);?>
</div><?php endif; ?>
        </td>
    </tr>
    <!--
    <tr>
        <td class="l_Cel_01_01">携帯電話番号<br />（半角数字）</td>
        <td width="550" class="s_Cel">例：090-1234-5789<br />
            <input type="text" id="in_k_phone_no1" name="in_k_phone_no1" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no1']; ?>
" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_k_phone_no2" name="in_k_phone_no2" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no2']; ?>
" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_k_phone_no3" name="in_k_phone_no3" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no3']; ?>
" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            <?php if (is_error ( 'in_k_phone_no1' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_k_phone_no1'), $this);?>
</div><?php endif; ?>
            <?php if (is_error ( 'in_k_phone_no2' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_k_phone_no2'), $this);?>
</div><?php endif; ?>
            <?php if (is_error ( 'in_k_phone_no3' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_k_phone_no3'), $this);?>
</div><?php endif; ?>
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_01">FAX番号<br />（半角数字）</td>
        <td width="550" class="s_Cel">例：03-5215-3020<br />
            <input type="text" id="in_fax_no1" name="in_fax_no1" value="<?php echo $this->_tpl_vars['app']['data']['fax_no1']; ?>
" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_fax_no2" name="in_fax_no2" value="<?php echo $this->_tpl_vars['app']['data']['fax_no2']; ?>
" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            －
            <input type="text" id="in_fax_no3" name="in_fax_no3" value="<?php echo $this->_tpl_vars['app']['data']['fax_no3']; ?>
" size="10" maxlength="5"  style="font-size:16px;width:100px;" class="tel-number" />
            <?php if (is_error ( 'in_fax_no1' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_fax_no1'), $this);?>
</div><?php endif; ?>
            <?php if (is_error ( 'in_fax_no2' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_fax_no2'), $this);?>
</div><?php endif; ?>
            <?php if (is_error ( 'in_fax_no3' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_fax_no3'), $this);?>
</div><?php endif; ?>
        </td>
    </tr>
    
    <tr>
        <td class="l_Cel_01_01">担当者名かな　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">
            <span>せい&nbsp;<input type="text" id="in_contractor_lkana" name="in_contractor_lkana" value="<?php echo $this->_tpl_vars['app']['data']['contractor_lkana']; ?>
" size="25" maxlength="50"  style="font-size:16px;width:200px;" /></span>&nbsp;&nbsp;
            <span>めい&nbsp;<input type="text" id="in_contractor_fkana" name="in_contractor_fkana" value="<?php echo $this->_tpl_vars['app']['data']['contractor_fkana']; ?>
" size="25" maxlength="50"  style="font-size:16px;width:200px;" /></span>
            <?php if (is_error ( 'in_contractor_lkana' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_contractor_lkana'), $this);?>
</div><?php endif; ?>
            <?php if (is_error ( 'in_contractor_fkana' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_contractor_fkana'), $this);?>
</div><?php endif; ?>
        </td>
    </tr>
        -->
    <tr>
        <td class="l_Cel_01_01">メールアドレス　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">
            <input type="text" id="in_email" name="in_email" value="<?php echo $this->_tpl_vars['app']['data']['email']; ?>
" size="50" maxlength="50"  style="font-size:16px;" />
            <?php if (is_error ( 'in_email' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_email'), $this);?>
</div><?php endif; ?>
        </td>
    </tr>
        
   
	<?php if (! $this->_tpl_vars['app']['alliance']): ?>
    <tr>
        <td class="l_Cel_01_01">サイトを知ったきっかけ　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">
            <select id='in_from' name="in_from" class="select_font_s"><?php echo $this->_tpl_vars['app_ne']['pulldown']['from']; ?>
</select>
            <?php if (is_error ( 'in_from' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_from'), $this);?>
</div><?php endif; ?>
        </td>
    </tr>
	<?php endif; ?>
    <!--
    <tr>
        <td class="l_Cel_01_01">メールマガジンの送付を希望する　<img src="img/hissu50_24.png" style="display:inline;margin-bottom:-6px;" /></td>
        <td width="550" class="s_Cel">
                        <br /><br /><span class="error"></span>        </td>
    </tr>
    -->
    <tr>
        <td colspan="3">
            <div class="kiyaku">
                <div class="title">ふるふるサービス、ナビットプリペイドポイントの購入・利用規約</div>
                <pre style="font-family:Meiryo;">
<?php echo $this->_tpl_vars['app_ne']['kiyaku_txt']; ?>

                </pre>
            </div>
                <div class="mod_form_importance_btn">
                <label for="privacy_1" style="font-size:100%;" id="privacy_label"><input value="<?php echo $this->_tpl_vars['app']['data']['privacy']; ?>
" type="checkbox" name="in_privacy" id="privacy_1">上記「利用規約」に同意します。 </label>
                &nbsp;<span style="font-size:1em"></span>
                <?php if (is_error ( 'in_privacy' )): ?><div class="error" style="color:red;"><?php echo smarty_function_message(array('name' => 'in_privacy'), $this);?>
</div><?php endif; ?>
                <br />
                </div>
                <!-- ここからトップへ戻る -->
	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
                <!-- ここまでトップへ戻る -->
        </td>
    </tr>
</table>
<div style="margin-left: 120px;">上記フォームに必要事項を入力後、確認ボタンを押してください。<br /></div>
<div style="margin-left: 120px;">ご登録頂いたメールアドレスにはナビットからのお役立ち情報を送付させて頂きます。<br /></div>
<br>
<div style="margin-left: 120px;">
    <span>
    【注意事項】<br />
    <span style="color:#610B0B;font-size:16px;">■</span>ログインID・パスワードはご登録頂いたメールアドレスにご連絡致します。<br />
    <span style="color:#610B0B;font-size:16px;">■</span>フリーメールアドレスはご利用頂けない場合がございます。<br />
    </span>
</div>        
<br />        
</div>
                <div class="mod_form_btn">
			<?php if ($this->_tpl_vars['app']['data']['act'] == 'furufuru_trial'): ?>
				<div style="margin-top:20px;margin-left:280px;">
					<a class="button" href="javascript:void(0)" onclick="back_submit();" id="back_btn">戻る</a>
				</div>
				<div style="margin-top:-46px;margin-left:520px;">
			<?php else: ?>
				<div style="margin-top:20px;margin-left:380px;">
			<?php endif; ?>
				<a class="button2" href="javascript:void(0)" onclick="javascript:do_submit();" id="confirm_btn">確認</a>
			</div>
                </div>
</div>
</form>                
	<!-- ここまで入力フォーム -->
<br />
<div align="center">
<span id="ss_gmo_img_wrapper_100-50_image_ja">
<a href="https://jp.globalsign.com/" target="_blank" rel="nofollow">
<img alt="SSL　GMOグローバルサインのサイトシール" border="0" id="ss_img" src="//seal.globalsign.com/SiteSeal/images/gs_noscript_100-50_ja.gif">
</a>
</span><br />
<span style="font-size:8px;">
<script type="text/javascript" src="//seal.globalsign.com/SiteSeal/gmogs_image_100-50_ja.js" defer="defer"></script>
このサイトはグローバルサインにより認証されています。<br />SSL対応ページからの情報送信は暗号化により保護されます。
</span><br />
</div>

<!-- ここまでメインコンテンツ -->
