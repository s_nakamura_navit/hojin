<?php /* Smarty version 2.6.27, created on 2017-07-28 17:23:18
         compiled from request/confirm.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'request/confirm.tpl', 190, false),array('modifier', 'number_format', 'request/confirm.tpl', 190, false),)), $this); ?>
<link rel="shortcut icon" href="">
<?php echo ' 

<script type="text/javascript">
//吹き出し
$(window).load(function(){
      $(\'label\').balloon();
});

function doblur() { 
    var element = document.getElementById("name"); 
    element.blur(); 
} 

function do_submit(kind){
    if(kind==\'back\'){
        document.f2.submit();
    }else if(kind==\'send\'){
        document.f3.submit();
    }else if(kind==\'transaction\'){
	document.f4.submit();
    }
}

/*
 　　全角->半角変換
 */
jQuery(function(){
 
    // 郵便番号の処理
    $(\'.zip-number\').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\\－|\\＋/g,function(s){return String.pointCharCode(s.charCodeAt(0)-0xFEE0)});
 
        // 半角数字のみ残す
        var zenkakuDel = new String( hankaku ).match(/\\d/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }
 
        $(this).val(zenkakuDel);
    });
    // 電話番号の処理
    $(\'.tel-number\').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\\－|\\＋/g,function(s){return String.pointCharCode(s.charCodeAt(0)-0xFEE0)});
 
        // 半角数字と+-のみ残す
        var zenkakuDel = new String( hankaku ).match(/\\d|\\-|\\+/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }
        
        $(this).val(zenkakuDel);
    });
 
    // メールアドレスの処理
    $(\'.mail-address\').change( function(){
        var zenkigou = "＠－ー＋＿．，、";
        var hankigou = "@--+_...";
        var data = $(this).val();
        var str = "";
 
        // 指定された全角記号のみを半角に変換
        for (i=0; i<data.length; i++)
        {
            var dataChar = data.charAt(i);
            var dataNum = zenkigou.indexOf(dataChar,0);
            if (dataNum >= 0) dataChar = hankigou.charAt(dataNum);
            str += dataChar;
        }
        // 定番の、アルファベットと数字の変換処理
        var hankaku = str.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.pointCharCode(s.charCodeAt(0)-0xFEE0)});
        $(this).val(hankaku);
    });
 
});

//確認ボタンの有効無効

jQuery(function(){
    $(\'#privacy_1\').change(function(){
            if ($(this).is(\':checked\')) {
                    $(\'#confirm_btn\').css({opacity:"1",cursor:"pointer"}).removeAttr(\'disabled\');
            } else {
                    $(\'#confirm_btn\').css({opacity:"0.5",cursor:"default"}).attr(\'disabled\',\'disabled\');
            }
    });
});  


</script>
'; ?>
 


<div class="top_header_title" ><span class="top_header_title_border">ご注文内容の確認</span></div>
<div style="margin-left: 120px;">ご注文内容をご確認ください。</div>
<div style="margin-left: 120px;">以下の内容でよろしければ、『送信』ボタンを押してください。</div>
<div style="font-size:0.7em;margin-left: 120px;">

</div>        
        
        
<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
    <tr>
        <td class="l_Cel_01_02" width="80px;">会社名</td>
        <td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['company_name']; ?>
</td>
    </tr>
    <!--tr>
        <td class="l_Cel_01_02">会社名かな</td>
        <td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['company_kana']; ?>
</td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">支店名</td>
        <td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['branch_name']; ?>
</td>
    </tr-->
    <tr>
        <td class="l_Cel_01_02">郵便番号</td>
        <td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['zip1']; ?>
－<?php echo $this->_tpl_vars['app']['data']['zip2']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">住所</td>
        <td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['address']; ?>

        </td>
    </tr>
    <!--tr>
        <td class="l_Cel_01_02">電話番号</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['phone_no1']; ?>
<?php echo $this->_tpl_vars['app']['data']['phone_no2']; ?>
<?php echo $this->_tpl_vars['app']['data']['phone_no3']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">携帯電話番号</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['k_phone_no1']; ?>
<?php echo $this->_tpl_vars['app']['data']['k_phone_no2']; ?>
<?php echo $this->_tpl_vars['app']['data']['k_phone_no3']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">FAX番号</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['fax_no1']; ?>
<?php echo $this->_tpl_vars['app']['data']['fax_no2']; ?>
<?php echo $this->_tpl_vars['app']['data']['fax_no3']; ?>

        </td>
    </tr-->
    <tr>
        <td class="l_Cel_01_02">部署名</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['department_name']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">役職名</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['post_name']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">担当者名</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['contractor_lname']; ?>
&nbsp;&nbsp;<?php echo $this->_tpl_vars['app']['data']['contractor_fname']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">担当者名かな</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['contractor_lkana']; ?>
&nbsp;&nbsp;<?php echo $this->_tpl_vars['app']['data']['contractor_fkana']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">メールアドレス</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['email']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">お支払い方法</td>
        <td class="s_Cel_01">
            <?php if ($this->_tpl_vars['app']['data']['payment'] == 'bank_transfer'): ?>銀行振込<?php endif; ?>
	    <?php if ($this->_tpl_vars['app']['data']['payment'] == 'credit_card'): ?>クレジットカード決済<?php endif; ?>
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">購入ポイント数</td>
        <td class="s_Cel_01">
            2,000ポイント&nbsp;&times;&nbsp;<?php echo $this->_tpl_vars['app']['data']['point']; ?>
&nbsp;口&nbsp;＝&nbsp;<?php echo smarty_function_math(array('equation' => "x * y",'x' => $this->_tpl_vars['app']['data']['point'],'y' => '2000','assign' => 'point'), $this);?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['point'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : smarty_modifier_number_format($_tmp)); ?>
&nbsp;ポイント
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">価格</td>
        <td class="s_Cel_01">
            <?php echo smarty_function_math(array('equation' => "x * y",'x' => $this->_tpl_vars['app']['data']['point'],'y' => '2000','assign' => 'num'), $this);?>
<?php echo ((is_array($_tmp=$this->_tpl_vars['num'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : smarty_modifier_number_format($_tmp)); ?>
&nbsp;円
        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">利用規約への同意</td>
        <td class="s_Cel_01">
            同意します
        </td>
    </tr>
    <tr>
        <td colspan="3">
                <!-- ここからトップへ戻る -->
	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
                <!-- ここまでトップへ戻る -->
        </td>
    </tr>
</table>
</div>
        <div class="mod_form_btn">
	    <div style="margin-top:20px;margin-left:280px;">
            <form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f2" method="POST" id="f2">
                <input type="hidden" name="action_request_registration" value="true">
                <input type="hidden" name="back" id="back" value="1">
                <input type="hidden" name="in_company_name" id="in_company_name" value="<?php echo $this->_tpl_vars['app']['data']['company_name']; ?>
">
                <input type="hidden" name="in_company_kana" id="in_company_kana" value="<?php echo $this->_tpl_vars['app']['data']['company_kana']; ?>
">
                <input type="hidden" name="in_branch_name" id="in_branch_name" value="<?php echo $this->_tpl_vars['app']['data']['branch_name']; ?>
"> 
                <input type="hidden" name="in_zip1" id="in_zip1" value="<?php echo $this->_tpl_vars['app']['data']['zip1']; ?>
">
                <input type="hidden" name="in_zip2" id="in_zip2" value="<?php echo $this->_tpl_vars['app']['data']['zip2']; ?>
"> 
                <input type="hidden" name="in_address" id="in_address" value="<?php echo $this->_tpl_vars['app']['data']['address']; ?>
"> 
                <input type="hidden" name="in_phone_no1" id="in_phone_no1" value="<?php echo $this->_tpl_vars['app']['data']['phone_no1']; ?>
">
                <input type="hidden" name="in_phone_no2" id="in_phone_no2" value="<?php echo $this->_tpl_vars['app']['data']['phone_no2']; ?>
"> 
                <input type="hidden" name="in_phone_no3" id="in_phone_no3" value="<?php echo $this->_tpl_vars['app']['data']['phone_no3']; ?>
"> 
                <input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no1']; ?>
">
                <input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no2']; ?>
">
                <input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no3']; ?>
">
                <input type="hidden" name="in_fax_no1" id="in_fax_no1" value="<?php echo $this->_tpl_vars['app']['data']['fax_no1']; ?>
">
                <input type="hidden" name="in_fax_no2" id="in_fax_no2" value="<?php echo $this->_tpl_vars['app']['data']['fax_no2']; ?>
"> 
                <input type="hidden" name="in_fax_no3" id="in_fax_no3" value="<?php echo $this->_tpl_vars['app']['data']['fax_no3']; ?>
"> 
                <input type="hidden" name="in_department_name" id="in_department_name" value="<?php echo $this->_tpl_vars['app']['data']['department_name']; ?>
"> 
                <input type="hidden" name="in_post_name" id="in_post_name" value="<?php echo $this->_tpl_vars['app']['data']['post_name']; ?>
"> 
                <input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="<?php echo $this->_tpl_vars['app']['data']['contractor_lname']; ?>
">
                <input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="<?php echo $this->_tpl_vars['app']['data']['contractor_fname']; ?>
">
                <input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="<?php echo $this->_tpl_vars['app']['data']['contractor_lkana']; ?>
">
                <input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="<?php echo $this->_tpl_vars['app']['data']['contractor_fkana']; ?>
"> 
                <input type="hidden" name="in_email" id="in_email" value="<?php echo $this->_tpl_vars['app']['data']['email']; ?>
">
                <input type="hidden" name="in_point" id="in_point" value="<?php echo $this->_tpl_vars['app']['data']['point']; ?>
">
                <input type="hidden" name="in_payment" id="in_payment" value="<?php echo $this->_tpl_vars['app']['data']['payment']; ?>
">
                <input type="hidden" name="in_privacy" id="in_privacy" value="<?php echo $this->_tpl_vars['app']['data']['privacy']; ?>
">

		<?php if ($this->_tpl_vars['app']['data']['act'] == 'request_point'): ?>
		    <input type="hidden" name="in_request_id" value="<?php echo $this->_tpl_vars['app']['data']['request_id']; ?>
" />
		    <input type="hidden" name="in_use_matching_phase" value="<?php echo $this->_tpl_vars['app']['data']['use_matching_phase']; ?>
" />
		    <input type="hidden" name="in_use_property_str" value="<?php echo $this->_tpl_vars['app']['data']['use_property_str']; ?>
" />
		    <input type="hidden" name="in_column_num_str" value="<?php echo $this->_tpl_vars['app']['data']['column_num_str']; ?>
" />
		    <input type="hidden" name="in_act" value="<?php echo $this->_tpl_vars['app']['data']['act']; ?>
" />
		<?php endif; ?>

                <a class="button" href="javascript:void(0)" onclick="javascript:do_submit('back');return false;" id="back_btn">戻る</a>
            </form>
            </div>
            <div style="margin-top:-46px;margin-left:500px;">
            <form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f3" method="POST" id="f3">
                <input type="hidden" name="action_request_complete" value="true">
                <input type="hidden" name="in_company_name" id="in_company_name" value="<?php echo $this->_tpl_vars['app']['data']['company_name']; ?>
">
                <input type="hidden" name="in_company_kana" id="in_company_kana" value="<?php echo $this->_tpl_vars['app']['data']['company_kana']; ?>
">
                <input type="hidden" name="in_branch_name" id="in_branch_name" value="<?php echo $this->_tpl_vars['app']['data']['branch_name']; ?>
"> 
                <input type="hidden" name="in_zip1" id="in_zip1" value="<?php echo $this->_tpl_vars['app']['data']['zip1']; ?>
">
                <input type="hidden" name="in_zip2" id="in_zip2" value="<?php echo $this->_tpl_vars['app']['data']['zip2']; ?>
"> 
                <input type="hidden" name="in_address" id="in_address" value="<?php echo $this->_tpl_vars['app']['data']['address']; ?>
"> 
                <input type="hidden" name="in_phone_no1" id="in_phone_no1" value="<?php echo $this->_tpl_vars['app']['data']['phone_no1']; ?>
">
                <input type="hidden" name="in_phone_no2" id="in_phone_no2" value="<?php echo $this->_tpl_vars['app']['data']['phone_no2']; ?>
"> 
                <input type="hidden" name="in_phone_no3" id="in_phone_no3" value="<?php echo $this->_tpl_vars['app']['data']['phone_no3']; ?>
"> 
                <input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no1']; ?>
">
                <input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no2']; ?>
">
                <input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no3']; ?>
">
                <input type="hidden" name="in_fax_no1" id="in_fax_no1" value="<?php echo $this->_tpl_vars['app']['data']['fax_no1']; ?>
">
                <input type="hidden" name="in_fax_no2" id="in_fax_no2" value="<?php echo $this->_tpl_vars['app']['data']['fax_no2']; ?>
"> 
                <input type="hidden" name="in_fax_no3" id="in_fax_no3" value="<?php echo $this->_tpl_vars['app']['data']['fax_no3']; ?>
"> 
                <input type="hidden" name="in_department_name" id="in_department_name" value="<?php echo $this->_tpl_vars['app']['data']['department_name']; ?>
"> 
                <input type="hidden" name="in_post_name" id="in_post_name" value="<?php echo $this->_tpl_vars['app']['data']['post_name']; ?>
"> 
                <input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="<?php echo $this->_tpl_vars['app']['data']['contractor_lname']; ?>
">
                <input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="<?php echo $this->_tpl_vars['app']['data']['contractor_fname']; ?>
">
                <input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="<?php echo $this->_tpl_vars['app']['data']['contractor_lkana']; ?>
">
                <input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="<?php echo $this->_tpl_vars['app']['data']['contractor_fkana']; ?>
"> 
                <input type="hidden" name="in_email" id="in_email" value="<?php echo $this->_tpl_vars['app']['data']['email']; ?>
">
                <input type="hidden" name="in_point" id="in_point" value="<?php echo $this->_tpl_vars['app']['data']['point']; ?>
">
                <input type="hidden" name="in_payment" id="in_payment" value="<?php echo $this->_tpl_vars['app']['data']['payment']; ?>
">
                <input type="hidden" name="in_privacy" id="in_privacy" value="<?php echo $this->_tpl_vars['app']['data']['privacy']; ?>
">
		<input type="hidden" name="in_request_point_id" id="in_request_point_id" value="<?php echo $this->_tpl_vars['app']['data']['request_point_id']; ?>
">

		<?php if ($this->_tpl_vars['app']['data']['act'] == 'request_point'): ?>
		    <input type="hidden" name="in_request_id" value="<?php echo $this->_tpl_vars['app']['data']['request_id']; ?>
" />
		    <input type="hidden" name="in_use_matching_phase" value="<?php echo $this->_tpl_vars['app']['data']['use_matching_phase']; ?>
" />
		    <input type="hidden" name="in_use_property_str" value="<?php echo $this->_tpl_vars['app']['data']['use_property_str']; ?>
" />
		    <input type="hidden" name="in_column_num_str" value="<?php echo $this->_tpl_vars['app']['data']['column_num_str']; ?>
" />
		    <input type="hidden" name="in_act" value="<?php echo $this->_tpl_vars['app']['data']['act']; ?>
" />
		<?php endif; ?>
            </form>

	    <form name="f4" action="<?php echo $this->_tpl_vars['app']['credit']['url']; ?>
" method="post"> 
		<input type="hidden" name="admin" value="<?php echo $this->_tpl_vars['app']['credit']['admin']; ?>
" /> 
		<input type="hidden" name="amount" value="<?php echo $this->_tpl_vars['app']['credit']['amount']; ?>
" /> 
		<input type="hidden" name="charge" value="<?php echo $this->_tpl_vars['app']['credit']['charge']; ?>
" /> 
		<input type="hidden" name="name" value="<?php echo $this->_tpl_vars['app']['credit']['name']; ?>
" /> 
		<input type="hidden" name="mail" value="<?php echo $this->_tpl_vars['app']['credit']['mail']; ?>
" /> 
		<input type="hidden" name="action_request_complete" value="true" /> 
		<input type="hidden" name="request_id" value="<?php echo $this->_tpl_vars['app']['credit']['request_id']; ?>
" />
		<input type="hidden" name="auto_matching_value" value="<?php echo $this->_tpl_vars['app']['credit']['auto_matching_value']; ?>
" /> 
	    </form>

            <?php if ($this->_tpl_vars['app']['data']['payment'] == 'bank_transfer'): ?>
                <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit('send');" id="send_btn">送信</a>
            <?php else: ?>
                <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit('transaction');" id="send_btn">送信</a>
            <?php endif; ?>
            </div>
        </div>