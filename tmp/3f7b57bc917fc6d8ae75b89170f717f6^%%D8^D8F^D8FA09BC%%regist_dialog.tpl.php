<?php /* Smarty version 2.6.27, created on 2017-07-28 13:06:53
         compiled from regist_dialog.tpl */ ?>
<link rel="stylesheet" href="./css/ditail.css" type="text/css">

<!-- ここからログイン制限ダイアログ -->
<div id="send_interest_confirm" style="display: none;">
    <div style="width:100%;background-color: #ffffff;">
        <img src="./img/logo.jpg" alt="名刺にふるふるサービス" style="width:250px;height:60px;"  />
        <div style="width:24px;">
            <a href="javascript:void(0);" style="position:relative; top:-60px; left:789px;"><img id="send_confirm_close" src="img/close_round.png" width="20px" height="20px" alt="閉じる" /></a>
        </div>
    </div>
    <hr />
    <div id="submit_btn">
        <span style="font-size:20px;color:#3A2409;">『名刺にふるふるサービス』のご利用には会員登録が必要となります。<br />会員登録はこちらからどうぞ。</span><br /><br />
        <a class="button5" href="https://youtu.be/SlCrFajv_oI" id="movie" style="width:550px;" data-lity="data-lity">名刺にふるふる使い方の動画を見る</a>
    </div>

    <div id="submit_btn">
        <a class="button5" href="./index.php?action_secure_registration=true"  id="registration" style="width:550px;">今すぐ無料会員登録！</a>
    </div>

    <div id="submit_btn">
            <a class="button5" href="http://www.navit-j.com/service/furu.html" onclick="" id="login_btn" style="width:550px; color:#fff;"><img src='img/wakaba_mark.png' width="17px" height="25px" style="display:inline; margin-bottom:-5px;"/>名刺にふるふるサービスについて詳しくはこちら</a>
    </div>
</div>
<!-- ここまでログイン制限ダイアログ -->

<?php echo '
<script type="text/javascript">
     function displaySendForm(){
         $("#send_interest_confirm").lightbox_me({centered: true,closeSelector:\'#send_confirm_close\',overlayCSS:{background:\'#D3C9BA\',opacity: .8}});
     }
</script>
'; ?>
