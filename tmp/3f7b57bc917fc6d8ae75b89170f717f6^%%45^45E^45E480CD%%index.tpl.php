<?php /* Smarty version 2.6.27, created on 2017-07-28 13:06:53
         compiled from index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'nl2br', 'index.tpl', 511, false),)), $this); ?>
<?php echo '
<!--
<script type="text/javascript" src="js/jquery.loading.js"></script>
-->
'; ?>


<?php echo '
<script type="text/javascript">
	function header_line(){
		check = document.f_get.in_header_check.checked;
		if(check == true){
			document.getElementById("disp").style.display = "block";
		}else{
			document.getElementById("disp").style.display = "none";
		}
	}

//参照ボタンデザイン変更
$(function(){
    fileUploader();
});
var fileUploader = function(){
    var target = $(\'.fileUploder\');

    target.each(function(){
        var txt = $(this).find(\'.txt\');
        var btn = $(this).find(\'.btn\');
        var uploader = $(this).find(\'.uploader\');

        uploader.bind(\'change\',function(){
            txt.val($(this).val());
        });

        btn.bind(\'click\',function(event){
            event.preventDefault();
            return false;
        });

        $(this).bind(\'mouseover\',function(){
            btn.css(\'background-position\',\'0 100%\');
        });
        $(this).bind(\'mouseout\',function(){
            btn.css(\'background-position\',\'0 0\');
        });

    });
}

function header_num_close(){
	document.getElementById("auto_disp").style.display = "none";
	document.getElementById("action_confirmation").disabled = false;
	document.getElementById("action_commission_registrationauto").disabled = true;
	document.getElementById("in_auto_check").value = \'off\';
	document.getElementById("manual_order_button").style.display = "block";
	document.getElementById("manual_order_button_n").style.display = "none";
	document.getElementById("auto_order_button").style.display = "none";
	document.getElementById("auto_order_button_n").style.display = "block";
}
function header_num_view(){
	document.getElementById("auto_disp").style.display = "block";
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = false;
	document.getElementById("in_auto_check").value = \'on\';
	document.getElementById("manual_order_button").style.display = "none";
	document.getElementById("manual_order_button_n").style.display = "block";
	document.getElementById("auto_order_button").style.display = "block";
	document.getElementById("auto_order_button_n").style.display = "none";
}

'; ?>
<?php if ($this->_tpl_vars['app']['auto_check'] == 'off' || $this->_tpl_vars['app']['auto_check'] == ""): ?><?php echo '
window.onload = function(){
	document.getElementById("auto_disp").style.display = "none";
	document.getElementById("action_confirmation").disabled = false;
	document.getElementById("action_commission_registrationauto").disabled = true;
	document.getElementById("in_auto_check").value = \'off\';
	document.getElementById("manual_order_button").style.display = "block";
	document.getElementById("manual_order_button_n").style.display = "none";
	document.getElementById("auto_order_button").style.display = "none";
	document.getElementById("auto_order_button_n").style.display = "block";
};
'; ?>
<?php else: ?><?php echo '
window.onload = function(){
	document.getElementById("auto_disp").style.display = "block";
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = false;
	document.getElementById("in_auto_check").value = \'on\';
	document.getElementById("manual_order_button").style.display = "none";
	document.getElementById("manual_order_button_n").style.display = "block";
	document.getElementById("auto_order_button").style.display = "block";
	document.getElementById("auto_order_button_n").style.display = "none";
};
'; ?>
<?php endif; ?><?php echo '

function do_trial(){
	document.getElementById("action_trial").disabled = false;
	document.getElementById("in_act").disabled = false;
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = true;
	document.f_get.submit();
}
function do_search_corp(){
	document.s_corp.submit();
}


window.onload = function () {
 	document.getElementById("auto_disp").style.display = "block";
	document.getElementById("action_confirmation").disabled = true;
	document.getElementById("action_commission_registrationauto").disabled = false;
	document.getElementById("in_auto_check").value = \'on\';
/*
	document.getElementById("manual_order_button").style.display = "none";
	document.getElementById("manual_order_button_n").style.display = "block";
	document.getElementById("auto_order_button").style.display = "block";
	document.getElementById("auto_order_button_n").style.display = "none";
*/	
	if (getParam() == "1d12NGnK" || $("#id_allcd").val() == "1d12NGnK") {
		$("#bulk_grant").css("display", "inline");
	}
};
function display_bulk_grant(){
	if ($("#bulk_grant").css("display") == "none"){
		$("#bulk_grant").css("display", "inline");
	}else{
		$("#bulk_grant").css("display", "none");
	}
}

function getParam() {
	var url   = location.href;
	parameters    = url.split("?");
	
	if (parameters[1]){
		params   = parameters[1].split("&");
		var paramsArray = [];
		for ( i = 0; i < params.length; i++ ) {
			neet = params[i].split("=");
			paramsArray.push(neet[0]);
			paramsArray[neet[0]] = neet[1];
		}
		var categoryKey = paramsArray["allcd"];
		return categoryKey;
	}else{
		return "";
	}
}
</script>
'; ?>


<!------------------------------------------------------------------------------>

<div align="center" style="margin:5px 0 0px 0;font-size:11px;">

<a href="https://www.navit-j.com">データ・リストの販売、調査代行ならナビット</a> > 名刺への住所電話番号などの顧客属性付与なら名刺にふるふる
</div>

<p style="font-size:12px; text-align:center; margin:5px auto;">
名刺への住所、電話番号、メールアドレスなどの顧客属性付与サービスなら、ナビットの「名刺にふるふる」にお任せください。<br />
お客様がお持ちの名刺リストに、業種、FAX番号、上場区分、従業員数規模、売上高規模、資本金規模、<br />
設立年月、URL、法人番号（マイナンバー）、メールアドレスなどの企業属性を付与するサービスです。
</p>
<!--<hr color="#017680">-->


        <!-- ここからバナー 
<div style="margin:0 auto 0 auto;text-align:center;">
	<a href="https://www.navit-j.com/blog/?p=24464" target="_blank"><img src="https://www.navit-j.com/service/openkun/img/0nichikoushin_bnr.jpg" onmouseover="this.src='https://www.navit-j.com/service/openkun/img/0nichikoushin_bnr_d.jpg'" onmouseout="this.src='https://www.navit-j.com/service/openkun/img/0nichikoushin_bnr.jpg'" alt="0日更新記念キャンペーン" width="800px"></a></div>
         ここまでバナー -->

<!--<div class="input_demo">
<div style="margin:30px auto 0;text-align:center;">
<a href="https://youtu.be/SlCrFajv_oI" data-lity="data-lity"><img src="img/movie_bnr.gif" onmouseover="this.src='img/movie_bnr_d.gif'" onmouseout="this.src='img/movie_bnr.gif'" alt="名刺にふるふる動画" border="0"></a>
</div>-->



<!--<div style="float:left; margin:0 0 0 130px; padding:15px;">
 <span style="font-size:30px; font-weight:bold; color:#F00;">NEW!</span>
<img src="img/new.png" />
</div>-->

<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="s_corp" method="POST" id="search_corp">
<input type="hidden" name="in_act" value="search_corp">
<input type="hidden" name="allcd" value="<?php echo $_COOKIE['opensite_alliance']; ?>
" id="id_allcd">
	<table class="input_table" style="width:1000px;height:150px;clear:both;margin-top:0px;margin-bottom:30px;background-color:#05727c;">
		<tr>
			<td class="input_td" style="color:#fff;text-align:right;">企業名<span style="font-size:80%;color:#fff;">(必須)</span></td>
			<td>
				<input type="text" name="sc_name" value="<?php echo $this->_tpl_vars['app']['sc_name']; ?>
"  placeholder="株式会社ナビット" style="width:310px;" />
			</td>
			<td class="input_td" style="color:#fff;text-align:right;">所在地<span style="font-size:80%;">(必須)</span></td>
			<td>
				<input type="text" name="sc_address" value="<?php echo $this->_tpl_vars['app']['sc_address']; ?>
" placeholder="東京都、東京都千代田区など" style="width:310px;" />
			</td>
		</tr>
		<!--<tr>
			<td colspan=2 style="text-align:center;"><span style="margin:0 0 0 0;font-size:12px;color:#fff;font-weight:bold;">※(株)などの省略表記ではなく、株式会社と入れてください。</span></td>
			<td colspan=2 style="text-align:center;"><span style="margin:0 0 0 0;font-size:12px;color:#fff;font-weight:bold;">※都道府県名、もしくは都道府県名＋市区町村名を１つ入れて下さい。</span></td>
		</tr>-->

		<?php if (isset ( $this->_tpl_vars['app']['search_check_err'] ) || isset ( $this->_tpl_vars['app']['search_check_err_address'] )): ?>
		<tr>
			<td colspan=4 align="center">
				<?php if (isset ( $this->_tpl_vars['app']['search_check_err'] )): ?><br />
				<span class="error"><?php echo $this->_tpl_vars['app']['search_check_err']; ?>
</span>
				<?php endif; ?>
				<?php if (isset ( $this->_tpl_vars['app']['search_check_err_address'] )): ?>
				<span class="error">　<?php echo $this->_tpl_vars['app']['search_check_err_address']; ?>
</span>
				<?php endif; ?>
			</td>
		</tr>
		<?php endif; ?>
	</table>
<div class="input_demo">
<!--<hr />-->

	<div class="search_btn" style="text-align:center;margin:15px auto 15px auto;">
		<a class="button" style="background-color:#F55700; border:2px solid #F55700;" href="javascript:void(0);" onclick="do_search_corp();" id="search_corp">検索</a>
	</div>
</form>

<div style="margin:30px auto 0;text-align:center;">
<!--<a href="https://youtu.be/SlCrFajv_oI" data-lity="data-lity"><img src="img/movie_bnr.gif" onmouseover="this.src='img/movie_bnr_d.gif'" onmouseout="this.src='img/movie_bnr.gif'" alt="名刺にふるふる動画" border="0"></a>-->
<a href="https://youtu.be/SlCrFajv_oI" style="font-size:1.5em;text-decoration: underline;font-weight:bold;">使い方の動画を見る</a>
</div>


	<div class="search_result" align="center">
			<?php if ($this->_tpl_vars['app']['result_corp'] != ""): ?>
			<span style="font-size:25px; font-weight-bold; color:#F85400;"><?php echo $this->_tpl_vars['app']['sc_name']; ?>
</span>の検索結果　<span style="font-size:25px; font-weight-bold; color:#F85400;"><?php echo $this->_tpl_vars['app']['result_corp_cnt']['sc_cnt']; ?>
社</span>ヒットしました。<br /><span style="font-size:18px; color:#F00;">企業名、所在地をクリックする事で詳細をご確認いただけます。</span>
	</div>

	<table class="result_table">
		<tr>
			<th class="result_table_th" width="70%">
				企業名
			</th>
			<th class="result_table_th" width="30%">
				所在地
			</th>
		</tr>


		<?php $_from = $this->_tpl_vars['app']['result_corp']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
		<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f_<?php echo $this->_tpl_vars['v']['corp_id']; ?>
" method="POST" id="trial_v2" target="_blank">
		<input type="hidden" id="action_trialone" name="action_trialone" value="true"/>
		<input type="hidden" id="in_act" name="in_act" value="trialone"/>
		<input type="hidden" name="corp_id" value="<?php echo $this->_tpl_vars['v']['corp_id']; ?>
">
		<input type="hidden" name="sc_name" value="<?php echo $this->_tpl_vars['app']['sc_name']; ?>
">
		<input type="hidden" name="sc_address" value="<?php echo $this->_tpl_vars['app']['sc_address']; ?>
">
		<input type="hidden" name="pref_cd" value="<?php echo $this->_tpl_vars['app']['pref_code']; ?>
">
		<input type="hidden" name="city_cd" value="<?php echo $this->_tpl_vars['app']['city_code']; ?>
">

		<tr>
			<td class="result_table_td" width="70%">
				<img src="img/link_point.png" />
				<a href="javaScript:document.f_<?php echo $this->_tpl_vars['v']['corp_id']; ?>
.submit();">
					<?php echo $this->_tpl_vars['v']['corp_name']; ?>

				</a>
			</td>
			<td class="result_table_td" width="30%">
				<a href="javaScript:document.f_<?php echo $this->_tpl_vars['v']['corp_id']; ?>
.submit();">
					<?php echo $this->_tpl_vars['v']['pref']; ?>
 <?php echo $this->_tpl_vars['v']['corp_city']; ?>

				</a>
			</td>
		</tr>


		</form>
<?php endforeach; endif; unset($_from); ?>
	</table>
<?php endif; ?>

<div style="margin:15px 0;">
<?php if ($this->_tpl_vars['app']['search_none'] != ""): ?>
<span style="font-size:25px; font-weight-bold; color:#F85400;"><?php echo $this->_tpl_vars['app']['sc_name']; ?>
</span>の検索結果　みつかりませんでした。
<?php endif; ?>
</div>





<!------------------------------------------------------------------------------>



<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f_get" method="POST" id="f_get" enctype="multipart/form-data">
<input type="hidden" id="action_trial" name="action_trial" value="true" disabled />
<input type="hidden" id="in_act" name="in_act" value="furufuru_trial" disabled />


<!--
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">名刺にふるふるサービス&nbsp;ご利用タイプの選択</div>
<br />


<div style="float:left; margin:20px 0 30px 175px;">
<img id="manual_order_button" style="display:block;" onmouseover="this.src='img/btn_service_select01_d.png'" onmouseout="this.src='img/btn_service_select01.png'" alt="手動版で抽出（中2日でお戻し）" src="img/btn_service_select01.png" onclick="header_num_close();">
<img id="manual_order_button_n" alt="手動版で抽出（中2日でお戻し）" src="img/btn_service_select01_n.png" style="display:none;" onclick="header_num_close();">
</div>

<div style="float:right; margin:20px 175px 30px 0px;">
<img id="auto_order_button" style="display:none;" onmouseover="this.src='img/btn_service_select02_d.png'" onmouseout="this.src='img/btn_service_select02.png'" alt="全自動で抽出（オンライン対応）" src="img/btn_service_select02.png" onclick="header_num_view();">
<img id="auto_order_button_n" alt="全自動で抽出（オンライン対応）" src="img/btn_service_select02_n.png" style="display:block;" onclick="header_num_view();">
</div>

-->







<div style="clear:both;"></div>

<div id="03"></div>

<script type="text/javascript" src="js/jquery.loading.js"></script>
<div id="04"></div>

<a name="service"></a>
<!--<div class="head_title_bgcolor" style="text-align:left;margin-top:40px;">-->
<div class="ikkastu_title" style="margin-top:40px;">
<a href="javascript:void(0);" onclick="display_bulk_grant();">複数のデータに一括で付与したい方はこちら</a>
</div>
<div id="bulk_grant" style="display:none;">



<div class="table">
        <input type="hidden" id="action_confirmation" name="action_confirmation" value="true" <?php if ($this->_tpl_vars['app']['auto_check'] != ""): ?>disabled<?php endif; ?>/>
        <input type="hidden" id="action_commission_registrationauto" name="action_commission_registrationauto" value="true" <?php if ($this->_tpl_vars['app']['auto_check'] == ""): ?>disabled<?php endif; ?> />
	<input id="in_auto_check" name="in_auto_check" type="hidden" value="" />
        <table border="0" cellpadding="5" cellspacing="1">
            <tr>
                <td class="l_Cel" style="vertical-align:top;">
                    <div class="middle_midashi">顧客属性付与先リスト</div>
                </td>
                <td colspan="2" class="r_Cel">
                    <p class="note">
                        属性を付与したいデータの記載されたファイルをご指定ください。<br />
                        <a href="ex_furu_article.html" target="_blank" style="text-decoration: underline;font-size:16px;"><span style="font-size:16px;">※ファイル形式：CSV（.csv）ファイル</span></a><br />
		    </p>
                    <div class="fileUploder">
                        <input type="text" class="txt" readonly>
                        <button class="btn">参照</button>
                        <input type="file" class="uploader" name="in_up_list">
                    </div>
                    <?php if ($this->_tpl_vars['app']['is_HTflg'] == false): ?>
                    <p class="note">
			ヘッダー(タイトル行)あり<input type="checkbox" name="in_header_check" value="on" style="margin:0 20px;vertical-align:-5px;" onclick="header_line();" <?php if ($this->_tpl_vars['app']['header_check'] != ""): ?>checked<?php endif; ?> >
			<span id="disp" style="display:<?php if ($this->_tpl_vars['app']['header_check'] != ""): ?>block<?php else: ?>none<?php endif; ?>;">ヘッダーの行数<input type="text" name="in_header_line" <?php if ($this->_tpl_vars['app']['header_line'] != ""): ?>value="<?php echo $this->_tpl_vars['app']['header_line']; ?>
"<?php else: ?>value="1"<?php endif; ?> style="margin:0 20px;width:50px;vertical-align:3px;"></span>
		    </p>
                    <p class="note">
                    <?php if ($this->_tpl_vars['app']['up_list_err'] != ""): ?><br /><span class="error"><?php echo $this->_tpl_vars['app']['up_list_err']; ?>
</span><?php endif; ?>
                    <?php if ($this->_tpl_vars['app']['row_err']): ?><br /><span style="font-size:12pt;">（<a href="./index.php?action_inquiry_registration=true">2000件を超える場合はお問合せください</a>）</span><?php endif; ?>
                    <?php if ($this->_tpl_vars['app']['tel_value_line_err'] != ""): ?><br /><span class="error"><?php echo $this->_tpl_vars['app']['tel_value_line_err']; ?>
</span><?php endif; ?>
                    <?php if ($this->_tpl_vars['app']['header_line_err'] != ""): ?><br /><span class="error"><?php echo $this->_tpl_vars['app']['header_line_err']; ?>
</span><?php endif; ?>
		    </p>

		    <table id="auto_disp" style="display:<?php if ($this->_tpl_vars['app']['auto_check'] == 'on'): ?>block<?php else: ?>none<?php endif; ?>;">
			<tr><td class="note" colspan="6" style="padding-bottom:10px;">列を指定して下さい。</td></tr>
			<tr>
			<td class="note" style="text-align:right;">会社名&nbsp;</td>
			<td class="note" style="padding-right:20px;"><input type="text" style="width:30px;" name="in_name_line_num" value="<?php echo $this->_tpl_vars['app']['name_line_num']; ?>
" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">郵便番号&nbsp;</td>
			<td class="note" style="padding-right:20px;"><input type="text" style="width:30px;" name="in_zipcode_line_num" value="<?php echo $this->_tpl_vars['app']['zipcode_line_num']; ?>
" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">住所&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_address_line_num" value="<?php echo $this->_tpl_vars['app']['address_line_num']; ?>
" />&nbsp;列目</td>
			</tr>
			<tr>
			<td class="note" style="text-align:right;">電話番号&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_tel_line_num" value="<?php echo $this->_tpl_vars['app']['tel_line_num']; ?>
" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">URL&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_I_URL_line_num" value="<?php echo $this->_tpl_vars['app']['I_URL_line_num']; ?>
" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">メールアドレス&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_I_MAILADDR_line_num" value="<?php echo $this->_tpl_vars['app']['I_MAILADDR_line_num']; ?>
" />&nbsp;列目</td>
			</tr>
		    </table>
		    	<?php else: ?>
                    <p class="note">
			ヘッダー(タイトル行)あり<input type="checkbox" name="in_header_check" value="on" style="margin:0 20px;vertical-align:-5px;" onclick="header_line();" checked disabled="disabled" >

			<span id="disp" style="display: block;">ヘッダーの行数<input type="text" name="in_header_line" value="1" style="margin:0 20px;width:50px;vertical-align:3px;" disabled="disabled" ></span>
		    </p>
                    <p class="note">
                    <?php if ($this->_tpl_vars['app']['up_list_err'] != ""): ?><br /><span class="error"><?php echo $this->_tpl_vars['app']['up_list_err']; ?>
</span><?php endif; ?>
                    <?php if ($this->_tpl_vars['app']['row_err']): ?><br /><span style="font-size:12pt;">（<a href="./index.php?action_inquiry_registration=true">2000件を超える場合はお問合せください</a>）</span><?php endif; ?>
                    <?php if ($this->_tpl_vars['app']['tel_value_line_err'] != ""): ?><br /><span class="error"><?php echo $this->_tpl_vars['app']['tel_value_line_err']; ?>
</span><?php endif; ?>
                    <?php if ($this->_tpl_vars['app']['header_line_err'] != ""): ?><br /><span class="error"><?php echo $this->_tpl_vars['app']['header_line_err']; ?>
</span><?php endif; ?>
		    </p>

		    <table id="auto_disp" style="display:<?php if ($this->_tpl_vars['app']['auto_check'] == 'on'): ?>block<?php else: ?>none<?php endif; ?>;">
			<tr><td class="note" colspan="6" style="padding-bottom:10px;">列を指定して下さい。</td></tr>
			<tr>
			<td class="note" style="text-align:right;">会社名&nbsp;</td>
			<td class="note" style="padding-right:20px;"><input type="text" style="width:30px;" name="in_name_line_num" value="2"  disabled="disabled" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">郵便番号&nbsp;</td>
			<td class="note" style="padding-right:20px;"><input type="text" style="width:30px;" name="in_zipcode_line_num" value="3" disabled="disabled"  />&nbsp;列目</td>
			<td class="note" style="text-align:right;">住所&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_address_line_num" value="4"  disabled="disabled" />&nbsp;列目</td>
			</tr>
			<tr>
			<td class="note" style="text-align:right;">電話番号&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_tel_line_num" value="5" disabled="disabled" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">URL&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_I_URL_line_num" value="6"  disabled="disabled" />&nbsp;列目</td>
			<td class="note" style="text-align:right;">メールアドレス&nbsp;</td>
			<td class="note"><input type="text" style="width:30px;" name="in_I_MAILADDR_line_num" value="<?php echo $this->_tpl_vars['app']['I_MAILADDR_line_num']; ?>
"  disabled="disabled" />&nbsp;列目</td>
			</tr>
		    </table>

		    <input type="hidden" name="in_header_check" value="on">
		    <input type="hidden" name="in_header_line" value="1">
		    <input type="hidden" name="in_name_line_num" value="2">
		    <input type="hidden" name="in_zipcode_line_num" value="3">
		    <input type="hidden" name="in_address_line_num" value="4">
		    <input type="hidden" name="in_tel_line_num" value="5">
		    <input type="hidden" name="in_I_URL_line_num" value="6">

		    	<?php endif; ?>

                    <p class="note">
                    <?php if ($this->_tpl_vars['app']['name_line_num_err'] != ""): ?><br /><span class="error"><?php echo $this->_tpl_vars['app']['name_line_num_err']; ?>
</span><?php endif; ?>
                    <?php if ($this->_tpl_vars['app']['address_line_num_err'] != ""): ?><br /><span class="error"><?php echo $this->_tpl_vars['app']['address_line_num_err']; ?>
</span><?php endif; ?>
                    <?php if ($this->_tpl_vars['app']['tel_line_num_err'] != ""): ?><br /><span class="error"><?php echo $this->_tpl_vars['app']['tel_line_num_err']; ?>
</span><?php endif; ?>
		    </p>
                </td>
            </tr>

            <tr>
                <td class="l_Cel" style="vertical-align:top;">
                    <div class="middle_midashi">付与したい属性項目</div>
                </td>
                <td colspan="2" class="r_Cel">
                    <p class="note">

		    <?php $_from = $this->_tpl_vars['app']['property_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
?>
                        <input type="checkbox" name="in_property_check[]" value="<?php echo $this->_tpl_vars['k']; ?>
" <?php if (isset ( $this->_tpl_vars['app']['property_checked'][$this->_tpl_vars['k']] )): ?><?php echo $this->_tpl_vars['app']['property_checked'][$this->_tpl_vars['k']]; ?>
<?php endif; ?>><?php echo $this->_tpl_vars['v']; ?>
<br/>
		    <?php endforeach; endif; unset($_from); ?>

                    <?php if ($this->_tpl_vars['app']['property_check_err'] != ""): ?><br /><span class="error"><?php echo $this->_tpl_vars['app']['property_check_err']; ?>
</span><?php endif; ?>

			<!--
                        【記載形式】<br />
                        <?php if ($this->_tpl_vars['logined_name'] != ""): ?>
			<a href="ex_furu_article.html#bunrui" target="_blank"/>業種リストを確認する</a><br />
			<a href="ex_furu_article.html#koumoku" target="_blank"/>項目リストを確認する</a>


                        <?php else: ?>
			<a href="ex_furu_article.html#bunrui" target="_blank"/>業種リストを確認する</a><br />
			<a href="ex_furu_article.html#koumoku" target="_blank"/>項目リストを確認する</a>
			<?php endif; ?>
                        </span>
			-->
                    </p>
                </td>
            </tr>

            <tr><td colspan="3" style="border-bottom :1px solid #017680;"></td></tr>

            <tr>
                <td colspan="4">
                    <div id="submit_btn">
                        <?php if ($this->_tpl_vars['logined_name'] != ""): ?>
                        <a class="button" href="javascript:void(0);" onclick="document.f_get.submit();" id="search_button">ご依頼の確認</a>
                        <?php else: ?>
                        <a class="button" href="javascript:void(0);" onclick="displaySendForm();" id="a_search">ご依頼の確認</a>
                        <?php endif; ?>
                    </div>
                </td>
            </tr>


            <tr>
                <td colspan="2">

	<!-- ここから採用事例 -->

<div style="text-align:center; margin:0 auto;">
	<a href="https://navit-j.com/blog/?p=16957" target="_blank"><img src="img/btn_faq.png" onmouseover="this.src='img/btn_faq_d.png'" onmouseout="this.src='img/btn_faq.png'"alt="ご依頼前に必ずお読みください" /></a>
</div>


	<!-- ここまで採用事例 -->
                </td>
            </tr>

        </table>

</div>



<!--
<div class="head_title_bgcolor" style="text-align:left;margin-top:10px;">お試し！&nbsp;名刺にふるふるサービス</div>
<br />
<div style="margin: 0 50px 0 50px;">
情報付与を行いたい企業データを以下フォームより、情報付与依頼をお試しいただけます！<br />
是非こちらより名刺にふるふるサービスをご体感下さい！<br />
<br />



<table style="width:900px;text-align:left;margin:10px auto 0px auto;border-collapse: collapse;">
<tr><td colspan="4">
	<?php if ($this->_tpl_vars['app']['trial_check_err']['uninput'] != ""): ?><br /><span style="font-size:80%;" class="error"><?php echo $this->_tpl_vars['app']['trial_check_err']['uninput']; ?>
</span><?php endif; ?>
	<?php if ($this->_tpl_vars['app']['trial_check_err']['uncolumn'] != ""): ?><br /><span style="font-size:80%;" class="error"><?php echo ((is_array($_tmp=$this->_tpl_vars['app']['trial_check_err']['uncolumn'])) ? $this->_run_mod_handler('nl2br', true, $_tmp) : smarty_modifier_nl2br($_tmp)); ?>
</span><?php endif; ?>
</td></tr>
<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td></td>
	<td style="padding:10px;font-size:80%;">企業名(必須)<br /><span style="font-size:70%;">※企業名を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">電話番号(必須)<br /><span style="font-size:70%;">※電話番号を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">所在地<br /><span style="font-size:70%;">※所在地を入れて下さい</span></td>
<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_1" value="<?php echo $this->_tpl_vars['app']['data_corp_1']; ?>
"  placeholder="株式会社ナビット" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_1" value="<?php echo $this->_tpl_vars['app']['data_tel_1']; ?>
" placeholder="03-5215-5713" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_1" value="<?php echo $this->_tpl_vars['app']['data_address_1']; ?>
" placeholder="東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F" style="width:300px;" />
	</td>
</tr>



<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td></td>
	<td style="padding:10px;font-size:80%;">企業名(必須)<br /><span style="font-size:70%;">※企業名を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">電話番号(必須)<br /><span style="font-size:70%;">※電話番号を入れて下さい</span></td>
	<td style="padding:10px;font-size:80%;">所在地<br /><span style="font-size:70%;">※所在地を入れて下さい</span></td>
<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_1" value="<?php echo $this->_tpl_vars['app']['data_corp_1']; ?>
"  placeholder="株式会社ナビット" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_1" value="<?php echo $this->_tpl_vars['app']['data_tel_1']; ?>
" placeholder="03-5215-5713" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_1" value="<?php echo $this->_tpl_vars['app']['data_address_1']; ?>
" placeholder="東京都千代田区九段南1-5-5 九段サウスサイドスクエア8F" style="width:300px;" />
	</td>
</tr>


<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報2</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_2" value="<?php echo $this->_tpl_vars['app']['data_corp_2']; ?>
"  placeholder="企業名" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_2" value="<?php echo $this->_tpl_vars['app']['data_tel_2']; ?>
" placeholder="電話番号" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_2" value="<?php echo $this->_tpl_vars['app']['data_address_2']; ?>
" placeholder="所在地" style="width:300px;" />
	</td>
</tr>
<tr style="background-color:#ECFEFF;border-bottom:solid 10px #ffffff;">
	<td style="padding:10px;font-size:80%;">企業情報3</td>
	<td style="padding:10px;">
		<input type="text" name="in_corp_3" value="<?php echo $this->_tpl_vars['app']['data_corp_3']; ?>
"  placeholder="企業名" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_tel_3" value="<?php echo $this->_tpl_vars['app']['data_tel_3']; ?>
" placeholder="電話番号" style="width:140px;" />
	</td>
	<td style="padding:10px;">
		<input type="text" name="in_address_3" value="<?php echo $this->_tpl_vars['app']['data_address_3']; ?>
" placeholder="所在地" style="width:300px;" />
	</td>
</tr>

</table>
<div style="text-align:center;margin:30px auto 50px auto;">
	<a class="button" href="javascript:void(0);" onclick="do_trial();" id="trial_button">お試し依頼の確認</a>
</div>
-->
    </form>





<div align="center" style="margin:0px auto 15px;">
	<div style="height:30px;"></div>
	<span style="font-weight:bold; font-size:25px;"><a href="https://www.navit-j.com/service/navit_opensite_20170612.html" target="_blank" style="color:#000000;">資料ダウンロードはこちら<span style="color:red; font-weight:bold;">NEW!</span></a></span>
</div>

<div style="text-align:center; margin:10px auto;"><a href="/service/furufuru/inquiry/sample_dl.html" target="_blank"><img src="img/sample_dl.jpg" alt="サンプルデータダウンロード" /></a></div>

</div> <!--hidden部分の終わり -->




<div align="center" style="margin:25px 0 15px 0;">
<span style="font-weight:bold; font-size:23px;"><a href="http://www.navit-j.com/blog/?p=497" target="_blank">ＳＦＡでの採用事例</a></span><br />
<div align="center" style="margin:15px 0 15px 0;">
<span style="font-weight:bold; font-size:23px;"><a href="https://www.navit-j.com/blog/?p=21346" target="_blank">名刺ソフトでの採用事例</a></span>

<div align="center" style="margin:50px auto 15px;">
	<a href="http://navit-j.com/furufuru_manga.html" target="_blank"><img src="img/dl_manga.jpg" onmouseover="this.src='img/dl_manga_d.jpg'" onmouseout="this.src='img/dl_manga.jpg'" alt="名刺にふるふるマンガダウンロード" /></a>
</div>
<!--
<span style="font-weight:bold; font-size:23px;"><a href="
http://www.navit-j.com/blog/?p=10159" target="_blank">名刺にふるふるサービス全自動版リリース</a></span>
-->
</div>


<a name="price"></a>

<div class="head_title_bgcolor" style="text-align:left; margin:60px auto 20px auto;">
	■価格表
	<span style="font-size:60%;">(税別)　</span>
	<span style="font-size:80%;">1件からでもお受けできます</span>
</div>
<div style="text-align:center;">
	<span style="font-weight:bold;">ポイント単価：1ポイントあたり1円　</span>
	<span style="color:#F00; font-weight:bold;">初期費用は0円です。</span>
</div>
                 <table border="1" cellpadding="5" style="width:900px; margin:0 auto 5px;">

                        <tr>
                          <td width="12%" style="background-color:#ECFEFF;" align="center">基本料金 </td>
                          <td width="15%" align="center">0ポイント</td>
                          <td width="12%" style="background-color:#ECFEFF;" align="center">売上高規模 </td>
                          <td width="15%" rowspan="5" align="center">40ポイント<span style="font-size:80%;">〔\40(税別)〕</span><br />
                          <span style="font-size:60%;">※1項目あたりの単価</span></td>
                          <!--<td width="15%" style="background-color:#ECFEFF;" align="center"><span style="background-color:#ECFEFF;" align="center">代表者役職 </span></td>-->
			  <td width="12%" style="background-color:#ECFEFF;" align="center" rowspan="3"><span style="background-color:#ECFEFF;" align="center">URL</span></td>
                          <td width="15%" rowspan="3" align="center">20ポイント<span style="font-size:80%;">〔\20(税別)〕</span><br />
                          <span style="font-size:60%;">※1項目あたりの単価</span></td>
                        </tr>


                       <tr>
                        <td style="background-color:#ECFEFF;" align="center">
                        業種
                        </td>
                        <td align="center">
                            20ポイント<span style="font-size:80%;">〔\20(税別)〕</span>
                        </td>
                        <td style="background-color:#ECFEFF;" align="center">資本金規模 </td>
                        <!--<td style="background-color:#ECFEFF;" align="center"><span style="background-color:#ECFEFF;">代表者氏名 </span></td>-->
                        </tr>

                        <tr>
                        <td style="background-color:#ECFEFF;" align="center">
                        FAX番号
                        </td>
                        <td align="center">
                            37ポイント<span style="font-size:80%;">〔\37(税別)〕</span>
                        </td>
                        <td style="background-color:#ECFEFF;" align="center">設立年月</td>
                        <!--<td style="background-color:#ECFEFF;" align="center"><span style="background-color:#ECFEFF;" align="center">URL</span></td>-->
                        </tr>

                        <tr>
                        <td style="background-color:#ECFEFF;" align="center">
                        上場区分
                        </td>
                        <td align="center">
                            20ポイント<span style="font-size:80%;">〔\20(税別)〕</span>
                        </td>
                        <td rowspan="2" align="center" style="background-color:#ECFEFF;"><span style="background-color:#ECFEFF;">メール<br>アドレス<br>
<span style="color:#F00; font-weight:bold;">NEW!</span> </span></td>
                        <td rowspan="2" align="center" style="background-color:#ECFEFF;"><span style="background-color:#ECFEFF;">法人番号 <span style="color:#F00; font-weight:bold;">NEW!</span><br />(マイナンバー)</span></td>
                        <td width="15%" rowspan="2" align="center">50ポイント<span style="font-size:80%;">〔\50(税別)〕</span></td>
                        </tr>
                        <tr>
                          <td style="background-color:#ECFEFF;" align="center">従業員規模</td>
                          <td align="center">40ポイント<span style="font-size:80%;">〔\40(税別)〕</span></td>
                        </tr>

                        <!--tr>
                        <td style="background-color:#ECFEFF;" align="center">
                        決算月
                        </td>
                        <td align="center">
                            <b>10ポイント<span style="font-size:80%;">〔\10(税別)〕</span></b><br /><span style="font-size:60%;">※1項目あたりの単価になります。</span></td>
                        </td>
                        <td align="center">20ポイント<span style="font-size:80%;">〔\20(税別)〕</span><span style="font-size:60%;">※1項目あたりの単価になります。</span></td>
                        </tr-->

                </table>
<!--*<div style="text-align:center;">
	<span style="color:#F00; font-weight:bold;">※全項目付与できた場合も、387ポイント(387円)です。</span>
</div>-->

<div align="center" style="margin:10px auto 10px;">


<a name="kensu"></a>
<div class="head_title_bgcolor" style="width:100%; text-align:left;margin:40px auto 10px auto;">
	■企業総件数：680万社
</div>
	<table border="1" cellpadding="5" style="width:800px; margin:0 auto; font-size:17px;">

		<tr>
			<td width="20%" style="background-color:#D7E0EA;">業種</td>
			<td align="left">　　　約624万社</td>
			<td width="20%" style="background-color:#D7E0EA;">売上規模</td>
			<td align="left">　　　約246万社 <!--<span style="color:#f00;">NEW</span>--></td>
		</tr>

		<tr>
			<td style="background-color:#D7E0EA;">FAX番号</td>
			<td align="left">　　　約353万社</td>
			<td style="background-color:#D7E0EA;">従業員数規模</td>
			<td align="left">　　　約117万社</td>
		</tr>

		<tr>
			<td style="background-color:#D7E0EA;">企業URL</td>
			<td align="left">　　　約190万社</td>
			<td style="background-color:#D7E0EA;">資本金規模</td>
			<td align="left">　　　約160万社</td>
		</tr>

		<tr>
			<!--<td style="background-color:#D7E0EA;">代表者名</td>-->
			<!--<td align="left">　　　約125万社</td>-->
			<td style="background-color:#D7E0EA;">企業<br />メールアドレス</td>
			<td align="left">　　　約23万社</td>
			<td style="background-color:#D7E0EA;">法人番号<br />(マイナンバー)</td>
			<td align="left">　　　約114万社</td>
		</tr>
		<tr>
			<td style="background-color:#D7E0EA;">設立年月</td>
			<td align="left">　　　約8万社</td>
			<td style="border-bottom-style: hidden;border-right-style: hidden;">&nbsp;</td>
			<td style="border-bottom-style: hidden;border-left-style: hidden;border-right-style: hidden;">&nbsp;</td>
		</tr>

		<!--<tr>
			<td style="background-color:#D7E0EA;">企業<br />メールアドレス</td>
			<td align="left">　　　約23万社</td>
			<td style="background-color:#D7E0EA;"> </td>
			<td> </td>
		</tr>-->

	</table>

<div class="text" style="margin:3px 0 0 700px;">
	<span style="font-size:80%;">2017年7月現在</span>
</div>



<div class="head_title_bgcolor" style="width:100%; text-align:left;margin:40px auto 10px auto;">
	■お知らせ＆ニュースリリース
</div>

                                    <iframe class="test_news" src="/service/common/announce/list.php" type="text/html" name="notice_test" width="90%" height="270px" style="text-algin:center;margin:10px 0 20px 20px;border:solid 1px #ccc;">
</iframe>
<p style="margin:-17px 0 0 720px;font-size:0.8em;"><a href="../../press/info.html">全記事はこちら</a></p>


<div style="margin:0 auto 30px auto;text-align:center;">
<span style="font-weight:bold; font-size:24px;"><a href="https://www.navit-j.com/blog/?p=27751" target="_blank">オープンサイト採用事例・便利な使い方</a></span>
</div>

</div>