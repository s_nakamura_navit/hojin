<?php /* Smarty version 2.6.27, created on 2017-07-28 13:24:26
         compiled from trialone.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'uniqid', 'trialone.tpl', 128, false),)), $this); ?>
<?php if ($this->_tpl_vars['app']['corp_detail'] != ""): ?>

<table class="result_table2">
	<tr>
		<th class="result_table_th2">
			企業名
		</th>
		<th class="result_table_th2">
			所在地
		</th>
	</tr>
	<tr>
		<td>
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_CORP']; ?>

		</td>
		<td>
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_ADDR_ALL']; ?>

		</td>
	</tr>
</table>

<div class="search_result2">
			<b>付与属性項目の有無</b>
</div>

<table class="result_table3">

	<tr>
		<th class="result_table_th2" width="50%">
			業種
		</th>
		<td class="result_table_td2" width="50%">
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_BUSINESS_CD_L_1']; ?>

		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			FAX番号
		</th>
		<td class="result_table_td2">
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_FAX']; ?>

		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			上場区分
		</th>
		<td class="result_table_td2">
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_LISTED_TYPE']; ?>

		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			従業員数規模
		</th>
		<td class="result_table_td2">
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_EMPLOYEE_DIV']; ?>

		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			売上高規模
		</th>
		<td class="result_table_td2">
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_SALE_DIV']; ?>

		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			資本金規模
		</th>
		<td class="result_table_td2">
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_CAPITAL_DIV']; ?>

		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			設立年月
		</th>
		<td class="result_table_td2">
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_FOUNDING_DT']; ?>

		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			URL
		</th>
		<td class="result_table_td2">
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_URL']; ?>

		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			法人番号（マイナンバー）
		</th>
		<td class="result_table_td2">
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_CORP_NUM']; ?>

		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			メールアドレス
		</th>
		<td class="result_table_td2">
			<?php echo $this->_tpl_vars['app']['corp_detail']['I_MAILADDR']; ?>

		</td>
	</tr>
	<tr>
		<th class="result_table_th2">
			金額
		</th>
		<td class="result_table_td2">
		<?php echo $this->_tpl_vars['app']['corp_detail']['total']; ?>
ポイント [ &yen;<?php echo $this->_tpl_vars['app']['corp_detail']['total']; ?>
(税込) ]
		</td>
	</tr>
</table>

<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f_back" method="POST" id="f_back" enctype="multipart/form-data">
    <input type="hidden" name="action_index" value="ture" />
    <input type="hidden" name="sc_name" value="<?php echo $this->_tpl_vars['app']['sc_name']; ?>
" />
    <input type="hidden" name="sc_address" value="<?php echo $this->_tpl_vars['app']['sc_address']; ?>
" />
    <input type="hidden" name="in_act" value="search_corp" />
</form>
<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f_get" method="POST" id="f_get" enctype="multipart/form-data">
    <?php if ($this->_tpl_vars['app']['name'] != ""): ?>
		<input type="hidden" name="action_trialoneconf" value="true" />
		<input type="hidden" name="corp_id" value="<?php echo $this->_tpl_vars['app']['corp_detail']['ID']; ?>
" />
		<?php echo smarty_function_uniqid(array(), $this);?>

		<input type="hidden" name="sc_name" value="<?php echo $this->_tpl_vars['app']['sc_name']; ?>
" />
    	<input type="hidden" name="sc_address" value="<?php echo $this->_tpl_vars['app']['sc_address']; ?>
" />
    	<input type="hidden" name="pref_cd" value="<?php echo $this->_tpl_vars['app']['pref_cd']; ?>
">
		<input type="hidden" name="city_cd" value="<?php echo $this->_tpl_vars['app']['city_cd']; ?>
">
    	<input type="hidden" name="in_act" value="trialoneconf" />
    <?php else: ?>
		<input type="hidden" name="action_secure_registration" value="true" />
    <?php endif; ?>
</form>

	<div class="search_btn">
		<a class="button" href="javascript:void(0)" onclick="document.f_back.submit();" id="back_button">戻る</a>
				    &nbsp;&nbsp;&nbsp;&nbsp;
		<?php if ($this->_tpl_vars['app']['corp_detail']['total'] > 0): ?>
		<a class="button" href="javascript:void(0)" onclick="document.f_get.submit();" id="a_search">購入する</a>
		<?php endif; ?>
	</div>
<?php endif; ?>
