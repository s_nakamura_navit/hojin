<?php /* Smarty version 2.6.27, created on 2017-07-28 16:48:13
         compiled from secure/confirm.tpl */ ?>
<?php echo ' 
<script type="text/javascript">
//吹き出し
$(window).load(function(){
      $(\'label\').balloon();
      
      
//    if ($(\'#privacy_1\').is(\':checked\')) {
//        $(\'#confirm_btn\').css({opacity:"1",cursor:"pointer"}).removeAttr(\'disabled\');
//    } else {
//        $(\'#confirm_btn\').css({opacity:"0.5",cursor:"default"}).attr(\'disabled\',\'disabled\');
//    }
});
 	
//全選択・全解除
$(function() {
    $(\'#all_check\').on("click",function(){
        $(\'.CheckList\').prop("checked", true);
    });
});
$(function() {
    $(\'#all_clear\').on("click",function(){
        $(\'.CheckList\').prop("checked", false);
    });
});

$(function() {
    $(\'#all_check2\').on("click",function(){
        $(\'.CheckList2\').prop("checked", true);
    });
});
$(function() {
    $(\'#all_clear2\').on("click",function(){
        $(\'.CheckList2\').prop("checked", false);
    });
});

$(function() {
    $(\'#all_check3\').on("click",function(){
        $(\'.CheckList3\').prop("checked", true);
    });
});
$(function() {
    $(\'#all_clear3\').on("click",function(){
        $(\'.CheckList3\').prop("checked", false);
    });
});

//リセット(自治体)
$(function() {
    $(\'#a_reset\').on("click",function(){
        //ラジオボタン初期値セット
        $(\'#in_kind_1\').prop("checked", true);
        $(\'#in_kind_2\').prop("checked", false);
        //プルダウン初期値セット
        $(\'select[name="in_area1"]\').val("");
        $(\'#in_area2\').html(\'\');//一度select内を空に
        $(\'#in_area2\').append(\'<option id="city00000" value="\'+\'\'+\'">\'+\'市区町村を選択\'+\'</option>\');
        
        //チェックボックスクリア
        $(\'.CheckList\').prop("checked", false);
        
        $("#a_reset").blur();
    });
});
//リセット(財団)
$(function() {
    $(\'#f_reset\').on("click",function(){
        //チェックボックスクリア
        $(\'.CheckList2\').prop("checked", false);
        $(\'.CheckList3\').prop("checked", false);
        $(\'#in_keyword\').val("");
        $("#f_reset").blur();
    });
});

function doblur() { 
    var element = document.getElementById("name"); 
    element.blur(); 
} 

function do_submit(kind){
    if(kind==\'back\'){
        document.f2.submit();
    }else{
        document.f3.submit();
    }
    
}

/*
 　　全角->半角変換
 */
jQuery(function(){
 
    // 郵便番号の処理
    $(\'.zip-number\').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\\－|\\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
 
        // 半角数字のみ残す
        var zenkakuDel = new String( hankaku ).match(/\\d/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }
 
        $(this).val(zenkakuDel);
    });
    // 電話番号の処理
    $(\'.tel-number\').change( function(){
        var data = $(this).val();
        var hankaku = data.replace(/[Ａ-Ｚａ-ｚ０-９]|\\－|\\＋/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
 
        // 半角数字と+-のみ残す
        var zenkakuDel = new String( hankaku ).match(/\\d|\\-|\\+/g);
        if(zenkakuDel){
            zenkakuDel = zenkakuDel.join("");
        }else {
            zenkakuDel = "";
        }
        
        $(this).val(zenkakuDel);
    });
 
    // メールアドレスの処理
    $(\'.mail-address\').change( function(){
        var zenkigou = "＠－ー＋＿．，、";
        var hankigou = "@--+_...";
        var data = $(this).val();
        var str = "";
 
        // 指定された全角記号のみを半角に変換
        for (i=0; i<data.length; i++)
        {
            var dataChar = data.charAt(i);
            var dataNum = zenkigou.indexOf(dataChar,0);
            if (dataNum >= 0) dataChar = hankigou.charAt(dataNum);
            str += dataChar;
        }
        // 定番の、アルファベットと数字の変換処理
        var hankaku = str.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
        $(this).val(hankaku);
    });
 
});

//確認ボタンの有効無効

jQuery(function(){
    $(\'#privacy_1\').change(function(){
            if ($(this).is(\':checked\')) {
                    $(\'#confirm_btn\').css({opacity:"1",cursor:"pointer"}).removeAttr(\'disabled\');
            } else {
                    $(\'#confirm_btn\').css({opacity:"0.5",cursor:"default"}).attr(\'disabled\',\'disabled\');
            }
    });
});  
</script>
'; ?>
 

<div class="top_header_title" ><span class="top_header_title_border">ご入力内容の確認</span></div>
<div style="margin-left: 120px;">以下の内容でよろしければ、『送信』ボタンを押してください。<br />修正する場合は、『戻る』ボタンを押して該当箇所を修正してください。</div>
<div style="font-size:0.7em;margin-left: 120px;">
<!--
<span>
【注意事項】<br />
<span style="color:#64c601;font-size:16px;">■</span>ID・PWはご登録頂いたメールアドレスにご連絡致します。<br />
<span style="color:#64c601;font-size:16px;">■</span>フリーメールアドレスはご利用頂けない場合がございます。<br />
</span>
-->
</div>        
        
        
<div class="table">
<table width="750" border="0" cellpadding="5" cellspacing="1" style="margin-left: 94px;">
    <tr>
        <td class="l_Cel_01_02" width="80px;">会社名</td>
        <td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['company_name']; ?>
</td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">部署名</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['department_name']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">役職名</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['post_name']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">担当者名</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['contractor_lname']; ?>
&nbsp;&nbsp;<?php echo $this->_tpl_vars['app']['data']['contractor_fname']; ?>

        </td>
    </tr>
    <!--
    <tr>
        <td class="l_Cel_01_02">会社名かな</td>
        <td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['company_kana']; ?>
</td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">支店名</td>
        <td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['branch_name']; ?>
</td>
    </tr>
    -->
    <tr>
        <td class="l_Cel_01_02">郵便番号</td>
        <td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['zip1']; ?>
－<?php echo $this->_tpl_vars['app']['data']['zip2']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">住所</td>
        <td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['address']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">電話番号</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['phone_no1']; ?>
<?php echo $this->_tpl_vars['app']['data']['phone_no2']; ?>
<?php echo $this->_tpl_vars['app']['data']['phone_no3']; ?>

        </td>
    </tr>
    <!--
    <tr>
        <td class="l_Cel_01_02">携帯電話番号</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['k_phone_no1']; ?>
<?php echo $this->_tpl_vars['app']['data']['k_phone_no2']; ?>
<?php echo $this->_tpl_vars['app']['data']['k_phone_no3']; ?>

        </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02">FAX番号</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['fax_no1']; ?>
<?php echo $this->_tpl_vars['app']['data']['fax_no2']; ?>
<?php echo $this->_tpl_vars['app']['data']['fax_no3']; ?>

        </td>
    </tr>
    
    <tr>
        <td class="l_Cel_01_02">担当者名かな</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['contractor_lkana']; ?>
&nbsp;&nbsp;<?php echo $this->_tpl_vars['app']['data']['contractor_fkana']; ?>

        </td>
    </tr>
    -->
    <tr>
        <td class="l_Cel_01_02">メールアドレス</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['email']; ?>

        </td>
    </tr>
	<?php if (! $this->_tpl_vars['app']['alliance']): ?>
    <tr>
        <td class="l_Cel_01_02">サイトを知ったきっかけ</td>
        <td class="s_Cel_01">
            <?php echo $this->_tpl_vars['app']['data']['from']; ?>

        </td>
    </tr>
	<?php endif; ?>
<!--
    <tr>
        <td class="l_Cel_01_02">メールマガジンの送付希望</td>
        <td class="s_Cel_01">
                    </td>
    </tr>
-->
    <tr>
        <td class="l_Cel_01_02">利用規約への同意</td>
        <td class="s_Cel_01">
            同意します
        </td>
    </tr>
    <tr>
        <td colspan="3">
                <!-- ここからトップへ戻る -->
	<div class="re_top"><a href="#01"><img src="img/re_top.jpg" alt="TOPへ戻る" /></a></div>
                <!-- ここまでトップへ戻る -->
        </td>
    </tr>
</table>
</div>
<div class="mod_form_btn">
                    
        <div style="margin-top:20px;margin-left:280px;">
            <form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f2" method="POST" id="f2">
                <input type="hidden" name="action_secure_registration" value="true">
                <a class="button" href="javascript:void(0)" onclick="javascript:do_submit('back');" id="back_btn">戻る</a>
                    <input type="hidden" name="in_company_name" id="in_company_name" value="<?php echo $this->_tpl_vars['app']['data']['company_name']; ?>
">
                    <input type="hidden" name="in_company_kana" id="in_company_kana" value="<?php echo $this->_tpl_vars['app']['data']['company_kana']; ?>
">
                    <input type="hidden" name="in_branch_name" id="in_branch_name" value="<?php echo $this->_tpl_vars['app']['data']['branch_name']; ?>
"> 
                    <input type="hidden" name="in_zip1" id="in_zip1" value="<?php echo $this->_tpl_vars['app']['data']['zip1']; ?>
">
                    <input type="hidden" name="in_zip2" id="in_zip2" value="<?php echo $this->_tpl_vars['app']['data']['zip2']; ?>
"> 
                    <input type="hidden" name="in_address" id="in_address" value="<?php echo $this->_tpl_vars['app']['data']['address']; ?>
"> 
                    <input type="hidden" name="in_phone_no1" id="in_phone_no1" value="<?php echo $this->_tpl_vars['app']['data']['phone_no1']; ?>
">
                    <input type="hidden" name="in_phone_no2" id="in_phone_no2" value="<?php echo $this->_tpl_vars['app']['data']['phone_no2']; ?>
"> 
                    <input type="hidden" name="in_phone_no3" id="in_phone_no3" value="<?php echo $this->_tpl_vars['app']['data']['phone_no3']; ?>
"> 
                    <input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no1']; ?>
">
                    <input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no2']; ?>
">
                    <input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no3']; ?>
">
                    <input type="hidden" name="in_fax_no1" id="in_fax_no1" value="<?php echo $this->_tpl_vars['app']['data']['fax_no1']; ?>
">
                    <input type="hidden" name="in_fax_no2" id="in_fax_no2" value="<?php echo $this->_tpl_vars['app']['data']['fax_no2']; ?>
"> 
                    <input type="hidden" name="in_fax_no3" id="in_fax_no3" value="<?php echo $this->_tpl_vars['app']['data']['fax_no3']; ?>
"> 
                    <input type="hidden" name="in_department_name" id="in_department_name" value="<?php echo $this->_tpl_vars['app']['data']['department_name']; ?>
"> 
                    <input type="hidden" name="in_post_name" id="in_post_name" value="<?php echo $this->_tpl_vars['app']['data']['post_name']; ?>
"> 
                    <input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="<?php echo $this->_tpl_vars['app']['data']['contractor_lname']; ?>
">
                    <input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="<?php echo $this->_tpl_vars['app']['data']['contractor_fname']; ?>
">
                    <input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="<?php echo $this->_tpl_vars['app']['data']['contractor_lkana']; ?>
">
                    <input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="<?php echo $this->_tpl_vars['app']['data']['contractor_fkana']; ?>
"> 
                    <input type="hidden" name="in_email" id="in_email" value="<?php echo $this->_tpl_vars['app']['data']['email']; ?>
">
                    <input type="hidden" name="in_from" id="in_from" value="<?php echo $this->_tpl_vars['app']['data']['from']; ?>
">
                    <input type="hidden" name="in_act" id="in_from" value="<?php echo $this->_tpl_vars['app']['data']['act']; ?>
">
                    <!--<input type="hidden" name="in_mailmaga" id="in_mailmaga" value="<?php echo $this->_tpl_vars['app']['data']['mailmaga']; ?>
">-->
                    <input type="hidden" name="in_privacy" id="in_privacy" value="<?php echo $this->_tpl_vars['app']['data']['privacy']; ?>
">
                    <input type="hidden" name="back" id="back" value="0"> 
            </form>
            </div>
        <div style="margin-top:-46px;margin-left:500px;">
            <form action="<?php echo $this->_tpl_vars['script']; ?>
" name="f3" method="POST" id="f3">
                <input type="hidden" name="action_secure_complete" value="true">
                <a class="button2" href="javascript:void(0)" onclick="javascript:do_submit('send');" id="send_btn">送信</a>
                    <input type="hidden" name="in_company_name" id="in_company_name" value="<?php echo $this->_tpl_vars['app']['data']['company_name']; ?>
">
                    <input type="hidden" name="in_company_kana" id="in_company_kana" value="<?php echo $this->_tpl_vars['app']['data']['company_kana']; ?>
">
                    <input type="hidden" name="in_branch_name" id="in_branch_name" value="<?php echo $this->_tpl_vars['app']['data']['branch_name']; ?>
"> 
                    <input type="hidden" name="in_zip1" id="in_zip1" value="<?php echo $this->_tpl_vars['app']['data']['zip1']; ?>
">
                    <input type="hidden" name="in_zip2" id="in_zip2" value="<?php echo $this->_tpl_vars['app']['data']['zip2']; ?>
"> 
                    <input type="hidden" name="in_address" id="in_address" value="<?php echo $this->_tpl_vars['app']['data']['address']; ?>
"> 
                    <input type="hidden" name="in_phone_no1" id="in_phone_no1" value="<?php echo $this->_tpl_vars['app']['data']['phone_no1']; ?>
">
                    <input type="hidden" name="in_phone_no2" id="in_phone_no2" value="<?php echo $this->_tpl_vars['app']['data']['phone_no2']; ?>
"> 
                    <input type="hidden" name="in_phone_no3" id="in_phone_no3" value="<?php echo $this->_tpl_vars['app']['data']['phone_no3']; ?>
"> 
                    <input type="hidden" name="in_k_phone_no1" id="in_k_phone_no1" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no1']; ?>
">
                    <input type="hidden" name="in_k_phone_no2" id="in_k_phone_no2" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no2']; ?>
">
                    <input type="hidden" name="in_k_phone_no3" id="in_k_phone_no3" value="<?php echo $this->_tpl_vars['app']['data']['k_phone_no3']; ?>
">
                    <input type="hidden" name="in_fax_no1" id="in_fax_no1" value="<?php echo $this->_tpl_vars['app']['data']['fax_no1']; ?>
">
                    <input type="hidden" name="in_fax_no2" id="in_fax_no2" value="<?php echo $this->_tpl_vars['app']['data']['fax_no2']; ?>
"> 
                    <input type="hidden" name="in_fax_no3" id="in_fax_no3" value="<?php echo $this->_tpl_vars['app']['data']['fax_no3']; ?>
"> 
                    <input type="hidden" name="in_department_name" id="in_department_name" value="<?php echo $this->_tpl_vars['app']['data']['department_name']; ?>
"> 
                    <input type="hidden" name="in_post_name" id="in_post_name" value="<?php echo $this->_tpl_vars['app']['data']['post_name']; ?>
"> 
                    <input type="hidden" name="in_contractor_lname" id="in_contractor_lname" value="<?php echo $this->_tpl_vars['app']['data']['contractor_lname']; ?>
">
                    <input type="hidden" name="in_contractor_fname" id="in_contractor_fname" value="<?php echo $this->_tpl_vars['app']['data']['contractor_fname']; ?>
">
                    <input type="hidden" name="in_contractor_lkana" id="in_contractor_lkana" value="<?php echo $this->_tpl_vars['app']['data']['contractor_lkana']; ?>
">
                    <input type="hidden" name="in_contractor_fkana" id="in_contractor_fkana" value="<?php echo $this->_tpl_vars['app']['data']['contractor_fkana']; ?>
"> 
                    <input type="hidden" name="in_email" id="in_email" value="<?php echo $this->_tpl_vars['app']['data']['email']; ?>
">
                    <input type="hidden" name="in_from" id="in_from" value="<?php echo $this->_tpl_vars['app']['data']['from']; ?>
">
                    <input type="hidden" name="in_act" id="in_from" value="<?php echo $this->_tpl_vars['app']['data']['act']; ?>
">
                    <!--<input type="hidden" name="in_mailmaga" id="in_mailmaga" value="<?php echo $this->_tpl_vars['app']['data']['mailmaga']; ?>
">-->
                    <input type="hidden" name="in_privacy" id="in_privacy" value="<?php echo $this->_tpl_vars['app']['data']['privacy']; ?>
">
                    <input type="hidden" name="back" id="back" value="0"> 
            </form>
        </div>
                </div>
</div>
</div>
<!-- ここまでメインコンテンツ -->