<?php /* Smarty version 2.6.27, created on 2017-07-28 17:22:48
         compiled from mypage.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', 'mypage.tpl', 45, false),)), $this); ?>
<?php echo '
<script type="text/javascript">
function do_dl(no){
    document.dl.in_request_id.value = no;
    document.dl.in_step.value = \'csv_dl\';
    document.dl.submit();
}
function go_estimate(no){
    document.estimate_detail.in_request_id.value = no;
    document.estimate_detail.submit();
}
function go_detail(no,list_type){
	document.list_detail.in_request_id.value = no;
	document.list_detail.in_list_type.value = list_type;
	document.list_detail.submit();
}
</script>
'; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "drew_dialog.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<div class="top_header_title" style="margin-top:20px;"><span class="top_header_title_border">マイページ</span></div>
<div style="font-size:80%;">
<div class="head_title_bgcolor" style="text-align:left;">お客様情報</div>
<table width="100%" border="0" cellpadding="5" cellspacing="1">
    <tr>
	<td colspan="6" style="text-align:right;">
	<a class="change_btn" href="./index.php?action_edit_registration=true" id="delete_button">会員情報編集</a>
	</td>
    </tr>
    <tr>
        <td class="l_Cel_01_02" width="80px;">名前</td>
        <td class="s_Cel_01" colspan="3">
		<table>
		<tr>
		<td style="border:0;text-align:left;"><span style="font-size:60%;"><?php echo $this->_tpl_vars['app']['data']['contractor_lkana']; ?>
</span></td>
		<td style="border:0;text-align:left;"><span style="font-size:60%;"><?php echo $this->_tpl_vars['app']['data']['contractor_fkana']; ?>
</span></td>
		<td style="border:0;text-align:left;"></td>
		</tr>
		<tr>
		<td style="border:0;text-align:left;"><span style="font-size:150%;"><?php echo $this->_tpl_vars['app']['data']['contractor_lname']; ?>
</span></td>
                <td style="border:0;text-align:left;"><span style="font-size:150%;"><?php echo $this->_tpl_vars['app']['data']['contractor_fname']; ?>
</span></td>
		<td style="border:0;text-align:left;"><span style="font-size:150%;">様</span></td>
		</tr>
		</table>
        <td class="l_Cel_01_02" width="80px;">ナビットポイント</td>
        <td class="s_Cel_01"><span style="font-size:120%;"><?php echo ((is_array($_tmp=$this->_tpl_vars['app']['point'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : smarty_modifier_number_format($_tmp)); ?>
&nbsp;pt</span></td>
    </tr>
    <tr>
	<td class="l_Cel_01_02" width="80px;">会社名</td>
	<td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['company_name']; ?>
</td>
	<td class="l_Cel_01_02" width="80px;">住所</td>
	<td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['zip1']; ?>
-<?php echo $this->_tpl_vars['app']['data']['zip2']; ?>
<br /><?php echo $this->_tpl_vars['app']['data']['address']; ?>
</td>
	<td class="l_Cel_01_02" width="80px;">E-MAIL</td>
	<td class="s_Cel_01"><?php echo $this->_tpl_vars['app']['data']['email']; ?>
</td>
    </tr>
    <tr>
	<td colspan="6" style="text-align:right;">
	<a class="withdrew_btn" href="javascript:void(0)" onclick="do_delete();" id="delete_button">退会</a>
	</td>
    </tr>
</table>
<br />
<br />
<br />
<div class="head_title_bgcolor" style="text-align:left;">リスト購入履歴</div>
<table width="100%" border="0" cellpadding="5" cellspacing="1">
    <tr>
    <td style="border:0;padding-top:15px;" colspan="4">
<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="estimate_detail" id="estimate_detail" method="POST">
    <input type="hidden" name="action_commission_registration" value="true" />
    <input type="hidden" name="in_request_id" id="in_request_id" value="" />
</form>
<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="dl" method="POST" id="dl">
    <input type="hidden" name="action_mypage" value="true" />
    <input type="hidden" name="in_request_id" id="in_request_id" value="" />
    <input type="hidden" name="in_step" id="in_step" value="" />
</form>
<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="list_detail" id="list_detail" method="POST">
    <input type="hidden" name="action_detail_list" value="true" />
    <input type="hidden" name="in_request_id" id="in_request_id" value="" />
    <input type="hidden" name="in_list_type" id="in_list_type" value="" />
</form>




<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="pager_m" method="POST" id="pager_m">
    <input type="hidden" name="action_mypage" value="true" />
    <input type="hidden" name="in_step" id="in_step" value="" />
    <input type="hidden" name="offset"     id="offset"     value="<?php echo $this->_tpl_vars['form']['offset']; ?>
">
</form>
<!--ページャ-->
<?php echo $this->_tpl_vars['app_ne']['pager']; ?>

<!--ページャ-->
    </td>
    </tr>
    <tr>
        <td class="l_Cel_01_02" width="130px;">ファイル情報</td>
        <td class="l_Cel_01_02" width="700px;">日時</td>
        <td class="l_Cel_01_02" width="60px;">付与情報</td>
        <td class="l_Cel_01_02" width="60px;">ステータス</td>
    </tr>
<?php if ($this->_tpl_vars['app']['counts'] == 0): ?>
    <tr>
        <td class="s_Cel_01" colspan="5" align="center">対象履歴が見つかりません</td>
    </tr>
<?php else: ?>
    <?php $_from = $this->_tpl_vars['app']['history']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['l'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['l']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['v']):
        $this->_foreach['l']['iteration']++;
?>
    <?php if ($this->_tpl_vars['v']['id'] != ""): ?>
    <tr>
        <td class="<?php echo $this->_tpl_vars['v']['style']; ?>
" rowspan="2">
	    <?php if ($this->_tpl_vars['v']['use_matching_phase'] == 'trial'): ?>
			お試しサービス利用（ファイル情報なし）
		<?php elseif ($this->_tpl_vars['v']['use_matching_phase'] == 'trialone'): ?>
		【付与情報取得 会社名】<br />
        	<?php echo $this->_tpl_vars['v']['list_name']; ?>
<br />
	    <?php else: ?>
		【ファイル名】<br />
        	<?php echo $this->_tpl_vars['v']['list_name']; ?>
<br />
		【総件数】<?php echo $this->_tpl_vars['v']['list_row']; ?>
&nbsp;件
	    <?php endif; ?>
        </td>


        <td class="<?php echo $this->_tpl_vars['v']['style']; ?>
" rowspan="2">

	    <?php if ($this->_tpl_vars['v']['use_matching_phase'] == ""): ?>
                【依頼】<?php echo $this->_tpl_vars['v']['created']; ?>
<br />
                <?php if ($this->_tpl_vars['v']['flg_processed'] > 1): ?>
                    <?php if ($this->_tpl_vars['v']['flg_processed'] == 2): ?>【購入】<?php elseif ($this->_tpl_vars['v']['flg_processed'] == 9): ?>【キャンセル】<?php endif; ?><?php echo $this->_tpl_vars['v']['client_order']; ?>
<br />
                <?php endif; ?>
	    <?php elseif ($this->_tpl_vars['v']['use_matching_phase'] == 'trial'): ?>
                【依頼】<?php echo $this->_tpl_vars['v']['created']; ?>
<br />
        <?php elseif ($this->_tpl_vars['v']['use_matching_phase'] == 'trialone'): ?>
        	<?php if ($this->_tpl_vars['v']['flg_processed'] == 1): ?>
        		【見積】<?php echo $this->_tpl_vars['v']['created_estimate']; ?>

	        <?php elseif ($this->_tpl_vars['v']['flg_processed'] == 2): ?>
	        	【購入】<?php echo $this->_tpl_vars['v']['client_order']; ?>

	        <?php elseif ($this->_tpl_vars['v']['flg_processed'] == 3): ?>
        		【見積】<?php echo $this->_tpl_vars['v']['created_estimate']; ?>

	        <?php elseif ($this->_tpl_vars['v']['flg_processed'] == 9): ?>
	        	【依頼・キャンセル】<?php echo $this->_tpl_vars['v']['created']; ?>

	        <?php endif; ?>
	        <br />
	    <?php else: ?>
		<?php if ($this->_tpl_vars['v']['flg_processed'] == 2): ?>【購入】<?php echo $this->_tpl_vars['v']['client_order']; ?>
<?php elseif ($this->_tpl_vars['v']['flg_processed'] == 9): ?>【依頼・キャンセル】<?php echo $this->_tpl_vars['v']['created']; ?>
<?php endif; ?><br />
	    <?php endif; ?>

        </td>


        <td class="<?php echo $this->_tpl_vars['v']['style']; ?>
" rowspan="2">
	    <?php if ($this->_tpl_vars['v']['use_matching_phase'] == ""): ?>
		【手動付与】<br />
	    <?php elseif ($this->_tpl_vars['v']['use_matching_phase'] == 'trial'): ?>
		【お試し付与】<br />
	    <?php else: ?>
		【自動付与】<br />
	    <?php endif; ?>
	    <?php if ($this->_tpl_vars['v']['use_matching_phase'] != 'trial'): ?>
		<?php $_from = $this->_tpl_vars['v']['property_name']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['name']):
?>
        	    <?php echo $this->_tpl_vars['app']['property_list'][$this->_tpl_vars['name']]; ?>
<br />
		<?php endforeach; endif; unset($_from); ?>
	    <?php endif; ?>
        </td>
<!--
        <td class="<?php echo $this->_tpl_vars['v']['style']; ?>
">
            【総件数】<?php echo $this->_tpl_vars['v']['list_row']; ?>
&nbsp;件<br />
	    <?php if ($this->_tpl_vars['v']['flg_processed'] == 1): ?>
            【付与可能件数】<?php echo $this->_tpl_vars['v']['get_row']; ?>
&nbsp;件<br />
	    <?php elseif ($this->_tpl_vars['v']['flg_processed'] == 2): ?>
	    【付与件数】<?php echo $this->_tpl_vars['v']['get_row']; ?>
&nbsp;件<br />
	    <?php endif; ?>
-->
        </td>
        <td class="<?php echo $this->_tpl_vars['v']['style']; ?>
">
        	<?php if ($this->_tpl_vars['v']['flg_processed'] == 0): ?>
				<?php if ($this->_tpl_vars['v']['use_matching_phase'] != 'trial'): ?>
					情報付与作業中
				<?php else: ?>
	        	   		見積もり作成中
				<?php endif; ?>
        	<?php elseif ($this->_tpl_vars['v']['flg_processed'] == 1): ?>

    		<?php if ($this->_tpl_vars['v']['use_matching_phase'] == 'trialone'): ?>
    			<?php if ($this->_tpl_vars['v']['est_flag']): ?>
				<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="toc_<?php echo $this->_tpl_vars['v']['id']; ?>
" id="toc_<?php echo $this->_tpl_vars['v']['id']; ?>
" method="POST">
					<input type="hidden" name="action_trialoneconf" value="true" />
					<input type="hidden" name="corp_id" value="<?php echo $this->_tpl_vars['v']['corp_id']; ?>
" />
					<input type="hidden" name="req_id" value="<?php echo $this->_tpl_vars['v']['req_id']; ?>
" />
					<input type="hidden" name="pref_cd" value="<?php echo $this->_tpl_vars['v']['pref_cd']; ?>
">
					<input type="hidden" name="in_act" value="trialoneconf_m" />
				</form>
				<input type="button" onclick="document.toc_<?php echo $this->_tpl_vars['v']['id']; ?>
.submit();" value="見積もり確認" />
				<?php else: ?>
					キャンセル済み
        	    <?php endif; ?>
    	    <?php else: ?>
    	    <input type="button" onclick="go_estimate(<?php echo $this->_tpl_vars['v']['id']; ?>
)" value="見積もり確認" />
    	    <?php endif; ?>


        	<?php elseif ($this->_tpl_vars['v']['flg_processed'] == 2): ?>
        		<?php if ($this->_tpl_vars['v']['use_matching_phase'] == 'trialone'): ?>
					<?php if ($this->_tpl_vars['v']['dl_flag']): ?>
					<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="tod_<?php echo $this->_tpl_vars['v']['id']; ?>
" id="tod_<?php echo $this->_tpl_vars['v']['id']; ?>
" method="POST">
					    <input type="hidden" name="action_trlonedetail" value="true" />
					    <input type="hidden" name="in_request_id" id="in_request_id" value="<?php echo $this->_tpl_vars['v']['corp_id']; ?>
" />
					    <input type="hidden" name="pref_cd" value="<?php echo $this->_tpl_vars['v']['pref_cd']; ?>
">
					    <input type="hidden" name="in_act" id="in_list_type" value="trlonedetail" />
					</form>

					<input type="button" onclick="document.tod_<?php echo $this->_tpl_vars['v']['id']; ?>
.submit();" value="詳細確認" />
					<?php endif; ?>
				<?php elseif ($this->_tpl_vars['v']['use_matching_phase'] != 'trial'): ?>
					<input type="button" onclick="go_detail(<?php echo $this->_tpl_vars['v']['id']; ?>
,<?php if ($this->_tpl_vars['v']['use_matching_phase'] == ""): ?>'manual'<?php else: ?>'auto'<?php endif; ?>);" value="詳細確認" /><br /><br />
				<?php else: ?>
	        	   		購入済み<br />
				<?php endif; ?>

			 	<?php if ($this->_tpl_vars['v']['dl_flag']): ?>
			 		<?php if ($this->_tpl_vars['v']['use_matching_phase'] != 'trialone'): ?>
	            			<input type="button" onclick="do_dl(<?php echo $this->_tpl_vars['v']['id']; ?>
);" value="ダウンロード" />
	            	<?php endif; ?>
				<?php else: ?>
					<?php if ($this->_tpl_vars['v']['use_matching_phase'] != 'trial'): ?>
						購入済み
					<?php endif; ?>
				<?php endif; ?>
			<?php elseif ($this->_tpl_vars['v']['flg_processed'] == 3): ?>
				<?php if ($this->_tpl_vars['v']['use_matching_phase'] == 'trialone'): ?>
	    			<?php if ($this->_tpl_vars['v']['est_flag']): ?>
					<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="toc_<?php echo $this->_tpl_vars['v']['id']; ?>
" id="toc_<?php echo $this->_tpl_vars['v']['id']; ?>
" method="POST">
						<input type="hidden" name="action_trialoneconf" value="true" />
						<input type="hidden" name="corp_id" value="<?php echo $this->_tpl_vars['v']['corp_id']; ?>
" />
						<input type="hidden" name="req_id" value="<?php echo $this->_tpl_vars['v']['req_id']; ?>
" />
						<input type="hidden" name="pref_cd" value="<?php echo $this->_tpl_vars['v']['pref_cd']; ?>
">
						<input type="hidden" name="in_act" value="trialoneconf_m" />
					</form>
					<input type="button" onclick="document.toc_<?php echo $this->_tpl_vars['v']['id']; ?>
.submit();" value="見積もり確認" />
					<?php else: ?>
						キャンセル済み
	        	    <?php endif; ?>
    	    	<?php endif; ?>

        	<?php elseif ($this->_tpl_vars['v']['flg_processed'] == 9): ?>
        	    キャンセル済み
        	<?php endif; ?>
        </td>
    </tr>
    <tr>
        <td class="<?php echo $this->_tpl_vars['v']['style']; ?>
">
	    <?php if ($this->_tpl_vars['v']['flg_processed'] > 1): ?>
		<?php if ($this->_tpl_vars['v']['use_matching_phase'] == ""): ?>
            		【返却】<?php echo ((is_array($_tmp=$this->_tpl_vars['v']['point_back'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : smarty_modifier_number_format($_tmp)); ?>
ポイント<br />
		<?php else: ?>
<!--
			【使用】<?php echo ((is_array($_tmp=$this->_tpl_vars['v']['use_point'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : smarty_modifier_number_format($_tmp)); ?>
ポイント<br />
-->
		<?php endif; ?>
	    <?php endif; ?>
        </td>
    </tr>
    <?php endif; ?>
    <?php endforeach; endif; unset($_from); ?>
<?php endif; ?>
<tr>
<td colspan="4">
<form action="<?php echo $this->_tpl_vars['script']; ?>
" name="pager" method="POST" id="pager">
<input type="hidden" name="action_mypage" value="true" />
<input type="hidden" name="step" id="step" value="" />
<input type="hidden" name="offset"     id="offset"     value="<?php echo $this->_tpl_vars['form']['offset']; ?>
">
<!--ページャ-->
<?php echo $this->_tpl_vars['app_ne']['pager']; ?>

<!--ページャ-->
</form>
</td>
</tr>
</table>
</div>
</div>
<!-- ここまでメインコンテンツ -->
</div>