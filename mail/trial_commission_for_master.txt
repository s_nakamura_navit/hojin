―――――――――――――――――――――――――――――――――――――――――――                  
「法人番号検索 お試しサービス」への依頼がありましたのでお知らせ致します
―――――――――――――――――――――――――――――――――――――――――――
【登録者情報】
ユーザーID：[USER_ID]
会社名：[COMPANY_NAME]
部署名：[DEPARTMENT_NAME]
役職名：[POST_NAME]
お名前：[CONTRACTOR_NAME]
郵便番号：[ZIP]
住所：[ADDRESS]
電話番号：[PHONE_NO]
FAX番号：[FAX_NO]
メールアドレス：[EMAIL]

【ご依頼内容】
https://www.navit-j.com/service/hojin/[FILE_PATH]
付与項目：[PROPERTY]


*********************************************************
法人番号検索
*********************************************************
